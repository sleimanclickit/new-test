#!/bin/bash
set -e

tar -xvzf buzzdrives-latest.tar.gz /home/buzzdrvs/public_html/
aws s3 cp /home/buzzdrvs/public_html/buzzdrives-latest.tar.gz s3://buzzdrives.com-ci-cd/ --recursive
rm -rf /home/buzzdrvs/public_html/
rsync -rav /var/www/buzzdrives/* /home/buzzdrvs/public_html/
chown -R buzzdrvs:buzzdrvs /home/buzzdrives/public_html/