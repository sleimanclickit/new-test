<html>

    <head>
        <link rel="icon" type="image/png" href="/favicon.png">
        <script async src="//www.googletagservices.com/tag/js/gpt.js"></script>
		
        <!--<script async src="//acdn.adnxs.com/prebid/not-for-prod/1/prebid.js"></script>-->
        <script async src="custom_prebid_unminified.js"></script>
		
        <script>
            var sizes = [
                [970, 90]
            ];
            var PREBID_TIMEOUT = 1000;
            var FAILSAFE_TIMEOUT = 3000;

            var adUnits = [{
				autozone: "off",
				bids: [{
					bidder: "districtm",
					params: {
						placementId: "11252970"
					},
					size: []
				}, {
					bidder: "appnexus",
					params: {
						placementId: "12269832",
						reserve: ""
					},
					size: []
				}, {
					bidder: "brealtime",
					params: {
						placementId: "11241047"
					},
					size: []
				}, {
					bidder: "oftmedia",
					params: {
						placementId: "11337397",
						reserve: ""
					},
					size: []
				}, {
					bidder: "oftmedia",
					params: {
						placementId: "11337398",
						reserve: ""
					},
					size: []
				}, {
					bidder: "aol",
					params: {
						alias: "",
						bidFloor: "",
						dcn: "",
						network: "10513.1",
						placement: "4545363",
						pos: "",
						server: ""
					},
					size: [
						[728, 90]
					]
				}, {
					bidder: "aol",
					params: {
						alias: "",
						bidFloor: "",
						dcn: "",
						network: "10513.1",
						placement: "5047117",
						pos: "",
						server: ""
					},
					size: [
						[970, 90]
					]
				}, {
					bidder: "aol",
					params: {
						alias: "",
						bidFloor: "",
						dcn: "",
						network: "10513.1",
						placement: "5047118",
						pos: "",
						server: ""
					},
					size: [
						[970, 250]
					]
				}],
				code: "/207764977/top_banner_desktop",
				device: "desktop",
				sizeMapping: [],
				sizes: [
					[728, 90],
					[970, 90],
					[970, 250]
				],
				slot: "top_banner_desktop",
				viewability: 1
			}];

            // ======== DO NOT EDIT BELOW THIS LINE =========== //
            var googletag = googletag || {};
            googletag.cmd = googletag.cmd || [];
            googletag.cmd.push(function() {
                googletag.pubads().disableInitialLoad();
            });

            var pbjs = pbjs || {};
            pbjs.que = pbjs.que || [];

            pbjs.que.push(function() {
                pbjs.addAdUnits(adUnits);
                pbjs.requestBids({
                    bidsBackHandler: initAdserver,
                    timeout: PREBID_TIMEOUT
                });
            });

            function initAdserver() {
                if (pbjs.initAdserverSet) return;
                pbjs.initAdserverSet = true;
                googletag.cmd.push(function() {
                    pbjs.setTargetingForGPTAsync && pbjs.setTargetingForGPTAsync();
                    googletag.pubads().refresh();
                });
            }
            
            // in case PBJS doesn't load
            setTimeout(function() {
                initAdserver();
            }, FAILSAFE_TIMEOUT);

            googletag.cmd.push(function() {
                googletag.defineSlot('/207764977/top_banner_desktop', sizes, 'top_banner_desktop')
                	.addService(googletag.pubads());
                googletag.pubads().enableSingleRequest();
                googletag.enableServices();
            });

        </script>

    </head>

    <body>
        <h2>Custom Prebid.js </h2>
        <h5>top_banner_desktop</h5>
        <div id='top_banner_desktop'>
            <script type='text/javascript'>
                googletag.cmd.push(function() {
                    googletag.display('top_banner_desktop');
                });

            </script>
        </div>
    </body>

</html>