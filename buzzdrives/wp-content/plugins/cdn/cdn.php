<?php
/*
	Plugin Name: CDN content
	Description: Replace local scripts/styles with CDN locations
	Author: GDM Webmedia
*/

function cdn_register_content() {
	if( !is_admin() ) {
		// jQuery & jQuery Migrate & jQuery Mobile
		wp_deregister_script('jquery');
		wp_deregister_script('jquery-migrate');
		
		wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js', false, '1.11.1');
		wp_register_script('jquery-migrate', '//code.jquery.com/jquery-migrate-1.2.1.min.js', false, '1.2.1');
		
		wp_enqueue_script('jquery');
		wp_enqueue_script('jquery-migrate');
		
		// jQuery UI
		$jquery_ui = array(
			"jquery-ui-core",
			"jquery-ui-widget",
			"jquery-ui-mouse",
			"jquery-ui-accordion",
			"jquery-ui-autocomplete",
			"jquery-ui-slider",
			"jquery-ui-tabs",
			"jquery-ui-sortable",
			"jquery-ui-draggable",
			"jquery-ui-droppable",
			"jquery-ui-selectable",
			"jquery-ui-position",
			"jquery-ui-datepicker",
			"jquery-ui-resizable",
			"jquery-ui-dialog",
			"jquery-ui-button"
		);
		foreach($jquery_ui as $script) {
			if( wp_script_is( $script, 'registered' ) ) {
				wp_deregister_script($script);
			}
		}
		wp_register_script('jquery-ui', '//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js', true, '1.11.2');
		wp_enqueue_script('jquery-ui');
	}
}
add_action('wp_enqueue_scripts', 'cdn_register_content', 9999);

function remove_query_strings_from_cdn_resources($src) {
	if( strpos( $src, '?ver=' )  ) {
		$src = remove_query_arg( 'ver', $src );
	}
	
	return $src;
}
add_filter( 'style_loader_src', 'remove_query_strings_from_cdn_resources', 10, 1);
add_filter( 'script_loader_src', 'remove_query_strings_from_cdn_resources', 10, 1);
?>