<?php
/*
	Plugin Name: Facebook like popup
	Description: This plugin adds a Facebook like popup on the single post page
	Author: GDM Webmedia
*/

/*
 * Temp function - This functions sets the initial meta_values of all posts
 * @null
 */
function init_pp_meta_values() {
	if( is_admin() ) {
		// WP_Query arguments
		$args = array (
			'post_type' => array( 'post' ),
			'post_status' => array( 'publish', ' pending', ' draft' ),
			'pagination' => false,
			'posts_per_page' => '-1',
		);

		// The Query
		$query = new WP_Query( $args );

		// The Loop
		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) {
				$query->the_post();
				
				$post_id = get_the_ID();
				$show_pp_on_all_posts = 'true';
				
				// var_dump(get_post_meta( $post_id, 'pp_facebook_like' ));
				// echo get_the_title() . '<br>';
				update_post_meta( $post_id, 'pp_facebook_like', $show_pp_on_all_posts );
			}
			print_r($query->found_posts . ' POSTS UPDATED!');
		} else {
			print_r('NO POSTS FOUND!');
		}
		
		// Restore original Post Data
		wp_reset_postdata();
	}
}
// add_filter('admin_init', 'init_pp_meta_values');

/**
 * Adds a box to the main column on the Post edit screens.
 */
function pp_add_meta_box() {
	// doc: https://codex.wordpress.org/Function_Reference/add_meta_box
	add_meta_box(
		'pp_field',
		'Additional Facebook Like Popup',
		'pp_meta_box_callback',
		'post',
		'side'
	);
}
add_filter('add_meta_boxes', 'pp_add_meta_box');

/**
 * Prints the box content.
 * 
 * @param WP_Post $post The object for the current post/page.
 */
function pp_meta_box_callback( $post ) {
	// Add a nonce field so we can check for it later.
	wp_nonce_field( 'pp_save_meta_box_data', 'pp_meta_box_nonce' );
?>
	<?php
		$pp_value = get_post_meta( $post->ID, 'pp_facebook_like', true );
	?>
	<label for="pp_facebook_like">Show the Additional Facebook Like popup on this post:</label>
	<br>
	<select name="pp_facebook_like" id="pp_facebook_like" size="0" style="width: 100%;">
		<option value="true" <?php echo $pp_value != 'false' ? 'selected' : ''; ?>>Yes (default)</option>
		<option value="false" <?php echo $pp_value == 'false' ? 'selected' : ''; ?>>No</option>
	</select>
	<br>
	<br>
	<em>This make take a couple of minutes to change in front.</em>
<?php
}

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function pp_save_meta_box_data( $post_id ) {

	/*
	 * We need to verify this came from our screen and with proper authorization,
	 * because the save_post action can be triggered at other times.
	 */

	// Check if our nonce is set.
	if ( ! isset( $_POST['pp_meta_box_nonce'] ) ) {
		return;
	}

	// Verify that the nonce is valid.
	if ( ! wp_verify_nonce( $_POST['pp_meta_box_nonce'], 'pp_save_meta_box_data' ) ) {
		return;
	}

	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	// Check the user's permissions.
	if ( isset( $_POST['post_type'] ) && 'post' == $_POST['post_type'] ) {
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}
	}

	/* OK, it's safe for us to save the data now. */
	
	// Make sure that it is set.
	if ( ! isset($_POST['pp_facebook_like']) || ! isset($_POST['pp_facebook_like'])) {
		return;
	}

	// Sanitize user input.
	$pp_value = sanitize_text_field( $_POST['pp_facebook_like'] );
	
	// Update the meta field in the database.
	update_post_meta( $post_id, 'pp_facebook_like', $pp_value );
	
	// clear the cache
	if( function_exists('w3tc_pgcache_flush_post') ) {
		w3tc_pgcache_flush_post( $post_id );
	}
}
add_action( 'save_post', 'pp_save_meta_box_data' );

function pp_show_in_front() {
	/*
		JS: see below
		CSS: /public_html/wp-content/themes/dqs/style.css
	*/
	global $post;
	
	if(isset($post->ID)) {
		$pp_value = get_post_meta( $post->ID, 'pp_facebook_like', true );
	}
	
	if( is_single() && isset($pp_value) && $pp_value == "true" ) { ?>
		<div id="pp" style="display: none;">
			<div id="pp_modal">
				<div id="pp_modal_inner">
					<div class="pp_head">
						<div class="pp-logo-inner">
							<div class="pp-logo-round">
								<img class="center-block img-responsive" src="<?php echo get_img_folder(); ?>/buzzdrives_logo.png" width="334" height="55" alt="BuzzDrives Logo">
							</div>
						</div>
					</div>
					<div class="pp_t1">Want more stories like these?</div>
					<div class="pp_t2">Join Us on Facebook to Read</div>
					<div class="pp_t3">Amazing Car Stories</div>
					<div class="pp_t4">Daily on our page</div>
					<div class="pp_like pp_arrows">
						<div id="pp_like_wrapper">
							<div id="pp_like_btn" class="fb-like"
								data-action="like"
								data-colorscheme="light"
								data-href="https://www.facebook.com/buzzdrives/"
								data-kid-directed-site="false"
								data-layout="button"
								data-share="false"
								data-show-faces="false"
								data-width="100px"
								>
							</div>
							<div id="pp_like_counter">120k</div>
						</div>
					</div>
					<a href="javascript:void(0)" class="pp_bored pp_close">No thanks, I'd rather be bored</a>
					
					<a href="javascript:void(0)" class="x pp_close glyphicon glyphicon-remove"></a>
				</div>
			</div>
			
			<script type="text/javascript">
				// Use fbAsyncInit method to make sure the FB was init.
				// doc: https://developers.facebook.com/docs/javascript/advanced-setup
				window.fbAsyncInit = function() {
					// init the FB api
					FB.init({
						xfbml: false, // prevent auto-rendering the plugins. We render them manually below.
						version: 'v2.4',
					});
					
					/*
					 * It renders manually the FB plugins.
					 * Dev note: as far as I've tested triggering manually the render of the FB plugins it lets you display them early especially on slow connection
					 */
					FB.XFBML.parse();
					
					// record the like/unlike events using the ga function
					try {
						if (FB && FB.Event && FB.Event.subscribe) {
							FB.Event.subscribe('edge.create', function(targetUrl) {
								console.warn('Facebook Like Event: ' + targetUrl);
								ga('send', 'social', 'facebook', 'like', document.URL);
								$('#pp .pp_close').first().click();
							});
							FB.Event.subscribe('edge.remove', function(targetUrl) {
								console.warn('Facebook Unlike Event: ' + targetUrl);
								ga('send', 'social', 'facebook', 'unlike', document.URL);
							});
						}
					} catch(e) {
						console.warn("FB API ERROR: " + e);
						ga('send', 'event', 'debug_fb_api', 'debug_fb_api_error', document.URL);
					}

					// the FB init flag
					$(document).trigger('fbInit');
					
					FB.Event.subscribe('xfbml.render', function(response) {
						// the FB plugins init flag
						$(document).trigger('xfbmlRender');
					});
				};
				
				/* FACEBOOK LIKE POPUP LOGIC - start */
				$(document).bind('xfbmlRender', function() {
					// it runs right after the fb like plugin was init
					var pp = $('#pp'); // our popup
					
					if( pp.length > 0 && typeof isMobile === 'function' && !isMobile() ) {
						console.warn('pp: true');
						
						var pp_close = pp.find('.pp_close'); // close elements - array
						var wbounce = $('#wbounce-modal'); // wbounce popup
						var pp_delay = 25000; // miliseconds - time between document ready and displaying our popup
						var timerStop = typeof timerStart != "undefined" ? Date.now() - timerStart : 0;
						
						if( timerStop >= pp_delay ) {
							pp_delay = 0; // display it imediatlly!
						} else {
							pp_delay = pp_delay - timerStop; // wait just the difference between the pp_delay and the elapsed time
						}
						
						console.warn("[timerStop]: " + timerStop);
						console.warn("[pp_delay]: " + pp_delay);
						
						var wb_delay = 10000; // miliseconds - time between hidding our popup and removing the force-hide class from the wbounce popup
						var c_name = "__pp";

						var stop_propagation = function(e) { // crossbrowser stopPropagation
							var evt = e ? e:window.event;
							if (evt.stopPropagation) {
								evt.stopPropagation();
							}
							if (evt.cancelBubble!=null) {
								evt.cancelBubble = true;
							}
						}
						var prevent_default = function(e) { // crossbrowser preventDefault
							var evt = e ? e:window.event;
							if( evt.preventDefault ) {
								evt.preventDefault();
							} else {
								evt.returnValue = false;
							}
						}
						var set_cookie = function(cname, cvalue, exdays) {
							var ms = exdays*24*60*60*1000;
							var date = new Date();
							date.setTime(date.getTime() + ms);
							var expires = "expires="+date.toUTCString();
							document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
						}
						var isset_cookie = function(cname) {
							if( document.cookie.indexOf(cname) > -1 ) {
								return true;
							} else {
								return false;
							}
						}
						
						if( isset_cookie(c_name) ) {
							return false; // exit function if the cookie is still set
						}
						
						pp.delay( pp_delay ).show( 0, function() {
							if( wbounce.length > 0 ) {
								wbounce.addClass('force-hide');
							}
							set_cookie(c_name, "true");
							
							pp.on('click', function(event) {
								stop_propagation(event); // used for not closing both popups at once
							});
							
							pp_close.each(function() {
								$(this).on('click', function(event) {
									stop_propagation(event); // used for not closing both popups at once
									prevent_default(event);
									
									pp.fadeOut();
									
									if( wbounce.length > 0 ) {
										wbounce.delay( wb_delay ).queue(function() {
											$(this).removeClass('force-hide').dequeue();
										});
									}
								});
							});
						});
					} else {
						console.warn('pp: false');
					}
				});
				/* FACEBOOK LIKE POPUP LOGIC - end */
			</script>
		</div>
	<?php
	}
}
add_action('wp_footer', 'pp_show_in_front');

function pp_load_fb_sdk_async() {
?>
	<div id="fb-root"></div>
	<script type="text/javascript">
		// Load the FB SDK Asynchronously
		(function(d, s, id){
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) {return;}
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_US/sdk.js";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
<?php
}
add_action('after_body', 'pp_load_fb_sdk_async');