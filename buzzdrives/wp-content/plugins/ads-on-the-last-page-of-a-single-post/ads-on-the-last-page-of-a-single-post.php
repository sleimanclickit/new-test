<?php
/*
	Plugin Name: Ads on the last page of a single post
	Description: Adds an ad on the last page of a single post
	Author: GDM Webmedia
*/

function ads_on_last_post_page($content){
	global $post, $page, $pages;
	
	// code kept as example
	if( is_single() && $page == count($pages) && 1==2 ) {
		
		$contentad = '';
		if( 1==2 ) {
			$contentad .= '<div class="contentad content_ad_content_worth_viewing">';
			$contentad .= '<div id="contentad275952"></div>'; /* Content Worth Viewing - BD.com */
			$contentad .= '</div>';
		}

		$revcontent = '';
		if( 1==2 ) {
			$revcontent .= '<div class="revcontent revcontent_v2 revcontent_last_page">';
			$revcontent .= '<div id="rcjsload_e13ca0"></div>'; /* RevContent BuzzDrives End of Slideshow - Internal Content */
			$revcontent .= '</div>';				
		}
			
		$content .= '<div id="ads-last-page">';
			$content = $content . $revcontent;
			$content = $content . $contentad;
		$content .= '</div>';
	}
	
	return $content;
}
add_filter('the_content', 'ads_on_last_post_page');
?>