<?php
/*
	Plugin Name: Hidden posts in front
	Description: This plugin sets an option for posts to hide them from homepage, category page and archive page. It makes them accesable only by knowing the url of the posts.
	Author: GDM Webmedia
*/

/**
 * Adds a box to the main column on the Post edit screens.
 */
function hidden_post_add_meta_box() {
	// doc: https://codex.wordpress.org/Function_Reference/add_meta_box
	add_meta_box(
		'hidden_post_field',
		'Hidden post in front',
		'hidden_post_meta_box_callback',
		'post',
		'side'
	);
}
add_filter('add_meta_boxes', 'hidden_post_add_meta_box');

/**
 * Prints the box content.
 * 
 * @param WP_Post $post The object for the current post/page.
 */
function hidden_post_meta_box_callback( $post ) {
	// Add a nonce field so we can check for it later.
	wp_nonce_field( 'hidden_post_save_meta_box_data', 'hidden_post_meta_box_nonce' );
?>
	<?php
		$hidden_post_value = get_post_meta( $post->ID, 'hidden_post', true );
	?>
	<p>Set this if you want to hide this post from the homepage, category page and archive page. It will still be published but it can only be accesses using the direct url.</p>
	<label>
		<input type="checkbox" name="hidden_post_field" id="hidden_post_field" <?php echo $hidden_post_value == "true" ? "checked": ""; ?> value="true" />
		Hide this post from homepage, category page and archive page.
	</label>
	<br>
	<br>
	<em>This make take a couple of minutes to change in front.</em>
<?php
}

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function hidden_post_save_meta_box_data( $post_id ) {

	/*
	 * We need to verify this came from our screen and with proper authorization,
	 * because the save_post action can be triggered at other times.
	 */

	// Check if our nonce is set.
	if ( ! isset( $_POST['hidden_post_meta_box_nonce'] ) ) {
		return;
	}

	// Verify that the nonce is valid.
	if ( ! wp_verify_nonce( $_POST['hidden_post_meta_box_nonce'], 'hidden_post_save_meta_box_data' ) ) {
		return;
	}

	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	// Check the user's permissions.
	if ( isset( $_POST['post_type'] ) && 'post' == $_POST['post_type'] ) {
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}
	}

	/* OK, it's safe for us to save the data now. */
	
	// Make sure that it is set.
	if ( ! isset($_POST['hidden_post_field'])) {
		// Delete the meta field from the database.
		delete_post_meta( $post_id, 'hidden_post' );
	} else {
		// Sanitize user input.
		$hidden_post_value = sanitize_text_field( $_POST['hidden_post_field'] );
		
		// Update the meta field in the database.
		update_post_meta( $post_id, 'hidden_post', $hidden_post_value );
	}
	
	// clear the cache
	if( function_exists('w3tc_pgcache_flush_post') ) {
		w3tc_pgcache_flush_post( $post_id );
	}
}
add_action( 'save_post', 'hidden_post_save_meta_box_data' );