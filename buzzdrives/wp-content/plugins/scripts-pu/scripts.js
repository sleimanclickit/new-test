$(document).ready(function() {
	var next_btn = $('#csp_btns li:last-child > a');
	
	/* start check to see if we should show the pop-under ( based on utm_source values ) */
	var get_string = document.location.search;
	get_string = get_string.toLowerCase();
	var found = false;
	var utm_source = '';
	
	if( get_string.length ) {
		// var utm_sources = ['yahoogemini','yg','revcontent','content-ad','mgid'];
		var utm_sources = ['revcontent'];
		for (i = 0 ; i < 4 ; i++) {
			if (get_string.indexOf('utm_source='+utm_sources[i]) > -1) {
				found = true;
				utm_source = '?utm_source='+utm_sources[i];
				break;
			}
		}
	}
	/* end check to see if we should show the pop-under */
	
	if(next_btn.length > 0 && found ) {		
		next_btn.on('click', function(event) {
			event.preventDefault ? event.preventDefault() : event.returnValue = false;
			
			ga('send', 'event', 'popunder', 'click_on_popunder', document.URL);
			
			var href = next_btn.attr('href');
			window.open(href, '_blank');
			
			window.location.replace('http://buzzdrives.com/WhatOthersAreReading.html'+utm_source);
		});
		console.warn("Popunder: js was loaded!");
	} else {
		console.warn("Popunder: next_btn does not exist!");
	}
});