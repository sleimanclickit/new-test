<?php
/*
	Plugin Name: Ads Popunder
	Description: Sets a page for each post on which the Popunder Ad will trigger
	Author: GDM Webmedia
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

function ads_popunder_load() {
	if( is_single() ) {
		global $post, $page, $pages;
		
		$popunder_auto = get_post_meta($post->ID, 'popunder_auto', true);
		
		if( $popunder_auto != "manual") {
			_load_popunder_script($page, _get_default_page_no(count($pages)));
		} else {
			$popunder_page = get_post_meta($post->ID, 'popunder_page', true);
			_load_popunder_script($page, $popunder_page);
		}		
	}
}
add_filter('wp_footer', 'ads_popunder_load');

function _load_popunder_script($current, $set) {
	if( $current == $set ) {
		echo '<script src="' . plugin_dir_url(__FILE__) . 'scripts.js"></script>';
	}
}

function popunder_save_meta_box($post_id) {	
	if ( ! isset( $_POST['popunder_page_nonce'] ) ) {
		return $post_id;
	}

	$nonce = $_POST['popunder_page_nonce'];

	// Verify that the nonce is valid.
	if ( ! wp_verify_nonce( $nonce, 'popunder_page' ) ) {
		return $post_id;
	}

	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return $post_id;
	}

	// Check the user's permissions.
	if ( 'post' == $_POST['post_type'] ) {
		if ( ! current_user_can( 'edit_page', $post_id ) ) {
			return $post_id;
		}
	}
	
	// Sanitize user input.
	if( isset($_POST['popunder_auto']) ) {
		$popunder_auto = 'auto';
	} else {
		$popunder_auto = 'manual';
	}
	
	if( isset($_POST['popunder_page']) ) {
		$popunder_page = sanitize_text_field( $_POST['popunder_page'] );
	} else {
		$popunder_page = false;
	}
	
	// Update the meta field in the database.
	update_post_meta( $post_id, 'popunder_auto', $popunder_auto );
	update_post_meta( $post_id, 'popunder_page', $popunder_page );
}
add_action( 'save_post', 'popunder_save_meta_box' );

function _get_default_page_no($pages = false) {
	if( $pages === false ) {
		return false;
	}
	
	if( $pages > 2 ) {
		return ($pages - 2); // second to last (antepenultimul)
	} else {
		return false; // disabled
	}
}

function popunder_meta_box_markup($obj) {
	$pages = substr_count($obj->post_content, '<!--nextpage-->') + 1;
	$auto = get_post_meta($obj->ID, "popunder_auto", true);
	$selected = get_post_meta($obj->ID, "popunder_page", true);
	
	if( $selected === '' || $selected > $pages ) {
		$selected = _get_default_page_no($pages);
	}
	$default = _get_default_page_no($pages);
	
	wp_nonce_field("popunder_page", "popunder_page_nonce");
?>
	<div>
		<div id="popunder_auto">
			<label>
				<input type="checkbox" name="popunder_auto" id="popunder_auto" value="anyvalue" <?php echo $auto != 'manual' ? 'checked' : ''; ?> />
				Set automaticaly on the second to last page.
			</label>
		</div>
		<div id="popunder_manual" <?php echo $auto != 'manual' ? 'style="display:none"' : ''; ?>>
			<p>You have to update the post in order to see all the pages in the dropdown</p>
			<label for="popunder_page">Popunder page</label>
			<select name="popunder_page">
				<option <?php echo $selected == false ? 'selected' : ''; ?> value="false">Disabled<?php echo false === $default ? ' (default)' : ''; ?></option>
				<?php for($i = 1; $i <= $pages; $i++) { ?>
				<option <?php echo $i == $selected ? 'selected' : ''; ?> value="<?php echo $i; ?>"><?php echo $i; ?><?php echo $i == $default ? ' (default)' : ''; ?></option>
				<?php } ?>
			</select>
		</div>
		<script type="text/javascript">
			jQuery(document).ready(function() {
				jQuery('#popunder_auto').change(function() {
					jQuery('#popunder_manual').slideToggle();
				});
			});
		</script>
	</div>
<?php
}
 
function add_popunder_meta_box() {
	add_meta_box("popunder-page", "Page to run Popunder ad", "popunder_meta_box_markup", "post", "side", "high", null);
}
add_action("add_meta_boxes", "add_popunder_meta_box");


// register_activation_hook( __FILE__, 'popunder_activate' );
function popunder_activate(){
	$args = [
		'post_type' => 'post',
		'post_status' => 'publish',
		'posts_per_page' => -1
	];
	
	$query = new WP_Query($args);
	if( $query->have_posts() )
	{
		while( $query->have_posts() )
		{
			$query->the_post();
			update_post_meta( get_the_ID(), 'popunder_auto', 'auto' );
		}
	}
	
	wp_reset_postdata();
}