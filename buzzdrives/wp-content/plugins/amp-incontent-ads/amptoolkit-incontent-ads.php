<?php
/*
Plugin Name: Advanced AMP ADS
Description: Advertisement addon for Accelerated Mobile Pages plugin. This plugin will Auto Insert Advertisements in the content and many more features.
Author: Mohammed Kaludi, Mohammed Khaled
Version: 1.6
Author URI: https://ampforwp.com
*/
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) exit;
if ( defined( 'AMPFORWP_PLUGIN_DIR' ) ) {
	add_filter( 'plugin_action_links', 'ampforwp_settings_link', 10, 5 );
} else {

	add_filter( 'plugin_action_links', 'ampforwp_plugin_activation_link', 10, 5 );

	// Add Activate Parent Plugin button in settings page
	if ( ! function_exists( 'ampforwp_plugin_activation_link' ) ) {
		function ampforwp_plugin_activation_link( $actions, $plugin_file ) {
			static $plugin;
			if (!isset($plugin))
				$plugin = plugin_basename(__FILE__);
				if ($plugin == $plugin_file) {
						$settings = array('settings' => '<a href="plugin-install.php?s=accelerated+mobile+pages&tab=search&type=term">' . __('Please Activate the Parent Plugin.', 'ampforwp_incontent_ads') . '</a>');
						$actions = array_merge($settings , $actions );
					}
				return $actions;
		}
	}
	// Return if Parent plugin is not active, and don't load the below code.
	return;
}

// Add settings Icon in the plugin activation page
if ( ! function_exists( 'ampforwp_settings_link' ) ) {
	function ampforwp_settings_link( $actions, $plugin_file )  {
			static $plugin;
			if (!isset($plugin))
				$plugin = plugin_basename(__FILE__);
				if ($plugin == $plugin_file) {
						$settings = array('settings' => '<a href="admin.php?page=amp_options&tab=8">' . __('Settings', 'ampforwp_incontent_ads') . '</a>');
			  		$actions = array_merge( $actions , $settings);
					}
				return $actions;
	}
}

/*
 * Advertisement Code Generator
 * this code has 5 paramaeters $adver_class, $width, $height, $ad_client, $ad_slot
 *
 * Example
 * $adver_class	= ampforwp-incontent-ad (class of the advert container )
 * $width  		= 300
 * $height 		= 250
 * $ad_client	= ca-pub-12345678912345678
 * $ad_slot		= 1234567891
 *
 * 	// Usage Example
	add_action( 'ampforwp_post_after_design_elements','ampforwp_advert_code_output', 10 );
	function ampforwp_advert_code_output() {
		echo ampforwp_advert_code_generator_adsense('ampforwp-incontent-ad', 728, 15,'data-client-id' ,'data-ad-slot' ) ;
	}
 *
*/
function ampforwp_advert_code_generator_adsense($adver_class, $width, $height, $ad_client, $ad_slot ) {
	$output  = '<div class="amp-ad-wrapper amp_ad_1 ampforwp-incontent-'. $adver_class .'">';
		$output	.=	'
			<amp-ad class="'. $adver_class .'"
				type="adsense"
				width="'. $width .'"
				height="'. $height .'"
				data-ad-client="'. $ad_client .'"
				data-ad-slot="'. $ad_slot .'"
			>';
		$output	.=	'</amp-ad>';
	$output	.= '</div>';
	return $output;
}


/*
 * Advertisement Code Generator for DoubleClick
 * this code has 4 paramaeters $adver_class, $width, $height, $ad_client, $ad_slot
 *
 * Example
 * $adver_class	= doubleclick-incontent (class of the advert container )
 * $width  		= 300
 * $height 		= 250
 * $data_slot	= /4****9/mobile_ad_banner
 *
 * 	// Usage Example
	add_action( 'ampforwp_post_after_design_elements','ampforwp_advert_code_output', 10 );
	function ampforwp_advert_code_output() {
		echo ampforwp_advert_code_generator_doubleclick('ampforwp-incontent-ad', 728, 15,'/4****9/mobile_ad_banner' ) ;
	}
 *
*/
function ampforwp_advert_code_generator_doubleclick($adver_class, $width, $height, $data_slot ) {
	$output = '<div class="amp-ad-wrapper amp_ad_1 ampforwp-incontent-'. $adver_class .'">';
		$output	.=	'
			<amp-ad class="ampforwp-incontent-ad ampforwp-incontent-doubleclick-incontent '. $adver_class .'"
				type="doubleclick"
				width="'. $width .'"
				height="'. $height .'"
				data-slot="'. $data_slot .'"
			>';
		$output	.=	'</amp-ad>';
	$output	.= '</div>';
	return $output;
}

/* Advertisement code */
require_once( trailingslashit( plugin_dir_path( __FILE__ ) ) . '/ads/ad-1.php' );
require_once( trailingslashit( plugin_dir_path( __FILE__ ) ) . '/ads/ad-2.php' );
require_once( trailingslashit( plugin_dir_path( __FILE__ ) ) . '/ads/ad-3.php' );
require_once( trailingslashit( plugin_dir_path( __FILE__ ) ) . '/ads/ad-doubleclick.php' );
// require_once( trailingslashit( plugin_dir_path( __FILE__ ) ) . '/ads/ad-custom.php' );
require_once( trailingslashit( plugin_dir_path( __FILE__ ) ) . '/ads/ad-sticky.php' );
require_once( trailingslashit( plugin_dir_path( __FILE__ ) ) . '/ads/standard-ads.php' );



/* Settings */
require_once( trailingslashit( plugin_dir_path( __FILE__ ) ) . '/settings.php' );


add_filter( 'amp_post_template_data', 'ampforwp_add_advaced_ads_scripts' );
function ampforwp_add_advaced_ads_scripts( $data ) {
	global $redux_builder_amp;

		if(		$redux_builder_amp['ampforwp-incontent-ad-1'] ||
					$redux_builder_amp['ampforwp-incontent-ad-2'] ||
					$redux_builder_amp['ampforwp-incontent-ad-3'] ||
					$redux_builder_amp['ampforwp-standard-ads-1'] ||
					$redux_builder_amp['ampforwp-standard-ads-2'] ||
					$redux_builder_amp['ampforwp-standard-ads-3'] ||
					$redux_builder_amp['ampforwp-standard-ads-4'] ||
					$redux_builder_amp['ampforwp-sticky-ad'] ) {
						if ( empty( $data['amp_component_scripts']['amp-ad'] ) ) {
							$data['amp_component_scripts']['amp-ad'] = 'https://cdn.ampproject.org/v0/amp-ad-0.1.js';
						}
					}

	return $data;
}


/* Theme Updater */
add_action( 'init', 'advance_amp_ads_updater' );
/**
 * Load and Activate Plugin Updater Class.
 * @since 0.1.0
 */
function advance_amp_ads_updater() {

    /* Load Plugin Updater */
    require_once( trailingslashit( plugin_dir_path( __FILE__ ) ) . '/updater/update.php' );

    /* Updater Config */
    $config = array(
        'base'         => plugin_basename( __FILE__ ), //required
        'repo_uri'     => 'http://magazine3.com/updates/',
        'repo_slug'    => 'advanced-amp-ads',
    );

    /* Load Updater Class */
    new AMPFORWP_Advanced_AMP_ADS_Updater( $config );
}