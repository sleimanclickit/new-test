<?php

function ampforwp_create_controls() {
	// New control on users feedback.
	// Incontent Ad #1
	$controls[] =  array(
        'id'        =>'ampforwp-incontent-ad-1',
        'type'      => 'switch',
        'title'     => __('Incontent Ad #1', 'redux-framework-demo'),
        'default'   => 0,
        'true'      => 'Enabled',
        'false'     => 'Disabled',
		'default'		=> 0,
	);
	$controls[] =  array(
		'id'       => 'ampforwp-advertisement-type-incontent-ad-1',
		'type'     => 'select',
		'title'    => __( 'Advertisement Type', 'redux-framework-demo' ),
		'subtitle' => __( 'Select Advertisement Type you want to show.', 'ampforwp' ),
		'options'  	=> array(
			'1' 	=> 'Adsense',
			'2'  	=> 'DoubleClick',
			'3'  	=> 'Custom Advertisement',
		),
		'default'  => '1',
		'required'	=> array('ampforwp-incontent-ad-1','=','1'),
	);
	// '1' 	=> 'Adsense',
		$controls[] =  array(
			'id'       => 'ampforwp-adsense-ad-position-incontent-ad-1',
			'type'     => 'select',
			'title'    => __( 'Google Adsense incontent Ads Position', 'redux-framework-demo' ),
			'subtitle' => __( 'Select after how many paragraphs your ads should show.', 'redux-framework-demo' ),
			'options'  => array(
					'50-percent' 	=> 'Show Ad after 50% of content',
					'1' 					=> '1 Paragraphs',
					'2'  					=> '2 Paragraphs',
					'3'  					=> '3 Paragraphs',
					'4'  					=> '4 Paragraphs',
					'5'  					=> '5 Paragraphs',
					'6'  					=> '6 Paragraphs',
					'7'  					=> '7 Paragraphs',
					'8'  					=> '8 Paragraphs',
					'9'  					=> '9 Paragraphs',
					'10' 					=> '10 Paragraphs',
					'custom' 				=> 'Custom',
				),
			'default'  => '50-percent',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-1','=','1'),
		);
		$controls[] =  array(
			'id'       		=> 'ampforwp-custom-adsense-ad-position-incontent-ad-1',
			'type'     		=> 'text',
			'title'    		=> __('Show ad after Paragraphs', 'redux-framework-demo'),
			'default'  		=> '5',
			'placeholder'	=> '5',
			'required'		=> array('ampforwp-adsense-ad-position-incontent-ad-1','=','custom'),
		);
		$controls[] =  array(
			'id'       		=> 'ampforwp-adsense-ad-width-incontent-ad-1',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'placeholder'	=> '300',
			'required'		=> array('ampforwp-advertisement-type-incontent-ad-1','=','1'),
		);
		$controls[] =  array(
			'id'       		=> 'ampforwp-adsense-ad-height-incontent-ad-1',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
			'placeholder'	=> '300',
			'required'		=> array('ampforwp-advertisement-type-incontent-ad-1','=','1'),
		);
		$controls[] =  array(
			'id'        =>'ampforwp-adsense-ad-data-ad-client-incontent-ad-1',
			'type'      => 'text',
			'title'     => __('Data AD Client'),
			'desc'      => __('Enter the Data Ad Client (data-ad-client) from the adsense ad code. e.g. ca-pub-2005XXXXXXXXX342', 'redux-framework-demo'),
			'default'   => '',
			'placeholder'=> 'ca-pub-2005XXXXXXXXX342',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-1','=','1'),
		);
		$controls[] =  array(
			'id'        => 'ampforwp-adsense-ad-data-ad-slot-incontent-ad-1',
			'type'      => 'text',
			'title'     => __('Data AD Slot'),
			'desc'      => __('Enter the Data Ad Slot (data-ad-slot) from the adsense ad code. e.g. 70XXXXXX12', 'redux-framework-demo'),
			'default'  => '',
			'placeholder'=> '70XXXXXX12',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-1','=','1'),
		);
	// '2'  	=> 'DoubleClick',
		$controls[] =  array(
			'id'       => 'ampforwp-doubleclick-ad-position-incontent-ad-1',
			'type'     => 'select',
			'title'    => __( 'DoubleClick incontent Ads Position', 'redux-framework-demo' ),
			'subtitle' => __( 'Select after how many paragraphs your ads should show.', 'redux-framework-demo' ),
			'options'  => array(
					'50-percent' 	=> 'Show Ad after 50% of content',
					'1' 					=> '1 Paragraphs',
					'2'  					=> '2 Paragraphs',
					'3'  					=> '3 Paragraphs',
					'4'  					=> '4 Paragraphs',
					'5'  					=> '5 Paragraphs',
					'6'  					=> '6 Paragraphs',
					'7'  					=> '7 Paragraphs',
					'8'  					=> '8 Paragraphs',
					'9'  					=> '9 Paragraphs',
					'10' 					=> '10 Paragraphs',
					'custom' 				=> 'Custom',
				),
			'default'  => '50-percent',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-1','=','2'),
		);
		$controls[] =  array(
			'id'       		=> 'ampforwp-custom-doubleclick-ad-position-incontent-ad-1',
			'type'     		=> 'text',
			'title'    		=> __('Show ad after Paragraphs', 'redux-framework-demo'),
			'default'  		=> '5',
			'placeholder'	=> '5',
			'required'		=> array('ampforwp-doubleclick-ad-position-incontent-ad-1','=','custom'),
		);
		$controls[] = 	array(
			'id'        =>'ampforwp-doubleclick-ad-width-incontent-ad-1',
			'type'      => 'text',
			'title'     => __('Width'),
			'placeholder'=> '300',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-1','=','2'),
		);
		$controls[] = 	array(
			'id'        =>'ampforwp-doubleclick-ad-height-incontent-ad-1',
			'type'      => 'text',
			'title'     => __('Height'),
			'placeholder'=> '250',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-1','=','2'),
		);
		$controls[] = 	array(
			'id'        => 'ampforwp-doubleclick-ad-data-slot-incontent-ad-1',
			'type'      => 'text',
			'title'     => __('Data Slot'),
			'placeholder'=> '/41****9/mobile_ad_banner',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-1','=','2'),
		);
	// '3'  	=> 'Custom Advertisement',
		$controls[] =  array(
			'id'       => 'ampforwp-custom-ads-ad-position-incontent-ad-1',
			'type'     => 'select',
			'title'    => __( 'Custom Ads incontent Ads Position', 'redux-framework-demo' ),
			'subtitle' => __( 'Select after how many paragraphs your ads should show.', 'redux-framework-demo' ),
			'options'  => array(
					'50-percent' 	=> 'Show Ad after 50% of content',
					'1' 					=> '1 Paragraphs',
					'2'  					=> '2 Paragraphs',
					'3'  					=> '3 Paragraphs',
					'4'  					=> '4 Paragraphs',
					'5'  					=> '5 Paragraphs',
					'6'  					=> '6 Paragraphs',
					'7'  					=> '7 Paragraphs',
					'8'  					=> '8 Paragraphs',
					'9'  					=> '9 Paragraphs',
					'10' 					=> '10 Paragraphs',
					'custom' 				=> 'Custom',
				),
			'default'  => '50-percent',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-1','=','3'),
		);
		$controls[] =  array(
			'id'       		=> 'ampforwp-custom-custom-ads-ad-position-incontent-ad-1',
			'type'     		=> 'text',
			'title'    		=> __('Show ad after Paragraphs', 'redux-framework-demo'),
			'default'  		=> '5',
			'placeholder'	=> '5',
			'required'		=> array('ampforwp-custom-ads-ad-position-incontent-ad-1','=','custom'),
		);
		$controls[] = array(
			'id'        	=> 'ampforwp-custom-advertisement-incontent-ad-1',
			'type'      	=> 'textarea',
			'title'     	=> __('Advertisement Code'),
			'placeholder'	=> ' <amp-ad width=300 height=250 type="XXXX" data-xxx="XXXX""></amp-ad>
OR
<a href="https://example.com"><amp-img src="/img/amp.jpg" width=300 height=250 alt="advert banner"></amp-img> </a>',
			'required'		=> array('ampforwp-advertisement-type-incontent-ad-1','=','3'),
		);
	// Incontent Ad #1 ENDS


	// Incontent Ad #2
	$controls[] =  array(
        'id'        =>'ampforwp-incontent-ad-2',
        'type'      => 'switch',
        'title'     => __('Incontent Ad #2', 'redux-framework-demo'),
        'default'   => 0,
        'true'      => 'Enabled',
        'false'     => 'Disabled',
		'default'		=> 0,
	);
	$controls[] =  array(
		'id'       => 'ampforwp-advertisement-type-incontent-ad-2',
		'type'     => 'select',
		'title'    => __( 'Advertisement Type', 'redux-framework-demo' ),
		'subtitle' => __( 'Select Advertisement Type you want to show.', 'ampforwp' ),
		'options'  	=> array(
			'1' 	=> 'Adsense',
			'2'  	=> 'DoubleClick',
			'3'  	=> 'Custom Advertisement',
		),
		'default'  => '1',
		'required'	=> array('ampforwp-incontent-ad-2','=','1'),
	);
	// '1' 	=> 'Adsense',
		$controls[] =  array(
			'id'       => 'ampforwp-adsense-ad-position-incontent-ad-2',
			'type'     => 'select',
			'title'    => __( 'Google Adsense incontent Ads Position', 'redux-framework-demo' ),
			'subtitle' => __( 'Select after how many paragraphs your ads should show.', 'redux-framework-demo' ),
			'options'  => array(
					'50-percent' 	=> 'Show Ad after 50% of content',
					'1' 					=> '1 Paragraphs',
					'2'  					=> '2 Paragraphs',
					'3'  					=> '3 Paragraphs',
					'4'  					=> '4 Paragraphs',
					'5'  					=> '5 Paragraphs',
					'6'  					=> '6 Paragraphs',
					'7'  					=> '7 Paragraphs',
					'8'  					=> '8 Paragraphs',
					'9'  					=> '9 Paragraphs',
					'10' 					=> '10 Paragraphs',
					'custom' 				=> 'Custom',
				),
			'default'  => '50-percent',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-2','=','1'),
		);
		$controls[] =  array(
			'id'       		=> 'ampforwp-custom-adsense-ad-position-incontent-ad-2',
			'type'     		=> 'text',
			'title'    		=> __('Show ad after Paragraphs', 'redux-framework-demo'),
			'default'  		=> '5',
			'placeholder'	=> '5',
			'required'		=> array('ampforwp-adsense-ad-position-incontent-ad-2','=','custom'),
		);
		$controls[] =  array(
			'id'       		=> 'ampforwp-adsense-ad-width-incontent-ad-2',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'placeholder'	=> '300',
			'required'		=> array('ampforwp-advertisement-type-incontent-ad-2','=','1'),
		);
		$controls[] =  array(
			'id'       		=> 'ampforwp-adsense-ad-height-incontent-ad-2',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
			'placeholder'	=> '300',
			'required'		=> array('ampforwp-advertisement-type-incontent-ad-2','=','1'),
		);
		$controls[] =  array(
			'id'        =>'ampforwp-adsense-ad-data-ad-client-incontent-ad-2',
			'type'      => 'text',
			'title'     => __('Data AD Client'),
			'desc'      => __('Enter the Data Ad Client (data-ad-client) from the adsense ad code. e.g. ca-pub-2005XXXXXXXXX342', 'redux-framework-demo'),
			'default'   => '',
			'placeholder'=> 'ca-pub-2005XXXXXXXXX342',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-2','=','1'),
		);
		$controls[] =  array(
			'id'        => 'ampforwp-adsense-ad-data-ad-slot-incontent-ad-2',
			'type'      => 'text',
			'title'     => __('Data AD Slot'),
			'desc'      => __('Enter the Data Ad Slot (data-ad-slot) from the adsense ad code. e.g. 70XXXXXX12', 'redux-framework-demo'),
			'default'  => '',
			'placeholder'=> '70XXXXXX12',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-2','=','1'),
		);
	// '2'  	=> 'DoubleClick',
		$controls[] =  array(
			'id'       => 'ampforwp-doubleclick-ad-position-incontent-ad-2',
			'type'     => 'select',
			'title'    => __( 'DoubleClick incontent Ads Position', 'redux-framework-demo' ),
			'subtitle' => __( 'Select after how many paragraphs your ads should show.', 'redux-framework-demo' ),
			'options'  => array(
					'50-percent' 	=> 'Show Ad after 50% of content',
					'1' 					=> '1 Paragraphs',
					'2'  					=> '2 Paragraphs',
					'3'  					=> '3 Paragraphs',
					'4'  					=> '4 Paragraphs',
					'5'  					=> '5 Paragraphs',
					'6'  					=> '6 Paragraphs',
					'7'  					=> '7 Paragraphs',
					'8'  					=> '8 Paragraphs',
					'9'  					=> '9 Paragraphs',
					'10' 					=> '10 Paragraphs',
					'custom' 				=> 'Custom',
				),
			'default'  => '50-percent',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-2','=','2'),
		);
		$controls[] =  array(
			'id'       		=> 'ampforwp-custom-doubleclick-ad-position-incontent-ad-2',
			'type'     		=> 'text',
			'title'    		=> __('Show ad after Paragraphs', 'redux-framework-demo'),
			'default'  		=> '5',
			'placeholder'	=> '5',
			'required'		=> array('ampforwp-doubleclick-ad-position-incontent-ad-2','=','custom'),
		);
		$controls[] = 	array(
			'id'        =>'ampforwp-doubleclick-ad-width-incontent-ad-2',
			'type'      => 'text',
			'title'     => __('Width'),
			'placeholder'=> '300',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-2','=','2'),
		);
		$controls[] = 	array(
			'id'        =>'ampforwp-doubleclick-ad-height-incontent-ad-2',
			'type'      => 'text',
			'title'     => __('Height'),
			'placeholder'=> '250',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-2','=','2'),
		);
		$controls[] = 	array(
			'id'        => 'ampforwp-doubleclick-ad-data-slot-incontent-ad-2',
			'type'      => 'text',
			'title'     => __('Data Slot'),
			'placeholder'=> '/41****9/mobile_ad_banner',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-2','=','2'),
		);
	// '3'  	=> 'Custom Advertisement',
		$controls[] =  array(
			'id'       => 'ampforwp-custom-ads-ad-position-incontent-ad-2',
			'type'     => 'select',
			'title'    => __( 'Custom Ads incontent Ads Position', 'redux-framework-demo' ),
			'subtitle' => __( 'Select after how many paragraphs your ads should show.', 'redux-framework-demo' ),
			'options'  => array(
					'50-percent' 	=> 'Show Ad after 50% of content',
					'1' 					=> '1 Paragraphs',
					'2'  					=> '2 Paragraphs',
					'3'  					=> '3 Paragraphs',
					'4'  					=> '4 Paragraphs',
					'5'  					=> '5 Paragraphs',
					'6'  					=> '6 Paragraphs',
					'7'  					=> '7 Paragraphs',
					'8'  					=> '8 Paragraphs',
					'9'  					=> '9 Paragraphs',
					'10' 					=> '10 Paragraphs',
					'custom' 				=> 'Custom',
				),
			'default'  => '50-percent',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-2','=','3'),
		);
		$controls[] =  array(
			'id'       		=> 'ampforwp-custom-custom-ads-ad-position-incontent-ad-2',
			'type'     		=> 'text',
			'title'    		=> __('Show ad after Paragraphs', 'redux-framework-demo'),
			'default'  		=> '5',
			'placeholder'	=> '5',
			'required'		=> array('ampforwp-custom-ads-ad-position-incontent-ad-2','=','custom'),
		);
		$controls[] = array(
			'id'        	=> 'ampforwp-custom-advertisement-incontent-ad-2',
			'type'      	=> 'textarea',
			'title'     	=> __('Advertisement Code'),
			'placeholder'	=> ' <amp-ad width=300 height=250 type="XXXX" data-xxx="XXXX""></amp-ad>
OR
<a href="https://example.com"><amp-img src="/img/amp.jpg" width=300 height=250 alt="advert banner"></amp-img> </a>',
			'required'		=> array('ampforwp-advertisement-type-incontent-ad-2','=','3'),
		);
	// Incontent Ad #2 ENDS

	// Incontent Ad #3
	$controls[] =  array(
        'id'        =>'ampforwp-incontent-ad-3',
        'type'      => 'switch',
        'title'     => __('Incontent Ad #3', 'redux-framework-demo'),
        'default'   => 0,
        'true'      => 'Enabled',
        'false'     => 'Disabled',
		'default'		=> 0,
	);
	$controls[] =  array(
		'id'       => 'ampforwp-advertisement-type-incontent-ad-3',
		'type'     => 'select',
		'title'    => __( 'Advertisement Type', 'redux-framework-demo' ),
		'subtitle' => __( 'Select Advertisement Type you want to show.', 'ampforwp' ),
		'options'  	=> array(
			'1' 	=> 'Adsense',
			'2'  	=> 'DoubleClick',
			'3'  	=> 'Custom Advertisement',
		),
		'default'  => '1',
		'required'	=> array('ampforwp-incontent-ad-3','=','1'),
	);
	// '1' 	=> 'Adsense',
		$controls[] =  array(
			'id'       => 'ampforwp-adsense-ad-position-incontent-ad-3',
			'type'     => 'select',
			'title'    => __( 'Google Adsense incontent Ads Position', 'redux-framework-demo' ),
			'subtitle' => __( 'Select after how many paragraphs your ads should show.', 'redux-framework-demo' ),
			'options'  => array(
					'50-percent' 	=> 'Show Ad after 50% of content',
					'1' 					=> '1 Paragraphs',
					'2'  					=> '2 Paragraphs',
					'3'  					=> '3 Paragraphs',
					'4'  					=> '4 Paragraphs',
					'5'  					=> '5 Paragraphs',
					'6'  					=> '6 Paragraphs',
					'7'  					=> '7 Paragraphs',
					'8'  					=> '8 Paragraphs',
					'9'  					=> '9 Paragraphs',
					'10' 					=> '10 Paragraphs',
					'custom' 				=> 'Custom',
				),
			'default'  => '50-percent',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-3','=','1'),
		);
		$controls[] =  array(
			'id'       		=> 'ampforwp-custom-adsense-ad-position-incontent-ad-3',
			'type'     		=> 'text',
			'title'    		=> __('Show ad after Paragraphs', 'redux-framework-demo'),
			'default'  		=> '5',
			'placeholder'	=> '5',
			'required'		=> array('ampforwp-adsense-ad-position-incontent-ad-3','=','custom'),
		);
		$controls[] =  array(
			'id'       		=> 'ampforwp-adsense-ad-width-incontent-ad-3',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'placeholder'	=> '300',
			'required'		=> array('ampforwp-advertisement-type-incontent-ad-3','=','1'),
		);
		$controls[] =  array(
			'id'       		=> 'ampforwp-adsense-ad-height-incontent-ad-3',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
			'placeholder'	=> '300',
			'required'		=> array('ampforwp-advertisement-type-incontent-ad-3','=','1'),
		);
		$controls[] =  array(
			'id'        =>'ampforwp-adsense-ad-data-ad-client-incontent-ad-3',
			'type'      => 'text',
			'title'     => __('Data AD Client'),
			'desc'      => __('Enter the Data Ad Client (data-ad-client) from the adsense ad code. e.g. ca-pub-2005XXXXXXXXX342', 'redux-framework-demo'),
			'default'   => '',
			'placeholder'=> 'ca-pub-2005XXXXXXXXX342',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-3','=','1'),
		);
		$controls[] =  array(
			'id'        => 'ampforwp-adsense-ad-data-ad-slot-incontent-ad-3',
			'type'      => 'text',
			'title'     => __('Data AD Slot'),
			'desc'      => __('Enter the Data Ad Slot (data-ad-slot) from the adsense ad code. e.g. 70XXXXXX12', 'redux-framework-demo'),
			'default'  => '',
			'placeholder'=> '70XXXXXX12',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-3','=','1'),
		);
	// '2'  	=> 'DoubleClick',
		$controls[] =  array(
			'id'       => 'ampforwp-doubleclick-ad-position-incontent-ad-3',
			'type'     => 'select',
			'title'    => __( 'DoubleClick incontent Ads Position', 'redux-framework-demo' ),
			'subtitle' => __( 'Select after how many paragraphs your ads should show.', 'redux-framework-demo' ),
			'options'  => array(
					'50-percent' 	=> 'Show Ad after 50% of content',
					'1' 					=> '1 Paragraphs',
					'2'  					=> '2 Paragraphs',
					'3'  					=> '3 Paragraphs',
					'4'  					=> '4 Paragraphs',
					'5'  					=> '5 Paragraphs',
					'6'  					=> '6 Paragraphs',
					'7'  					=> '7 Paragraphs',
					'8'  					=> '8 Paragraphs',
					'9'  					=> '9 Paragraphs',
					'10' 					=> '10 Paragraphs',
					'custom' 				=> 'Custom',
				),
			'default'  => '50-percent',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-3','=','2'),
		);
		$controls[] =  array(
			'id'       		=> 'ampforwp-custom-doubleclick-ad-position-incontent-ad-3',
			'type'     		=> 'text',
			'title'    		=> __('Show ad after Paragraphs', 'redux-framework-demo'),
			'default'  		=> '5',
			'placeholder'	=> '5',
			'required'		=> array('ampforwp-doubleclick-ad-position-incontent-ad-2','=','custom'),
		);
		$controls[] = 	array(
			'id'        =>'ampforwp-doubleclick-ad-width-incontent-ad-3',
			'type'      => 'text',
			'title'     => __('Width'),
			'placeholder'=> '300',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-3','=','2'),
		);
		$controls[] = 	array(
			'id'        =>'ampforwp-doubleclick-ad-height-incontent-ad-3',
			'type'      => 'text',
			'title'     => __('Height'),
			'placeholder'=> '250',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-3','=','2'),
		);
		$controls[] = 	array(
			'id'        => 'ampforwp-doubleclick-ad-data-slot-incontent-ad-3',
			'type'      => 'text',
			'title'     => __('Data Slot'),
			'placeholder'=> '/41****9/mobile_ad_banner',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-3','=','2'),
		);
	// '3'  	=> 'Custom Advertisement',
		$controls[] =  array(
			'id'       => 'ampforwp-custom-ads-ad-position-incontent-ad-3',
			'type'     => 'select',
			'title'    => __( 'Custom Ads incontent Ads Position', 'redux-framework-demo' ),
			'subtitle' => __( 'Select after how many paragraphs your ads should show.', 'redux-framework-demo' ),
			'options'  => array(
					'50-percent' 	=> 'Show Ad after 50% of content',
					'1' 					=> '1 Paragraphs',
					'2'  					=> '2 Paragraphs',
					'3'  					=> '3 Paragraphs',
					'4'  					=> '4 Paragraphs',
					'5'  					=> '5 Paragraphs',
					'6'  					=> '6 Paragraphs',
					'7'  					=> '7 Paragraphs',
					'8'  					=> '8 Paragraphs',
					'9'  					=> '9 Paragraphs',
					'10' 					=> '10 Paragraphs',
					'custom' 				=> 'Custom',
				),
			'default'  => '50-percent',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-3','=','3'),
		);
		$controls[] =  array(
			'id'       		=> 'ampforwp-custom-custom-ads-ad-position-incontent-ad-3',
			'type'     		=> 'text',
			'title'    		=> __('Show ad after Paragraphs', 'redux-framework-demo'),
			'default'  		=> '5',
			'placeholder'	=> '5',
			'required'		=> array('ampforwp-custom-ads-ad-position-incontent-ad-3','=','custom'),
		);
		$controls[] = array(
			'id'        	=> 'ampforwp-custom-advertisement-incontent-ad-3',
			'type'      	=> 'textarea',
			'title'     	=> __('Advertisement Code'),
			'placeholder'	=> ' <amp-ad width=300 height=250 type="XXXX" data-xxx="XXXX""></amp-ad>
OR
<a href="https://example.com"><amp-img src="/img/amp.jpg" width=300 height=250 alt="advert banner"></amp-img> </a>',
			'required'		=> array('ampforwp-advertisement-type-incontent-ad-3','=','3'),
		);
	// Incontent Ad #3 ENDS

  // Standard Ads - starts

	$controls[] =  array(
        'id'        =>'ampforwp-standard-ads-1',
        'type'      => 'switch',
        'title'     => __('Below the Header (SiteWide)', 'redux-framework-demo'),
        'default'   => 0,
	);

  //standard ad 1 - starts here
	$controls[] =  array(
		'id'       => 'ampforwp-advertisement-type-standard-1',
		'type'     => 'select',
		'title'    => __( 'Advertisement Type', 'redux-framework-demo' ),
		'subtitle' => __( 'Select Advertisement Type you want to show.', 'ampforwp' ),
		'options'  	=> array(
			'1' 	=> 'Adsense',
			'2'  	=> 'DoubleClick',
			'3'  	=> 'Custom Advertisement',
		),
		'default'  => '1',
		'required'	=> array('ampforwp-standard-ads-1','=','1'),
	);
	// '1' 	=> 'Adsense',
		$controls[] =  array(
			'id'       		=> 'ampforwp-adsense-ad-width-standard-1',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'placeholder'	=> '300',
			'required'		=> array(
                          array('ampforwp-standard-ads-1','=','1'),
                          array('ampforwp-advertisement-type-standard-1','=','1'),
		));
		$controls[] =  array(
			'id'       		=> 'ampforwp-adsense-ad-height-standard-1',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
			'placeholder'	=> '300',
      'required'		=> array(
                          array('ampforwp-standard-ads-1','=','1'),
                          array('ampforwp-advertisement-type-standard-1','=','1'))
		);
		$controls[] =  array(
			'id'        =>'ampforwp-adsense-ad-data-ad-client-standard-1',
			'type'      => 'text',
			'title'     => __('Data AD Client'),
			'desc'      => __('Enter the Data Ad Client (data-ad-client) from the adsense ad code. e.g. ca-pub-2005XXXXXXXXX342', 'redux-framework-demo'),
			'default'   => '',
			'placeholder'=> 'ca-pub-2005XXXXXXXXX342',
      'required'		=> array(
                          array('ampforwp-standard-ads-1','=','1'),
                          array('ampforwp-advertisement-type-standard-1','=','1')),
		);
		$controls[] =  array(
			'id'        => 'ampforwp-adsense-ad-data-ad-slot-standard-1',
			'type'      => 'text',
			'title'     => __('Data AD Slot'),
			'desc'      => __('Enter the Data Ad Slot (data-ad-slot) from the adsense ad code. e.g. 70XXXXXX12', 'redux-framework-demo'),
			'default'  => '',
			'placeholder'=> '70XXXXXX12',
      'required'		=> array(
                          array('ampforwp-standard-ads-1','=','1'),
                          array('ampforwp-advertisement-type-standard-1','=','1')),
		);
	// '2'  	=> 'DoubleClick',
		$controls[] = 	array(
			'id'        =>'ampforwp-doubleclick-ad-width-standard-1',
			'type'      => 'text',
			'title'     => __('Width'),
			'placeholder'=> '300',
      'required'		=> array(
                          array('ampforwp-standard-ads-1','=','1'),
                          array('ampforwp-advertisement-type-standard-1','=','2')),
		);
		$controls[] = 	array(
			'id'        =>'ampforwp-doubleclick-ad-height-standard-1',
			'type'      => 'text',
			'title'     => __('Height'),
			'placeholder'=> '250',
      'required'		=> array(
                          array('ampforwp-standard-ads-1','=','1'),
                          array('ampforwp-advertisement-type-standard-1','=','2')),
		);
		$controls[] = 	array(
			'id'        => 'ampforwp-doubleclick-ad-data-slot-standard-1',
			'type'      => 'text',
			'title'     => __('Data Slot'),
			'placeholder'=> '/41****9/mobile_ad_banner',
      'required'		=> array(
                          array('ampforwp-standard-ads-1','=','1'),
                          array('ampforwp-advertisement-type-standard-1','=','2')),
		);
	// '3'  	=> 'Custom Advertisement'
		$controls[] = array(
			'id'        	=> 'ampforwp-custom-advertisement-standard-1',
			'type'      	=> 'textarea',
			'title'     	=> __('Advertisement Code'),
			'placeholder'	=> ' <amp-ad width=300 height=250 type="XXXX" data-xxx="XXXX""></amp-ad>
OR
<a href="https://example.com"><amp-img src="/img/amp.jpg" width=300 height=250 alt="advert banner"></amp-img> </a>',
'required'		=> array(
                    array('ampforwp-standard-ads-1','=','1'),
                    array('ampforwp-advertisement-type-standard-1','=','3')),
		);
    //standard ad -1 ends here

    $controls[] =  array(
          'id'        =>'ampforwp-standard-ads-2',
          'type'      => 'switch',
          'title'     => __('Below the Footer (SiteWide)', 'redux-framework-demo'),
          'default'   => 0,
  	);
  //standard ad 2- starts here
	$controls[] =  array(
		'id'       => 'ampforwp-advertisement-type-standard-2',
		'type'     => 'select',
		'title'    => __( 'Advertisement Type', 'redux-framework-demo' ),
		'subtitle' => __( 'Select Advertisement Type you want to show.', 'ampforwp' ),
		'options'  	=> array(
			'1' 	=> 'Adsense',
			'2'  	=> 'DoubleClick',
			'3'  	=> 'Custom Advertisement',
		),
		'default'  => '1',
		'required'	=> array('ampforwp-standard-ads-2','=','1'),
	);
	// '1' 	=> 'Adsense',
		$controls[] =  array(
			'id'       		=> 'ampforwp-adsense-ad-width-standard-2',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'placeholder'	=> '300',
			'required'		=> array(
                          array('ampforwp-standard-ads-2','=','1'),
                          array('ampforwp-advertisement-type-standard-2','=','1'),
		));
		$controls[] =  array(
			'id'       		=> 'ampforwp-adsense-ad-height-standard-2',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
			'placeholder'	=> '300',
      'required'		=> array(
                          array('ampforwp-standard-ads-2','=','1'),
                          array('ampforwp-advertisement-type-standard-2','=','1'))
		);
		$controls[] =  array(
			'id'        =>'ampforwp-adsense-ad-data-ad-client-standard-2',
			'type'      => 'text',
			'title'     => __('Data AD Client'),
			'desc'      => __('Enter the Data Ad Client (data-ad-client) from the adsense ad code. e.g. ca-pub-2005XXXXXXXXX342', 'redux-framework-demo'),
			'default'   => '',
			'placeholder'=> 'ca-pub-2005XXXXXXXXX342',
      'required'		=> array(
                          array('ampforwp-standard-ads-2','=','1'),
                          array('ampforwp-advertisement-type-standard-2','=','1')),
		);
		$controls[] =  array(
			'id'        => 'ampforwp-adsense-ad-data-ad-slot-standard-2',
			'type'      => 'text',
			'title'     => __('Data AD Slot'),
			'desc'      => __('Enter the Data Ad Slot (data-ad-slot) from the adsense ad code. e.g. 70XXXXXX12', 'redux-framework-demo'),
			'default'  => '',
			'placeholder'=> '70XXXXXX12',
      'required'		=> array(
                          array('ampforwp-standard-ads-2','=','1'),
                          array('ampforwp-advertisement-type-standard-2','=','1')),
		);
	// '2'  	=> 'DoubleClick',
		$controls[] = 	array(
			'id'        =>'ampforwp-doubleclick-ad-width-standard-2',
			'type'      => 'text',
			'title'     => __('Width'),
			'placeholder'=> '300',
      'required'		=> array(
                          array('ampforwp-standard-ads-2','=','1'),
                          array('ampforwp-advertisement-type-standard-2','=','2')),
		);
		$controls[] = 	array(
			'id'        =>'ampforwp-doubleclick-ad-height-standard-2',
			'type'      => 'text',
			'title'     => __('Height'),
			'placeholder'=> '250',
      'required'		=> array(
                          array('ampforwp-standard-ads-2','=','1'),
                          array('ampforwp-advertisement-type-standard-2','=','2')),
		);
		$controls[] = 	array(
			'id'        => 'ampforwp-doubleclick-ad-data-slot-standard-2',
			'type'      => 'text',
			'title'     => __('Data Slot'),
			'placeholder'=> '/41****9/mobile_ad_banner',
      'required'		=> array(
                          array('ampforwp-standard-ads-2','=','1'),
                          array('ampforwp-advertisement-type-standard-2','=','2')),
		);
	// '3'  	=> 'Custom Advertisement'
		$controls[] = array(
			'id'        	=> 'ampforwp-custom-advertisement-standard-2',
			'type'      	=> 'textarea',
			'title'     	=> __('Advertisement Code'),
			'placeholder'	=> ' <amp-ad width=300 height=250 type="XXXX" data-xxx="XXXX""></amp-ad>
OR
<a href="https://example.com"><amp-img src="/img/amp.jpg" width=300 height=250 alt="advert banner"></amp-img> </a>',
'required'		=> array(
                    array('ampforwp-standard-ads-2','=','1'),
                    array('ampforwp-advertisement-type-standard-2','=','3')),
		);
    //standard ad -2 ends here
    $controls[] =  array(
          'id'        =>'ampforwp-standard-ads-3',
          'type'      => 'switch',
          'title'     => __('Above the Post Content (Single Post)', 'redux-framework-demo'),
          'default'   => 0,
  	);

  //standard ad 3 - starts here
	$controls[] =  array(
		'id'       => 'ampforwp-advertisement-type-standard-3',
		'type'     => 'select',
		'title'    => __( 'Advertisement Type', 'redux-framework-demo' ),
		'subtitle' => __( 'Select Advertisement Type you want to show.', 'ampforwp' ),
		'options'  	=> array(
			'1' 	=> 'Adsense',
			'2'  	=> 'DoubleClick',
			'3'  	=> 'Custom Advertisement',
		),
		'default'  => '1',
		'required'	=> array('ampforwp-standard-ads-3','=','1'),
	);
	// '1' 	=> 'Adsense',3
  $controls[] =  array(
			'id'       		=> 'ampforwp-adsense-ad-width-standard-3',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'placeholder'	=> '300',
			'required'		=> array(
                          array('ampforwp-standard-ads-3','=','1'),
                          array('ampforwp-advertisement-type-standard-3','=','1'),
		));
		$controls[] =  array(
			'id'       		=> 'ampforwp-adsense-ad-height-standard-3',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
			'placeholder'	=> '300',
      'required'		=> array(
                          array('ampforwp-standard-ads-3','=','1'),
                          array('ampforwp-advertisement-type-standard-3','=','1'))
		);
		$controls[] =  array(
			'id'        =>'ampforwp-adsense-ad-data-ad-client-standard-3',
			'type'      => 'text',
			'title'     => __('Data AD Client'),
			'desc'      => __('Enter the Data Ad Client (data-ad-client) from the adsense ad code. e.g. ca-pub-2005XXXXXXXXX342', 'redux-framework-demo'),
			'default'   => '',
			'placeholder'=> 'ca-pub-2005XXXXXXXXX342',
      'required'		=> array(
                          array('ampforwp-standard-ads-3','=','1'),
                          array('ampforwp-advertisement-type-standard-3','=','1')),
		);
		$controls[] =  array(
			'id'        => 'ampforwp-adsense-ad-data-ad-slot-standard-3',
			'type'      => 'text',
			'title'     => __('Data AD Slot'),
			'desc'      => __('Enter the Data Ad Slot (data-ad-slot) from the adsense ad code. e.g. 70XXXXXX12', 'redux-framework-demo'),
			'default'  => '',
			'placeholder'=> '70XXXXXX12',
      'required'		=> array(
                          array('ampforwp-standard-ads-3','=','1'),
                          array('ampforwp-advertisement-type-standard-3','=','1')),
		);
	// '2'  	=> 'DoubleClick',
		$controls[] = 	array(
			'id'        =>'ampforwp-doubleclick-ad-width-standard-3',
			'type'      => 'text',
			'title'     => __('Width'),
			'placeholder'=> '300',
      'required'		=> array(
                          array('ampforwp-standard-ads-3','=','1'),
                          array('ampforwp-advertisement-type-standard-3','=','2')),
		);
		$controls[] = 	array(
			'id'        =>'ampforwp-doubleclick-ad-height-standard-3',
			'type'      => 'text',
			'title'     => __('Height'),
			'placeholder'=> '250',
      'required'		=> array(
                          array('ampforwp-standard-ads-3','=','1'),
                          array('ampforwp-advertisement-type-standard-3','=','2')),
		);
		$controls[] = 	array(
			'id'        => 'ampforwp-doubleclick-ad-data-slot-standard-3',
			'type'      => 'text',
			'title'     => __('Data Slot'),
			'placeholder'=> '/41****9/mobile_ad_banner',
      'required'		=> array(
                          array('ampforwp-standard-ads-3','=','1'),
                          array('ampforwp-advertisement-type-standard-3','=','2')),
		);
	// '3'  	=> 'Custom Advertisement'
		$controls[] = array(
			'id'        	=> 'ampforwp-custom-advertisement-standard-3',
			'type'      	=> 'textarea',
			'title'     	=> __('Advertisement Code'),
			'placeholder'	=> ' <amp-ad width=300 height=250 type="XXXX" data-xxx="XXXX""></amp-ad>
OR
<a href="https://example.com"><amp-img src="/img/amp.jpg" width=300 height=250 alt="advert banner"></amp-img> </a>',
'required'		=> array(
                    array('ampforwp-standard-ads-3','=','1'),
                    array('ampforwp-advertisement-type-standard-3','=','3')),
		);
    //standard ad -3 ends here

		//standard ad - 4 starts here
    $controls[] =  array(
          'id'        =>'ampforwp-standard-ads-4',
          'type'      => 'switch',
          'title'     => __('Below the Post Content (Single Post)', 'redux-framework-demo'),
          'default'   => 0,
  	);
	$controls[] =  array(
		'id'       => 'ampforwp-advertisement-type-standard-4',
		'type'     => 'select',
		'title'    => __( 'Advertisement Type', 'redux-framework-demo' ),
		'subtitle' => __( 'Select Advertisement Type you want to show.', 'ampforwp' ),
		'options'  	=> array(
			'1' 	=> 'Adsense',
			'2'  	=> 'DoubleClick',
			'3'  	=> 'Custom Advertisement',
		),
		'default'  => '1',
		'required'	=> array('ampforwp-standard-ads-4','=','1'),
	);
	// '1' 	=> 'Adsense',
		$controls[] =  array(
			'id'       		=> 'ampforwp-adsense-ad-width-standard-4',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'placeholder'	=> '300',
			'required'		=> array(
                          array('ampforwp-standard-ads-4','=','1'),
                          array('ampforwp-advertisement-type-standard-4','=','1'),
		));
		$controls[] =  array(
			'id'       		=> 'ampforwp-adsense-ad-height-standard-4',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
			'placeholder'	=> '300',
      'required'		=> array(
                          array('ampforwp-standard-ads-4','=','1'),
                          array('ampforwp-advertisement-type-standard-4','=','1'))
		);
		$controls[] =  array(
			'id'        =>'ampforwp-adsense-ad-data-ad-client-standard-4',
			'type'      => 'text',
			'title'     => __('Data AD Client'),
			'desc'      => __('Enter the Data Ad Client (data-ad-client) from the adsense ad code. e.g. ca-pub-2005XXXXXXXXX342', 'redux-framework-demo'),
			'default'   => '',
			'placeholder'=> 'ca-pub-2005XXXXXXXXX342',
      'required'		=> array(
                          array('ampforwp-standard-ads-4','=','1'),
                          array('ampforwp-advertisement-type-standard-4','=','1')),
		);
		$controls[] =  array(
			'id'        => 'ampforwp-adsense-ad-data-ad-slot-standard-4',
			'type'      => 'text',
			'title'     => __('Data AD Slot'),
			'desc'      => __('Enter the Data Ad Slot (data-ad-slot) from the adsense ad code. e.g. 70XXXXXX12', 'redux-framework-demo'),
			'default'  => '',
			'placeholder'=> '70XXXXXX12',
      'required'		=> array(
                          array('ampforwp-standard-ads-4','=','1'),
                          array('ampforwp-advertisement-type-standard-4','=','1')),
		);
	// '2'  	=> 'DoubleClick',
		$controls[] = 	array(
			'id'        =>'ampforwp-doubleclick-ad-width-standard-4',
			'type'      => 'text',
			'title'     => __('Width'),
			'placeholder'=> '300',
      'required'		=> array(
                          array('ampforwp-standard-ads-4','=','1'),
                          array('ampforwp-advertisement-type-standard-4','=','2')),
		);
		$controls[] = 	array(
			'id'        =>'ampforwp-doubleclick-ad-height-standard-4',
			'type'      => 'text',
			'title'     => __('Height'),
			'placeholder'=> '250',
      'required'		=> array(
                          array('ampforwp-standard-ads-4','=','1'),
                          array('ampforwp-advertisement-type-standard-4','=','2')),
		);
		$controls[] = 	array(
			'id'        => 'ampforwp-doubleclick-ad-data-slot-standard-4',
			'type'      => 'text',
			'title'     => __('Data Slot'),
			'placeholder'=> '/41****9/mobile_ad_banner',
      'required'		=> array(
                          array('ampforwp-standard-ads-4','=','1'),
                          array('ampforwp-advertisement-type-standard-4','=','2')),
		);
	// '3'  	=> 'Custom Advertisement'
		$controls[] = array(
			'id'        	=> 'ampforwp-custom-advertisement-standard-4',
			'type'      	=> 'textarea',
			'title'     	=> __('Advertisement Code'),
			'placeholder'	=> ' <amp-ad width=300 height=250 type="XXXX" data-xxx="XXXX""></amp-ad>
OR
<a href="https://example.com"><amp-img src="/img/amp.jpg" width=300 height=250 alt="advert banner"></amp-img> </a>',
'required'		=> array(
                    array('ampforwp-standard-ads-4','=','1'),
                    array('ampforwp-advertisement-type-standard-4','=','3')),
		);
    //standard ad -4 ends here

	//standard ad - 5 starts here
    $controls[] =  array(
          'id'        =>'ampforwp-standard-ads-5',
          'type'      => 'switch',
          'title'     => __('Below the Title (Single Post)', 'redux-framework-demo'),
          'default'   => 0,
  	);
	$controls[] =  array(
		'id'       => 'ampforwp-advertisement-type-standard-5',
		'type'     => 'select',
		'title'    => __( 'Advertisement Type', 'redux-framework-demo' ),
		'subtitle' => __( 'Select Advertisement Type you want to show.', 'ampforwp' ),
		'options'  	=> array(
			'1' 	=> 'Adsense',
			'2'  	=> 'DoubleClick',
			'3'  	=> 'Custom Advertisement',
		),
		'default'  => '1',
		'required'	=> array('ampforwp-standard-ads-5','=','1'),
	);
	// '1' 	=> 'Adsense',
		$controls[] =  array(
			'id'       		=> 'ampforwp-adsense-ad-width-standard-5',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'placeholder'	=> '300',
			'required'		=> array(
                          array('ampforwp-standard-ads-5','=','1'),
                          array('ampforwp-advertisement-type-standard-5','=','1'),
		));
		$controls[] =  array(
			'id'       		=> 'ampforwp-adsense-ad-height-standard-5',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
			'placeholder'	=> '300',
      'required'		=> array(
                          array('ampforwp-standard-ads-5','=','1'),
                          array('ampforwp-advertisement-type-standard-5','=','1'))
		);
		$controls[] =  array(
			'id'        =>'ampforwp-adsense-ad-data-ad-client-standard-5',
			'type'      => 'text',
			'title'     => __('Data AD Client'),
			'desc'      => __('Enter the Data Ad Client (data-ad-client) from the adsense ad code. e.g. ca-pub-2005XXXXXXXXX342', 'redux-framework-demo'),
			'default'   => '',
			'placeholder'=> 'ca-pub-2005XXXXXXXXX342',
      'required'		=> array(
                          array('ampforwp-standard-ads-5','=','1'),
                          array('ampforwp-advertisement-type-standard-5','=','1')),
		);
		$controls[] =  array(
			'id'        => 'ampforwp-adsense-ad-data-ad-slot-standard-5',
			'type'      => 'text',
			'title'     => __('Data AD Slot'),
			'desc'      => __('Enter the Data Ad Slot (data-ad-slot) from the adsense ad code. e.g. 70XXXXXX12', 'redux-framework-demo'),
			'default'  => '',
			'placeholder'=> '70XXXXXX12',
      'required'		=> array(
                          array('ampforwp-standard-ads-5','=','1'),
                          array('ampforwp-advertisement-type-standard-5','=','1')),
		);
	// '2'  	=> 'DoubleClick',
		$controls[] = 	array(
			'id'        =>'ampforwp-doubleclick-ad-width-standard-5',
			'type'      => 'text',
			'title'     => __('Width'),
			'placeholder'=> '300',
      'required'		=> array(
                          array('ampforwp-standard-ads-5','=','1'),
                          array('ampforwp-advertisement-type-standard-5','=','2')),
		);
		$controls[] = 	array(
			'id'        =>'ampforwp-doubleclick-ad-height-standard-5',
			'type'      => 'text',
			'title'     => __('Height'),
			'placeholder'=> '250',
      'required'		=> array(
                          array('ampforwp-standard-ads-5','=','1'),
                          array('ampforwp-advertisement-type-standard-5','=','2')),
		);
		$controls[] = 	array(
			'id'        => 'ampforwp-doubleclick-ad-data-slot-standard-5',
			'type'      => 'text',
			'title'     => __('Data Slot'),
			'placeholder'=> '/41****9/mobile_ad_banner',
      'required'		=> array(
                          array('ampforwp-standard-ads-5','=','1'),
                          array('ampforwp-advertisement-type-standard-5','=','2')),
		);
	// '3'  	=> 'Custom Advertisement'
		$controls[] = array(
			'id'        	=> 'ampforwp-custom-advertisement-standard-5',
			'type'      	=> 'textarea',
			'title'     	=> __('Advertisement Code'),
			'placeholder'	=> ' <amp-ad width=300 height=250 type="XXXX" data-xxx="XXXX""></amp-ad>
OR
<a href="https://example.com"><amp-img src="/img/amp.jpg" width=300 height=250 alt="advert banner"></amp-img> </a>',
'required'		=> array(
                    array('ampforwp-standard-ads-5','=','1'),
                    array('ampforwp-advertisement-type-standard-5','=','3')),
		);
    //standard ad -5 ends here

		//standard ad -6 starts here
		$controls[] =  array(
					'id'        =>'ampforwp-standard-ads-6',
					'type'      => 'switch',
					'title'     => __('Above Related Posts (Single Post)', 'redux-framework-demo'),
					'default'   => 0,
		);
	$controls[] =  array(
		'id'       => 'ampforwp-advertisement-type-standard-6',
		'type'     => 'select',
		'title'    => __( 'Advertisement Type', 'redux-framework-demo' ),
		'subtitle' => __( 'Select Advertisement Type you want to show.', 'ampforwp' ),
		'options'  	=> array(
			'1' 	=> 'Adsense',
			'2'  	=> 'DoubleClick',
			'3'  	=> 'Custom Advertisement',
		),
		'default'  => '1',
		'required'	=> array('ampforwp-standard-ads-6','=','1'),
	);
	// '1' 	=> 'Adsense',
		$controls[] =  array(
			'id'       		=> 'ampforwp-adsense-ad-width-standard-6',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'placeholder'	=> '300',
			'required'		=> array(
													array('ampforwp-standard-ads-6','=','1'),
													array('ampforwp-advertisement-type-standard-6','=','1'),
		));
		$controls[] =  array(
			'id'       		=> 'ampforwp-adsense-ad-height-standard-6',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
			'placeholder'	=> '300',
			'required'		=> array(
													array('ampforwp-standard-ads-6','=','1'),
													array('ampforwp-advertisement-type-standard-6','=','1'))
		);
		$controls[] =  array(
			'id'        =>'ampforwp-adsense-ad-data-ad-client-standard-6',
			'type'      => 'text',
			'title'     => __('Data AD Client'),
			'desc'      => __('Enter the Data Ad Client (data-ad-client) from the adsense ad code. e.g. ca-pub-2005XXXXXXXXX342', 'redux-framework-demo'),
			'default'   => '',
			'placeholder'=> 'ca-pub-2005XXXXXXXXX342',
			'required'		=> array(
													array('ampforwp-standard-ads-6','=','1'),
													array('ampforwp-advertisement-type-standard-6','=','1')),
		);
		$controls[] =  array(
			'id'        => 'ampforwp-adsense-ad-data-ad-slot-standard-6',
			'type'      => 'text',
			'title'     => __('Data AD Slot'),
			'desc'      => __('Enter the Data Ad Slot (data-ad-slot) from the adsense ad code. e.g. 70XXXXXX12', 'redux-framework-demo'),
			'default'  => '',
			'placeholder'=> '70XXXXXX12',
			'required'		=> array(
													array('ampforwp-standard-ads-6','=','1'),
													array('ampforwp-advertisement-type-standard-6','=','1')),
		);
	// '2'  	=> 'DoubleClick',
		$controls[] = 	array(
			'id'        =>'ampforwp-doubleclick-ad-width-standard-6',
			'type'      => 'text',
			'title'     => __('Width'),
			'placeholder'=> '300',
			'required'		=> array(
													array('ampforwp-standard-ads-6','=','1'),
													array('ampforwp-advertisement-type-standard-6','=','2')),
		);
		$controls[] = 	array(
			'id'        =>'ampforwp-doubleclick-ad-height-standard-6',
			'type'      => 'text',
			'title'     => __('Height'),
			'placeholder'=> '250',
			'required'		=> array(
													array('ampforwp-standard-ads-6','=','1'),
													array('ampforwp-advertisement-type-standard-6','=','2')),
		);
		$controls[] = 	array(
			'id'        => 'ampforwp-doubleclick-ad-data-slot-standard-6',
			'type'      => 'text',
			'title'     => __('Data Slot'),
			'placeholder'=> '/41****9/mobile_ad_banner',
			'required'		=> array(
													array('ampforwp-standard-ads-6','=','1'),
													array('ampforwp-advertisement-type-standard-6','=','2')),
		);
	// '3'  	=> 'Custom Advertisement'
		$controls[] = array(
			'id'        	=> 'ampforwp-custom-advertisement-standard-6',
			'type'      	=> 'textarea',
			'title'     	=> __('Advertisement Code'),
			'placeholder'	=> ' <amp-ad width=300 height=250 type="XXXX" data-xxx="XXXX""></amp-ad>
OR
<a href="https://example.com"><amp-img src="/img/amp.jpg" width=300 height=250 alt="advert banner"></amp-img> </a>',
'required'		=> array(
										array('ampforwp-standard-ads-6','=','1'),
										array('ampforwp-advertisement-type-standard-6','=','3')),
		);
		//standard ad -6 ends here


	//Standard Ads ENDS




          // #Sticky AD
        $controls[] = 	array(
									'id'        =>'ampforwp-sticky-ad',
									'type'      => 'switch',
									'title'     => __('Sticky Ads', 'redux-framework-demo'),
									'subtitle' 	=> __('Shows Ad as sticky ad at the bottom of the screen.','redux-framework-demo'),
									'default'   => 0,
									'true'      => 'Enabled',
									'false'     => 'Disabled',
								);
                $controls[] =  array(
                  'id'       => 'ampforwp-advertisement-sticky-type',
                  'type'     => 'select',
                  'title'    => __( 'Advertisement Type', 'redux-framework-demo' ),
                  'subtitle' => __( 'Select Advertisement Type you want to show.', 'ampforwp' ),
                  'options'  	=> array(
                    '1' 	=> 'Adsense',
                    '2'  	=> 'DoubleClick',
                    '3'  	=> 'Custom Advertisement',
                  ),
                  'default'  => '1',
                  'required'	=> array('ampforwp-sticky-ad','=','1'),
                );
              $controls[] = 	array(
									'id'        =>'ampforwp-sticky-ad-width',
									'type'      => 'text',
									'title'     => __('Width', 'redux-framework-demo'),
									'default'   => '300',
									'placeholder'=> '300',
									'required'	=> array(
                             array('ampforwp-sticky-ad','=','1'),
									           array('ampforwp-advertisement-sticky-type','=','1'))
								);
              $controls[] = 	array(
									'id'        =>'ampforwp-sticky-ad-height',
									'type'      => 'text',
									'title'     => __('Height', 'redux-framework-demo'),
									'default'   => '50',
									'placeholder'=> '50',
                  'required'	=> array(
                             array('ampforwp-sticky-ad','=','1'),
									           array('ampforwp-advertisement-sticky-type','=','1'))
								);
              $controls[] = array(
									'id'        =>'ampforwp-sticky-ad-data-ad-client',
									'type'      => 'text',
									'title'     => __('Data AD Client', 'redux-framework-demo'),
									'desc'      => __('Enter the Data Ad Client (data-ad-client) from the adsense ad code. e.g. ca-pub-2005XXXXXXXXX342', 'redux-framework-demo'),
									'default'   => '',
									'placeholder'=> 'ca-pub-2005XXXXXXXXX342',
                  'required'	=> array(
                             array('ampforwp-sticky-ad','=','1'),
									           array('ampforwp-advertisement-sticky-type','=','1'))
								);
              $controls[] = array(
									'id'        => 'ampforwp-sticky-ad-data-ad-slot',
									'type'      => 'text',
									'title'     => __('Data AD Slot', 'redux-framework-demo'),
									'desc'      => __('Enter the Data Ad Slot (data-ad-slot) from the adsense ad code. e.g. 70XXXXXX12', 'redux-framework-demo'),
									'default'  => '',
									'placeholder'=> '70XXXXXX12',
                  'required'	=> array(
                             array('ampforwp-sticky-ad','=','1'),
									           array('ampforwp-advertisement-sticky-type','=','1'))
								);
                // '2'  	=> 'DoubleClick',
              		$controls[] = 	array(
              			'id'        =>'sticky-ampforwp-doubleclick-ad-width',
              			'type'      => 'text',
              			'title'     => __('Width'),
              			'placeholder'=> '300',
                    'required'	=> array(
                               array('ampforwp-sticky-ad','=','1'),
  									           array('ampforwp-advertisement-sticky-type','=','2'))
              		);
              		$controls[] = 	array(
              			'id'        =>'sticky-ampforwp-doubleclick-ad-height',
              			'type'      => 'text',
              			'title'     => __('Height'),
              			'placeholder'=> '250',
                    'required'	=> array(
                               array('ampforwp-sticky-ad','=','1'),
  									           array('ampforwp-advertisement-sticky-type','=','2'))
              		);
              		$controls[] = 	array(
              			'id'        => 'sticky-ampforwp-doubleclick-ad-data-slot',
              			'type'      => 'text',
              			'title'     => __('Data Slot'),
              			'placeholder'=> '/41****9/mobile_ad_banner',
                    'required'	=> array(
                               array('ampforwp-sticky-ad','=','1'),
  									           array('ampforwp-advertisement-sticky-type','=','2'))
              		);
              	// '3'  	=> 'Custom Advertisement',
              		$controls[] = array(
              			'id'        	=> 'sticky-ampforwp-custom-advertisement',
              			'type'      	=> 'textarea',
              			'title'     	=> __('Advertisement Code'),
              			'desc'     	=> __('Please have a look at the Example and then form your own Sticky-Ad code '),
              			'placeholder'	=> ' <amp-ad width="320"
      height="50"
      type="doubleclick"
      data-slot="/XXXXXX/amptesting/formats/sticky">
   </amp-ad>',
              'required'	=> array(
                         array('ampforwp-sticky-ad','=','1'),
                         array('ampforwp-advertisement-sticky-type','=','3'))
              		);

	return $controls;
}

if ( ! function_exists( 'ampforwp_ads_section' ) ) {
	function ampforwp_ads_section($sections){

	    $sections[] = array(
	        'title' => __('Advanced AMP ADS', 'redux-framework-demo'),
	        'icon' => 'el el-th-large',
					'desc'  => 'Create Ads either Custom or Adsense in both AD #1 and AD #2 ',
	        'fields' =>  ampforwp_create_controls(),
	        );
	    return $sections;
	}
}

add_filter("redux/options/redux_builder_amp/sections", 'ampforwp_ads_section');
?>
