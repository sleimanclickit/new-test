	<style>
		.post-content img {
			opacity: 0;
		}
	</style>
	<script>
		$(document).ready(() => {
			$('.post-content img').delay(2000).animate({opacity: 1}, 700);
		});
	</script>
<div class="loop-single">
	<?php
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();
	?>
				<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>>
					<h1 class="post-title"><?php the_title(); ?></h1><!-- post title - end -->
                    
					<?php // PREBID JS START ?>
					<div id="top_banner_mobile"></div>
					<!-- removed on 14.08.2017
					<div id='div_top_banner_mobile' style="text-align:center;" >
						<script type='text/javascript'>
							if( isMobile() ) {
								googletag.cmd.push(function() { googletag.display('div_top_banner_mobile'); });
							} else {
								$('#div_top_banner_mobile').remove();
							}
						</script>
					</div>
					-->
					<?php // PREBID JS END ?>
							
							
					<?php /* ADS -- AFTER TITLE - start */ ?>					
					<!--
					<div id="ads-after-title">
					</div><!-- ads after title - end -->					
                    <?php /* ADS -- AFTER TITLE - end */ ?>
					
					<ul id="social-buttons">
						<li class="count"><div class="counter"></div><span class="shares">shares</span></li>
						<li><a class="social tw" href="javascript:void(0);" onclick="window.open('https://twitter.com/intent/tweet?text=' + encodeURIComponent(document.title) + '&amp;url=' + encodeURIComponent(document.location.href), 'sharer', 'top=200, left=200, toolbar=0, status=0, width=520, height=350'); return false;" target="_black"><span class="hidden-xxs">Twitter</span></a></li>
						<li><a class="social fb" href="javascript:void(0);" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(document.location.href), 'sharer', 'top=200, left=200, toolbar=0, status=0, width=520, height=350'); return false;" target="_blank"><span class="hidden-xxs">Facebook</span></a></li>
					</ul><!-- social button - end -->

					<div class="post-content">
						<div id="content" itemprop="articleBody">
							<?php the_content(); ?>
						</div>
						
						<?php /* ADS -- BEFORE PAGINATION - start */ ?>
						<div id="ads-before-pagination" class="text-center">						
							
							<?php if( !isLastPageOnMultipagePost() ) { ?>						
							<?php // Adsense START ?>
							<div class="adsense" id="adsense_before_pag" >
								<div class="adsense_header_title">Advertisement</div>
								<script>
									(function(){
										// original 
										var adsense_client = "ca-pub-6449643190876495";
										var adsense_classes = 'adsbygoogle';
										var adsense_slot = "";
										var adsense_size = [];

										if ( winWid >= 768 ) {
											adsense_classes += ' adsense_bottom_desktop';
											adsense_slot = "9013822165"; /* buzzdrives.com - before pagination */
											adsense_size = ["100%", "90px"];
										} else {
											adsense_classes += ' adsense_bottom_mobile';
											adsense_slot = "1490555366"; /* buzzdrives.com - before pagination - m */
											adsense_size = ["300px", "250px"];
										}

										document.write (
										'<ins class="' + adsense_classes + '" style="display:inline-block;width:' 
											+ adsense_size[0] + ';height:' 
											+ adsense_size[1] + '" data-ad-client="' 
											+ adsense_client + '" data-ad-slot="' 
											+ adsense_slot + '"></ins>'
										);
									})();
								</script>
							</div>
							<?php // Adsense END ?>							
							<?php } ?>
						
						</div><!-- ads before pagination - end -->
						<?php /* ADS -- BEFORE PAGINATION - end */ ?>
						
						<?php do_action('single_pagination'); ?>
						
						<?php /* ADS -- AFTER PAGINATION - start */ ?>
						<div id="ads-after-pagination" class="text-center">
							
							<?php // PREBID JS START ?>							
							<div id='content_widget_mobile'></div>
							<?php // PREBID JS END ?>
							
						</div><!-- ads after pagination - end -->
						<?php /* ADS -- AFTER PAGINATION - end */ ?>
						
					</div><!-- post content - end -->
				</article><!-- post article - end -->
	<?php
			} // end while
		} else {
	?>
		<p>No content found.</p>
	<?php
		}
	?>
	<?php edit_post_link('edit', '<p>', '</p>'); ?>
</div><!-- main loop of the post - end -->
