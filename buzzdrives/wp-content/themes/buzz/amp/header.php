<!DOCTYPE html>
<html amp>
	<head>		
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<title>Buzzdrives.com | 50 Cars that are Plummeting in Value</title>
		<meta charset="utf-8">
		<meta name="description" content="Fill your tank with our satisfying bites of trending car articles and news on latest models and moves from within the industry.">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta property="og:url" content="https://buzzdrives.com/50-cars-that-are-plummeting-in-value-22/" />
		<meta property="og:type" content="website" />
		<meta property="og:title" content="50 Cars that are Plummeting in Value | Buzzdrives.com" />
		<meta property="og:description" content="50 Cars that are Plummeting in Value" />
		<meta property="og:image" content="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0]; ?>" />
		<meta name="google-site-verification" content="vuobpu9ZEcVJfqbyAG8iqiFZM3WtGjdwz44QmIVQLCM" />
		<meta name="google-site-verification" content="GICwiYwnx4mHDw4kZdilY2wHe129B2hNjXwoR5d0I40" />
		<link rel="canonical" href="https://buzzdrives.com/50-cars-that-are-plummeting-in-value-22/" />
		<script async src="https://cdn.ampproject.org/v0.js"></script>
		
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-180x180.png">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/favicon-16x16.png" sizes="16x16">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/favicon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-36x36.png" sizes="36x36">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-48x48.png" sizes="48x48">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-72x72.png" sizes="72x72">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-144x144.png" sizes="144x144">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-192x192.png" sizes="192x192">
		<link rel="manifest" href="<?php echo get_img_folder(); ?>/favicons/manifest.json">
		<meta name="apple-mobile-web-app-title" content="BuzzDrives">
		<meta name="application-name" content="BuzzDrives">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?php echo get_img_folder(); ?>/favicons/ms-icon-70x70.png">
		<meta name="msapplication-TileImage" content="<?php echo get_img_folder(); ?>/favicons/ms-icon-144x144.png">
		<meta name="msapplication-TileImage" content="<?php echo get_img_folder(); ?>/favicons/ms-icon-150x150.png">
		<meta name="msapplication-TileImage" content="<?php echo get_img_folder(); ?>/favicons/ms-icon-310x310.png">
		<meta name="theme-color" content="#ffffff">
		
		<style amp-boilerplate>
			body{
				-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;
				-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;
				-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;
				animation:-amp-start 8s steps(1,end) 0s 1 normal both;
			}
			@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}
			@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}
			@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}
			@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}
			@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}
		</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
		
		<script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
		<script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
		<script async custom-element="amp-social-share" src="https://cdn.ampproject.org/v0/amp-social-share-0.1.js"></script>
		<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
		<script async custom-element="amp-ad" src="https://cdn.ampproject.org/v0/amp-ad-0.1.js"></script>
		<script async custom-element="amp-accordion" src="https://cdn.ampproject.org/v0/amp-accordion-0.1.js"></script>
		<script async custom-element="amp-sticky-ad" src="https://cdn.ampproject.org/v0/amp-sticky-ad-1.0.js"></script>
        <script async custom-element="amp-user-notification" src="https://cdn.ampproject.org/v0/amp-user-notification-0.1.js"></script>
        <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>


        <style amp-custom>
			@font-face {
				font-family: 'Glyphicons Halflings';
				font-style: normal;
				font-weight: normal;
				src: url(<?php echo get_template_directory_uri() . '/fonts/glyphicons-halflings-regular.eot' ?>);
				src: url(<?php echo get_template_directory_uri() . '/fonts/glyphicons-halflings-regular.eot#iefix' ?>) format('embedded-opentype'),
				url(<?php echo get_template_directory_uri() . '/fonts/glyphicons-halflings-regular.woff' ?>) format('woff'),
				url(<?php echo get_template_directory_uri() . '/fonts/glyphicons-halflings-regular.ttf' ?>) format('truetype'),
				url(<?php echo get_template_directory_uri() . '/fonts/glyphicons-halflings-regular.svg' ?>) format('svg');
			}
			@font-face {
				font-family: 'mashsb-font';
				font-weight: normal;
				font-style: normal;
				src: url(<?php echo get_template_directory_uri() . '/fonts/mashsb-font.eot' ?>);
				src: url(<?php echo get_template_directory_uri() . '/fonts/mashsb-font.eot#iefix' ?>) format('embedded-opentype'),
				url(<?php echo get_template_directory_uri() . '/fonts/mashsb-font.woff' ?>) format('woff'),
				url(<?php echo get_template_directory_uri() . '/fonts/mashsb-font.ttf' ?>) format('truetype'),
				url(<?php echo get_template_directory_uri() . '/fonts/mashsb-font.svg' ?>) format('svg');
			}
			.amp-sticky-ad-close-button {
				min-width: 0;
			}
			<?php readfile( get_template_directory_uri() . "/amp/amp-old-style.css"); ?>
		<style>
		
	</head>
	
	<body class="post-template-default single single-post postid-10618 single-format-standard" data-id="10618">
        <?php
            if(!isset($_COOKIE['_ga'])) {
        ?>
            <div id="cookie" class="cookie-container" [class]="visible ? '' : 'hide'">
                <amp-user-notification id="my-notification" class="cookie" layout="nodisplay">
                    <div class="cookie-text">
                        <p class="cc_message">
                            This website uses cookies to ensure you get the best experience on our website
                            <a class="cc_more_info" href="/privacy-policy/">More info</a>
                        </p>
                        <button on="tap:my-notification.dismiss,AMP.setState({visible: !visible})" class="cc_btn cc_btn_accept_all">Got it!</button>
                </div>
                </amp-user-notification>
            </div>
        <?php } ?>
		<amp-analytics type="googleanalytics" id="analytics1">
			<script type="application/json">
			{
				"vars": {
                        "account": "UA-64470501-1"
				},
				"triggers": {
					"trackPageview": {
						"on": "visible",
						"request": "pageview"
					}
				}
			}
			</script>
		</amp-analytics>
		<!-- GOOGLE ANALYTICS - end -->

		<!-- Facebook Pixel Code -->
		<amp-pixel src="https://www.facebook.com/tr?id=1861783217183966&ev=PageView&noscript=1"
        layout="nodisplay"></amp-pixel>
		<!-- End Facebook Pixel Code -->

		<amp-pixel src="https://s.yimg.com/wi/ytc.js" layout="nodisplay"></amp-pixel>

		<!-- Google Tag Manager -->
		<amp-analytics type="gtag" data-credentials="include">
		<script type="application/json">
		{
		  "vars" : {
			"gtag_id": "GTM-KMZ7DSR",
			"config" : {
			  "GTM-KMZ7DSR": { "groups": "default" }
			}
		  }
		}
		</script>
		</amp-analytics>
		<!-- End Google Tag Manager -->

		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KMZ7DSR"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->

		<div id="wrapper">
			<div id="main-gradient"></div>
            <?php /* Header for mobile and tablet - start */ ?>
			<header id="header" style="position:relative;">
				<div id="header-inner">				
					<div id="header-nav-wrapper">
						<div class="container">
							<button id="mobile-menu-btn" on="tap:prop-accordion.toggle(section=first)">Menu <span class="glyphicon glyphicon-menu-hamburger"></span></button>						
						</div>
					</div><!-- end - #header-nav-wrapper -->
						
					<div id="header-logo">
						<div class="container">
							<div id="header-logo-inner">
								<div id="header-logo-round">
									<amp-accordion disable-session-states id="prop-accordion" class="prop-accordion">
										<section id="first">
										<h2></h2>
										<div class="content">
											<?php wp_nav_menu(
												array(
													'theme_location' => 'primary-menu',
													'container' => '',
													'menu_id' => '',
													'menu_class' => 'clearfix',
												)
											); ?>
										</div>
										</section>
									</amp-accordion>
									<a href="<?php echo get_home_url(); ?>">
										<amp-img class="center-block img-responsive"
											src="<?php echo get_img_folder(); ?>/buzzdrives_logo.png"
											width="334"
											height="55"
											alt="BuzzDrives Logo"
										>
										</amp-img>
									</a>
								</div>
							</div>
							<?php if ( !is_404() ) { ?>
								<div id="header-ad-inner" class="add-tablet">
									<div id='div_top_banner_desktop'>				
										<?php /* MM / DFP - START */ ?>
											<amp-ad width=970 height=90
												type="doubleclick"
												data-slot="/207764977/BD_AMP_top_banner_tablet"
												data-multi-size="728x90">
											</amp-ad>
										<?php /* MM / DFP - END */ ?>
												
									</div>
								</div><!-- end - #header-ad-inner -->
							<?php } ?>
						</div>
					</div><!-- end - #header-logo -->
				</div><!-- end - #header-inner -->
			</header><!-- end - #header -->
            <?php /* Header for mobile and tablet - end */ ?>

            <?php /* Header for desktop - start */ ?>
            <header id="header-desktop" style="position:relative;">
                <div id="header-inner-desktop">
                    <div id="header-nav-wrapper-desktop">
                        <div class="container">
                            <nav id="nav-desktop">
                                <?php wp_nav_menu(
                                    array(
                                    'theme_location' => 'primary-menu',
                                    'container' => '',
                                    'menu_id' => '',
                                    'menu_class' => 'clearfix',
                                    )
                                ); ?>
                            </nav>
                            <div id="header-search-desktop" class="hidden-xs">
                            </div>

                        </div>
                    </div>

                    <div id="header-logo-desktop">
                        <div class="container">
                            <div id="header-logo-inner-desktop">
                                <div id="header-logo-round-desktop">
                                    <a href="<?php echo get_home_url(); ?><?php get_redesign_query_parameter(); ?>">
                                        <amp-img class="center-block img-responsive" src="<?php echo get_img_folder(); ?>/buzzdrives_logo.png" width="150" height="25" alt="BuzzDrives Logo"></amp-img>
                                    </a>
                                </div>
                            </div>

                            <?php /* Don't show ads on the 404 page  */ ?>
                            <?php if ( !is_404() ) { ?>
                                <div id="header-ad-inner">
                                    <div id='div_top_banner_desktop'>
                                        <?php /* MM / DFP - START */ ?>
                                            <amp-ad width=970 height=90
                                            type="doubleclick"
                                            data-slot="/207764977/BD_AMP_top_banner_tablet"
                                            data-multi-size="728x90">
                                            </amp-ad>
                                        <?php /* MM / DFP - END */ ?>
                                    </div>
                                </div>
                            <?php } ?>
                             <?php /* Don't show ads on the 404 page  */ ?>
                        </div>
                    </div>
                </div>
            </header>
            <?php /* Header for desktop - end */ ?>

			<div id="main">