<?php include("header.php"); ?>
<div id="single" class="extra_width extra_tablet">
	<div class="container <?php if( isCampaignTraffic() ) {echo 'left-sidebar';} ?>">
		<div id="ads-top-single-page" class="hidden-xs">
		</div>
		
		<div class="row <?php if( isCampaignTraffic() ) {echo 'left-sidebar';} ?>">
			<?php /* MM Left Sidebar */ ?>
				<?php if( isCampaignTraffic() ) { ?>
					<div id="left_sidebar" class="hidden-xs" >
						<div id="BD_top_left_sidebar_desktop">
							<amp-ad width=160 height=600
								type="doubleclick"
								data-slot="/207764977/BD_AMP_top_left_sidebar_tablet"
								data-multi-size="120x600">
							</amp-ad>
						</div>
					</div>
				<?php } ?>
			<?php /* MM Left Sidebar - end */ ?>
			
			<div id="content-with-aside">
				<?php include("loop-single.php"); ?>
			</div>
			
			<aside id="aside" class="mobile-below">
				<?php include("sidebar-ads.php"); ?>
			</aside>
		</div>
	</div>
</div><!-- single post template - end -->
<?php include("footer.php"); ?>