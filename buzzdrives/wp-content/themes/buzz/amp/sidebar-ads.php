<?php /*
	ADS -- SIDEBAR - start
*/ ?>

<div id="ads-aside" class="hidden-xs">

	<?php /* MM/DFP JS START */ ?>
	<div id="top_right_sidebar_desktop">
		<amp-ad width=336 height=280
			type="doubleclick"
			data-slot="/207764977/BD_AMP_top_right_sidebar_tablet"
			data-multi-size="300x250">
		</amp-ad>
	</div>
	<?php /* MM/DFP JS END */ ?>

	<?php /* Sticky Sidebar MM ad - start */ ?>
		<div class="sticky-ad">
			<amp-ad width=300 height=250
				type="doubleclick"
				data-slot="/207764977/BD_AMP_sticky_sidebar_tablet">
			</amp-ad>
		</div>
	<?php /* Sticky Sidebar MM ad - end */ ?>
	
</div>
<?php /*
	ADS -- SIDEBAR - end
*/ ?>