<div class="loop-single">
	<?php
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();
	?>
				<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>>
					<h1 class="post-title"><?php the_title(); ?></h1><!-- post title - end -->
                    
					<?php /* MM/DFP JS START */ ?>
					<div id="top-article-mobile">
						<amp-ad width=320 height=100
							type="doubleclick"
							data-slot="/207764977/BD_AMP_top_banner_mobile">
						</amp-ad>
					</div>					
					<?php /* MM/DFP JS END */ ?>
					
					<?php 
						$url = get_permalink();
						$title = get_the_title();
						$encoded_url = urlencode( $url );
					?>
					<ul id="social-buttons">
						<?php
							$count = 1;
							$countTitle = strlen($title);
							if( $countTitle > 0 ) {
								$count = floatval( $countTitle / 13.3242 );
							}
							while ($count > 9) {
								$count = $count / 1.42;
							}		
						?>
						<li class="count"><div class="counter"><?php echo number_format($count, 1); ?>k</div><span class="shares">shares</span></li>
						<li>
							<div class="as-twitter">
								<a href="https://twitter.com/share?text=<?php echo urlencode( $title ); ?>&amp;url=<?php echo $encoded_url; ?>" class="social tw" target="_blank" title="Tweet">
									<span class="hidden-xxs">Twitter</span>
								</a>
							</div>					
                        </li>
						<li>
							<div class="as-facebook">
								<a target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo $encoded_url; ?>" class="social fb" title="Share on Facebook">
									<span class="hidden-xxs">Facebook</span>
								</a>
							</div>
						</li>						
					</ul><!-- social button - end -->
					
					<div class="post-content">
						<div id="content" itemprop="articleBody">
							<?php the_content(); ?>
						</div>
						
						<?php /* ADS -- BEFORE PAGINATION - start */ ?>
						<div id="ads-before-pagination" class="text-center">					
							
							<?php if( !isLastPageOnMultipagePost() ) { ?>			
							
								<?php if( switchAdUnitBeforePaginationMM() ) { ?>			
									<?php // MM START ?>
										
									<?php // MM END  ?>
									
								<?php } else { ?>
							
									<?php // Adsense START ?>
                                    <div class="adsense" id="adsense_before_pag_amp_mobile" >
                                        <div class="adsense_header_title">Advertisement</div>
                                        <amp-ad
                                                width=300
                                                height=250
                                                type="adsense"
                                                data-ad-client="ca-pub-6449643190876495"
                                                data-ad-slot="1490555366"
                                        >
                                            <div overflow></div>
                                        </amp-ad>
                                    </div>
                                    <div class="adsense" id="adsense_before_pag_amp_desktop" >
                                        <div class="adsense_header_title">Advertisement</div>
                                        <amp-ad
                                                layout="fixed-height"
                                                height=90
                                                type="adsense"
                                                data-ad-client="ca-pub-6449643190876495"
                                                data-ad-slot="1490555366"
                                        >
                                            <div overflow></div>
                                        </amp-ad>
                                    </div>
									<?php // Adsense END ?>		
								<?php } ?>
							
							<?php } ?>
						
						</div><!-- ads before pagination - end -->
						<?php /* ADS -- BEFORE PAGINATION - end */ ?>
						
						<?php do_action('single_pagination'); ?>
						
					</div><!-- post content - end -->
				</article><!-- post article - end -->
	<?php
			} // end while
		} else {
	?>
		<p>No content found.</p>
	<?php
		}
	?>
	<?php edit_post_link('edit', '<p>', '</p>'); ?>
</div><!-- main loop of the post - end -->