			</div><!-- end - #main -->
			
			<footer id="footer">
				<div id="footer-inner">
					<div class="container">
						<div id="footer-logo-wrapper">
							<a href="<?php echo get_home_url(); ?>">
								<amp-img class="center-block img-responsive"
									src="<?php echo get_img_folder(); ?>/buzzdrives_logo_footer.png"
									alt="BuzzDrives Logo"
									width="208"
									height="23"
								>
								</amp-img>
							</a>
						</div><!-- end - #footer-logo-wrapper -->
						
						<div id="footer-menu">
							<?php wp_nav_menu(
								array(
									'theme_location' => 'footer-menu',
									'container' => '',
									'menu_id' => 'menu-footer',
									'menu_class' => 'list-inline clearfix text-center',
								)
							); ?>
						</div><!-- end - #footer-menu -->
					</div>
				</div><!-- end - #footer-inner -->
			</footer><!-- end - #footer -->
		</div><!-- end - #wrapper -->
						
		<?php /* MM sticky ads - start */ ?>
		<amp-sticky-ad layout="nodisplay">
			<amp-ad width=728 height=90
				type="doubleclick"
				data-slot="/207764977/BD_AMP_Sticky_Bottom_tablet">
			</amp-ad>
		</amp-sticky-ad>
		<?php /* MM sticky ads - end */ ?>
		
	</body>
</html>