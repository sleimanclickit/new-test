<?php
	global $postOld;
    $post_slug_old = $postOld->post_name;
	// if ( is_amp_endpoint() && $post_slug_old === '50-cars-that-are-plummeting-in-value-22' ){

	// 	include("amp/single-post-50-cars-that-are-plummeting-in-value-22.php");

		// include("amp/single-post-50-cars-that-are-plummeting-in-value-22.php");

	// } else {
		get_header(); ?>

		<div id="single" class="extra_width extra_tablet <?php echo ( hideNumbersOnPagination() ) ? 'no_numbers' : ''; ?> <?php echo ( makeButtonsYellow() ) ? 'buttons_yellow' : ''; ?>">
			<div class="container <?php if( isCampaignTraffic() ) {echo 'left-sidebar';} ?>">
				<div id="ads-top-single-page" class="hidden-xs">
					
				</div>
				
				<div class="row <?php if( isCampaignTraffic() ) {echo 'left-sidebar';} ?>">
					<?php /* MM Left Sidebar */ ?>
					<?php if( isCampaignTraffic() ) { ?>
						<div id="left_sidebar" class="hidden-xs" >
							<div id="BD_top_left_sidebar_desktop"></div>
						</div>
					<?php } ?>
					<?php /* MM Left Sidebar - end */ ?>
				
					<div id="content-with-aside" >
						<?php get_template_part('loop', 'single'); ?>
					</div><!-- end - main content -->
					
					<aside id="aside" class="mobile-below">
						<?php get_sidebar('ads'); ?>
					</aside><!-- end - #sidebar -->
				</div>
			</div>
		</div><!-- single post template - end -->

		<?php get_footer(); ?>

