<?php get_header(); ?>

<div id="single">
        <div class="container">
        <div id="ads-top-single-page" class="hidden-xs">
            
        </div>

                <div class="row">
                        <div id="content-with-aside" >
                                <?php get_template_part('loop', 'single-50-cars-that-are-plummeting-in-value-9'); ?>
                        </div><!-- end - main content -->

                        <aside id="aside" class="mobile-below">
                                <?php get_sidebar('ads'); ?>
                        </aside><!-- end - #sidebar -->
                </div>
        </div>
</div><!-- single post template - end -->

<?php get_footer(); ?>

