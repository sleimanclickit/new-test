/*
NB: The file includes all the js required for ads widgets to appear
*/

/* Helper functions - start */
function hideContentAd() {
	if(typeof isFirstpage != "function") return false;
	
	if(window.location.search.toLowerCase().indexOf('__noca') > -1 && isFirstpage()) {
		return true;
	} else {
		return false;
	}
}
function hideRevContent() {
	if(typeof isFirstpage != "function") return false;
	
	if(window.location.search.toLowerCase().indexOf('__norc') > -1 && isFirstpage()) {
		return true;
	} else {
		return false;
	}
}
function isTaboola() {
	if(window.location.search.toLowerCase().indexOf('=taboola') > -1) {
		return true;
	} else {
		return false;
	}
}
function isYahooGemini() {
	if(window.location.search.toLowerCase().indexOf('=yahoogemini') > -1) {
		return true;
	} else {
		return false;
	}
}
/* Helper functions - end */

/* ADSENSE - start */
(function() {
	// init every adsense banner
	for(var i = 0; i < document.getElementsByClassName('adsbygoogle').length; i++) {
		(adsbygoogle = window.adsbygoogle || []).push({});
	}
})();
/* ADSENSE - end */

/* AdBlocker check - start 
(function() {
    if( typeof loadAds !== 'undefined' ) {
		// no ad blocker
		if( winWid >= 768 ) {
           
        } else {
            
        }  
    } else {
		// adblocker activated 		
	}
})();
/*  AdBlocker check - end */

/* REVCONTENT - start */
(function() {
	
	// ASIDE - RevContent BuzzDrives Aside 
	if( document.getElementById("rcjsload_575ed4") != null ) {
        console.log('ASIDE - RevContent BuzzDrives Aside Taboola - rcjsload_575ed4');
		(function() {
			var referer="";try{if(referer=document.referrer,"undefined"==typeof referer)throw"undefined"}catch(exception){referer=document.location.href,(""==referer||"undefined"==typeof referer)&&(referer=document.URL)}referer=referer.substr(0,700);
			var rcel = document.createElement("script");
			rcel.id = 'rc_' + Math.floor(Math.random() * 1000);
			rcel.type = 'text/javascript';
			rcel.src = "http://trends.revcontent.com/serve.js.php?w=31498&t="+rcel.id+"&c="+(new Date()).getTime()+"&width="+(window.outerWidth || document.documentElement.clientWidth)+"&referer="+referer;
			rcel.async = true;
			var rcds = document.getElementById("rcjsload_575ed4"); rcds.appendChild(rcel);
		})();
	}
	
	// BELOW BUTTONS - BuzzDrives Below Article
    /*
	if( document.getElementById("rcjsload_93e271") != null ) {
		(function() {
			var rcel = document.createElement("script");
			rcel.id = 'rc_' + Math.floor(Math.random() * 1000);
			rcel.type = 'text/javascript';
			rcel.src = "http://trends.revcontent.com/serve.js.php?w=4372&t="+rcel.id+"&c="+(new Date()).getTime()+"&width="+(window.outerWidth || document.documentElement.clientWidth);
			rcel.async = true;
			var rcds = document.getElementById("rcjsload_93e271"); rcds.appendChild(rcel);
		})();
	}
    */
    
	/*
	// BELOW BUTTONS - BuzzDrives Below Article - AdBlock
	if( document.getElementById("rcjsload_d733b8") != null ) {
		(function() {
            function ad_block_test(e,o){if("undefined"!=typeof document.body){var t="0.1.2-dev",o=o?o:"sponsorText",n=document.createElement("DIV");n.id=o,n.style.position="absolute",n.style.left="-999px",n.appendChild(document.createTextNode("&nbsp;")),document.body.appendChild(n),setTimeout(function(){if(n){var o=0==n.clientHeight;try{}catch(d){console&&console.log&&console.log("ad-block-test error",d)}e(o,t),document.body.removeChild(n)}},175)}}
            ad_block_test(function(is_blocked){
            var widget_id = 4372;
            if (is_blocked === true) {
            widget_id = 39589;
            }
            var referer="";try{if(referer=document.referrer,"undefined"==typeof referer)throw"undefined"}catch(exception){referer=document.location.href,(""==referer||"undefined"==typeof referer)&&(referer=document.URL)}referer=referer.substr(0,700);
            var rcel = document.createElement("script");
            rcel.id = 'rc_' + Math.floor(Math.random() * 1000);
            rcel.type = 'text/javascript';
            rcel.src = "http://trends.revcontent.com/serve.js.php?w="+widget_id+"&t="+rcel.id+"&c="+(new Date()).getTime()+"&width="+(window.outerWidth || document.documentElement.clientWidth) +"&referer="+referer + '&is_blocked=' + is_blocked;
            rcel.async = true;
            var rcds = document.getElementById("rcjsload_d733b8"); rcds.appendChild(rcel);
            });
        })();
	}	
	*/
	
})();
/* REVCONTENT - end */
