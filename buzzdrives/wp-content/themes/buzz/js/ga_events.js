function record_ga_event(eventCategory, eventAction) {
	// console.log(eventCategory);
	ga('send', 'event', eventCategory, eventAction, document.URL);
}

function record_ga_events(device_type) {
	var dt = device_type;
	
	if( !(dt == 's' || dt == 't' || dt == 'd') ) {
		return;
	}
	
	/* revcontent - start */
	jQuery(document).on('click', '.revcontent_last_page .rc-cta', function() {
		record_ga_event(dt + '__revcontent_last_page', dt + '__click_on_revcontent_last_page');
	});
	jQuery(document).on('click', '.revcontent_below_buttons .rc-cta', function() {
		record_ga_event(dt + '__revcontent_below_buttons', dt + '__click_on_revcontent_below_buttons');
	});
	jQuery(document).on('click', '.revcontent_aside .rc-cta', function() {
		record_ga_event(dt + '__revcontent_aside', dt + '__click_on_revcontent_aside');
	});
		// total per device
		jQuery(document).on('click', '.rc-cta', function() {
			record_ga_event(dt + '__revcontent', dt + '__click_on_revcontent');
		});
	/* revcontent - end */
	
	/* content.ad - start */
	jQuery(document).on('click', '.content_ad_promoted_content .ac_container a', function() {
		record_ga_event(dt + '__content_ad_promoted_content', dt + '__click_on_content_ad_promoted_content');
		
		// custom event -- all banners except the one from the last page of the post
		record_ga_event(dt + '__content_ad_all_except_last_page', dt + '__click_on_content_ad_all_except_last_page');
	});
	jQuery(document).on('click', '.content_ad_trending_articles .ac_container a', function() {
		record_ga_event(dt + '__content_ad_trending_articles', dt + '__click_on_content_ad_trending_articles');
		
		// custom event -- all banners except the one from the last page of the post
		record_ga_event(dt + '__content_ad_all_except_last_page', dt + '__click_on_content_ad_all_except_last_page');
	});
	jQuery(document).on('click', '.content_ad_wbounce .ac_container a', function() {
		record_ga_event(dt + '__content_ad_wbounce', dt + '__click_on_content_ad_wbounce');

		// custom event -- all banners except the one from the last page of the post
		record_ga_event(dt + '__content_ad_all_except_last_page', dt + '__click_on_content_ad_all_except_last_page');
	});
	jQuery(document).on('click', '.content_ad_aside_one_col .ac_container a', function() {
		record_ga_event(dt + '__content_ad_aside', dt + '__click_on_content_ad_aside');
		
		// custom event -- all banners except the one from the last page of the post
		record_ga_event(dt + '__content_ad_all_except_last_page', dt + '__click_on_content_ad_all_except_last_page');
	});
	jQuery(document).on('click', '.content_ad_content_worth_viewing .ac_container a', function() {
		record_ga_event(dt + '__content_ad_content_worth_viewing', dt + '__click_on_content_ad_content_worth_viewing');
	});
	
		// total per device
		jQuery(document).on('click', '.ac_container a', function() {
			record_ga_event(dt + '__content_ad', dt + '__click_on_content_ad');
		});
	/* content.ad - end */
}

(function() {
	/* revcontent - start */
		// total for all devices
		jQuery(document).on('click', '.rc-cta', function() {
			record_ga_event('all__revcontent', 'all__click_on_revcontent');
		});
	/* revcontent - end */
	
	/* content.ad - start */
		// total for all devices
		jQuery(document).on('click', '.ac_container a', function() {
			record_ga_event('all__content_ad', 'all__click_on_content_ad');
		});
	/* content.ad - end */
})();

(function() {
	/* Cookie Consent - start */
	jQuery(document).on('click', '.cc_more_info', function() {
		record_ga_event('cookie_consent_privacy_policy','click_on_cookie_consent_privacy_policy');
	});
	jQuery(document).on('click', '.cc_btn_accept_all', function() {
		record_ga_event('cookie_consent_accept','click_on_cookie_consent_accept');
	});
	/* Cookie Consent - end */
})();

function js_detect() {
	if(typeof device != 'undefined') {
		if( device.mobile() ) {
			return 's';
		} else if( device.tablet() ) {
			return 't';
		} else if ( device.desktop() ) {
			return 'd';
		} else {
			return false;
		}
	} else {
		return false;
	}
}

(function() {
	var dt = false;
	var counter = 1;
	var attempts = 10;
	
	var call_js_detect = setInterval(function() {
		if(counter <= attempts) {
			dt = js_detect();
			
			if( dt ) {
				clearInterval( call_js_detect );
			}
		} else {
			record_ga_event('debug_js_detection', 'debug_js_detection_failed');
			clearInterval( call_js_detect );
			return false;
		}
		
		console.warn(counter + ': ' + dt);
		
		if( dt ) {
			record_ga_events( dt );
		}
		counter++;
	}, 1000);	
})();