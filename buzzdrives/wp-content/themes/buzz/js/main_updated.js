$(document).ready(function() {
    if(typeof loadAds === 'undefined') {
        $('body').addClass('blocked');
    }
    
	/* MOBILE MENU - start */
	if( $('#mobile-menu-btn').length > 0 ) {
		$(document).on('click', '#mobile-menu-btn', function(event) {
			// event.preventDefault ? event.preventDefault() : event.returnValue = false;
			
			if( $(this).hasClass('open') ) {
				$('#mobile-menu-btn').removeClass('open');
				$('#nav-mobile').removeClass('open').slideUp('normal');
			} else {
				$('#mobile-menu-btn').addClass('open');
				$('#nav-mobile').addClass('open').slideDown('normal');
			}
		});
		
		$(document).on('click', function() {
			if( !(event.originalEvent === undefined) ) { // check if the event is triggered by human not by a script
				if(!jQuery(event.target).closest('#nav-mobile').length) {
					$('#mobile-menu-btn').removeClass('open');
					$('#nav-mobile').removeClass('open').slideUp('normal');
				}
			}
		});
	} else {
		console.warn('BUZZ: There is not mobile button!');
	}
	/* MOBILE MENU - end */
	
	/* SEARCH FORM - start */
	if( $('#s').length > 0 ) {
		$(document).on('click', '#s', function(event) {
			if( $(this).parent().hasClass('show') ) {
				$(this).parent().removeClass('show');
			} else {
				$(this).parent().addClass('show');
			}
		});
		
		$(document).on('click', function(event) {
			if( !(event.originalEvent === undefined) ) { // check if the event is triggered by human not by a script
				if(!jQuery(event.target).closest('#searchform').length) { // check if the menu is NOT a child of the clicked element
					jQuery('#s').parent().removeClass('show');
				}
			}
		});
	} else {
		console.warn('BUZZ: There is not search form!');
	}
	/* SEARCH FORM - start */
	
	/* INFINITE SCROLL - start */
	// doc: https://github.com/infinite-scroll/infinite-scroll
    (function() {
        var $isWrapper = $('#is-wrapper');
        
        if( $isWrapper.length > 0 && jQuery().infinitescroll !== 'undefined' ) {
            var $loadMoreBtn = $('#is-load-more-btn');
			var hasLoadMoreBtn = $loadMoreBtn.length > 0 ? true : false;
            
            $isWrapper.infinitescroll({
                navSelector: '#is-nav',
                nextSelector : '#is-nav a',
                itemSelector : '.is-item',
                donetext: 'No more items to display.',
                loading: {
                    finishedMsg: "<em>No more items to display.</em>",
                    msgText: "<em>Loading the next set of posts...</em>",
                },
                extraScrollPx: 10,
                state: {
					isPaused: hasLoadMoreBtn,
				},
                errorCallback: function() {
					$loadMoreBtn.parent().remove();
				},
                maxPage: $loadMoreBtn.data('max_num_pages'),
            }, function(json, opts) {
                console.log('Current page: ' + opts.state.currPage);
                console.log('Total pages: ' + opts.maxPage);
                if( opts.state.currPage == opts.maxPage ) {
                    $loadMoreBtn.parent().html(opts.loading.finishedMsg);
                }
            });
            
            if( hasLoadMoreBtn ) {
				$loadMoreBtn.on('click', function(event) {
					event.preventDefault ? event.preventDefault() : event.returnValue = false;
					
					$isWrapper.infinitescroll('retrieve');
				});
			}
        }
    })();
	/* INFINITE SCROLL - end */
	
	/* COUNT SHARES - start */
	(function () {
		if( $('#social-buttons .counter').length > 0 ) {
			var c = 1;
			
			if( $('.post-title').length > 0 ) {
				var c = parseFloat( $('.post-title').text().length / 13.3242 );
			}
			
			while (c > 9) {
				c = c / 1.42;
			}
			
			$('#social-buttons .counter').text( c.toFixed(1) + 'k');
		}
	})();
	/* COUNT SHARES - end */
	
	/* RESPONSIVE YouTube iframe - start */
	(function() {
		var yt = $('.post-content iframe[src*="youtube.com"]');
		
		if(yt.length > 0) {
			var ratio = new Array();
			var isNumeric = function(obj) {
				return !jQuery.isArray( obj ) && (obj - parseFloat( obj ) + 1) >= 0;
			}
			
			// calc ratio
			yt.each(function() {
				var el = $(this);
				if( !isNumeric(el.attr('width')) ) return;
				if( !isNumeric(el.attr('height')) ) return;
				
				ratio.push(Math.abs( el.attr('height') / el.attr('width') ));
			});
			
			// setHeight
			var setHeight = function() {
				yt.each(function(i) {
					$(this).css('height', Math.ceil($(this).innerWidth() * ratio[i]) + 'px');
				});
			};
			
			// on load
			setHeight();
			
			// on resize - workaround to trigger the "end" of the resize event
			var resizeEnd;
			$(window).resize(function(){
				console.warn("resize");
				clearTimeout(resizeEnd);
				resizeEnd = setTimeout(function(){
					setHeight()
				},500);
			});
		}
	})();
	/* RESPONSIVE YouTube iframe - end */
	
	/* FORCE HIDE wBounce - start */
	(function() {
		wbounce = $('#wbounce-modal');
		if(wbounce.length == 1 && typeof isFirstpage == "function") {	
			if(window.location.search.indexOf('__noca') > -1 && isFirstpage()) {
				console.warn('wBounce: prevented');
				wbounce.addClass('force-hide hidden');
				
				// if the cookie is not set when the user arrives on the page, then make sure the cookie will not be set then he leaves it
				if( document.cookie.indexOf("wBounce") == -1 ) {
					console.warn('wBounce cookie: no set');
					$(window).bind('beforeunload', function(){
						document.cookie = 'wBounce=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
						console.warn('wBounce cookie: deleted');
					});
				}
			}
		}
	})();
	/* FORCE HIDE wBounce - end */
    
    /* NEXT ARROW - start */
	(function() {
		var cc = $('#custom_classes');
		var total_pages = cc.data('total_pages');
		
		if( cc.length > 0 ) {
			console.warn('Custom classes: ' + cc.attr('class'));
			console.warn('Total pages: ' + total_pages);
			// 6233 - http://buzzdrives.com/30-vehicles-you-are-not-allowed-to-own-in-united-states/
			if( cc.hasClass('cc_multipage') && cc.hasClass('cc_firstpage') && total_pages > 2 ) {
				var img = $('.post-content #content img:first');
				var next_btn = $('#csp_btns li:last-child > a');
				
				var show_next_button = window.location.search.toLowerCase().indexOf('hnpb=1') == -1;
				
				if( img.length > 0 && next_btn.length > 0 && show_next_button ) {
					img.wrap('<div id="next_arrow_wrapper"></div>');
					img.addClass('img-responsive');

					var next_btn_clone = next_btn.clone();
					next_btn_clone.attr('id', 'next_arrow');
					next_btn_clone.attr('title', 'Next page');
					next_btn_clone.text('');
					
					$('#next_arrow_wrapper').append(next_btn_clone);
				}
			}
		}
	})();
	/* NEXT ARROW - end */
    
    /* Load Cookie consent - start */
	(function() {
		// if the 'no_c' query param is fount, then don't show the cookie consent ( the 'no_c' query param is set up in the Google Experiment dashboard and it marks the B test version )
		// removed the no_c condition as it's not required anymore : "&& window.location.search.toLowerCase().indexOf('no_c=1') == -1"
		if( document.cookie.indexOf('cookieconsent_dismissed') == -1 && typeof ajaxurl !== 'undefined' && window.location.search.toLowerCase().indexOf('no_c=1') == -1 ) {
			$.ajax({
				url: ajaxurl,
				async: true,
				type: "POST",
				dataType: "html",
				context: document.body,
				data: {
					action: "loadCookieConsentAction",
					security: ajaxnonce,
				},
				success: function(data) {
					$('head').append(data);
				},
			});
		} else {
			console.warn("Cookie consent already triggered!");
		}
	})();
	/* Load Cookie consent - end */
});

$(window).load(function() {
	
});