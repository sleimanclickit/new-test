<?php
/*
	Template name: Homepage
*/
?>

<?php // get_header(); ?>
<?php get_template_part('new_template/header'); ?>

<div id="home">
    <div class="container">
        <div class="homepage_container">
            <div class="left_homepage">
                <?php if( have_rows('slider_section', 'option') ) : ?>
                    <div class="owl-carousel-container">
                        <div class="owl-carousel-left-slide">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/new_template/images/arr.svg" width="20" height="30" alt="Previous Slide">
                        </div>
                        <div class="owl-carousel owl-theme">
                            <?php
                            while( have_rows('slider_section', 'option') ): the_row(); ?>
                                <?php
                                $slidePost = get_sub_field('post');
                                ?>
                                <div class="item">
                                    <div style="background-image:url(<?php echo get_the_post_thumbnail_url($slidePost->ID, 'full')?>)" class="post-image-container">
                                    </div>
                                    <div class="text-post">
                                        <?php
                                        $categories = get_the_category($slidePost->ID);
                                        ?>
                                        <div class="link-category">
                                            <a href="<?php echo get_category_link($categories[0]);?>"><?php echo $categories[0] ? $categories[0]->name : 'N/A'; ?></a>
                                        </div>
                                        <div class="link-title">
                                            <p class="aside-post-title"><?php echo get_the_title($slidePost->ID); ?></p>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="article-more">
                                            <a href="<?php echo get_the_permalink($slidePost->ID); ?>">Read more<div class="right_arrow"></div></a>
                                        </div>
                                    </div>
                                </div>
                            <?php endwhile;	?>
                        </div>
                        <div class="owl-carousel-right-slide">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/new_template/images/arr.svg" width="20" height="30" alt="Next Slide">
                        </div>
                    </div>
                <?php endif; ?>

                <?php include('includes/video-articles.php'); ?>

                <div id="content">
                    <?php get_template_part( 'new_template/section', 'all_articles' ); ?>
                </div>
                <?php include('includes/most-read-articles.php'); ?>
                <?php include('includes/miss-articles.php'); ?>
            </div>
            <aside id="aside">
                <a href="https://www.facebook.com/buzzdrives/" target="_blank">
                    <div class="facebook_widget">
                        <?php if( get_field('facebook_likes', 'option') ) { ?>
                            <span><?php the_field('facebook_likes', 'option'); ?> likes</span>
                        <?php } ?>
                    </div>
                </a>
                <?php get_sidebar('ads'); ?>
            </aside><!-- end - #sidebar -->
        </div>
    </div>
</div><!-- homepage template - end -->
	
<?php // get_footer(); ?>
<?php get_template_part('new_template/footer'); ?>