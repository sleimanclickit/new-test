<div id="all-articles">
	<div id="is-wrapper" class="articles-shadow">
        <h1 class="articles_title">Latest articles</h1>
		<?php
		// set the page no.
		if ( get_query_var('paged') ) {
			$paged = get_query_var('paged');
		} elseif ( get_query_var('page') ) {
			$paged = get_query_var('page');
		} else {
			$paged = 1;
		}
		
		// set posts_per_page
		$posts_per_page = 4;

		// set the offset
		//$offset = 3;
		
		//$page_offset = $offset + ($paged-1) * $posts_per_page;

		// WP_Query arguments
		$args = array (
			'pagination' => true,
			'post_type' => 'post',
			'posts_per_page' => $posts_per_page,
			'paged' => $paged,
			//'offset' => $page_offset,
			'order' => 'DESC',
			'orderby' => 'date',
			'tax_query' => array(
				array(
					'taxonomy'  => 'category',
					'field'     => 'slug',
					'terms'     => 'uncategorized',
					'operator'  => 'NOT IN'
				)
			),
			'meta_query' => array(
				array(
					'key'       => 'hidden_post',
					'compare'   => 'NOT EXISTS',
					'type'      => 'CHAR',
				),
			),
		);

		// The Query
		$all_articles = new WP_Query( $args );

		// The Loop
		if ( $all_articles->have_posts() ) {
            $i = 1;
            while ($all_articles->have_posts()) {
                $all_articles->the_post();

                if ($i == 4) { ?>
                    <div class="infinite-posts">
                        <hr>
                        <div class="is-item hp-article">
                            <div class="separator_full_post">
                                <div class="article-top big-post">
                                    <a href="<?php the_permalink(); ?><?php get_redesign_query_parameter(); ?>">
                                        <div class="article-image big-post">
                                            <div class="overlay-img">
                                                <div style="background-image:url(<?php echo get_the_post_thumbnail_url(); ?>)">
                                                </div>
                                                <div class="gradient"></div>
                                            </div>
                                        </div>
                                        <h2 class="article-title big-post"><a class="il" href="<?php the_permalink(); ?><?php get_redesign_query_parameter(); ?>"><?php the_title(); ?></a></h2>
                                    </a>
                                    <div class="article-cat link-category big-post">
                                        <?php $all_articles_category = get_the_category(get_the_ID()); ?>
                                        <a href="<?php echo get_category_link($all_articles_category[0]); ?><?php get_redesign_query_parameter(); ?>"><?php echo $all_articles_category[0] ? $all_articles_category[0]->name : 'N/A'; ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="container">
							<div id="header-ad-inner">
								<div id='div_top_banner_desktop'>


								</div>

							</div>
						</div>
                    </div>
                <?php } else {
                    ?>
                    <div class="infinite-posts <?=($i===7) ? 'ttt' : ''?>">
                        <div class="article_structure">
                            <div class="is-item hp-article col-xs-12 col-sm-7 <?php echo $i % 2 == 0 ? 'even' : 'odd'; ?>">
                                <div class="article-top same-height">
                                    <a href="<?php the_permalink(); ?><?php get_redesign_query_parameter(); ?>">
                                        <div class="article-image" style="background-image:url(<?php echo get_the_post_thumbnail_url(); ?>)">
                                        </div>
                                    </a>
                                    <div class="article-cat link-category">
                                        <?php $all_articles_category = get_the_category(get_the_ID()); ?>
                                        <a href="<?php echo get_category_link($all_articles_category[0]); ?><?php get_redesign_query_parameter(); ?>"><?php echo $all_articles_category[0] ? $all_articles_category[0]->name : 'N/A'; ?></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-5">
                                <div class="article-bottom same-height">
                                    <h2 class="article-title"><a class="il" href="<?php the_permalink(); ?><?php get_redesign_query_parameter(); ?>"><?php the_title(); ?></a></h2>
                                    <p><?php $content_post = get_the_content(); echo substr($content_post, 0, 100).'...'; ?></p>
                                    <div class="article-more">
                                        <a href="<?php the_permalink(); ?><?php get_redesign_query_parameter(); ?>">Read More<div class="right_arrow"></div></a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php $i++;
            }
			wp_reset_postdata();
        }
        ?>
    </div>


    <?php if ( $all_articles->have_posts() ) { ?>
        <div id="is-load-more">
            <?php if( $all_articles->max_num_pages > 1 ) { ?>
                <a href="/" id="is-load-more-btn" data-max_num_pages="<?php echo $all_articles->max_num_pages; ?>">Load More</a>
            <?php } else { ?>
                <em>No more items to display.</em>
            <?php } ?>
        </div>
        <div id="is-nav" class="hidden"><?php next_posts_link( 'Older Entries', $all_articles->max_num_pages ); ?></div>
    <?php } ?>

    <?php wp_reset_postdata(); ?>
</div><!-- end - #all-articles -->