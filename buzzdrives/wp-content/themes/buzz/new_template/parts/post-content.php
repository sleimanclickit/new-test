<div class="main-post">
    <a href="<?php echo get_the_permalink(); ?>">
        <div class="text-read">
            <?php if (has_post_thumbnail( $post->ID ) ): ?>
                <?php $videoPostImage = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                <div class="post_image_container" style="background-image: url('<?php echo $videoPostImage[0]; ?>')">
                </div>
            <?php endif; ?>
            <h2 class="aside-post-title"><?php echo get_the_title(); ?></h2>
        </div>
    </a>
</div>