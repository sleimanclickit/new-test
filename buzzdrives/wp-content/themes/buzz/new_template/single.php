<?php 
	global $post;
    $post_slug=$post->post_name;
	// if ( is_amp_endpoint() && $post_slug === '50-cars-that-are-plummeting-in-value-22'){
	// 	include("amp/single-post-50-cars-that-are-plummeting-in-value-22.php");
	// } else {
	get_template_part('new_template/header'); ?>

	<div id="single" class="extra_width extra_tablet <?php echo ( hideNumbersOnPagination() ) ? 'no_numbers' : ''; ?> <?php echo ( makeButtonsYellow() ) ? 'buttons_yellow' : ''; ?>">
		<div class="container <?php if( isCampaignTraffic() ) {echo 'left-sidebar';} ?>">
			<div id="ads-top-single-page" class="hidden-xs">
			
			</div>
			
			
			<?php /* Don't show ads on the 404 page  */ ?>
			<?php if ( !is_404() ) { ?>
																	
				<div id="header-ad-inner">
					<div id='div_top_banner_desktop'>				
						<?php /* MM / DFP - START  */ ?>
							<script>
								(function(){
									if( !isMobile() ) {
										document.write ('<div id="top_banner_desktop"></div>');
									}												
								}());
							</script>
						<?php /* MM / DFP - END */ ?>
							
					</div>
				</div><!-- end - #header-ad-inner -->
						
			<?php } ?>
			<?php /* Don't show ads on the 404 page  */ ?>
			
			
			
			<div class="row <?php if( isCampaignTraffic() ) {echo 'left-sidebar';} ?>">
			
				<?php /* MM Left Sidebar - start */ ?>
				<?php if( isCampaignTraffic() ) { ?>
					<div id="left_sidebar" class="hidden-xs" >
						<div id="BD_top_left_sidebar_desktop"></div>
					</div>
				<?php } ?>
				<?php /* MM Left Sidebar - end */ ?>
			
				<div id="content-with-aside" >
					<?php get_template_part('new_template/loop', 'single'); ?>
				</div><!-- end - main content -->
				
				<aside id="aside" class="mobile-below">
					<?php get_sidebar('ads'); ?>
				</aside><!-- end - #sidebar -->
			</div>
		</div>
	</div><!-- single post template - end -->
	<?php get_template_part('new_template/footer');
// } 

?>