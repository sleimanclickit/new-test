<?php /* Template Name: adstestpagenativo */ ?>
<!DOCTYPE html>
<html lang="en">
	<head>		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php bloginfo('name'); ?> | <?php IsHome() ? bloginfo('description') : wp_title(''); ?></title>
	
		<?php /* globals.js includes: GA code, global variables */ ?>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/globals.js"></script>
				
		<?php /* Favicons - start */ ?>
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_img_folder(); ?>/new_favicons/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_img_folder(); ?>/new_favicons/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_img_folder(); ?>/new_favicons/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_img_folder(); ?>/new_favicons/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_img_folder(); ?>/new_favicons/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_img_folder(); ?>/new_favicons/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_img_folder(); ?>/new_favicons/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_img_folder(); ?>/new_favicons/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_img_folder(); ?>/new_favicons/apple-icon-180x180.png">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/new_favicons/favicon-16x16.png" sizes="16x16">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/new_favicons/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/new_favicons/favicon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/new_favicons/android-icon-36x36.png" sizes="36x36">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/new_favicons/android-icon-48x48.png" sizes="48x48">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/new_favicons/android-icon-72x72.png" sizes="72x72">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/new_favicons/android-icon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/new_favicons/android-icon-144x144.png" sizes="144x144">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/new_favicons/android-icon-192x192.png" sizes="192x192">
		<link rel="manifest" href="<?php echo get_img_folder(); ?>/new_favicons/manifest.json">
		<meta name="apple-mobile-web-app-title" content="BuzzDrives">
		<meta name="application-name" content="BuzzDrives">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?php echo get_img_folder(); ?>/new_favicons/ms-icon-70x70.png">
		<meta name="msapplication-TileImage" content="<?php echo get_img_folder(); ?>/new_favicons/ms-icon-144x144.png">
		<meta name="msapplication-TileImage" content="<?php echo get_img_folder(); ?>/new_favicons/ms-icon-150x150.png">
		<meta name="msapplication-TileImage" content="<?php echo get_img_folder(); ?>/new_favicons/ms-icon-310x310.png">
		<meta name="theme-color" content="#ffffff">
		<?php /* Favicons - end */ ?>
			
		
		
		<?php //<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"> ?>
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" />
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/owl-carousel/owl.carousel.min.css" />
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/owl-carousel/owl.theme.default.min.css" />
			
		<!--<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />-->
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/new_template/style.css" />
		

		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		        
        <?php /* load a fake adframe.js file that will be blocked by adblocker. this way we can determine if user uses an adblocker extension */ ?>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/adframe.js"></script>
					
		<?php wp_head(); ?>
				
		<!-- NEW template -->
		
		<!-- Nativo JS Start -->
		<!--<script type="text/javascript" src="//s.ntv.io/serve/load.js" async></script>-->
		<script type="text/javascript" id="quantx-embed-tag" src="//cdn.elasticad.net/native/serve/js/quantx/nativeEmbed.gz.js"></script>
		<!-- Nativo JS End -->
		
		
	</head>
	
<body <?php body_class(); ?> data-id="<?php echo get_the_ID(); ?>">
			
		<?php /* add class to the body for desktops */ ?>
		<script type="text/javascript" >
			if( isDesktop() ) {
				$('body').addClass('is_desktop');
			}
			if( isMobile() ) {
				$('body').addClass('is_mobile');
			}
		</script>
		
		<?php do_action('after_body'); // it hooks any plugins that require an early load and it includes HTML. ex: FB SDK ?>
				
		
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KMZ7DSR"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		

			<header id="header" class="navbar navbar-default">
                <div class="mobile-overlay" style="display: none"></div>
				<div class="container">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="<?php echo get_home_url(); ?><?php get_redesign_query_parameter(); ?>">
                            <img class="img-responsive" src="<?php echo get_img_folder(); ?>/bd-logo.png" srcset="<?php echo get_img_folder(); ?>/bd-logo@2x.png 2x, <?php echo get_img_folder(); ?>/bd-logo@3x.png 3x" width="178" height="41" alt="BuzzDrives Logo Header">
                        </a>
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse-1">
                            <span class="sr-only"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
                        <?php
                        wp_nav_menu(
                            array(
                                'theme_location' => 'primary-menu',
                                'container' => '',
                                'menu_id' => '',
                                'menu_class' => 'nav navbar-nav',
                            )
                        );
                        ?>
                    </div>
				</div><!-- end - #header -->
			</header>

			
			<div id="main">
			
				<div id="single">
					<div class="container">
						<div id="ads-top-single-page" class="hidden-xs">
							
						</div>
						
						<div class="row">
							<div id="content-with-aside" >
								<div class="loop-single">
									<?php
										if ( have_posts() ) {
											while ( have_posts() ) {
												the_post();
									?>
												<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>>
													<h1 class="post-title"><?php the_title(); ?></h1><!-- post title - end -->
															
															
													<?php /* ADS -- AFTER TITLE - start */ ?>					
													<!--
													<div id="ads-after-title">
													</div><!-- ads after title - end -->					
													<?php /* ADS -- AFTER TITLE - end */ ?>
													

													<div class="post-content">
														<div id="content" itemprop="articleBody">
															<?php the_content(); ?>
														</div>
														
														<?php /* ADS -- BEFORE PAGINATION - start */ ?>
														<div id="ads-before-pagination" class="text-center">						
																				
															<?php // Adsense START ?>
															<div class="adsense" id="adsense_before_pag" >
																<div class="adsense_header_title">Advertisement</div>
																
															</div>
															<?php // Adsense END ?>		
														
														</div><!-- ads before pagination - end -->
														<?php /* ADS -- BEFORE PAGINATION - end */ ?>
														
														<?php // do_action('single_pagination'); ?>
														
														<?php /* ADS -- AFTER PAGINATION - start */ ?>
														<div id="ads-after-pagination" class="text-center">
															
															<!-- Placeholder for the nativo ad test -->
															<div id="native1"></div>
															
														</div><!-- ads after pagination - end -->
														<?php /* ADS -- AFTER PAGINATION - end */ ?>
														
													</div><!-- post content - end -->
												</article><!-- post article - end -->
									<?php
											} // end while
										} else {
									?>
										<p>No content found.</p>
									<?php
										}
									?>
									<?php edit_post_link('edit', '<p>', '</p>'); ?>
								</div><!-- main loop of the post - end -->

							</div><!-- end - main content -->
							
							<aside id="aside" class="mobile-below">
								<div id="ads-aside" class="hidden-xs">
													
								</div>
							</aside><!-- end - #sidebar -->
						</div>
					</div>
				</div><!-- single post template - end -->
				
			</div><!-- end - #main -->
	
			
			<footer id="footer">
				<div id="footer-inner">
					<div class="container">
                        <div class="footer-content">
                            <div id="footer-logo-wrapper">
                                <a href="<?php echo get_home_url(); ?><?php get_redesign_query_parameter(); ?>">
                                    <img class="img-responsive" src="<?php echo get_img_folder(); ?>/bd-logo.png" srcset="<?php echo get_img_folder(); ?>/bd-logo@2x.png 2x, <?php echo get_img_folder(); ?>/bd-logo@3x.png 3x" width="178" height="41" alt="BuzzDrives Logo Footer">
                                </a>
                            </div><!-- end - #footer-logo-wrapper -->

                            <div id="footer-menu">
                                <?php wp_nav_menu(
                                    array(
                                        'theme_location' => 'footer-menu',
                                        'container' => '',
                                        'menu_id' => 'menu-footer',
                                        'menu_class' => 'list-inline',
                                    )
                                ); ?>
                            </div><!-- end - #footer-menu -->
                            <div class="clearfix"></div>
                        </div>
					</div>
				</div><!-- end - #footer-inner -->
			</footer><!-- end - #footer -->
                		
		<?php wp_footer(); ?>

		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.infinitescroll.min.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/owl-carousel/owl.carousel.min.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.matchHeight-min.js"></script>
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
				
		
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/new_template/main.js"></script>
			
		
							
		
		
	</body>
</html>