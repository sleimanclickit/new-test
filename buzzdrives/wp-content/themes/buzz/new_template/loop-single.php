<div class="loop-single">
	<?php
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();
	?>
				<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>>
					<h1 class="post-title"><?php the_title(); ?></h1><!-- post title - end -->
					
                    <?php /* MM / DFP - START */ ?>
						<script>
							(function(){
								if( isMobile() ) {
									document.write ('<div id="top_banner_mobile"></div>');
								}												
							}());
						</script>					
					<?php /* MM / DFP - END */ ?>
					
							
					<?php /* ADS -- AFTER TITLE - start */ ?>
					<!--
					<div id="ads-after-title">
					</div><!-- ads after title - end -->					
                    <?php /* ADS -- AFTER TITLE - end */ ?>
					
					<ul id="social-buttons">
                        <li>
                            <a class="social-facebook" href="javascript:void(0);" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(document.location.href), 'sharer', 'top=200, left=200, toolbar=0, status=0, width=520, height=350'); return false;" target="_blank">
                                <img class="img-responsive" src="<?php echo get_img_folder(); ?>/button-facebook.png" srcset="<?php echo get_img_folder(); ?>/button-facebook@2x.png 2x, <?php echo get_img_folder(); ?>/button-facebook@3x.png 3x" width="30" height="29" alt="Share Facebook">
                            </a>
                        </li>
						<li>
                            <a class="social-twitter" href="javascript:void(0);" onclick="window.open('https://twitter.com/intent/tweet?text=' + encodeURIComponent(document.title) + '&amp;url=' + encodeURIComponent(document.location.href), 'sharer', 'top=200, left=200, toolbar=0, status=0, width=520, height=350'); return false;" target="_black">
                                <img class="img-responsive" src="<?php echo get_img_folder(); ?>/button-twitter.png" srcset="<?php echo get_img_folder(); ?>/button-twitter@2x.png 2x, <?php echo get_img_folder(); ?>/button-twitter@3x.png 3x" width="30" height="29" alt="Share Twitter">
                            </a>
                        </li>
                        <li class="count"><div class="counter"></div><span class="shares">shares</span></li>
					</ul><!-- social button - end -->
					
					
					<div class="post-content">
						<div id="content" itemprop="articleBody">
							<?php the_content(); ?>
						</div>
						
						<?php /* ADS -- BEFORE PAGINATION - start */ ?>
						<div id="ads-before-pagination" class="text-center">						
							
							<?php if( !isLastPageOnMultipagePost() ) { ?>			
							
								<?php if( switchAdUnitBeforePaginationMM() ) { ?>			
									<?php // MM START ?>
									<script>
									(function(){
										if( isMobile() ) {
											document.write ('<div id="BD_Leaderboard_above_pagination_Mobile"></div>');
										} else {
											document.write ('<div id="BD_Leaderboard_above_pagination_Desktop"></div>');
										}										
									}());
									</script>		
									<?php // MM END  ?>
								
								<?php } else if( isDummyABtest() ){ ?>
								
									<?php // Adsense START ?>
									<div class="adsense" id="adsense_before_pag" >
										<div class="adsense_header_title">Advertisement</div>
										<script>
											(function(){
												// original 
												var adsense_client = "ca-pub-6449643190876495";
												var adsense_classes = 'adsbygoogle';
												var adsense_slot = "";
												var adsense_size = [];

												if ( winWid >= 768 ) {
													adsense_classes += ' adsense_bottom_desktop';
													adsense_slot = "3112298940"; /* buzzdrives.com - before pagination - AB test: dmy */
													adsense_size = ["100%", "90px"];
												} else {
													adsense_classes += ' adsense_bottom_mobile';
													adsense_slot = "3850665549"; /* buzzdrives.com - before pagination - m - AB test: dmy */
													adsense_size = ["300px", "250px"];
												}

												document.write (
												'<ins class="' + adsense_classes + '" style="display:inline-block;width:' 
													+ adsense_size[0] + ';height:' 
													+ adsense_size[1] + '" data-ad-client="' 
													+ adsense_client + '" data-ad-slot="' 
													+ adsense_slot + '"></ins>'
												);
											})();
										</script>
									</div>
									<?php // Adsense END ?>		
								
								<?php } else if( makeButtonsYellow() ){ ?>
								
									<?php // Adsense START ?>
									<div class="adsense" id="adsense_before_pag" >
										<div class="adsense_header_title">Advertisement</div>
										<script>
											(function(){
												// original 
												var adsense_client = "ca-pub-6449643190876495";
												var adsense_classes = 'adsbygoogle';
												var adsense_slot = "";
												var adsense_size = [];

												if ( winWid >= 768 ) {
													adsense_classes += ' adsense_bottom_desktop';
													adsense_slot = "8747769002"; /* buzzdrives.com - before pagination - AB test: bpy */
													adsense_size = ["100%", "90px"];
												} else {
													adsense_classes += ' adsense_bottom_mobile';
													adsense_slot = "8688059256"; /* buzzdrives.com - before pagination - m - AB test: bpy */
													adsense_size = ["300px", "250px"];
												}

												document.write (
												'<ins class="' + adsense_classes + '" style="display:inline-block;width:' 
													+ adsense_size[0] + ';height:' 
													+ adsense_size[1] + '" data-ad-client="' 
													+ adsense_client + '" data-ad-slot="' 
													+ adsense_slot + '"></ins>'
												);
											})();
										</script>
									</div>
									<?php // Adsense END ?>		
								
								<?php } else if( hideNumbersOnPagination() ){ ?>
								
									<?php // Adsense START ?>
									<div class="adsense" id="adsense_before_pag" >
										<div class="adsense_header_title">Advertisement</div>
										<script>
											(function(){
												// original 
												var adsense_client = "ca-pub-6449643190876495";
												var adsense_classes = 'adsbygoogle';
												var adsense_slot = "";
												var adsense_size = [];

												if ( winWid >= 768 ) {
													adsense_classes += ' adsense_bottom_desktop';
													adsense_slot = "2916857823"; /* buzzdrives.com - before pagination - AB test: nnp */
													adsense_size = ["100%", "90px"];
												} else {
													adsense_classes += ' adsense_bottom_mobile';
													adsense_slot = "5348058973"; /* buzzdrives.com - before pagination - m - AB test: nnp */
													adsense_size = ["300px", "250px"];
												}

												document.write (
												'<ins class="' + adsense_classes + '" style="display:inline-block;width:' 
													+ adsense_size[0] + ';height:' 
													+ adsense_size[1] + '" data-ad-client="' 
													+ adsense_client + '" data-ad-slot="' 
													+ adsense_slot + '"></ins>'
												);
											})();
										</script>
									</div>
									<?php // Adsense END ?>										
								
								<?php } else if( addTwoAdsenseUnits() ){ ?>

                                    <?php // Adsense test - start ?>
                                    <div class="adsense" id="adsense_before_pag_test">
                                        <div class="ad_placeholder_left">
                                            <div class="adsense_header_title">Advertisement</div>
                                            <script>
                                                (function(){
													if( winWid >= 750 ) {
														// original
														var adsense_client = "ca-pub-6449643190876495";
														var adsense_classes = 'adsbygoogle';
														var adsense_slot = "";
														var adsense_size = [];

														adsense_classes += ' adsense_above_pagination';
														adsense_slot = "9566636127"; /* buzzdrives_300x250_above_pagination */
														adsense_size = ["300px", "250px"];

														document.write (
															'<ins class="' + adsense_classes + '" style="display:inline-block;width:'
															+ adsense_size[0] + ';height:'
															+ adsense_size[1] + '" data-ad-client="'
															+ adsense_client + '" data-ad-slot="'
															+ adsense_slot + '"></ins>'
														);
													}
                                                })();
                                            </script>
                                        </div>
                                        <div class="ad_placeholder_right">
                                            <div class="adsense_header_title">Advertisement</div>
                                            <script>
                                                (function(){
													if( winWid >= 750 ) {
														// original
														var adsense_client = "ca-pub-6449643190876495";
														var adsense_classes = 'adsbygoogle';
														var adsense_slot = "";
														var adsense_size = [];

														adsense_classes += ' adsense_above_pagination';
														adsense_slot = "7128175041"; /* buzzdrives_300x250_above_pagination_2ndunit */
														adsense_size = ["300px", "250px"];

														document.write (
															'<ins class="' + adsense_classes + '" style="display:inline-block;width:'
															+ adsense_size[0] + ';height:'
															+ adsense_size[1] + '" data-ad-client="'
															+ adsense_client + '" data-ad-slot="'
															+ adsense_slot + '"></ins>'
														);
													}
                                                })();
                                            </script>
                                        </div>
                                    </div>
                                    <div class="adsense" id="adsense_before_pag_test_single">
                                        <div class="adsense_header_title">Advertisement</div>
                                        <script>
                                            (function(){
												if( winWid < 750 ) {
													// original
													var adsense_client = "ca-pub-6449643190876495";
													var adsense_classes = 'adsbygoogle';
													var adsense_slot = "";
													var adsense_size = [];

													adsense_classes += ' adsense_bottom_mobile';
													adsense_slot = "1490555366"; /* buzzdrives.com - before pagination - m */
													adsense_size = ["300px", "250px"];

													document.write (
														'<ins class="' + adsense_classes + '" style="display:inline-block;width:'
														+ adsense_size[0] + ';height:'
														+ adsense_size[1] + '" data-ad-client="'
														+ adsense_client + '" data-ad-slot="'
														+ adsense_slot + '"></ins>'
													);
												}
                                            })();
                                        </script>
                                    </div>
                                    <?php // Adsense test - end ?>

                                <?php } else { ?>
								
									<?php if( switchHardcodedAdxWithDFPunit() ){ ?>
										<div class="adsense_header_title">Advertisement</div>
										<?php /* Show the DFP Adx units */ ?>										
										<!-- GPT AdSlot 1 for Ad unit 'BD_AdSense_Pagination_Desktop' ### Size: [[728,90]] -->
										<div id='div-gpt-ad-1542649-1'></div>
										<!-- GPT AdSlot 2 for Ad unit 'BD_AdSense_Pagination_Mobile' ### Size: [[300,250]] -->
										<div id='div-gpt-ad-1542649-2'></div>
										<script>											
											(function(){
												if( isMobile() ) {
													googletag.cmd.push(function() { googletag.display('div-gpt-ad-1542649-2'); });
													$('#div-gpt-ad-1542649-1').remove();
												} else {
													googletag.cmd.push(function() { googletag.display('div-gpt-ad-1542649-1'); });
													$('#div-gpt-ad-1542649-2').remove();
												}
											  }());	
										</script>
										
									<?php } elseif( switchHardcodedAdxWithDFPunit_control() ) { ?>
										<?php /* AB test Control ( New ) hardcoded adsense START */ ?>	
										<div class="adsense" id="adsense_before_pag" >
											<div class="adsense_header_title">Advertisement</div>
											<script>
												(function(){
													var adsense_client = "ca-pub-6449643190876495";
													var adsense_classes = 'adsbygoogle';
													var adsense_slot = "";
													var adsense_size = [];

													if ( winWid >= 768 ) {
														adsense_classes += ' adsense_bottom_desktop';
														adsense_slot = "6403512971"; // buzzdrives.com - before pagination - AB control: shadco
														adsense_size = ["100%", "90px"];
													} else {
														adsense_classes += ' adsense_bottom_mobile';
														adsense_slot = "3125655083"; // buzzdrives.com - before pagination - AB control: shadco - m
														adsense_size = ["300px", "250px"];
													}

													document.write (
													'<ins class="' + adsense_classes + '" style="display:inline-block;width:' 
														+ adsense_size[0] + ';height:' 
														+ adsense_size[1] + '" data-ad-client="' 
														+ adsense_client + '" data-ad-slot="' 
														+ adsense_slot + '"></ins>'
													);
												}());
											</script>
											
										</div><!-- adsense - end -->	
										
									<?php } elseif( oldVsNew() ) { ?>
                                        <?php /* New ad units for old vs new template */?>
                                        <div class="adsense" id="adsense_before_pag" >
											<div class="adsense_header_title">Advertisement</div>
											<script>
                                        (function(){
                                            var adsense_client = "ca-pub-6449643190876495";
                                            var adsense_classes = 'adsbygoogle';
                                            var adsense_slot = "";
                                            var adsense_size = [];

                                            if ( winWid >= 768 ) {
                                                adsense_classes += ' adsense_bottom_desktop';
                                                adsense_slot = "8778123747"; // buzzdrives.com - before pagination - AB control: shadco
                                                adsense_size = ["100%", "90px"];
                                            } else {
                                                adsense_classes += ' adsense_bottom_mobile';
                                                adsense_slot = "4305182803"; // buzzdrives.com - before pagination - AB control: shadco - m
                                                adsense_size = ["300px", "250px"];
                                            }

                                            document.write (
                                                '<ins class="' + adsense_classes + '" style="display:inline-block;width:'
                                                + adsense_size[0] + ';height:'
                                                + adsense_size[1] + '" data-ad-client="'
                                                + adsense_client + '" data-ad-slot="'
                                                + adsense_slot + '"></ins>'
                                            );
                                        }());
											</script>

										</div><!-- adsense - end -->
                                    <?php } elseif ( switchHarcodedAdsenseWithAdxAndHbs() ) { ?>
                                        <div class="adsense_header_title">Advertisement</div>
                                        <?php /* Show the AdX + HBS units */ ?>
                                        <div id='bd_adxhbs_above_pagination'></div>
                                        <div id='bd_adxhbs_above_pagination_mob'></div>
                                        <script>
                                            (function(){
                                                if( isMobile() ) {
                                                    googletag.cmd.push(function() { googletag.display('bd_adxhbs_above_pagination_mob'); });
                                                    $('#bd_adxhbs_above_pagination').remove();
                                                } else {
                                                    googletag.cmd.push(function() { googletag.display('bd_adxhbs_above_pagination'); });
                                                    $('#bd_adxhbs_above_pagination_mob').remove();
                                                }
                                            }());
                                        </script>
                                    <?php } else { ?>
										<?php /* Default setup - hardcoded adsense */ ?>
										<?php // Adsense START ?>
										<div class="adsense" id="adsense_before_pag" >
											<div class="adsense_header_title">Advertisement</div>
											<script>
												(function(){
													// original
													var adsense_client = "ca-pub-6449643190876495";
													var adsense_classes = 'adsbygoogle';
													var adsense_slot = "";
													var adsense_size = [];

													if ( winWid >= 768 ) {
														adsense_classes += ' adsense_bottom_desktop';
														adsense_slot = "9013822165"; /* buzzdrives.com - before pagination */
														adsense_size = ["100%", "90px"];
													} else {
														adsense_classes += ' adsense_bottom_mobile';
														adsense_slot = "1490555366"; /* buzzdrives.com - before pagination - m */
														adsense_size = ["300px", "250px"];
													}

													document.write (
													'<ins class="' + adsense_classes + '" style="display:inline-block;width:'
														+ adsense_size[0] + ';height:'
														+ adsense_size[1] + '" data-ad-client="'
														+ adsense_client + '" data-ad-slot="'
														+ adsense_slot + '"></ins>'
													);
												})();
											</script>
										</div>
										<?php // Adsense END ?>
									<?php } ?>
								<?php } ?>
							
							<?php } ?>
						
						</div><!-- ads before pagination - end -->
						<?php /* ADS -- BEFORE PAGINATION - end */ ?>
						
						<?php do_action('single_pagination'); ?>
						
						<?php /* ADS -- AFTER PAGINATION - start */ ?>
						<div id="ads-after-pagination" class="text-center">
						
							<?php /* Tan Media tags - start -removed on 26.03.2019 ?>
							<?php if( showTanMediaTags() ) { ?> 
								<script type="text/javascript" id="quantx-embed-tag" src="//cdn.elasticad.net/native/serve/js/quantx/nativeEmbed.gz.js"></script>
								<div id="quantx_generic_placement_34764"></div>
							<?php } ?> 
							<?php /* Tan Media tags - end */ ?>
							
							
							<?php /* TAN MEDIA DIV TAG START - removed on 22.11.2018 ?>
							<?php if(injectTanMediaFooterTag()) { ?>
								<script>
									(function(){
										if( isDesktop() ) {
											document.write ('<div id="tan_media_footer_tag"></div>');
										}												
									}());
								</script>
							<?php } ?>
							<?php /* TAN MEDIA DIV TAG END */ ?>

                            <?php /* Taboola Below Articles Widget - start */ ?>
                            <?php if( showTaboolaBelowArticlesTags() ) { ?>
                                <div id="taboola-below-article-thumbnails"></div>
                                <script type="text/javascript">
                                    window._taboola = window._taboola || [];
                                    _taboola.push({
                                        mode: 'thumbnails-pag',
                                        container: 'taboola-below-article-thumbnails',
                                        placement: 'Below Article Thumbnails',
                                        target_type: 'mix'
                                    });
                                </script>
                            <?php } ?>
                            <?php /* Taboola Below Articles Widget - start */ ?>
								
						</div><!-- ads after pagination - end -->
						<?php /* ADS -- AFTER PAGINATION - end */ ?>
						
					</div><!-- post content - end -->
				</article><!-- post article - end -->
	<?php
			} // end while
		} else {
	?>
		<p>No content found.</p>
	<?php
		}
	?>
	<?php edit_post_link('edit', '<p>', '</p>'); ?>
</div><!-- main loop of the post - end -->