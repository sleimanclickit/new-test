<div id="video-articles">
    <h1 class="page-title">Recent Videos</h1>
    <div class="video_wrapper">
        <?php
        // set the page no.
        if ( get_query_var('paged') ) {
            $paged = get_query_var('paged');
        } elseif ( get_query_var('page') ) {
            $paged = get_query_var('page');
        } else {
            $paged = 1;
        }

        // set posts_per_page
        $posts_per_page = 3;

        // set the offset
        $offset = 0;

        $page_offset = $offset + ($paged-1) * $posts_per_page;

        // WP_Query arguments
        $args = array (
            'pagination' => true,
            'post_type' => 'post',
            'posts_per_page' => $posts_per_page,
            'paged' => $paged,
            'offset' => $page_offset,
            'category_name' => 'videos',
            'order' => 'DESC',
            'orderby' => 'date',
            'tax_query' => array(
                // 'relation' => 'AND',
                array(
                    'taxonomy' => 'category',
                    'field'    => 'slug',
                    'terms'    => array( 'videos' ),
                ),
            ),
			'post__not_in' => array(11246),
        );

        // The Query
        $video_articles = new WP_Query( $args );

        // The Loop
        if ( $video_articles->have_posts() ) {
            $i = 1;
            ?>
            <div class="videos_container">
                <?php while ( $video_articles->have_posts() ) {
                    $video_articles->the_post();
                        get_template_part('new_template/parts/post-content');
                    $i++;
                } ?>
                <div class="clear pag-container">
                    <a href="<?php echo get_home_url(); ?>/category/videos/" class="more-videos-button">More videos</a>
                </div>
            </div>
        <?php } else {
            ?>
            <div class="col-xs-12">
                <br />
                <p>No posts found.</p>
                <br />
            </div>
            <?php
        } ?>
        <?php wp_reset_postdata(); ?>
    </div>

</div><!-- end - #all-articles -->