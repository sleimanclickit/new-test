<div id="miss_articles">
    <?php
    $args = array(
        'posts_per_page' => 5,
        'post_type' => 'post',
        'post_status' => 'publish',
        'orderby' => 'rand'
    );

    $missPost = new WP_Query($args);
    if($missPost->have_posts()):?>
        <div class="aside-title">
            <hr>
            <h1 class="articles_title">Don't Miss These</h1>
        </div>
        <div class="miss_container">
            <?php while($missPost->have_posts()) : $missPost->the_post(); ?>
                <div class="main-post">
                    <a href="<?php echo get_the_permalink(); ?>">
                        <div class="text-read">
                            <?php if (has_post_thumbnail( $post->ID ) ) { ?>
                                <?php $missPostImage = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                                <div class="post_image_container" style="background-image: url('<?php echo $missPostImage[0]; ?>')">
                                </div>
                            <?php } else { ?>
                                <div class="post_image_container no_image">
                                </div>
                            <?php } ?>
                            <?php
                            $miss_title = $post->post_title;
                            if( strlen($miss_title) > 40 && strlen($miss_title) <= 46 ) {
                                ?>
                                <h2 class="aside-post-title"><?php echo substr($miss_title, 0, 40).'...'; ?></h2>
                                <?php
                            } else if ( strlen($miss_title) >= 47 ) { ?>
                                <h2 class="aside-post-title"><?php echo substr($miss_title, 0, 35).'...'; ?></h2>
                            <?php } else { ?>
                                <h2 class="aside-post-title"><?php echo $miss_title; ?></h2>
                            <?php } ?>
                        </div>
                    </a>
                </div>
            <?php endwhile;?>
        </div>
    <?php endif; ?>
</div>