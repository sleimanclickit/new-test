<div id="most_read">
    <?php
    $args = array(
        'posts_per_page' => 3,
        'post_type' => 'post',
        'post_status' => 'publish',
        'orderby' => 'rand',
        'order' => 'DESC',
        'pagination' => false,
        'tax_query' => array(
            array(
                'taxonomy'  => 'category',
                'field'     => 'slug',
                'terms'     => 'uncategorized',
                'operator'  => 'NOT IN'
            )
        ),
        'meta_query' => array(
            array(
                'key'       => 'hidden_post',
                'compare'   => 'NOT EXISTS',
                'type'      => 'CHAR',
            ),
        ),
    );

    $mostReadPost = new WP_Query($args);
    if($mostReadPost->have_posts()):?>
        <div class="aside-title">
            <hr>
            <h1 class="articles_title">Most read</h1>
        </div>
        <div class="most_read_container">
            <?php while($mostReadPost->have_posts()) : $mostReadPost->the_post(); ?>
            <div class="main-post">
                <a href="<?php echo get_the_permalink(); ?>">
                    <div class="text-read">
                        <?php if (has_post_thumbnail( $post->ID ) ): ?>
                            <?php $readPostImage = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                            <div class="post_image_container" style="background-image: url('<?php echo $readPostImage[0]; ?>')">
                            </div>
                        <?php endif; ?>
                        <h2 class="aside-post-title"><?php echo get_the_title(); ?></h2>
                    </div>
                </a>
            </div>
            <?php endwhile;?>
        </div>
    <?php endif; ?>

</div>