 
			
			<footer id="footer">
				<div id="footer-inner">
					<div class="container">
                        <div class="footer-content">
                            <div id="footer-logo-wrapper">
                                <a href="<?php echo get_home_url(); ?>">
                                    <amp-img class="img-responsive" 
										src="<?php echo get_img_folder(); ?>/bd-logo.png"
										width="178"
										height="41"
										alt="BuzzDrives Logo Footer"
									>
									</amp-img>
                                </a>
                            </div><!-- end - #footer-logo-wrapper -->

                            <div id="footer-menu">
                                <ul id="menu-footer" class="list-inline">
									<li id="menu-item-9916" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-9916">
										<a href="mailto:advertise@reversemediagroup.com">Advertise</a>
									</li>
									<li id="menu-item-8406" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8406">
										<a href="https://buzzdrives.com/privacy-policy/">Privacy policy</a>
									</li>
									<li id="menu-item-10022" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10022">
										<a href="https://buzzdrives.com/contact-us/">Contact us</a>
									</li>
								</ul>
							</div><!-- end - #footer-menu -->
                            <div class="clearfix"></div>
                        </div>
					</div>
				</div><!-- end - #footer-inner -->
			</footer><!-- end - #footer -->		
		
	</body>
</html>