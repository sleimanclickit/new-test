<!DOCTYPE html>
<html amp>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<title>Buzzdrives</title>
		<meta charset="utf-8">
		<meta name="description" content="Fill your tank with our satisfying bites of trending car articles and news on latest models and moves from within the industry.">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta property="og:url" content="https://buzzdrives.com/50-cars-that-are-plummeting-in-value-22/" />
		<meta property="og:type" content="website" />
		<meta property="og:title" content="50 Cars that are Plummeting in Value | Buzzdrives.com" />
		<meta property="og:description" content="50 Cars that are Plummeting in Value" />
		<meta property="og:image" content="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0]; ?>" />
		<meta name="google-site-verification" content="vuobpu9ZEcVJfqbyAG8iqiFZM3WtGjdwz44QmIVQLCM" />
		<meta name="google-site-verification" content="GICwiYwnx4mHDw4kZdilY2wHe129B2hNjXwoR5d0I40" />
		<link rel="canonical" href="https://buzzdrives.com/50-cars-that-are-plummeting-in-value-22/" />
		<script async src="https://cdn.ampproject.org/v0.js"></script>				
		
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_img_folder(); ?>/new_favicons/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_img_folder(); ?>/new_favicons/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_img_folder(); ?>/new_favicons/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_img_folder(); ?>/new_favicons/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_img_folder(); ?>/new_favicons/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_img_folder(); ?>/new_favicons/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_img_folder(); ?>/new_favicons/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_img_folder(); ?>/new_favicons/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_img_folder(); ?>/new_favicons/apple-icon-180x180.png">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/new_favicons/favicon-16x16.png" sizes="16x16">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/new_favicons/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/new_favicons/favicon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/new_favicons/android-icon-36x36.png" sizes="36x36">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/new_favicons/android-icon-48x48.png" sizes="48x48">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/new_favicons/android-icon-72x72.png" sizes="72x72">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/new_favicons/android-icon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/new_favicons/android-icon-144x144.png" sizes="144x144">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/new_favicons/android-icon-192x192.png" sizes="192x192">
		<link rel="manifest" href="<?php echo get_img_folder(); ?>/new_favicons/manifest.json">
		<meta name="apple-mobile-web-app-title" content="BuzzDrives">
		<meta name="application-name" content="BuzzDrives">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?php echo get_img_folder(); ?>/new_favicons/ms-icon-70x70.png">
		<meta name="msapplication-TileImage" content="<?php echo get_img_folder(); ?>/new_favicons/ms-icon-144x144.png">
		<meta name="msapplication-TileImage" content="<?php echo get_img_folder(); ?>/new_favicons/ms-icon-150x150.png">
		<meta name="msapplication-TileImage" content="<?php echo get_img_folder(); ?>/new_favicons/ms-icon-310x310.png">
		<meta name="theme-color" content="#ffffff">
		
		<style amp-boilerplate>
			body{
				-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;
				-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;
				-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;
				animation:-amp-start 8s steps(1,end) 0s 1 normal both;
			}
			@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}
			@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}
			@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}
			@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}
			@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}
		</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
		
		<script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
		<script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
		<script async custom-element="amp-social-share" src="https://cdn.ampproject.org/v0/amp-social-share-0.1.js"></script>
		<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
		<script async custom-element="amp-ad" src="https://cdn.ampproject.org/v0/amp-ad-0.1.js"></script>
		
		<style amp-custom>
			@font-face {
				font-family: 'Glyphicons Halflings';
				font-style: normal;
				font-weight: normal;
				src: url(<?php echo get_template_directory_uri() . '/new_template/fonts/glyphicons-halflings-regular.eot' ?>);
				src: url(<?php echo get_template_directory_uri() . '/new_template/fonts/glyphicons-halflings-regular.eot#iefix' ?>) format('embedded-opentype'),
				url(<?php echo get_template_directory_uri() . '/new_template/fonts/glyphicons-halflings-regular.woff' ?>) format('woff'),
				url(<?php echo get_template_directory_uri() . '/new_template/fonts/glyphicons-halflings-regular.ttf' ?>) format('truetype'),
				url(<?php echo get_template_directory_uri() . '/new_template/fonts/glyphicons-halflings-regular.svg' ?>) format('svg');
			}
			<?php readfile( get_template_directory_uri() . "/new_template/amp/css/bootstrap.min.css"); ?>
			<?php readfile( get_template_directory_uri() . "/new_template/amp/amp-style.css"); ?>
			[class*="amphtml-sidebar-mask"] {
				top: 42px!important;
				overflow-y: hidden !important;
			}
		<style>	
		
	</head>
	
	<body class="post-template-default single single-post postid-10618 single-format-standard" data-id="10618">
        <?php
            if(!isset($_COOKIE['_ga'])) {
        ?>
            <div id="cookie" class="cookie-container" [class]="visible ? '' : 'hide'">
                <amp-user-notification id="my-notification" class="cookie" layout="nodisplay">
                    <div class="cookie-text">
                        <p class="cc_message">
                            This website uses cookies to ensure you get the best experience on our website
                            <a class="cc_more_info" href="/privacy-policy/">More info</a>
                        </p>
                        <button on="tap:my-notification.dismiss,AMP.setState({visible: !visible})" class="cc_btn cc_btn_accept_all">Got it!</button>
                    </div>
                </amp-user-notification>
            </div>
        <?php } ?>
		<!-- GOOGLE ANALYTICS - start -->
		<amp-analytics type="googleanalytics" id="analytics1">
			<script type="application/json">
			{
				"vars": {
                        "account": "UA-64470501-1"
				},
				"triggers": {
					"trackPageview": {
						"on": "visible",
						"request": "pageview"
					}
				}
			}
			</script>
		</amp-analytics>
		<!-- GOOGLE ANALYTICS - end -->

		<!-- Facebook Pixel Code -->
		<amp-pixel src="https://www.facebook.com/tr?id=1861783217183966&ev=PageView&noscript=1"
        layout="nodisplay"></amp-pixel>
		<!-- End Facebook Pixel Code -->
		
		<amp-pixel src="https://s.yimg.com/wi/ytc.js" layout="nodisplay"></amp-pixel>
		
		<!-- Google Tag Manager -->
		<amp-analytics type="gtag" data-credentials="include">
		<script type="application/json">
		{
		  "vars" : {
			"gtag_id": "GTM-KMZ7DSR",
			"config" : {
			  "GTM-KMZ7DSR": { "groups": "default" }
			}
		  }
		}
		</script>
		</amp-analytics>
		<!-- End Google Tag Manager -->				
		
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KMZ7DSR"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		

			<header id="header" class="navbar navbar-default">
				<div class="container">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="<?php echo get_home_url(); ?>">
                            <amp-img
								src="<?php echo get_img_folder(); ?>/bd-logo-n.png"
								width="178"
								height="23"
								alt="BuzzDrives Logo Header"
							>
							</amp-img>
                        </a>
						<div role="button" on="tap:sidebarmenu.toggle" tabindex="0" class="navbar-toggle">
							<span class="sr-only"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
						</div>
                    </div>
				</div><!-- end - #header -->
			</header>
			<amp-sidebar id="sidebarmenu" layout="nodisplay" side="right">
				<?php
					wp_nav_menu(
						array(
							'theme_location' => 'primary-menu',
							'container' => '',
							'menu_id' => '',
							'menu_class' => 'nav navbar-nav',
						)
					);
				?>
			</amp-sidebar>
