<?php include("header.php"); ?>
<div id="single" class="extra_width extra_tablet">
	<div class="container">
        <div id="ads-top-single-page" class="hidden-xs"></div>		
		
		<?php /* Don't show ads on the 404 page  */ ?>
		<?php if ( !is_404() ) { ?>
																
			<div id="header-ad-inner">
				<div id='div_top_banner_desktop'>				
					<amp-ad width=970 height=90
						type="doubleclick"
						data-slot="/207764977/BD_AMP_top_banner_tablet"
						data-multi-size="728x90">
					</amp-ad>		
				</div>
			</div><!-- end - #header-ad-inner -->
					
		<?php } ?>
		<?php /* Don't show ads on the 404 page  */ ?>
		
		
        
		<div class="row">
		
			<?php /* MM Left Sidebar - start */ ?>
			<?php if( isCampaignTraffic() ) { ?>
				<div id="left_sidebar" class="hidden-xs">
					<div id="BD_top_left_sidebar_desktop">
						<amp-ad width=160 height=600
							type="doubleclick"
							data-slot="/207764977/BD_AMP_top_left_sidebar_tablet"
							data-multi-size="120x600">
						</amp-ad>
					</div>
				</div>
			<?php } ?>
			<?php /* MM Left Sidebar - end */ ?>
		
			<div id="content-with-aside" >
				<?php include("loop-single.php"); ?>
			</div><!-- end - main content -->
			
			<aside id="aside" class="mobile-below">
				<?php include("sidebar-ads.php"); ?>
			</aside><!-- end - #sidebar -->
		</div>
	</div>
</div><!-- single post template - end -->

<?php include("footer.php"); ?>