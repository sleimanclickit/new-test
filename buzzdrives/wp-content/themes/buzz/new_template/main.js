/* Send a facebook custom event when a adsense ad is clicked - start */
/*!
 * jQuery iframe click tracking plugin
 *
 * @author Vincent Paré
 * @copyright © 2013-<%= grunt.template.today("yyyy") %> Vincent Paré
 * @license http://opensource.org/licenses/Apache-2.0
 * @version <%= pkg.version %>
 */
(function() {
	// Tracking handler manager
	$.fn.iframeTracker = function(handler) {
		// Building handler object from handler function
		if (typeof handler == "function") {
			handler = {
				blurCallback: handler
			};
		}

		var target = this.get();
		if (handler === null || handler === false) {
			$.iframeTracker.untrack(target);
		} else if (typeof handler == "object") {
			$.iframeTracker.track(target, handler);
		} else {
			throw new Error("Wrong handler type (must be an object, or null|false to untrack)");
		}
		return this;
	};

	// Iframe tracker common object
	$.iframeTracker = {
		// State
		focusRetriever: null,  // Element used for restoring focus on window (element)
		focusRetrieved: false, // Says if the focus was retrieved on the current page (bool)
		handlersList: [],      // Store a list of every trakers (created by calling $(selector).iframeTracker...)
		isIE8AndOlder: false,  // true for Internet Explorer 8 and older

		// Init (called once on document ready)
		init: function() {
			// Determine browser version (IE8-) ($.browser.msie is deprecated since jQuery 1.9)
			try {
				if ($.browser.msie === true && $.browser.version < 9) {
					this.isIE8AndOlder = true;
				}
			} catch (ex) {
				try {
					var matches = navigator.userAgent.match(/(msie) ([\w.]+)/i);
					if (matches[2] < 9) {
						this.isIE8AndOlder = true;
					}
				} catch (ex2) {}
			}

			// Listening window blur
			$(window).focus();
			$(window).blur(function(e) {
				$.iframeTracker.windowLoseFocus(e);
			});

			// Focus retriever (get the focus back to the page, on mouse move)
			$("body").append('<div style="position:fixed; top:0; left:0; overflow:hidden;"><input style="position:absolute; left:-300px;" type="text" value="" id="focus_retriever" readonly="true" /></div>');
			this.focusRetriever = $("#focus_retriever");
			this.focusRetrieved = false;
			$(document).mousemove(function(e) {
				if (document.activeElement && document.activeElement.tagName === "IFRAME") {
					$.iframeTracker.focusRetriever.focus();
					$.iframeTracker.focusRetrieved = true;
				}
			});

			// Special processing to make it work with my old friend IE8 (and older) ;)
			if (this.isIE8AndOlder) {
				// Blur doesn't works correctly on IE8-, so we need to trigger it manually
				this.focusRetriever.blur(function(e) {
					e.stopPropagation();
					e.preventDefault();
					$.iframeTracker.windowLoseFocus(e);
				});

				// Keep focus on window (fix bug IE8-, focusable elements)
				$("body").click(function(e) {
					$(window).focus();
				});
				$("form").click(function(e) {
					e.stopPropagation();
				});

				// Same thing for "post-DOMready" created forms (issue #6)
				try {
					$("body").on("click", "form", function(e) {
						e.stopPropagation();
					});
				} catch (ex) {
					console.log("[iframeTracker] Please update jQuery to 1.7 or newer. (exception: " + ex.message + ")");
				}
			}
			console.log('init');
		},

		// Add tracker to target using handler (bind boundary listener + register handler)
		// target: Array of target elements (native DOM elements)
		// handler: User handler object
		track: function(target, handler) {
			// Adding target elements references into handler
			handler.target = target;

			// Storing the new handler into handler list
			$.iframeTracker.handlersList.push(handler);

			// Binding boundary listener
			$(target)
				.bind("mouseover", { handler: handler }, $.iframeTracker.mouseoverListener)
				.bind("mouseout",  { handler: handler }, $.iframeTracker.mouseoutListener);
		},

		// Remove tracking on target elements
		// target: Array of target elements (native DOM elements)
		untrack: function(target) {
			if (typeof Array.prototype.filter != "function") {
				console.log("Your browser doesn't support Array filter, untrack disabled");
				return;
			}

			// Unbinding boundary listener
			$(target).each(function(index) {
				$(this)
					.unbind("mouseover", $.iframeTracker.mouseoverListener)
					.unbind("mouseout", $.iframeTracker.mouseoutListener);
			});

			// Handler garbage collector
			var nullFilter = function(value) {
				return value === null ? false : true;
			};
			for (var i in this.handlersList) {
				// Prune target
				for (var j in this.handlersList[i].target) {
					if ($.inArray(this.handlersList[i].target[j], target) !== -1) {
						this.handlersList[i].target[j] = null;
					}
				}
				this.handlersList[i].target = this.handlersList[i].target.filter(nullFilter);

				// Delete handler if unused
				if (this.handlersList[i].target.length === 0) {
					this.handlersList[i] = null;
				}
			}
			this.handlersList = this.handlersList.filter(nullFilter);
		},

		// Target mouseover event listener
		mouseoverListener: function(e) {
			e.data.handler.over = true;
			try {
				e.data.handler.overCallback(this, e);
			} catch (ex) {}
		},

		// Target mouseout event listener
		mouseoutListener: function(e) {
			e.data.handler.over = false;
			$.iframeTracker.focusRetriever.focus();
			try {
				e.data.handler.outCallback(this, e);
			} catch (ex) {}
		},

		// Calls blurCallback for every handler with over=true on window blur
		windowLoseFocus: function(e) {
			for (var i in this.handlersList) {
				if (this.handlersList[i].over === true) {
					try {
						this.handlersList[i].blurCallback(e);
					} catch (ex) {}
				}
			}
		}
	};

	// Init the iframeTracker on document ready
	$(document).ready(function() {
		$.iframeTracker.init();
	});
})();

$(window).load(function() {
	$('.adsense iframe').iframeTracker({
		blurCallback: function(event) {
			// Do something when iframe is clicked (like firing an XHR request)
			// You can know which iframe element is clicked via this._overId
			fbq('trackCustom', 'Adsense', {ad_id: this._overId});
			
			// Outbrain Adsense click tracking
			obApi('track', 'Adsense Click');
			
			// GA event tracking
			ga('send', 'event', 'Adsense Clicks', 'Adsense Click', 'Adsense Click');

		},
		overCallback: function(element, event) {
			// Saving the iframe wrapper id
			var adsense_id = $(element).parents('.adsense').attr('id');
			var adsense_parent_id = $(element).parents('.adsense').parent().attr('id');
			var adsense_child_id = $(element).parents('.adsense').children().first().attr('id');
			if(adsense_id){
				this._overId = adsense_id;
			} else if(adsense_parent_id){
				this._overId = adsense_parent_id;
			} else if(adsense_child_id){
				this._overId = adsense_child_id;
			} else {
				this._overId =  'unnamed ad section';
			}
		},
		outCallback: function(element, event) {
			// Reset hover iframe wrapper id
			this._overId = null;
		},
		_overId: null
	});
});
/* Send a facebook custom event when a adsense ad is clicked - end */


$(document).ready(function() {
	/* Slider Homepage - Owl-Carousel - start */
	var owl = $(".owl-carousel:not(.carousel-miss-articles)").owlCarousel({
		loop:true,
        margin:10,
        nav:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1200:{
                items:1
            }
        },
	});

    $('.owl-carousel-left-slide').click(function() {
        owl.trigger('prev.owl.carousel');
    });
    $('.owl-carousel-right-slide').click(function() {
        owl.trigger('next.owl.carousel');
    });
	/* Slider Homepage - Owl-Carousel - end */

    $('.carousel-left-slide').click(function() {
        missArticles.trigger('prev.owl.carousel');
    });
    $('.carousel-right-slide').click(function() {
        missArticles.trigger('next.owl.carousel');
    });
	/* Carousel Miss Articles - Owl-Carousel - end */
	
	/*Effective method to hide email from bots - start*/
    $('#contact-us-page .link-contact').on("click", function () {
        var dataName = $(this).attr('data-name');
        var dataDomain = $(this).attr('data-domain');
        var dataExtention = $(this).attr('data-extention');

        var myHref = dataName + '@' + dataDomain + '.' + dataExtention;

        window.location.href = 'mailto:' + myHref;
    });
	/*Effective method to hide email from bots - end*/

	/* Make all boxes to have same height - start */
    $('#all-articles .same-height').matchHeight({
		byRow: true
	});
	/* Make all boxes to have same height - end */

	/* Make same height at transition header - mobile - start */
    $('#bs-navbar-collapse-1').on('show.bs.collapse', function () {
        $("#menu-main-menu").css("height", "calc(100vh - 83px)");
        $("html").addClass("overflow-header");
        $(".mobile-overlay").show();

    });

    $('#bs-navbar-collapse-1').on('hide.bs.collapse', function () {
        $(".mobile-overlay").hide();
        $("html").removeClass("overflow-header");

    });

    var mouseOverActiveElement;
    $('#header .navbar-toggle, #header .navbar-collapse.collapse.in').live('mouseenter', function(){
        mouseOverActiveElement = true;
    }).live('mouseleave', function(){
        mouseOverActiveElement = false;
    });

    $("html").click(function(){
    	var navbar = $("#bs-navbar-collapse-1");
        if (!mouseOverActiveElement) {
        	navbar.collapse('hide');
            navbar.trigger('hide.bs.collapse');
            // $(".mobile-overlay").css("display", "none");
        } else {
            navbar.trigger('show.bs.collapse');
            navbar.collapse('show');
            // $(".mobile-overlay").css("display", "block");
        }
    });
	
	/* Add class on article if the left sidebar exist - start */
	if( $('#left_sidebar').length > 0) {
		$('#single.extra_width').addClass('left_ad');
	} else {
		$('#single.extra_width').removeClass('left_ad');
	}
	/* Add class on article if the left sidebar exist - end */

    var no_prev = $('#single.buttons_yellow .no_prev');
    if(no_prev.length !== 0) {
        $('#single.buttons_yellow .next_button').addClass('no_floating');
    } else {
        $('#single.buttons_yellow .next_button').removeClass('no_floating');
    }
});

$(document).ready(function() {
    if(typeof loadAds === 'undefined') {
        $('body').addClass('blocked');
    }
    
	/* MOBILE MENU - start */
	if( $('#mobile-menu-btn').length > 0 ) {
		$(document).on('click', '#mobile-menu-btn', function(event) {
			// event.preventDefault ? event.preventDefault() : event.returnValue = false;
			
			if( $(this).hasClass('open') ) {
				$('#mobile-menu-btn').removeClass('open');
				$('#nav-mobile').removeClass('open').slideUp('normal');
			} else {
				$('#mobile-menu-btn').addClass('open');
				$('#nav-mobile').addClass('open').slideDown('normal');
			}
		});
		
		$(document).on('click', function() {
			if( !(event.originalEvent === undefined) ) { // check if the event is triggered by human not by a script
				if(!jQuery(event.target).closest('#nav-mobile').length) {
					$('#mobile-menu-btn').removeClass('open');
					$('#nav-mobile').removeClass('open').slideUp('normal');
				}
			}
		});
	} else {
		console.warn('BUZZ: There is not mobile button!');
	}
	/* MOBILE MENU - end */
	
	/* SEARCH FORM - start */
    jQuery('#searchsubmit').on('click', function(){
        jQuery(this).closest('#header-search').toggleClass('search-transition');
        jQuery(this).siblings('#s-wrapper').toggleClass('container-transition');
    });
	/* SEARCH FORM - start */
	
	/* INFINITE SCROLL - start */
	// doc: https://github.com/infinite-scroll/infinite-scroll
    (function() {
        var $isWrapper = $('#is-wrapper');
        if( $isWrapper.length > 0 && jQuery().infinitescroll !== 'undefined') {
            var $loadMoreBtn = $('#is-load-more-btn');
			var hasLoadMoreBtn = $loadMoreBtn.length > 0 ? true : false;
            
            $isWrapper.infinitescroll({
                navSelector: '#is-nav',
                nextSelector : '#is-nav a',
                itemSelector : '.infinite-posts',
                donetext: 'No more items to display.',
                loading: {
                    finishedMsg: "<em>No more items to display.</em>",
                    msgText: "",
					img: stylesheet_directory_uri + '/new_template/images/smallLoading.gif'
                },
                extraScrollPx: 10,
                state: {
					isPaused: hasLoadMoreBtn,
				},
                errorCallback: function() {
					$loadMoreBtn.parent().remove();
				},
                maxPage: $loadMoreBtn.data('max_num_pages'),
            }, function(json, opts) {
                console.log('Current page: ' + opts.state.currPage);
                console.log('Total pages: ' + opts.maxPage);
                if( opts.state.currPage == opts.maxPage ) {
                    $loadMoreBtn.parent().html(opts.loading.finishedMsg);
                }
                setTimeout(function(){
                    $('#all-articles .same-height').matchHeight({remove: true});
				}, 100);

            });
            
            if( hasLoadMoreBtn ) {
				$loadMoreBtn.on('click', function(event) {
					event.preventDefault ? event.preventDefault() : event.returnValue = false;
					
					$isWrapper.infinitescroll('retrieve');
				});
			}
        }
    })();
	/* INFINITE SCROLL - end */
	
	/* COUNT SHARES - start */
	(function () {
		if( $('#social-buttons .counter').length > 0 ) {
			var c = 1;
			
			if( $('.post-title').length > 0 ) {
				var c = parseFloat( $('.post-title').text().length / 13.3242 );
			}
			
			while (c > 9) {
				c = c / 1.42;
			}
			
			$('#social-buttons .counter').text( c.toFixed(1) + 'k');
		}
	})();
	/* COUNT SHARES - end */
	
	/* RESPONSIVE YouTube iframe - start */
	(function() {
		var yt = $('.post-content iframe[src*="youtube.com"]');
		
		if(yt.length > 0) {
			var ratio = new Array();
			var isNumeric = function(obj) {
				return !jQuery.isArray( obj ) && (obj - parseFloat( obj ) + 1) >= 0;
			}
			
			// calc ratio
			yt.each(function() {
				var el = $(this);
				if( !isNumeric(el.attr('width')) ) return;
				if( !isNumeric(el.attr('height')) ) return;
				
				ratio.push(Math.abs( el.attr('height') / el.attr('width') ));
			});
			
			// setHeight
			var setHeight = function() {
				yt.each(function(i) {
					$(this).css('height', Math.ceil($(this).innerWidth() * ratio[i]) + 'px');
				});
			};
			
			// on load
			setHeight();
			
			// on resize - workaround to trigger the "end" of the resize event
			var resizeEnd;
			$(window).resize(function(){
				console.warn("resize");
				clearTimeout(resizeEnd);
				resizeEnd = setTimeout(function(){
					setHeight()
				},500);
			});
		}
	})();
	/* RESPONSIVE YouTube iframe - end */
	
	/* FORCE HIDE wBounce - start */
	(function() {
		wbounce = $('#wbounce-modal');
		if(wbounce.length == 1 && typeof isFirstpage == "function") {	
			if(window.location.search.indexOf('__noca') > -1 && isFirstpage()) {
				console.warn('wBounce: prevented');
				wbounce.addClass('force-hide hidden');
				
				// if the cookie is not set when the user arrives on the page, then make sure the cookie will not be set then he leaves it
				if( document.cookie.indexOf("wBounce") == -1 ) {
					console.warn('wBounce cookie: no set');
					$(window).bind('beforeunload', function(){
						document.cookie = 'wBounce=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
						console.warn('wBounce cookie: deleted');
					});
				}
			}
		}
	})();
	/* FORCE HIDE wBounce - end */
    
    /* NEXT ARROW - start */
	(function() {
		var cc = $('#custom_classes');
		var total_pages = cc.data('total_pages');
		
		if( cc.length > 0 ) {
			if( cc.hasClass('cc_multipage') && cc.hasClass('cc_firstpage') && total_pages > 2 ) {
				var img = $('.post-content #content img:first');
				var next_btn = $('#csp_btns li:last-child > a');
				
				var next_button_v1 = window.location.search.toLowerCase().indexOf('spvb=1') != -1;
				var next_button_v2 = window.location.search.toLowerCase().indexOf('spvb=2') != -1;
			}
		}
	})();

	/* NEXT ARROW - end */
    
    /* Load Cookie consent - start */
	(function() {
		// get the query parameters 
		var urlParams = new URLSearchParams(window.location.search);
		var cct = urlParams.get('cct');		
		
		// if the 'no_c' query param is fount, then don't show the cookie consent ( the 'no_c' query param is set up in the Google Experiment dashboard and it marks the B test version )
		// removed the no_c condition as it's not required anymore : "&& window.location.search.toLowerCase().indexOf('no_c=1') == -1"
		// if( document.cookie.indexOf('cookieconsent_dismissed') == -1 && typeof ajaxurl !== 'undefined' && window.location.search.toLowerCase().indexOf('no_c=1') == -1 ) {
		if( document.cookie.indexOf('cookieconsent_dismissed') == -1 && typeof ajaxurl !== 'undefined' && cct != null && typeof cct !== 'undefined' ) {
			// console.log('doing ajax call');
			// ajaxurl = 'http://bd.gdm.ro/wp-admin/admin-ajax.php';
			// var cct = 1;
			
			$.ajax({
				url: ajaxurl,
				async: true,
				type: "POST",
				dataType: "html",
				context: document.body,
				data: {
					action: "loadCookieConsentAction",
					security: ajaxnonce,
					cct: cct,
				},
				success: function(data) {
					$('head').append(data);										
				},
			});
		} else {
			console.warn("Cookie consent already triggered!");
		}
	})();
	/* Load Cookie consent - end */

	/* Single article pages, left sidebar add class fixed on scroll window to make it sticky - start */
	if($('#left_sidebar').length > 0){
		$(window).on('scroll', function(){
			var headerHeaight = $('.cc_banner-wrapper ').height() + $('header').height() + $('#header-ad-inner').height();
			 if($(window).scrollTop() > headerHeaight){
				$('#left_sidebar').addClass('fixed');
				$('#content-with-aside').addClass('fixed');
			} else{
				$('#left_sidebar').removeClass('fixed');
				$('#content-with-aside').removeClass('fixed');
			}
		})
	}
	/* Single article pages, left sidebar add class fixed on scroll window to make it sticky - end */
	
});

$(window).load(function() {
	var no_prev = $('#single.buttons_yellow .no_prev');
    if(no_prev.length !== 0) {
        $('#single.buttons_yellow .next_button').addClass('no_floating');
    } else {
        $('#single.buttons_yellow .next_button').removeClass('no_floating');
    }
});