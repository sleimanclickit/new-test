<?php // get_header(); ?>
<?php get_template_part('new_template/header'); ?>

    <div id="page-all-articles">
        <div class="container">
            <br />
            <div class="row test">
                <div id="content-with-aside">
                    <div id="all-articles">
                        <h1 class="page-title">Results for '<?php the_search_query(); ?>'</h1>
                        <div id="is-wrapper">
                            <?php
                            // set the page no.
                            if ( get_query_var('paged') ) {
                                $paged = get_query_var('paged');
                            } elseif ( get_query_var('page') ) {
                                $paged = get_query_var('page');
                            } else {
                                $paged = 1;
                            }

                            // set posts_per_page
                            $posts_per_page = 8;

                            // set the offset
                            $offset = $paged * $posts_per_page;

                            // WP_Query arguments
                            $args = array (
                                'pagination' => true,
                                'post_type' => 'post',
                                'posts_per_page' => $posts_per_page,
                                'paged' => $paged,
                                'offset' => $offset,
                                'order' => 'DESC',
                                'orderby' => 'date',
                                's' => get_search_query(),
                                'tax_query' => array(
                                    array(
                                        'taxonomy'  => 'category',
                                        'field'     => 'slug',
                                        'terms'     => 'uncategorized',
                                        'operator'  => 'NOT IN'
                                    )
                                ),
                                // no meta_query restriction on search page
                                // 'meta_query' => array(
                                // array(
                                // 'key'       => 'hidden_post',
                                // 'compare'   => 'NOT EXISTS',
                                // 'type'      => 'CHAR',
                                // ),
                                // ),
                                'meta_query' => array(
                                    'relation' => 'OR',
                                    array(
                                        'key' => 'exclude_post_from_search',
                                        'value' => '1',
                                        'compare' => '!='
                                    ),
                                    array(
                                        'key' => 'exclude_post_from_search',
                                        'compare' => 'NOT EXISTS'
                                    )
                                ),
                            );

                            // The Query
                            $all_articles = new WP_Query( $args );

                            // The Loop
                            if ( $all_articles->have_posts() ) {
                                $i = 1;
                                while ( $all_articles->have_posts() ) {
                                    $all_articles->the_post();
                                    ?>
                                    <div class="infinite-posts">
                                        <hr>
                                        <div class="is-item hp-article">
                                            <div class="article-top">
                                                <a href="<?php the_permalink(); ?>">
                                                    <div class="article-image" style="background-image:url(<?php echo get_the_post_thumbnail_url(); ?>)">
                                                        <?php //the_post_thumbnail('post-item', array('class' => "")); ?>
                                                    </div>
                                                </a>
                                                <div class="article-cat link-category">
                                                    <?php $all_articles_category = get_the_category(get_the_ID()); ?>
                                                    <a href="<?php echo get_category_link($all_articles_category[0]); ?>"><?php echo $all_articles_category[0] ? $all_articles_category[0]->name : 'N/A'; ?></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="article-bottom">
                                            <h2 class="article-title"><a class="il" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                            <p><?php $content_post = get_the_content(); echo substr($content_post, 0, 100).'...'; ?></p>
                                            <div class="article-more">
                                                <a href="<?php the_permalink(); ?>">Read More<i
                                                            class="glyphicon glyphicon-arrow-right"></i></a>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <?php
                                    $i++;
                                }
                            } else {
                                ?>
                                <div class="col-xs-12">
                                    <br />
                                    <p>No posts found.</p>
                                    <br />
                                </div>
                                <?php
                            }
                            ?>
                        </div>

                        <?php if ( $all_articles->have_posts() ) { ?>
                            <div id="is-load-more">
                                <?php if( $all_articles->max_num_pages > 1 ) { ?>
                                    <a href="/" id="is-load-more-btn" data-max_num_pages="<?php echo $all_articles->max_num_pages; ?>">Load More</a>
                                <?php } else { ?>
                                    <em>No more items to display.</em>
                                <?php } ?>
                            </div>
                            <div id="is-nav" class="hidden"><?php next_posts_link( 'Older Entries', $all_articles->max_num_pages ); ?></div>
                        <?php } ?>

                        <?php wp_reset_postdata(); ?>
                    </div><!-- end - #all-articles -->
                </div><!-- end - main content -->

                <aside id="aside">
                    <br />
                    <?php get_sidebar('ads'); ?>
                </aside><!-- end - #sidebar -->
            </div>
        </div>
    </div><!-- page with sidebar template - end -->

<?php // get_footer(); ?>
<?php get_template_part('new_template/footer'); ?>