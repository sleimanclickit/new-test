<?php
/*
	Template Name: Page - Contact us
*/
?>
<?php
$show_form = true;

if (isset($_POST['cu_submit']) && $_SERVER['REQUEST_METHOD'] === 'POST') {
    if ( ! isset( $_POST['cu_field'] ) || ! wp_verify_nonce( $_POST['cu_field'], 'cu_action' ) ) {
        print 'Silence is golden.';
        exit;
    } else {

        $cu_name = sanitize_text_field(trim($_POST['cu_name']));
        $cu_email = sanitize_text_field(trim($_POST['cu_email']));
        $cu_subject = sanitize_text_field(trim($_POST['cu_subject']));
        $cu_message = sanitize_text_field(trim($_POST['cu_message']));


        // validate fields
        if($cu_name === '') {
            $errorName = 'Please enter your name.';
            $hasError = true;
        }

        if($cu_email === '')  {
            $errorEmail = 'Please enter your email address.';
            $hasError = true;
        } else if (!preg_match("/^[[:alnum:]][a-z0-9_.-]*@[a-z0-9.-]+\.[a-z]{2,4}$/i", $cu_email)) {
            $errorEmail = 'You entered an invalid email address.';
            $hasError = true;
        }

        if($cu_subject === '') {
            $errorSubject = 'Please enter the subject.';
            $hasError = true;
        }

        if($cu_message === '') {
            $errorMessage = 'Please enter the message.';
            $hasError = true;
        }

        // validation recaptcha
        $params = array();
        $params['secret'] = '6LdB1qYUAAAAADfLMVYpFVndKPmqf-6Q50mzbicb';
        if (!empty($_POST) && isset($_POST['g-recaptcha-response'])) {
            $params['response'] = urlencode($_POST['g-recaptcha-response']);
        }
        $params['remoteip'] = $_SERVER['REMOTE_ADDR'];

        $params_string = http_build_query($params);
        $requestURL = 'https://www.google.com/recaptcha/api/siteverify?' . $params_string;

        // Get cURL resource
        $curl = curl_init();

        // Set some options
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $requestURL,
        ));

        // Send the request
        $response = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);

        $response = @json_decode($response, true);

        if ($response["success"] != true) {
            $errorRecaptcha = 'Please prove you are not a robot.';
            $hasError = true;
        }


        // send email
        if( !$hasError ) {
            $show_form = false;
            $result_msg = "Something went wrong, please try again later.";

            $mail_to = "advertise@reversemediagroup.com";
            $mail_subject = "New message from buzzdrives.com";

            $mail_body = "";
            $mail_body .= "<strong>Name</strong>: " . $cu_name . "<br>" ;
            $mail_body .= "<strong>Email</strong>: " . $cu_email . "<br>";
            $mail_body .= "<br>";
            $mail_body .= "<br>";
            $mail_body .= "<strong>Subject</strong>: " . $cu_subject . "<br>";
            $mail_body .= "<strong>Message</strong>: <br>" . $cu_message;

            add_filter( 'wp_mail_content_type', 'set_content_type' );
            function set_content_type( $content_type ) {
                return 'text/html';
            }

            if( wp_mail( $mail_to, $mail_subject, $mail_body ) ) {
                $result_msg = "The message was successfully sent.";
            }
        }
    }
}
?>
<?php get_template_part('new_template/header'); ?>
			
			<div id="main">
				<div id="contact-us-page">
					<div class="container">
						<h1 class="page-title text-center"><?php the_title(); ?></h1>
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="information-content">
									<h4>Buzzdrives.com<br>Reverse Media Group Ltd</h4>
									<p>1 Lindsey Street<br>London,<br>EC1A 9HP</p>
									<p><strong>Telephone: </strong><a href="tel:+442081339954">+44 208 133 9954</a></p>
									<p><strong>For media and advertising enquiries: </strong><span class="link-contact" data-name="advertise" data-domain="reversemediagroup" data-extention="com">advertise@reversemediagroup.com</span></p>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-container">
                                    <?php if( $show_form ) { ?>
                                        <form id="cu_form" action="<?php echo esc_url( $_SERVER['REQUEST_URI'] ); ?>" method="post">
                                            <?php echo wp_nonce_field('cu_action', 'cu_field'); ?>
                                            <div class="form-group">
                                                <label for="cu_name">Your name <span>*</span></label>
                                                <input type="text" class="form-control" id="cu_name" name="cu_name" value="<?php echo isset($cu_name) ? $cu_name : ''; ?>" placeholder="">
                                                <?php if( isset($errorName) ) { ?><label class="error" for="cu_name"><?php echo $errorName; ?></label><?php } ?>
                                            </div>
                                            <div class="form-group">
                                                <label for="cu_email">Your email address <span>*</span></label>
                                                <input type="email" class="form-control" id="cu_email" name="cu_email" value="<?php echo isset($cu_email) ? $cu_email : ''; ?>" placeholder="">
                                                <?php if( isset($errorEmail) ) { ?><label class="error" for="cu_name"><?php echo $errorEmail; ?></label><?php } ?>
                                            </div>
                                            <div class="form-group">
                                                <label for="cu_subject">Subject <span>*</span></label>
                                                <input type="text" class="form-control" id="cu_subject" name="cu_subject" value="<?php echo isset($cu_subject) ? $cu_subject : ''; ?>" placeholder="">
                                                <?php if( isset($errorSubject) ) { ?><label class="error" for="cu_name"><?php echo $errorSubject; ?></label><?php } ?>
                                            </div>
                                            <div class="form-group">
                                                <label for="cu_message">Message <span>*</span></label>
                                                <textarea class="form-control" id="cu_message" name="cu_message" rows="5" placeholder=""><?php echo isset($cu_message) ? $cu_message : ''; ?></textarea>
                                                <?php if( isset($errorMessage) ) { ?><label class="error" for="cu_name"><?php echo $errorMessage; ?></label><?php } ?>
                                            </div>
                                            <div class="form-group">
                                                <script src="https://www.google.com/recaptcha/api.js" async defer></script>
                                                <?php /* ?>
                            <div id="cu_recaptcha" class="g-recaptcha" data-sitekey="6LeVOgsTAAAAADpN1uSBYJs5VSyCbOZ5vUji3ztR" data-callback="recaptchaCallback"></div>
                            <?php */ ?>
                                                <div id="cu_recaptcha" class="g-recaptcha" data-sitekey="6LdB1qYUAAAAAN5GmoCLLhGTSyKW29UzSSZL6t4_" data-callback="recaptchaCallback"></div>
                                                <label id="cu_recaptcha_error" class="cu_error" style="display: none;">Please prove you are not a robot.</label>
                                            </div>
                                            <button type="submit" id="cu_submit" name="cu_submit" class="btn btn-blue">Send</button>
                                        </form>
                                    <?php } else { ?>
                                        <h4><?php echo $result_msg; ?></h4>
                                    <?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>

<?php // get_footer(); ?>
<?php get_template_part('new_template/footer'); ?>