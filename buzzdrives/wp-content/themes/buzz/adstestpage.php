<?php /* Template Name: adstestpage */ ?>
<!DOCTYPE html>
<html lang="en">
	<head>		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php bloginfo('name'); ?> | <?php IsHome() ? bloginfo('description') : wp_title(''); ?></title>
	
		<?php /* globals.js includes: GA code, global variables */ ?>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/globals.js"></script>
		
		<!-- GOOGLE ANALYTICS - start -->
		<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		ga('create', 'UA-64470501-1', 'auto');
		ga('send', 'pageview');
		</script>
		<!-- GOOGLE ANALYTICS - end -->
		
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-KMZ7DSR');</script>
		<!-- End Google Tag Manager -->
				
		<?php /* PREBID JS / HEADERBIDDING - START  ?>
		<script type="text/javascript" src="http://m2d.m2.ai/m2d.buzzdrives.alldevices.min.js" async="async" ></script>
		<?php /* PREBID JS / HEADERBIDDING - END */ ?>
		
		<?php /* MM anchor sticky ads - START */  ?>
		<script src="//m2d.m2.ai/m2d.buzzdrives.alldevices_anchor.min.js" async></script> 
		<?php /* MM anchor sticky ads - END */ ?>
				
		<?php /* Favicons - start */ ?>
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-180x180.png">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/favicon-16x16.png" sizes="16x16">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/favicon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-36x36.png" sizes="36x36">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-48x48.png" sizes="48x48">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-72x72.png" sizes="72x72">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-144x144.png" sizes="144x144">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-192x192.png" sizes="192x192">
		<link rel="manifest" href="<?php echo get_img_folder(); ?>/favicons/manifest.json">
		<meta name="apple-mobile-web-app-title" content="BuzzDrives">
		<meta name="application-name" content="BuzzDrives">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?php echo get_img_folder(); ?>/favicons/ms-icon-70x70.png">
		<meta name="msapplication-TileImage" content="<?php echo get_img_folder(); ?>/favicons/ms-icon-144x144.png">
		<meta name="msapplication-TileImage" content="<?php echo get_img_folder(); ?>/favicons/ms-icon-150x150.png">
		<meta name="msapplication-TileImage" content="<?php echo get_img_folder(); ?>/favicons/ms-icon-310x310.png">
		<meta name="theme-color" content="#ffffff">
		<?php /* Favicons - end */ ?>
		
		<?php //<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"> ?>
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />

		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		        
        <?php /* load a fake adframe.js file that will be blocked by adblocker. this way we can determine if user uses an adblocker extension */ ?>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/adframe.js"></script>
					
		<?php wp_head(); ?>
				
		<?php /* Admedia Vslider - start  ?>
		<script type="text/javascript" src="http://vslider.admedia.com/?id=MzouPyA&style=1"></script>
		<?php /* Admedia Vslider - end */ ?>

	</head>
	
	<body <?php body_class(); ?> data-id="<?php echo get_the_ID(); ?>">
		<?php do_action('after_body'); // it hooks any plugins that require an early load and it includes HTML. ex: FB SDK ?>
				
		<div id="wrapper">
			<div id="main-gradient"></div><!-- end - #main-gradient -->
			
			<header id="header">
				<div id="header-inner">		
				
					<div id="header-nav-wrapper">
						<div class="container">
							<button type="button" id="mobile-menu-btn" class="visible-xs">Menu <span class="glyphicon glyphicon-menu-hamburger"></span></button>
							<nav id="nav" class="hidden-xs">
								<?php wp_nav_menu(
									array(
										'theme_location' => 'primary-menu',
										'container' => '',
										'menu_id' => '',
										'menu_class' => 'clearfix',
									)
								); ?>
							</nav><!-- end - #nav -->
							
							<div id="header-search" class="hidden-xs">
								<?php get_search_form(); ?>
							</div><!-- end - #header-search -->
						</div>
					</div><!-- end - #header-nav-wrapper -->
						
					<div id="header-logo">
						<div class="container">
							<div id="header-logo-inner">
								<div id="header-logo-round">
									<nav id="nav-mobile">
									<?php wp_nav_menu(
										array(
											'theme_location' => 'primary-menu',
											'container' => '',
											'menu_id' => 'menu-mobile',
											'menu_class' => 'clearfix',
										)
									); ?>
									</nav><!-- end - #nav-mobile -->
									<a href="<?php echo get_home_url(); ?>">
										<img class="center-block img-responsive" src="<?php echo get_img_folder(); ?>/buzzdrives_logo.png" width="334" height="55" alt="BuzzDrives Logo">
									</a>
								</div>
							</div>
							
							<div id="header-ad-inner">
																
								<div id='div_top_banner_desktop'>
																	
								</div>
									
							</div><!-- end - #header-ad-inner -->
						</div>
					</div><!-- end - #header-logo -->
				</div><!-- end - #header-inner -->
			</header><!-- end - #header -->
			
			<div id="main">
			
				<div id="single">
					<div class="container">
						<div id="ads-top-single-page" class="hidden-xs">
							
						</div>
						
						<div class="row">
							<div id="content-with-aside" >
								<div class="loop-single">
									<?php
										if ( have_posts() ) {
											while ( have_posts() ) {
												the_post();
									?>
												<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>>
													<h1 class="post-title"><?php the_title(); ?></h1><!-- post title - end -->
															
															
													<?php /* ADS -- AFTER TITLE - start */ ?>					
													<!--
													<div id="ads-after-title">
													</div><!-- ads after title - end -->					
													<?php /* ADS -- AFTER TITLE - end */ ?>
													
													<ul id="social-buttons">
														<li class="count"><div class="counter"></div><span class="shares">shares</span></li>
														<li><a class="social tw" href="javascript:void(0);" onclick="window.open('https://twitter.com/intent/tweet?text=' + encodeURIComponent(document.title) + '&amp;url=' + encodeURIComponent(document.location.href), 'sharer', 'top=200, left=200, toolbar=0, status=0, width=520, height=350'); return false;" target="_black"><span class="hidden-xxs">Twitter</span></a></li>
														<li><a class="social fb" href="javascript:void(0);" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(document.location.href), 'sharer', 'top=200, left=200, toolbar=0, status=0, width=520, height=350'); return false;" target="_blank"><span class="hidden-xxs">Facebook</span></a></li>
													</ul><!-- social button - end -->

													<div class="post-content">
														<div id="content" itemprop="articleBody">
															<?php the_content(); ?>
														</div>
														
														<?php /* ADS -- BEFORE PAGINATION - start */ ?>
														<div id="ads-before-pagination" class="text-center">						
																				
															<?php // Adsense START ?>
															<div class="adsense" id="adsense_before_pag" >
																<div class="adsense_header_title">Advertisement</div>
																
															</div>
															<?php // Adsense END ?>		
														
														</div><!-- ads before pagination - end -->
														<?php /* ADS -- BEFORE PAGINATION - end */ ?>
														
														<?php do_action('single_pagination'); ?>
														
														<?php /* ADS -- AFTER PAGINATION - start */ ?>
														<div id="ads-after-pagination" class="text-center">
															
															<?php /* Admedia Videosense - start  ?>
															<div id="videosense_300x250" style="width:300px; height:250px; overflow:hidden"></div>
															<script src="http://videosense.com/ads/?id=MzouPyA&js=1&page_url=<?php echo urlencode( esc_url(the_permalink()) );?>&size=300x250" ></script>							
															<?php /* Admedia Videosense - end */ ?>
															
															<?php /* Monetizemore Sticky - start */ ?>
															<!--
															<div id="BD_Sticky_Bottom_desktop"  style="position:fixed; bottom:0; left:0; right:0; margin:0 auto; z-index:9999999" ></div> 
															<div id="BD_Sticky_Bottom_mobile"  style="position:fixed; bottom:0; left:0; right:0; margin:0 auto; z-index:9999999" ></div>
															-->
															
															<div id="BD_Sticky_Bottom_desktop"></div>
															<div id="BD_Sticky_Bottom_mobile"></div>
															<?php /* Monetizemore Sticky - end */ ?>
															
														</div><!-- ads after pagination - end -->
														<?php /* ADS -- AFTER PAGINATION - end */ ?>
														
													</div><!-- post content - end -->
												</article><!-- post article - end -->
									<?php
											} // end while
										} else {
									?>
										<p>No content found.</p>
									<?php
										}
									?>
									<?php edit_post_link('edit', '<p>', '</p>'); ?>
								</div><!-- main loop of the post - end -->

							</div><!-- end - main content -->
							
							<aside id="aside" class="mobile-below">
								<div id="ads-aside" class="hidden-xs">
													
								</div>
							</aside><!-- end - #sidebar -->
						</div>
					</div>
				</div><!-- single post template - end -->
				
			</div><!-- end - #main -->
			
			<footer id="footer">
				<div id="footer-inner">
					<div class="container">
						<div id="footer-logo-wrapper">
							<a href="<?php echo get_home_url(); ?>">
								<img class="center-block img-responsive" src="<?php echo get_img_folder(); ?>/buzzdrives_logo_footer.png" alt="BuzzDrives Logo" width="208" height="23" />
							</a>
						</div><!-- end - #footer-logo-wrapper -->
						
						<div id="footer-menu">
							<?php wp_nav_menu(
								array(
									'theme_location' => 'footer-menu',
									'container' => '',
									'menu_id' => 'menu-footer',
									'menu_class' => 'list-inline clearfix text-center',
								)
							); ?>
						</div><!-- end - #footer-menu -->
					</div>
				</div><!-- end - #footer-inner -->
			</footer><!-- end - #footer -->
		</div><!-- end - #wrapper -->
                		
		<?php wp_footer(); ?>
		
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.infinitescroll.min.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/scripts.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/ga_events.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
		
		
	</body>
</html>
