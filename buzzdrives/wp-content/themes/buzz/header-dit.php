<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php bloginfo('name'); ?> | <?php IsHome() ? bloginfo('description') : wp_title(''); ?></title>



	<script>

		<?php
		
		// we use only one dimension for the current tests : 
		// echo injectCustomDimensionValue('dimension1', ['dittest']);
		?>

	</script>
	<!-- GOOGLE ANALYTICS - end -->

	<!-- Facebook Pixel Code -->
	<script>

	</script>
	<noscript><img height="1" width="1" style="display:none"
	               src="https://www.facebook.com/tr?id=1861783217183966&ev=PageView&noscript=1"
		/></noscript>
	<!-- DO NOT MODIFY -->
	<!-- End Facebook Pixel Code -->

	<?php /* FACEBOOK SEO TAGS - start */ ?>
	<?php if( is_single() ) { ?>
		<meta property="og:url" content="<?php the_permalink($post->ID); ?>" />
		<meta property="og:type" content="website" />
		<meta property="og:title" content="<?php wp_title( '|', true, 'right' ) . bloginfo( 'name' ); ?>" />
		<meta property="og:description" content="<?php the_title(); ?>" />
		<meta property="og:image" content="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0]; ?>" />
	<?php } else { ?>
		<meta name="description" content="Fill your tank with our satisfying bites of trending car articles and news on latest models and moves from within the industry.">
	<?php } ?>
	<?php /* FACEBOOK SEO TAGS - end */ ?>

	<?php /* Google Verification - start */?>
	<meta name="google-site-verification" content="vuobpu9ZEcVJfqbyAG8iqiFZM3WtGjdwz44QmIVQLCM" />
	<meta name="google-site-verification" content="GICwiYwnx4mHDw4kZdilY2wHe129B2hNjXwoR5d0I40" />
	<?php /* Google Verification - end */?>

	<?php /* globals.js includes: GA code, global variables */ ?>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/globals.js"></script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-KMZ7DSR');</script>
    <!-- End Google Tag Manager -->


	<?php /* MM sticky ads - start  ?>
	<script>

	</script>
	<?php /* MM sticky ads - end */ ?>

	<?php /* MM ads Device Check - start  ?>
	<script>
	</script>
	<?php /* MM ads Device Check - end */ ?>



	<?php /* MM/DFP Video tag - START  ?>
		<?php if( testMmVideoTag() ) { ?>
			<script src="//m2d.m2.ai/pghb.buzzdrives.ibv.js" async></script>
		<?php } ?>
		<?php /*  MM/DFP Video tag - END */ ?>





	<?php /* Favicons - start */ ?>
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-36x36.png" sizes="36x36">
	<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-48x48.png" sizes="48x48">
	<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-72x72.png" sizes="72x72">
	<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-144x144.png" sizes="144x144">
	<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-192x192.png" sizes="192x192">
	<link rel="manifest" href="<?php echo get_img_folder(); ?>/favicons/manifest.json">
	<meta name="apple-mobile-web-app-title" content="BuzzDrives">
	<meta name="application-name" content="BuzzDrives">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php echo get_img_folder(); ?>/favicons/ms-icon-70x70.png">
	<meta name="msapplication-TileImage" content="<?php echo get_img_folder(); ?>/favicons/ms-icon-144x144.png">
	<meta name="msapplication-TileImage" content="<?php echo get_img_folder(); ?>/favicons/ms-icon-150x150.png">
	<meta name="msapplication-TileImage" content="<?php echo get_img_folder(); ?>/favicons/ms-icon-310x310.png">
	<meta name="theme-color" content="#ffffff">
	<?php /* Favicons - end */ ?>

	<?php //<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"> ?>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" />

	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />


	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->


	<?php /* load a fake adframe.js file that will be blocked by adblocker. this way we can determine if user uses an adblocker extension */ ?>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/adframe.js"></script>


	<?php wp_head(); ?>


</head>

<body <?php body_class(); ?> data-id="<?php echo get_the_ID(); ?>">

<?php /* add class to the body for desktops */ ?>
<script type="text/javascript" >
    if( isDesktop() ) {
        document.body.classList.add("is_desktop");
    }
    if( isMobile() ) {
        document.body.classList.add("is_mobile");
    }
</script>

<?php do_action('after_body'); // it hooks any plugins that require an early load and it includes HTML. ex: FB SDK ?>


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KMZ7DSR"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="wrapper">
	<div id="main-gradient"></div><!-- end - #main-gradient -->

	<header id="header" style="position:relative;" >
		<div id="header-inner">

			<div id="header-nav-wrapper">
				<div class="container">
					<button type="button" id="mobile-menu-btn" class="visible-xs">Menu <span class="glyphicon glyphicon-menu-hamburger"></span></button>
					<nav id="nav" class="hidden-xs">
						<?php wp_nav_menu(
							array(
								'theme_location' => 'primary-menu',
								'container' => '',
								'menu_id' => '',
								'menu_class' => 'clearfix',
							)
						); ?>
					</nav><!-- end - #nav -->

					<!--
							<div id="header-search" class="hidden-xs">
								<?php // get_search_form(); ?>
							</div>  end - #header-search -->

				</div>
			</div><!-- end - #header-nav-wrapper -->

			<div id="header-logo">
				<div class="container">
					<div id="header-logo-inner">
						<div id="header-logo-round">
							<nav id="nav-mobile">
								<?php wp_nav_menu(
									array(
										'theme_location' => 'primary-menu',
										'container' => '',
										'menu_id' => 'menu-mobile',
										'menu_class' => 'clearfix',
									)
								); ?>
							</nav><!-- end - #nav-mobile -->

							<a href="<?php echo get_home_url(); ?><?php get_redesign_query_parameter(); ?>">
								<img class="center-block img-responsive" src="<?php echo get_img_folder(); ?>/buzzdrives_logo.png" width="334" height="55" alt="BuzzDrives Logo">
							</a>
						</div>
					</div>

					<?php /* Don't show ads on the 404 page  */ ?>
					<?php if ( !is_404() ) { ?>

						<div id="header-ad-inner">
							<div id='div_top_banner_desktop'>
								<?php /* MM / DFP - START */ ?>
								<script>
                                    (function(){
                                        if( !isMobile() ) {
                                            document.write ('<div id="top_banner_desktop"></div>');
                                        }
                                    }());
								</script>
								<?php /* MM / DFP - END */ ?>

							</div>
						</div><!-- end - #header-ad-inner -->


					<?php } ?>
					<?php /* Don't show ads on the 404 page  */ ?>


				</div>
			</div><!-- end - #header-logo -->
		</div><!-- end - #header-inner -->
	</header><!-- end - #header -->

	<div id="main">
