<?php /*
	ADS -- SIDEBAR - start
*/ ?>

<div id="ads-aside" class="hidden-xs">

	<?php /* MM/DFP JS START */ ?>
	<?php if( showMMSidebar2() || is_front_page() ) { ?>
	<div id="BD_top_right_sidebar_desktop_2"></div>
	<?php } else if( isset($post) && isset($post->ID) && $post->ID == 11246 && isCampaignTraffic() ) { ?>
		<div id="Epom_Native_test"></div>
	<?php } else { ?>
	<div id="top_right_sidebar_desktop" ></div>
	<?php } ?>
	<?php /* MM/DFP JS END */ ?>

    <?php /* Show the small video in the right sidebat - start */ ?>
	<?php if( isCampaignTraffic() ) { ?>
		<div id="BD_Top_Right_Sidebar_Video"></div>
		
		
		<?php /* Removed on 27.02.2019 ( now is shown via M2M script ) - kept as reference 
		if( showSmallVideoRightSidebar() ) { ?>
			<!-- /207764977/BD_Top_Right_Sidebar_Video -->
			<div id='div-gpt-ad-1548229964157-0' style='height:250px; width:300px;'>
				<script>
					googletag.cmd.push(function() { googletag.display('div-gpt-ad-1548229964157-0'); });
				</script>
			</div>
		<?php */ ?>
		
    <?php } ?>
    <?php /* Show the small video in the right sidebat - end */ ?>
	
		
	<?php /* Tan Media tags - start - removed on 26.03.2019 ?>
	<?php if( showTanMediaTags() ) { ?> 
		<script async type='text/javascript' src='https://cdn.connatix.com/min/connatix.renderer.infeed.min.js' data-connatix-token='706d6a44-23ee-47d9-90bd-0a225a489555'></script>
	<?php } ?> 
	<?php /* Tan Media tags - end */ ?>
	
	
	<?php /* Adsense link Ad unit 1 - start */ ?>
	<?php if( isCampaignTraffic() ) { ?>
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<!-- buzzdrives.com - link ad - sidebar 1 -->
		<ins class="adsbygoogle"
			 style="display:block"
			 data-ad-client="ca-pub-6449643190876495"
			 data-ad-slot="6442249183"
			 data-ad-format="link"
			 data-full-width-responsive="true"></ins>
		<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
	<?php } ?>	
	<?php /* Adsense link Ad unit 1 - end */ ?>
	
			
	<?php /* MM/DFP Video tag - START - removed on 26.03.2019 ?>
	<?php if( testMmVideoTag() ) { ?>
		<div id="BD_IBV_Right_Sidebar_Desktop"></div>
	<?php } ?>
	<?php /*  MM/DFP Video tag - END */ ?>   	
	
	
	
	<?php /* MM sticky sidebar widget - start */ ?>	
	<?php if( isCampaignTraffic() ) { ?>
		<script>
			(function(){
				if( isDesktop() ) {
					document.write ('<div id="sidebar"><div id="BD_sticky_sidebar_desktop"  class="sidebar-sticky" style="margin-top:5px;z-index:99999;"></div></div> ');
				}												
			}());
		</script>

        <?php if( !isControlDitTest() ) { ?>
            <script>
                    $(window).load(function() {
                        var issticky = 0;
                        var initial_sticky = $('.sidebar-sticky').offset().top;
                        initial_sticky = 0;

                        // on scroll action
                        $(window).scroll(function() {

                            if( issticky == 0 ) {
                                // is Static and we check if we should make it Sticky
                                if( $(window).scrollTop() >= $('.sidebar-sticky').offset().top  ) {
                                    // get it's current position and set it as the "initial"
                                    if( initial_sticky == 0 ) {
                                        initial_sticky = $('.sidebar-sticky').offset().top;
                                    }

                                    $('#BD_sticky_sidebar_desktop').css({"position":'fixed'});
                                    $('#BD_sticky_sidebar_desktop').css({"top":'50px'});
                                    $('#BD_sticky_sidebar_desktop').css({"margin-top":'0px'});
                                    issticky = 1 ;
                                }
                            }else{
                                // is Sticky and we check if we should make it Static
                                if( initial_sticky > $(window).scrollTop() ) {
                                    $('#BD_sticky_sidebar_desktop').css({"position":'static'});
                                    $('#BD_sticky_sidebar_desktop').css({"top":'0px'});
                                    $('#BD_sticky_sidebar_desktop').css({"margin-top":'5px'});
                                    issticky = 0;
                                }
                            }


                        });
                    });
            </script>
        <?php } ?>
	<?php } ?>
	<?php /* MM sticky sidebar widget - end */ ?>

	<?php /* Test new Nativo/Tan media video tag - START - removed on 26.03.2019 ?>
	<?php if( testNativoVideoTag() ) { ?>
		<script async type='text/javascript' src='https://cdn.connatix.com/min/connatix.renderer.infeed.min.js' data-connatix-token='706d6a44-23ee-47d9-90bd-0a225a489555'></script>
		<div id="nativo_wrapper" style="" >
		</div>
	<?php } ?>
	<?php /*  Test new Nativo/Tan media video tag - END */ ?>   	
	
	<?php /* TABOOLA - "Right Rail Thumbnails" widget */ ?>
	<?php if( !hideTaboolaSidebarWidget() ) { ?>
		<div id="taboola-right-rail-thumbnails"  class="taboola-side" style="margin-top:15px;" ></div>
		<script type="text/javascript">
		  window._taboola = window._taboola || [];
		  _taboola.push({
			mode: 'organic-thumbnails-rr',
			container: 'taboola-right-rail-thumbnails',
			placement: 'Right Rail Thumbnails',
			target_type: 'mix'
		  });
		</script>
	<?php } ?>
	<?php /* TABOOLA - end */ ?>
	
	
</div>
<div></div>
<?php /*
	ADS -- SIDEBAR - end
*/ ?>