<?php
require 'inc/quiz/quiz.php';

/* Make sure we have a session started */
// add_action('init', 'start_session', 1);
// function start_session() {
	// if(!session_id()) {
		// session_start();
	// }
// }

/* Disable Emoji code - Start - added on 05.04.2019  */
/**
 * Disable the emoji's
 */
function disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
	add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 * 
 * @param array $plugins 
 * @return array Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @param array $urls URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed for.
 * @return array Difference betwen the two arrays.
 */
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
	if ( 'dns-prefetch' == $relation_type ) {
		/** This filter is documented in wp-includes/formatting.php */
		$emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

		$urls = array_diff( $urls, array( $emoji_svg_url ) );
	}

	return $urls;
}
/* Disable Emoji code - End  */

/* TEMP */
if( !function_exists('pre') ) {
	function pre($data) {
		print_r("<pre>");
		print_r($data);
		print_r("</pre>");
	}
}

/*
 * Stop wordpress username enumeration vulnerability
 */
add_action('init', 'stop_wordpresss_username_enumeration');
function stop_wordpresss_username_enumeration()
{	
	// prevent query string that includes 'author'
	// example: http://example.com/?string&author=1&another_string
	if(isset($_REQUEST['author'])){
		wp_redirect(home_url(), 301);
		exit;
	}
	
	// prevent direct access of the author page
	// example: http://example.com/author/admin/
	if(preg_match('/\/author\//', $_SERVER['REQUEST_URI']) === 1) {
		wp_redirect(home_url(), 301);
		exit;
	}
}

add_theme_support( 'html5', array( 'search-form' ) );

/* GET INCLUDES FOLDER */
function get_includes_folder() {
	return get_template_directory_uri() . '/includes';
}
/* GET IMAGES FOLDER */
function get_img_folder() {
	return get_template_directory_uri() . '/images';
}

/* REGISTER CUSTOM MENU - start */
function RegisterCustomMenus() {
	register_nav_menus(
		array(
			'primary-menu' => __( 'Primary Menu' ),
			'footer-menu' => __( 'Footer Menu' ),
		)
	);
}
add_action( 'init', 'RegisterCustomMenus' );
/* REGISTER CUSTOM MENU - end */

/* REGISTER SIDEBARS - start */
register_sidebar(
	array(
		'name' 			=> 'Sidebar',
		'id' 			=> 'sidebar-1',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' 	=> '</div> <!-- end .widget -->',
		'before_title' 	=> '<h4 class="widgettitle">',
		'after_title' 	=> '</h4>',
	)
);
/* REGISTER SIDEBARS - end*/

/* CHECK IF THE PAGE LOADED IS HOME OR FRONTPAGE - start */
function IsHome(){
	if( is_front_page() || is_home() ) {
		return true;
	} else {
		return false;
	}
}
add_action( 'get_header', 'IsHome' );
/* CHECK IF THE PAGE LOADED IS HOME OR FRONTPAGE - end */

/* CUSTOM SEARCH FORM - start */
function custom_search_form( $form ) {
	$form = '';
	$newUrl = 'https://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
	
	// if (!strpos($newUrl,'?test_redesign=123')) {	
	if ( !isCampaignTraffic() ) {	
		$form .= '<form role="search" method="get" id="searchform" class="searchform" action="' . home_url( '/' ) . '" autocomplete="off">';
			$form .= '<div class="clearfix">';
				$form .= '<button type="button" id="searchsubmit"><span class="glyphicon glyphicon-search"></span></button>';
				$form .= '<div id="s-wrapper">';
					$form .= '<input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="Search" />';
				$form .= '</div>';
			$form .= '</div>';
		$form .= '</form>';
	} else {
		$form .= '<form role="search" method="get" id="searchform" class="searchform" action="' . home_url( '/' ) . '?tict=1" autocomplete="off">';
			$form .= '<div class="clearfix">';
				$form .= '<button type="button" id="searchsubmit"><span class="glyphicon glyphicon-search"></span></button>';
				$form .= '<div id="s-wrapper">';
					$form .= '<input type="hidden" name="tict" value="1" />';
					$form .= '<input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="Search" />';
				$form .= '</div>';
			$form .= '</div>';
		$form .= '</form>';
	}

	return $form;
}
add_filter( 'get_search_form', 'custom_search_form' );
/* CUSTOM SEARCH FORM - end */

/* SETS THE POST EXCERPT/CONTENT LENGTH  - start */
function excerpt($limit) {
	$excerpt = explode(' ', get_the_excerpt(), $limit);
	if (count($excerpt)>=$limit) {
		array_pop($excerpt);
		$excerpt = implode(" ",$excerpt).'...';
	} else {
		$excerpt = implode(" ",$excerpt);
	} 
	$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	return $excerpt;
}
function custom_excerpt_length( $length ) {
	return 8;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
function content($limit) {
	$content = explode(' ', get_the_content(), $limit);
	if (count($content)>=$limit) {
		array_pop($content);
		$content = implode(" ",$content).'...';
	} else {
		$content = implode(" ",$content);
	} 
	$content = preg_replace('/\[.+\]/','', $content);
	$content = apply_filters('the_content', $content); 
	$content = str_replace(']]>', ']]&gt;', $content);
	return $content;
}
/* SETS THE POST EXCERPT/CONTENT LENGTH  - end */

/* CUSTOM SINGLE PAGINATION - start */
function _set_custom_args($url) {
	// Check and persist the in-use GET variables across pagination ( we use the GET variable in order to show / hide certain widgets )
	
	/* Yahoo traffic query string checks - Start */
	// if 'utm_source' exists, we'll replace it with '?tmpyg=1' in order to be used for the isYahooSource check
	if( isset($_GET['utm_source']) && ( strtolower( $_GET['utm_source'] ) == 'yg' || strtolower( $_GET['utm_source'] ) == 'yahoo' ) && !isset($_GET['tmpyg']) ) {
        $url = add_query_arg( array('tmpyg' => 1), $url );
    }
	// // make sure we also persist the temp 'yahoo' param
	if( isset($_GET['tmpyg']) ) {
        $url = add_query_arg( array('tmpyg' => $_GET['tmpyg']), $url );
    }
	/* Yahoo traffic query string checks - End */
	
	
	
	/* Facebook traffic query string checks - Start */
	// if 'utm_source' exists and is 'facebook' , we'll replace it with '?tmpfb=1' in order to be used after the user navigates to the next page / post
	if( isset($_GET['utm_source']) && ( strtolower( $_GET['utm_source'] ) == 'facebook' ) && !isset($_GET['tmpfb']) ) {
        $url = add_query_arg( array('tmpfb' => 1), $url );
    }
	// // make sure we also persist the temp 'Facebook' param
	if( isset($_GET['tmpfb']) ) {
        $url = add_query_arg( array('tmpfb' => $_GET['tmpfb']), $url );
    }
	/* Facebook traffic query string checks - End */
	
	
	/* UTM Campaign traffic query string checks - Start */
	// if 'utm_source' exists, we'll replace it with '?tict=1' in order to be used for the isCampaignTraffic check
	// removed 'facebook' from campaign check on 29.05.2018 
	if( isset($_GET['utm_source']) && !isset($_GET['tict']) && 
		( 
			strtolower( $_GET['utm_source'] ) == 'yg' || strtolower( $_GET['utm_source'] ) == 'yahoo' || strtolower( $_GET['utm_source'] ) == 'zemanta' ||
			strtolower( $_GET['utm_source'] ) == 'outbrain' || strtolower( $_GET['utm_source'] ) == 'taboola' 
			/* || strtolower( $_GET['utm_source'] ) == 'facebook' */
		) ) {
			
        $url = add_query_arg( array('tict' => 1), $url );
    }
	// // make sure we also persist the temp 'yahoo' param
	if( isset($_GET['tict']) ) {
        $url = add_query_arg( array('tict' => $_GET['tict']), $url );
    }
	/* UTM Campaign traffic query string checks - End */
	
		
	// Persist the Hide Cookie Consent query param
	if( isset($_GET['no_c']) ) {
        $url = add_query_arg( array('no_c' => $_GET['no_c']), $url );
    }
	
		
	// Persist the MonetizeMore Sidebar Widget 2 param
	if( isset($_GET['smms']) ) {
        $url = add_query_arg( array('smms' => $_GET['smms']), $url );
    }
	
	
	// Persist the Redesign query param
	if( isset($_GET['test_redesign']) ) {
        $url = add_query_arg( array('test_redesign' => $_GET['test_redesign']), $url );
    }
		
	
	// Persist the 'Tan media footer Tag'  param
	if( isset($_GET['itmft']) ) {
        $url = add_query_arg( array('itmft' => $_GET['itmft']), $url );
    }
	
	// Persist the  MM video sidebar tag  param
	if( isset($_GET['test_mm_video_tag']) ) {
        $url = add_query_arg( array('test_mm_video_tag' => $_GET['test_mm_video_tag']), $url );
    }
	
	// Persist the new Nativo/Tan media video tag
	if( isset($_GET['test_nativo_tag']) ) {
        $url = add_query_arg( array('test_nativo_tag' => $_GET['test_nativo_tag']), $url );
    }
	
	// Persist the MM before pagination param
	if( isset($_GET['smmap']) ) {
        $url = add_query_arg( array('smmap' => $_GET['smmap']), $url );
    }
	
	// Persist the AMP query param
	if( isset($_GET['amp']) ) {
        $url = add_query_arg( array('amp' => $_GET['amp']), $url );
    }
	
	// Persist the Freewheel query param
	if( isset($_GET['sfsv']) ) {
        $url = add_query_arg( array('sfsv' => $_GET['sfsv']), $url );
    }
	
	// Persist the Tan Media tags
    if( isset($_GET['stmt']) ) {
        $url = add_query_arg( array('stmt' => $_GET['stmt']), $url );
    }

    // Persist the Taboola Below Articles tag
    if( isset($_GET['tba']) ) {
        $url = add_query_arg( array('tba' => $_GET['tba']), $url );
    }

    // No numbers in pagination
    if( isset($_GET['nnp']) ) {
        $url = add_query_arg( array('nnp' => $_GET['nnp']), $url );
    }

    // Added two adsense units 300x250 (desktop + tablet)
    if( isset($_GET['tau']) ) {
        $url = add_query_arg( array('tau' => $_GET['tau']), $url );
    }

    // Make buttons from pagination bigger and yellow
    if( isset($_GET['bpy']) ) {
        $url = add_query_arg( array('bpy' => $_GET['bpy']), $url );
    }
	
	// Persist the dummy parameter
    if( isset($_GET['dmy']) ) {
        $url = add_query_arg( array('dmy' => $_GET['dmy']), $url );
    }
	
	// Persist the "hide Taboola Right Sidebar widget" query param
    if( isset($_GET['htsw']) ) {
        $url = add_query_arg( array('htsw' => $_GET['htsw']), $url );
    }
	
	// Persist the "switchHardcodedAdxWithDFPunit" query param
    if( isset($_GET['shad']) ) {
        $url = add_query_arg( array('shad' => $_GET['shad']), $url );
    }
	
	// Persist the switchHardcodedAdxWithDFPunit_control param
	if( isset($_GET['shadco']) ) {
        $url = add_query_arg( array('shadco' => $_GET['shadco']), $url );
    }
	
	
	/* Persist the Dit Test - variation parameters - START */
	// "htmlized" variation
	if( isset($_GET['htmlized']) ) {
        $url = add_query_arg( array('htmlized' => $_GET['htmlized']), $url );
    }	
	// "control" variation
	if( isset($_GET['control']) ) {
        $url = add_query_arg( array('control' => $_GET['control']), $url );
    }
	// "htmlized and defered" variation
	if( isset($_GET['htmlized_defered']) ) {
        $url = add_query_arg( array('htmlized_defered' => $_GET['htmlized_defered']), $url );
    }
	// "defered" variation
	if( isset($_GET['defered']) ) {
        $url = add_query_arg( array('defered' => $_GET['defered']), $url );
    }
	/* Persist the Dit Test - variation parameters - END */

    /* Old template vs New template - start */
    if( isset($_GET['ovn']) ) {
        $url = add_query_arg( array('ovn' => $_GET['ovn']), $url );
    }
    /* Old template vs New template - end */

    /* Ad unit runs AdX + HBS - start */
    if( isset($_GET['adxhbs']) ) {
        $url = add_query_arg( array('adxhbs' => $_GET['adxhbs']), $url );
    }
    /* Ad unit runs AdX + HBS - end */

	
	// Persist the 'dfp' query param used for MonetizeMore tracking
	if( isset($_GET['dfp']) ) {
        $url = add_query_arg( array('dfp' => $_GET['dfp']), $url );
    }
	// Persist the 't' query param used for MonetizeMore tracking of test variations
	if( isset($_GET['t']) ) {
        $url = add_query_arg( array('t' => $_GET['t']), $url );
    }
	
	return esc_url($url);
}

// Ad units runs AdX + HBS
function switchHarcodedAdsenseWithAdxAndHbs() {
    if( isset($_GET['adxhbs']) && $_GET['adxhbs'] == '1' ) {
        return true;
    }
    return false;
}

// added on 11.04.2019 in order to switch between the hard-coded above pagination Adx unit with a the new hard-coded one ( used as control for DFP test )
function switchHardcodedAdxWithDFPunit_control() {
	
	// returns true when to inject the new hard-coded Adx pagination unit
	if( isset($_GET['shadco']) && $_GET['shadco'] == '1' ) {
		return true;
	}
	return false;
}

// added on 04.04.2019 in order to switch between the hard-coded above pagination Adx unit with a DFP one
function switchHardcodedAdxWithDFPunit() {
	
	// returns true when to inject the DFP adx unit
	if( isset($_GET['shad']) && $_GET['shad'] == '1' ) {
		return true;
	}
	return false;
}

// added on 25.03.2019 in order to hide the Taboola Right Sidebar widget
function hideTaboolaSidebarWidget() {
	
	// returns true when we hide the Taboola Right Sidebar widget
	if( isset($_GET['htsw']) && $_GET['htsw'] == '1' ) {
		return true;
	}
	return false;
}

// added on 03.04.2019 in order to check if it's a "dummy" request
function isDummyABtest() {
	
	// returns true if it's a "dummy" request
	if( isset($_GET['dmy']) && $_GET['dmy'] == '1' ) {
		return true;
	}
	return false;
}

// added on 09.01.2019 in order to inject the Tan Media tags
function showTanMediaTags() {
	
	// returns true when we inject the Tan Media tags
	if( isset($_GET['stmt']) && $_GET['stmt'] == '1' ) {
		return true;
	}
	return false;
}
// added on 11.01.2019 in order to inject Taboola - Below articles
function showTaboolaBelowArticlesTags() {

    // returns true when we inject the Taboola - Below Articles
    if( isset($_GET['tba']) && $_GET['tba'] == '1' ) {
        return true;
    }
    return false;
}
// added on 16.01.2019 in order to hide numbers on pagination
function hideNumbersOnPagination() {

    // returns true when we hide numbers on pagination
    if( isset($_GET['nnp']) && $_GET['nnp'] == '1' ) {
        return true;
    }
    return false;
}
// added on 17.01.2019 in order to add two adsense units 300x250 (desktop + tablet)
function addTwoAdsenseUnits() {
    if( isset($_GET['tau']) && $_GET['tau'] == '1' ) {
        return true;
    }
    return false;
}
// added on 18.01.2019 in order to make buttons from pagination bigger and yellow
function makeButtonsYellow() {
    if( isset($_GET['bpy']) && $_GET['bpy'] == '1' ) {
        return true;
    }
    return false;
}
// added on 20.12.2018 in order to inject the Freewheel Sidebar Video 
function showFreewheelSidebarVideo() {
	
	// returns true when we inject the Freewheel Sidebar Video 
	if( isset($_GET['sfsv']) && $_GET['sfsv'] == '1' ) {
		return true;
	}
	return false;
}
// added on 18.12.2018 in order to inject any of the GA custom dimensions used for AB/Split testing
function injectTestingCustomDimensionsGA() {
	$custom_dimensions = '';
	
	// check if we inject the 'VWO Test Id 1' custom dimension
	if( isset($_GET['sbtest']) ) {
		$custom_dimensions .= "ga('set', 'dimension1', 'sbtest=".urlencode($_GET['sbtest'])."'); ";
	}
	
	// check if we inject the 'VWO Test Id 2' custom dimension
	if( isset($_GET['sbtest2']) ) {
		$custom_dimensions .= "ga('set', 'dimension2', 'sbtest2=".urlencode($_GET['sbtest2'])."'); ";
	}
	
	// check if we inject the 'VWO Test Id 3' custom dimension
	if( isset($_GET['sbtest3']) ) {
		$custom_dimensions .= "ga('set', 'dimension3', 'sbtest3=".urlencode($_GET['sbtest3'])."'); ";
	}
	
	// check if we inject the 'VWO Test Id 4' custom dimension
	if( isset($_GET['sbtest4']) ) {
		$custom_dimensions .= "ga('set', 'dimension4', 'sbtest4=".urlencode($_GET['sbtest4'])."'); ";
	}
	
	// check if we inject the 'VWO Test Id 5' custom dimension
	if( isset($_GET['sbtest5']) ) {
		$custom_dimensions .= "ga('set', 'dimension5', 'sbtest5=".urlencode($_GET['sbtest5'])."'); ";
	}
	
	return $custom_dimensions;
}


// added on 03.12.2018 in order to switch between the hard-coded Adsense unit before the pagination and the MM ones 
function switchAdUnitBeforePaginationMM() {
	
	// returns true when we inject the MM Ad unit before pagination
	if( isset($_GET['smmap']) && $_GET['smmap'] == '1' ) {
		return true;
	}
	return false;
}


// added on 29.11.2018 in order to inject the new Nativo/Tan media video tag
function testNativoVideoTag() {
	
	// returns true when we inject the tag
	if( isset($_GET['test_nativo_tag']) && $_GET['test_nativo_tag'] == '1' ) {
		return true;
	}
	return false;
}

// added on 22.11.2018 in order to inject the MM video sidebar tag
function testMmVideoTag() {
	
	// returns true when we inject the tag
	if( isset($_GET['test_mm_video_tag']) && $_GET['test_mm_video_tag'] == '1' ) {
		return true;
	}
	return false;
}


// added on 13.11.2018 in order to inject the 'Tan media footer Tag' 
function injectTanMediaFooterTag() {
	
	// returns true when we inject the 'Tan media footer Tag' 
	if( isset($_GET['itmft']) && $_GET['itmft'] == '1' ) {
		return true;
	}
	return false;
}

// added on 02.10.2018 in order to show the MonetizeMore Sidebar Widget 2 ( replace the old one with the new one in the sidebar )
function showMMSidebar2() {
	
	// returns true when we show the MonetizeMore Sidebar Widget 2
	if( isset($_GET['smms']) && $_GET['smms'] == '1' ) {
		return true;
	}
	return false;
}

// old template vs new template
function oldVsNew(){
    if( isset($_GET['ovn']) && $_GET['ovn'] == '1' ) {
        return true;
    }
    return false;
}
// added on 04.12.2017 in order to check if it's a UTM Campaign visitor - it is used to show the Applaud Media combined tag
function isCampaignTraffic() {
	// removed all query string checks on 11.04.2018
	//return false; back on 17.04.2018
	
	// returns true when 'utm_source' is 'YG', 'yahoo', 'zemanta', 'outbrain' , 'facebook' or 'taboola' ( regardless of case )
	// removed 'facebook' from campaign check on 29.05.2018 
	if( isset($_GET['utm_source']) && 		
		( 
			strtolower( $_GET['utm_source'] ) == 'yg' || strtolower( $_GET['utm_source'] ) == 'yahoo' || strtolower( $_GET['utm_source'] ) == 'zemanta' ||
			strtolower( $_GET['utm_source'] ) == 'outbrain' || strtolower( $_GET['utm_source'] ) == 'taboola' 
			/* || strtolower( $_GET['utm_source'] ) == 'facebook' */			
		)		
		) {
		return true;
	}
	// returns true when the 'utm_source' replacemen - 'tict' - is '1'
	if( isset($_GET['tict']) && $_GET['tict'] == '1' ) {
		return true;
	}
	
	return false;
}

add_action('single_pagination', 'custom_single_pagination');
function custom_single_pagination() {
	global $page, $pages, $multipage;
	
	$output = '';
	$cc_output = '';
	$cc = array(); // custom classes
	
	// 15.06.2018 - pagination switch ended - we'll use pag_var_5 from now on
	$pagination_variation_class = 'pag_var_5';
	
	$output .= '<div id="csp_btns" class="'.$pagination_variation_class.'" ><ul class="clearfix">';
		if( $multipage ) {
			$cc[] = "cc_multipage";
			if( $page != 1 ) {
				$output .= gdm_wp_link_pages(array(
					'before' => '<li class="prev_button">',
					'link_before' => '<span class="visible-s-ib">&laquo; </span>',
					'link_after' => '',
					'after' => '</li>',
					'previouspagelink' => 'Prev',
					'nextpagelink' => '',
					'next_or_number' => 'next',
					'next_or_prev' => 'prev',
					'echo' => false,
				));
			} else {
				$cc[] = "cc_firstpage";
				$output .= '<li class="no_prev"></li>';
			}

			$output .= '<li class="numb_page">' . $page . ' of '. count($pages) . '</li>';

			if( $page != count($pages) ) {
				$output .= gdm_wp_link_pages(array(
					'before' => '<li class="next_button">',
					'link_before' => '',
					'link_after' => '<span class="visible-s-ib"> &raquo;</span>',
					'after' => '</li>',
					'previouspagelink' => '',
					'nextpagelink' => 'Next',
					'next_or_number' => 'next',
					'next_or_prev' => 'next',
					'echo' => false,
				));
			} else {
				$cc[] = "cc_lastpage";
				$output .= _get_next_post_link();
			}
		} else {
			$cc[] = "cc_singlepage";
			$output .= _get_next_post_link();
		}
	$output .= '</ul></div>';
	
	// display an invisible element which has set custom classes with pagination information
	if( count($cc) > 0 ) {
		$cc_output .= '<div id="custom_classes" style="display:none!important" class="';
			foreach($cc as $class) {
				$cc_output .= $class . " ";
			}
		$cc_output .= '" data-total_pages="' . count($pages) . '"></div>';
	}
	
	echo $cc_output . $output;
}
// add_filter( 'wp_link_pages_link', '_post_pagination_link');
function _post_pagination_link( $link ) {
	if (strpos($link, '><') !== false) {
		return;
	}
   return '<li>'.$link.'</li>';
}
// function to be used al query filter in order to get multipage posts or single page posts ( ie: posts that contain or don't contain '<!--nextpage-->' )
// PS: it's not used anymore - kept as reference 
function gdm_multipage_posts_filter( $where, &$wp_query )
{
    global $wpdb;
    if ( $wp_query->get( 'is_multipage_post' ) &&  $wp_query->get( 'is_multipage_post' ) == '1' ) {
        $where .= ' AND ( ' . $wpdb->posts . '.post_content LIKE \'%--nextpage-->%\' ) ';
    } else {
		$where .= ' AND ( ' . $wpdb->posts . '.post_content NOT LIKE \'%--nextpage-->%\' ) ';
	}
    return $where;
}

function _get_next_post_link() {
	global $post;
	/* custom conditions for the next post link for certain articles: ( added on 22.02.2018 ) */
	/*
		/40-cars-that-will-last-more-than-250000-miles/ (ID: 9759 ) 	--> 	/50-cars-that-are-plummeting-in-value/ (ID: 9817 )	
		/50-cars-that-are-plummeting-in-value/ 			(ID: 9817 ) 	--> 	/20-of-the-best-suvs-nobody-is-buying/ (ID: 8967 )
		/20-cars-that-are-plummeting-in-value-3/ 		(ID: 8014 ) 	--> 	/20-of-the-best-suvs-nobody-is-buying/ (ID: 8967 )
		/20-of-the-best-suvs-nobody-is-buying/ 			(ID: 8967 ) 	--> 	/50-cars-that-are-plummeting-in-value/ (ID: 9817 )
	*/
	if( $post->ID == 9759 ) {
		$link = get_permalink( 9817 );
	} elseif( $post->ID == 9817 ) {
		$link = get_permalink( 8967 );
	} elseif( $post->ID == 8014 ) {
		$link = get_permalink( 8967 );
	} elseif( $post->ID == 8967 ) {
		$link = get_permalink( 9817 );		
	}
	// check if the article is still available
	if( isset($link) && $link ) {
		$output = '<li><a href="'._set_custom_args($link).'">Next post<span class="visible-s-ib"> &raquo;</span></a></li>';
		return $output;	
	}
	/* End custom condition for the next post link */
	
	
	$next_post = get_adjacent_post(false, '', false);
	if( isset($next_post->ID) ) {
		$meta_val = get_post_meta( $next_post->ID, "hidden_post", true);
	}
	
	$output = "";
	$output .= '<li><a href="';
		
	if( isset($next_post->ID) && is_a( $next_post, 'WP_Post' ) && !$meta_val ) {
		// next post if exists
		$link = get_permalink( $next_post->ID );
	} else {
		// random post
		$random_post = new WP_Query(array(
			'pagination'             => false,
			'post_type'              => 'post',
			'posts_per_page'         => '1',
			'orderby'                => 'rand',
			'tax_query' => array(
				array(
					'taxonomy'  => 'category',
					'field'     => 'slug',
					'terms'     => 'uncategorized',
					'operator'  => 'NOT IN'
				),
			),
			'meta_query' => array(
				array(
					'key'       => 'hidden_post',
					'compare'   => 'NOT EXISTS',
					'type'      => 'CHAR',
				),
			),
		));
		
		$link = get_permalink( $random_post->posts[0]->ID );
	}
	
	$output .= _set_custom_args($link);
	
	$output .= '">Next post<span class="visible-s-ib"> &raquo;</span></a></li>';
	
	return $output;
	
	/* New implementation - is buggy : needs some more query conditions in order to get valid posts ( this also returns revisions or posts that shouldn't be visible )- Kept as refference
	global $post, $pages, $multipage;
	if( $multipage ) {
		// if we're on a multipage article, the next article should be also a multipage one
		add_filter( 'posts_where', 'gdm_multipage_posts_filter', 10, 2 );
		$custom_posts = new WP_Query(array(
			'pagination'             => false,
			'is_multipage_post'		 => '1',
			'post_type'              => 'post',
			'posts_per_page'         => '1',
			'orderby'                => 'rand',
			'post__not_in' 			 => array( $post->ID ),
			'tax_query' => array(
				array(
					'taxonomy'  => 'category',
					'field'     => 'slug',
					'terms'     => 'uncategorized',
					'operator'  => 'NOT IN'
				),
			),
			'meta_query' => array(
				array(
					'key'       => 'hidden_post',
					'compare'   => 'NOT EXISTS',
					'type'      => 'CHAR',
				),
			),
		));
		remove_filter( 'posts_where', 'gdm_multipage_posts_filter', 10, 2 );
		$next_post = $custom_posts->posts[0];
	} else {
		// if we're on a non-multipage article, the next article should be also a non-multipage one
		add_filter( 'posts_where', 'gdm_multipage_posts_filter', 10, 2 );
		$custom_posts = new WP_Query(array(
			'pagination'             => false,
			// 's'              		 => '%<!-- next page -->%',
			'is_multipage_post'		 => '0',
			'post_type'              => 'post',
			'posts_per_page'         => '1',
			'orderby'                => 'rand',
			'post__not_in' 			 => array( $post->ID ),
			'tax_query' => array(
				array(
					'taxonomy'  => 'category',
					'field'     => 'slug',
					'terms'     => 'uncategorized',
					'operator'  => 'NOT IN'
				),
			),
			'meta_query' => array(
				array(
					'key'       => 'hidden_post',
					'compare'   => 'NOT EXISTS',
					'type'      => 'CHAR',
				),
			),
		));
		remove_filter( 'posts_where', 'gdm_multipage_posts_filter', 10, 2 );
		$next_post = $custom_posts->posts[0];
	}
	$link = get_permalink( $next_post->ID );
	$output = "";
	$output .= '<li><a href="';
	
	$output .= _set_custom_args($link);
	
	$output .= '">Next post<span class="visible-s-ib"> &raquo;</span></a></li>';
	
	return $output;
	*/	
}
/* CUSTOM SINGLE PAGINATION - end */

// REWRITE CORE WP FUNCTIONS - start
/**
 * The formatted output of a list of pages.
 *
 * Displays page links for paginated posts (i.e. includes the <!--nextpage-->.
 * Quicktag one or more times). This tag must be within The Loop.
 *
 * @since 1.2.0
 *
 * @param string|array $args {
 *     Optional. Array or string of default arguments.
 *
 *     @type string       $before           HTML or text to prepend to each link. Default is `<p> Pages:`.
 *     @type string       $after            HTML or text to append to each link. Default is `</p>`.
 *     @type string       $link_before      HTML or text to prepend to each link, inside the `<a>` tag.
 *                                          Also prepended to the current item, which is not linked. Default empty.
 *     @type string       $link_after       HTML or text to append to each Pages link inside the `<a>` tag.
 *                                          Also appended to the current item, which is not linked. Default empty.
 *     @type string       $next_or_number   Indicates whether page numbers should be used. Valid values are number
 *                                          and next. Default is 'number'.
 *     @type string       $separator        Text between pagination links. Default is ' '.
 *     @type string       $nextpagelink     Link text for the next page link, if available. Default is 'Next Page'.
 *     @type string       $previouspagelink Link text for the previous page link, if available. Default is 'Previous Page'.
 *     @type string       $pagelink         Format string for page numbers. The % in the parameter string will be
 *                                          replaced with the page number, so 'Page %' generates "Page 1", "Page 2", etc.
 *                                          Defaults to '%', just the page number.
 *     @type int|bool     $echo             Whether to echo or not. Accepts 1|true or 0|false. Default 1|true.
 * }
 * @return string Formatted output in HTML.
 */
function gdm_wp_link_pages( $args = '' ) {
	$defaults = array(
		'before'           => '<p>' . __( 'Pages:' ),
		'after'            => '</p>',
		'link_before'      => '',
		'link_after'       => '',
		'next_or_number'   => 'number',
		'separator'        => ' ',
		'nextpagelink'     => __( 'Next page' ),
		'previouspagelink' => __( 'Previous page' ),
		'pagelink'         => '%',
		'next_or_prev'     => 'next',
		'echo'             => 1
	);

	$params = wp_parse_args( $args, $defaults );

	/**
	 * Filter the arguments used in retrieving page links for paginated posts.
	 *
	 * @since 3.0.0
	 *
	 * @param array $params An array of arguments for page links for paginated posts.
	 */
	$r = apply_filters( 'wp_link_pages_args', $params );

	global $page, $numpages, $multipage, $more;

	$output = '';
	if ( $multipage ) {
		if ( 'number' == $r['next_or_number'] ) {
			$output .= $r['before'];
			for ( $i = 1; $i <= $numpages; $i++ ) {
				$link = $r['link_before'] . str_replace( '%', $i, $r['pagelink'] ) . $r['link_after'];
				if ( $i != $page || ! $more && 1 == $page ) {
					$link = _gdm_wp_link_page( $i ) . $link . '</a>';
				}
				/**
				 * Filter the HTML output of individual page number links.
				 *
				 * @since 3.6.0
				 *
				 * @param string $link The page number HTML output.
				 * @param int    $i    Page number for paginated posts' page links.
				 */
				$link = apply_filters( 'wp_link_pages_link', $link, $i );

				// Use the custom links separator beginning with the second link.
				$output .= ( 1 === $i ) ? ' ' : $r['separator'];
				$output .= $link;
			}
			$output .= $r['after'];
		} elseif ( $more ) {
			$output .= $r['before'];
			$prev = $page - 1;
			$next = $page + 1;
			
			if($r['next_or_prev'] == 'prev') {
				if ( $prev ) {
					$link = _gdm_wp_link_page( $prev ) . $r['link_before'] . $r['previouspagelink'] . $r['link_after'] . '</a>';

					/** This filter is documented in wp-includes/post-template.php */
					$output .= apply_filters( 'wp_link_pages_link', $link, $prev );
				}
			} elseif($r['next_or_prev'] == 'next') {
				if ( $next <= $numpages ) {
					if ( $prev ) {
						$output .= $r['separator'];
					}
					$link = _gdm_wp_link_page( $next ) . $r['link_before'] . $r['nextpagelink'] . $r['link_after'] . '</a>';

					/** This filter is documented in wp-includes/post-template.php */
					$output .= apply_filters( 'wp_link_pages_link', $link, $next );
				}
			}
			$output .= $r['after'];
		}
	}

	/**
	 * Filter the HTML output of page links for paginated posts.
	 *
	 * @since 3.6.0
	 *
	 * @param string $output HTML output of paginated posts' page links.
	 * @param array  $args   An array of arguments.
	 */
	$html = apply_filters( 'wp_link_pages', $output, $args );

	if ( $r['echo'] ) {
		echo $html;
	}
	return $html;
}
/**
 * Helper function for wp_link_pages().
 *
 * @since 3.1.0
 * @access private
 *
 * @param int $i Page number.
 * @return string Link.
 */
function _gdm_wp_link_page( $i ) {
	global $wp_rewrite;
	$post = get_post();

	if ( 1 == $i ) {
		$url = get_permalink();
	} else {
		if ( '' == get_option('permalink_structure') || in_array($post->post_status, array('draft', 'pending')) )
			$url = add_query_arg( 'page', $i, get_permalink() );
		elseif ( 'page' == get_option('show_on_front') && get_option('page_on_front') == $post->ID )
			$url = trailingslashit(get_permalink()) . user_trailingslashit("$wp_rewrite->pagination_base/" . $i, 'single_paged');
		else
			$url = trailingslashit(get_permalink()) . user_trailingslashit($i, 'single_paged');
	}

	if ( is_preview() ) {
		$url = add_query_arg( array(
			'preview' => 'true'
		), $url );

		if ( ( 'draft' !== $post->post_status ) && isset( $_GET['preview_id'], $_GET['preview_nonce'] ) ) {
			$url = add_query_arg( array(
				'preview_id'    => wp_unslash( $_GET['preview_id'] ),
				'preview_nonce' => wp_unslash( $_GET['preview_nonce'] )
			), $url );
		}
	}
	
	$url = _set_custom_args($url);

	return '<a href="' . $url . '">';
}
// REWRITE CORE WP FUNCTIONS - end

/* CUSTOM IMAGE SIZES - start */
if ( function_exists( 'add_theme_support' ) ) {
  add_theme_support( 'post-thumbnails' );
}

add_action( 'after_setup_theme', 'custom_img_sizes' );
function custom_img_sizes() {
	// homepage
	add_image_size('home-featured-big', 720);
	add_image_size('home-featured-small', 360);
	add_image_size('post-item', 360);
}
/* CUSTOM IMAGE SIZES - end */

function hideContentAd() {
	global $page;
	 
	if(is_single() && isset($_GET['__noca']) && $page == 1) {
		return true;
	} else {
		return false;
	}
}
function hideRevContent() {
	global $page;
	 
	if(is_single() && isset($_GET['__norc']) && $page == 1) {
		return true;
	} else {
		return false;
	}
}

/* AJAX URL & NONCE - start */
add_action('wp_head', 'loadAjaxUrl');
function loadAjaxUrl() {	
	$output = '';
	$output .= '<script type="text/javascript">';
		$output .= 'var ajaxurl = "' . admin_url('admin-ajax.php') . '";';
		$output .= 'var ajaxnonce = "' . wp_create_nonce( "ajax_hi4kZ9D00P9g3fO" ) . '";';
	$output .= '</script>';
	
	echo $output;
}
/* AJAX URL & NONCE - end */

/* AJAX LOAD COOKIE CONSENT - start */
add_action("wp_ajax_loadCookieConsentAction", "loadCookieConsentAjax");
add_action("wp_ajax_nopriv_loadCookieConsentAction", "loadCookieConsentAjax");
function loadCookieConsentAjax(){
    check_ajax_referer( 'ajax_hi4kZ9D00P9g3fO', 'security' );
	
	$timer = 0;
	if(isset($_POST['cct'])) {
		$timer =(int)$_POST['cct'];
	}
	sleep($timer);
    // $url = wp_get_referer();
    $o = '';
    
	header('Content-Type: text/html; charset=utf-8');
    
	 $o .= '<script type="text/javascript">';
		 $o .= 'window.cookieconsent_options = {';
			 $o .= '"message": "This website uses cookies to ensure you get the best experience on our website",';
			 $o .= '"dismiss": "Got it!",';
			 $o .= '"learnMore": "More info",';
			 $o .= '"link":"/privacy-policy/",';
			 $o .= '"theme":"light-top"';
		 $o .= '};';
	 $o .= '</script>';
	 $o .= '<script type="text/javascript" async src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>';
		    
	echo $o;
	
	wp_die();
}
/* AJAX LOAD COOKIE CONSENT - end */

/* CUSTOM REDIRECT FOR POSTS - start */
function customRedirectTo() {
    global $post;
    
    $redirectArr = array(
        // /15-most-expensive-cars-in-the-world => /38-most-expensive-cars-in-the-world/
        6398 => 7011,
    );
    
    if( isset($post) && is_singular('post') && array_key_exists($post->ID, $redirectArr) && get_the_permalink($redirectArr[$post->ID]) ) {
        wp_redirect( get_the_permalink($redirectArr[$post->ID]), 302 );
        exit();
    }
}
// add_action( 'template_redirect', 'customRedirectTo' );
/* CUSTOM REDIRECT FOR POSTS - end */


/* GDM function to check if the post is the last page of a multipage post */
function isLastPageOnMultipagePost() {
	global $multipage , $page , $pages;
	$theResponse = false;
	// check if globals were set
	if( isset($multipage) && isset($page) && isset($pages) ) {
		// check if they are not null
		if( !is_null($multipage) && !is_null($page) && !is_null($pages) ) {
			// final check 
			if( $multipage && $page == count($pages) ) {
				$theResponse = true;
			}
		}
	}
	return $theResponse;
}
/* GDM function - end */
/* GDM function to insert adsense ads in WP post content */
function create_adsense_shortcode( $atts ) {
	extract(shortcode_atts(array(
		'ad' => '11',
		'mobile' => '300px|250px',
		'tablet' => '610px|90px',
		'desktop' => '100%|90px'
	), $atts));
	
	$ad_slots = [
		'1' => '6940051765', // "buzzdrives.com - snippet ad 1"
		'2' => '8416784965', // "buzzdrives.com - snippet ad 2"
		'3' => '9893518163', // "buzzdrives.com - snippet ad 3"
		'4' => '3413005762', // "buzzdrives.com - snippet ad 4"
		'5' => '4889738963', // "buzzdrives.com - snippet ad 5"
		'6' => '6366472160', // "buzzdrives.com - snippet ad 6"
		'7' => '7843205360', // "buzzdrives.com - snippet ad 7"
		'8' => '9319938563', // "buzzdrives.com - snippet ad 8"
		'9' => '1796671760', // "buzzdrives.com - snippet ad 9"
		'10' => '4750138169', // "buzzdrives.com - snippet ad 10"
		
		'11' => '8563247367', // default "buzzbreed.com - before pagination - m"
	];
	if( isset($ad_slots[$ad]) ) {
		$adsense_slot = $ad_slots[$ad];
	} else {
		// default adsense_slot "buzzbreed.com - before pagination - m"
		$adsense_slot = '8563247367';
	}
	
	// we make sure we close the WP beggining and end <p> tags as they are messing up the immages 
	$ad = '
	</p>
	<div class="adsense" style="text-align:center;">
		<script>
			(function(){
				var adsense_client = "ca-pub-6449643190876495";
				var adsense_classes = "adsbygoogle";
				var adsense_slot = "'.$adsense_slot.'";
				var adsense_size = ["300px", "250px"];
				
				if ( winWid >= 768 && winWid < 992 ) {
	';
	if( $tablet != 'none' && strpos($tablet, '|') !== false ) {	
		$t_sizes = explode('|' , $tablet);
		$ad .= '
		adsense_slot = "'.$adsense_slot.'";
		adsense_size = ["'.$t_sizes[0].'", "'.$t_sizes[1].'"];
		
		document.write ( \'<div class="adsense_header_title">Advertisement</div>\');
		
		document.write (
		\'<ins class="\' + adsense_classes + \'" style="display:inline-block;width:\' 
			+ adsense_size[0] + \';height:\' 
			+ adsense_size[1] + \'" data-ad-client="\' 
			+ adsense_client + \'" data-ad-slot="\' 
			+ adsense_slot + \'"></ins>\'
		);
		';
	}
	$ad .= 	' } else if( winWid < 768 ) { ';
	if( $mobile != 'none' && strpos($mobile, '|') !== false ) {
		$m_sizes = explode('|' , $mobile);
		$ad .= '
		adsense_slot = "'.$adsense_slot.'";
		adsense_size = ["'.$m_sizes[0].'", "'.$m_sizes[1].'"];
		
		document.write ( \'<div class="adsense_header_title">Advertisement</div>\');
		
		document.write (
		\'<ins class="\' + adsense_classes + \'" style="display:inline-block;width:\' 
			+ adsense_size[0] + \';height:\' 
			+ adsense_size[1] + \'" data-ad-client="\' 
			+ adsense_client + \'" data-ad-slot="\' 
			+ adsense_slot + \'"></ins>\'
		);
		';
	}
	$ad .= 	' } else { ';
	if( $desktop != 'none' && strpos($desktop, '|') !== false ) {
		$d_sizes = explode('|' , $desktop);
		$ad .= '	
		adsense_slot = "'.$adsense_slot.'";
		adsense_size = ["'.$d_sizes[0].'", "'.$d_sizes[1].'"];
		
		document.write ( \'<div class="adsense_header_title">Advertisement</div>\');
		
		document.write (
		\'<ins class="\' + adsense_classes + \'" style="display:inline-block;width:\' 
			+ adsense_size[0] + \';height:\' 
			+ adsense_size[1] + \'" data-ad-client="\' 
			+ adsense_client + \'" data-ad-slot="\' 
			+ adsense_slot + \'"></ins>\'
		);
		';
	}
	$ad .= ' }
			})();
		</script>
	</div>
	<p>
	';
		
	return $ad;
}
add_shortcode('adsense', 'create_adsense_shortcode');
/* GDM function - end */
/* GDM function - add the GA experiments code to the page header */
function add_ga_experiment() {
	global $post;
	
	if( isset($post) && isset($post->ID) ) {
		if( function_exists('get_field') ) {
			echo get_field( 'ga_code' , $post->ID );
		}
	}
}
add_action('wp_head' , 'add_ga_experiment');
/* GDM function - end */
/* GDM function - check if the post has the 'custom_widget' field and returns it's value - false otherwise */
function has_custom_widget() {
	global $post;
	$response = false;
	if( function_exists('get_field') ) {
		$response = get_field( 'custom_widget' , $post->ID );
		if( $response == '' ) {
			$response = false;
		}
	}
	
	return $response;
}
/* GDM function - end */

/* GDM function to insert DFP prebidjs ads in WP post content */
function create_dfp_shortcode( $atts ) {
	extract(shortcode_atts(array(
		'ad_unit' => 'BD_in_content_desktop'
	), $atts));
	
	/*
	Ad units defined inn DFP dashboard for in content widgets:
	BD_in_content_desktop
	BD_in_content_desktop_2
	// BD_in_content_mobile
	// BD_in_content_mobile_2
	*/	
	
	
	if( $ad_unit != 'BD_in_content_desktop' && $ad_unit != 'BD_in_content_desktop_2' ) {
		$ad_unit = 'BD_in_content_desktop';
	}
	
	$widget = '
		<div id="div_'.$ad_unit.'">
			<script type="text/javascript">
				googletag.cmd.push(function() { googletag.display("div_'.$ad_unit.'"); });
			</script>
		</div>
	';
		
	return $widget;
}
add_shortcode('dfpwidget', 'create_dfp_shortcode');
/* GDM function - end */

/* GDM function to insert Stickyadstv widgets in WP post content */
function create_stickyads_shortcode( $atts ) {
	/*
	extract(shortcode_atts(array(
		'var_name' => 'default_value'
	), $atts));
	*/		
	
	$widget = "<script type='text/javascript'>
			if( isMobile() ) {
				
				(function() {
				var st = document.createElement('script'); st.type = 'text/javascript'; st.async = true;
				st.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + '//cdn.stickyadstv.com/prime-time/intext-roll.min.js?zone=3045233&lang=en&p=0';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(st, s);
				})();

				
			} else {
				
				(function() {
				var st = document.createElement('script'); st.type = 'text/javascript'; st.async = true;
				st.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + '//cdn.stickyadstv.com/prime-time/intext-roll.min.js?zone=3045249&lang=en&p=0';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(st, s);
				})();
				
			}
			</script>";
	
	return $widget;
}
add_shortcode('stickyads', 'create_stickyads_shortcode');
/* GDM function - end */

/* GDM function to insert NativeAds widgets in WP post content */
function create_nativeads_shortcode( $atts ) {
	/*
	extract(shortcode_atts(array(
		'var_name' => 'default_value'
	), $atts));
	*/		
	
	$widget = "
	<script>
		if( !isMobile() ) {
			
			/* desktop widget */
			(function()
			{
			  var lkqdSettings = {
				pid: 314,
				sid: 334019,
				playerContainerId: 'dnative',
				playerId: '',
				playerWidth: '640',
				playerHeight: '360',
				execution: 'outstream',
				placement: 'incontent',
				playInitiation: 'auto',
				volume: 0,
				trackImp: '',
				trackClick: '',
				custom1: '',
				custom2: '',
				custom3: '',
				pubMacros: '',
				dfp: false,
				lkqdId: new Date().getTime().toString() + Math.round(Math.random()*1000000000).toString()
			  };

			  var lkqdVPAID;
			  var creativeData = '';
			  var environmentVars = { slot: document.getElementById(lkqdSettings.playerContainerId), videoSlot: document.getElementById(lkqdSettings.playerId), videoSlotCanAutoPlay: true, lkqdSettings: lkqdSettings };

			  function onVPAIDLoad()
			  {
				lkqdVPAID.subscribe(function() { lkqdVPAID.startAd(); }, 'AdLoaded');
				lkqdVPAID.subscribe(function() { lkqdVPAID.pauseAd(); }, 'AdNotViewable');
				lkqdVPAID.subscribe(function() { lkqdVPAID.resumeAd(); }, 'AdViewable');
			  }

			  var vpaidFrame = document.createElement('iframe');
			  vpaidFrame.id = lkqdSettings.lkqdId;
			  vpaidFrame.name = lkqdSettings.lkqdId;
			  vpaidFrame.style.display = 'none';
			  var vpaidFrameLoaded = function() {
				vpaidLoader = vpaidFrame.contentWindow.document.createElement('script');
				vpaidLoader.src = 'https://ad.lkqd.net/vpaid/formats.js?pid=314&sid=334019';
				vpaidLoader.onload = function() {
				  lkqdVPAID = vpaidFrame.contentWindow.getVPAIDAd();
				  onVPAIDLoad();
				  lkqdVPAID.handshakeVersion('2.0');
				  lkqdVPAID.initAd(lkqdSettings.playerWidth, lkqdSettings.playerHeight, 'normal', 600, creativeData, environmentVars);
				};
				vpaidFrame.contentWindow.document.body.appendChild(vpaidLoader);
			  };
			  vpaidFrame.onload = vpaidFrameLoaded;
			  vpaidFrame.onerror = vpaidFrameLoaded;
			  document.documentElement.appendChild(vpaidFrame);
			})();
		
		} else {
			
			/* mobile widget */
			(function()
			{
			  var lkqdSettings = {
				pid: 314,
				sid: 334020,
				playerContainerId: 'mnative',
				playerId: '',
				playerWidth: '300',
				playerHeight: '169',
				execution: 'outstream',
				placement: 'incontent',
				playInitiation: 'auto',
				volume: 0,
				trackImp: '',
				trackClick: '',
				custom1: '',
				custom2: '',
				custom3: '',
				pubMacros: '',
				dfp: false,
				lkqdId: new Date().getTime().toString() + Math.round(Math.random()*1000000000).toString()
			  };

			  var lkqdVPAID;
			  var creativeData = '';
			  var environmentVars = { slot: document.getElementById(lkqdSettings.playerContainerId), videoSlot: document.getElementById(lkqdSettings.playerId), videoSlotCanAutoPlay: true, lkqdSettings: lkqdSettings };

			  function onVPAIDLoad()
			  {
				lkqdVPAID.subscribe(function() { lkqdVPAID.startAd(); }, 'AdLoaded');
				lkqdVPAID.subscribe(function() { lkqdVPAID.pauseAd(); }, 'AdNotViewable');
				lkqdVPAID.subscribe(function() { lkqdVPAID.resumeAd(); }, 'AdViewable');
			  }

			  var vpaidFrame = document.createElement('iframe');
			  vpaidFrame.id = lkqdSettings.lkqdId;
			  vpaidFrame.name = lkqdSettings.lkqdId;
			  vpaidFrame.style.display = 'none';
			  var vpaidFrameLoaded = function() {
				vpaidLoader = vpaidFrame.contentWindow.document.createElement('script');
				vpaidLoader.src = 'https://ad.lkqd.net/vpaid/formats.js?pid=314&sid=334020';
				vpaidLoader.onload = function() {
				  lkqdVPAID = vpaidFrame.contentWindow.getVPAIDAd();
				  onVPAIDLoad();
				  lkqdVPAID.handshakeVersion('2.0');
				  lkqdVPAID.initAd(lkqdSettings.playerWidth, lkqdSettings.playerHeight, 'normal', 600, creativeData, environmentVars);
				};
				vpaidFrame.contentWindow.document.body.appendChild(vpaidLoader);
			  };
			  vpaidFrame.onload = vpaidFrameLoaded;
			  vpaidFrame.onerror = vpaidFrameLoaded;
			  document.documentElement.appendChild(vpaidFrame);
			})();
		
		}
		
	</script>	
	";
	
	return $widget;
}
add_shortcode('nativeads', 'create_nativeads_shortcode');
/* GDM function - end */

/* GDM function to insert yhmg widgets in WP post content */
function create_yhmg_shortcode( $atts ) {
	/*
	extract(shortcode_atts(array(
		'var_name' => 'default_value'
	), $atts));
	*/		
	$widget = '';
	$cachebuster = mt_rand(1, time());
	
	$widget = '<div><SCRIPT SRC="http://dat.springserve.com/ttj?id=11650836&cb='.$cachebuster.'" TYPE="text/javascript"></SCRIPT></div>';
		
	return $widget;
}
add_shortcode('yhmg', 'create_yhmg_shortcode');
/* GDM function - end */
/* GDM function to insert glomex video player in WP post content */
function create_glomex_shortcode( $atts ) {
	extract(shortcode_atts(array(
		'player_id' => '2zaw72csj95hhouf',
		'playlist_id' => 'v-b8miuzdestn5',
		'width' => '588', // 588
		'height' => '330' // 330
	), $atts));
	
	$dimensions_string = '';
	if( $width != 'auto' && $height != 'auto' ) {
		$dimensions_string = ' data-width="'.$width.'" data-height="'.$height.'" ';
	}
	$widget = '<div class="responsive-glomex-container">';
	
	 
	// Initiate the player via native iFrame ( without external JS ) : - kept as reference
	// $widget .= '<iframe src="https://component-vvs.glomex.com/embed/1/index.html?playerId='.$player_id.'&playlistId='.$playlistId.'"></iframe>';
	
	// Initiate the player via external JS: 	
	$widget .= '<script async src="//component-vvs.glomex.com/glomex-embed/1/glomex-embed.js"></script>';
	$widget .='<glomex-player data-player-id="'.$player_id.'" data-playlist-id="'.$playlist_id.'" '.$dimensions_string.' ></glomex-player>';
	
	$widget .= '</div>';
	
	return $widget;
}
add_shortcode('glomex', 'create_glomex_shortcode');
/* GDM function - end */

/* GDM function to insert DFP BD_content_widget_mobile widget */
function create_dfp_mobile_shortcode( $atts ) {
	extract(shortcode_atts(array(
		'ad_unit' => 'content_widget_mobile'
	), $atts));
		
	$widget = "<div id='content_widget_mobile'></div>";
			
	return $widget;
}
add_shortcode('dfpmobile', 'create_dfp_mobile_shortcode');
/* GDM function - end */

/* GDM function to add the search box to the wp menu */
/* Add search box to primary menu */
function wpgood_nav_search($items, $args) {
    // If this isn't the primary menu, do nothing
    if( !($args->theme_location == 'primary-menu') ) 
    return $items;
    // Otherwise, add search form
    return $items . '<li><div id="header-search" class="mobile_search" >' . get_search_form(false) . '</div></li>';
}
add_filter('wp_nav_menu_items', 'wpgood_nav_search', 10, 2);
/* GDM function - end */

/* GDM function - start - use different template for the static pages - homepage and the categories pages */
add_filter( 'template_include', '_custom_static_pages_template' , 99 );
function _custom_static_pages_template( $page_template ) {
	global $post;

	if( isDeferedDitTest() || isHtmlizedDeferedDitTest() ) {
			
		$page_template = get_stylesheet_directory() . '/dit-test-single.php';
		return $page_template;
	}

	if( !isCampaignTraffic() ) {
		
		// we have a 'special' page for Nativo testing - it has the slug 'sponsored' and is the only one that uses the 'adstestpagenativo' template		
		if( $post->post_name == 'sponsored' ) {
			$page_template = get_stylesheet_directory() . '/new_template/adstestpagenativo.php';
			return $page_template;	
		} else if( $post->post_name == 'ad-bid-central' ){
            $page_template = get_stylesheet_directory() . '/new_template/adbidcentraltestpage.php';
            return $page_template;
        } elseif( is_single() ) {
			$page_template = get_stylesheet_directory() . '/new_template/single.php';
			return $page_template;			
		} elseif( is_front_page() ) {
			$page_template = get_stylesheet_directory() . '/new_template/front-page.php';
			return $page_template;			
		} elseif( is_category() ) {
			$page_template = get_stylesheet_directory() . '/new_template/category.php';
			return $page_template;		
		} elseif ( is_search() ) {
			$page_template = get_stylesheet_directory() . '/new_template/search.php';
			return $page_template;
		} elseif( isset($post) && isset($post->ID) && $post->ID == 3757 ) {
			// 'contact us' page 
			$page_template = get_stylesheet_directory() . '/new_template/page-contact.php';
			return $page_template;
		} else {
			// all other pages ?!?!?
			$page_template = get_stylesheet_directory() . '/new_template/page.php';
			return $page_template;	
		}
	} else if( is_single() && oldVsNew() ){
        $page_template = get_stylesheet_directory() . '/new_template/single.php';
        return $page_template;
    }
	
	
	return $page_template;
}
/* GDM function - end */

/* GDM function - start - function that echoes "?test_redesign=123" if the GET 'test_redesign' query parameter existis and is equal to 123 ; else it echoes an empty string */
function get_redesign_query_parameter(){
	
	if( isCampaignTraffic() ) {
		echo '?tict=1';
	} else {
		echo "";
	}
	
}
/* GDM function - end */

// function that generates a userId and keeps it in the session
// function get_user_id_from_session() {

	// if (!isset($_SESSION['customUserId'])) {
		// $userId = md5(uniqid(rand(), true));
		// $_SESSION['customUserId'] = $userId;
	// } else {
		// $userId = $_SESSION['customUserId'];
	// }
	
	// return $userId;
// }

/* Add options to wp-admin for acf - start */
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
}
/* Add options to wp-admin for acf - end */

// add_theme_support( 'amp', array(
// 	'template_dir' => 'amp',
// ));


/* 
	function for the AB test that was setup using Thrive tracker - this function registers the GA custom dimension 1 with the value based on the query strings 
	
	PS: this function can nolw be replace with the more general 'injectCustomDimensionValue'
	EX: calling " injectCustomDimensionValue('dimension1', ['tau' , 'bpy' , 'tba' , 'nnp' , 'dmy']); " will do exactly the same thing as " manualInjectCustomDimensionValues() " 
	
	Will keep this as refference
*/
function manualInjectCustomDimensionValues() {
    global $post;
    $custom_dimension_code = '';
	
	// check if we already set a cookie for this
	// if(isset($_COOKIE['ga_custom_dimension_1_value'])) {
		// $custom_dimension_code = "ga('set', 'dimension1', '".$_COOKIE['ga_custom_dimension_1_value']."');";
	// }	
	
	// check which value to use for GA custom dimension
	if( isset($_GET['tau']) && $_GET['tau'] == 1 ) {
		$custom_dimension_code = "ga('set', 'dimension1', 'tau');";
		// create cookie for future visits
		// setcookie('ga_custom_dimension_1_value',  'tau' );
		
	} elseif( isset($_GET['bpy']) && $_GET['bpy'] == 1 ) {
		$custom_dimension_code = "ga('set', 'dimension1', 'bpy');";	
		// create cookie for future visits
		// setcookie('ga_custom_dimension_1_value',  'bpy' );
		
	} elseif( isset($_GET['tba']) && $_GET['tba'] == 1 ) {
		$custom_dimension_code = "ga('set', 'dimension1', 'tba');";					
		// create cookie for future visits
		// setcookie('ga_custom_dimension_1_value',  'tba' );
		
	} elseif( isset($_GET['nnp']) && $_GET['nnp'] == 1 ) {
		$custom_dimension_code = "ga('set', 'dimension1', 'nnp');";
		// create cookie for future visits
		// setcookie('ga_custom_dimension_1_value',  'nnp' );
		
	} elseif( isset($_GET['dmy']) && $_GET['dmy'] == 1 ) {
		$custom_dimension_code = "ga('set', 'dimension1', 'dmy');";
		// create cookie for future visits
		// setcookie('ga_custom_dimension_1_value',  'dmy' );
	}
	
	// test the code
	// $custom_dimension_code = '';
	
	return $custom_dimension_code;
}


/* 
	function for the AB test that was setup using Thrive tracker 
	- this function registers the GA custom dimension with the value based on the query strings 
	- accepts 1 parameter - the dimension to be used ( by default it uses 'dimension1' 
*/
function injectCustomDimensionValue( $dimension = 'dimension1' , $query_string_checks = [] ) {
    global $post;
    $custom_dimension_code = '';
	
	foreach($query_string_checks as $query_string_check ) {
		// check if the query string exists in GET and if it's equal to 1 
		if( isset($_GET[ $query_string_check ]) && $_GET[ $query_string_check ] == 1 ) {
			$custom_dimension_code = "ga('set', '".$dimension."', '".$query_string_check."');";
			
			// we stop the loop as we don't want to overwrite the first matched query param in case others exist
			break;
		}
	}
			
	return $custom_dimension_code;
}


// Deregister Jquery and Jquery migrate

// decomment the below line to defer jquery globally (can't do it on a single post)

// add_filter( 'script_loader_tag', 'wsds_defer_scripts', 10, 3 );

function wsds_defer_scripts( $tag, $handle, $src ) {

	global $post;

	// defer the jquery scripts for our dit-test
	if(isCampaignTraffic() && isset($post) && ( $post->post_name == '50-cars-that-are-plummeting-in-value-d' || $post->post_name == '50-cars-that-will-last-more-than-250000-miles-d' || $post->post_name == '50-cars-that-will-last-more-than-250000-miles-dd' || $post->post_name == '50-cars-that-are-plummeting-in-value-dd' ) ) {
		// The handles of the enqueued scripts we want to defer
		$defer_scripts = array(
			'prismjs',
			'admin-bar',
			'et_monarch-ouibounce',
			'et_monarch-custom-js',
			'wpshout-js-cookie-demo',
			'cookie',
			'jquery-migrate',
			'jquery-ui',
			'wpshout-no-broken-image',
			'goodbye-captcha-public-script',
			'devicepx',
			'search-box-value',
			'page-min-height',
			'kamn-js-widget-easy-twitter-feed-widget',
			'__ytprefs__',
			'__ytprefsfitvids__',
			'icegram',
			'disqus',
		);

		if ( in_array( $handle, $defer_scripts ) ) {
			return '<script src="' . $src . '" defer type="text/javascript"></script>' . "\n";
		}

		return $tag;
	}

}

/* Check for DIT Test Variation : Defered */
function isDeferedDitTest() {
    global $post;

    if( isset($post) && isset($_GET['defered']) && $_GET['defered'] == 1 &&
        ( $post->post_name == '50-cars-that-are-plummeting-in-value-d' || $post->post_name == '50-cars-that-will-last-more-than-250000-miles-d' || $post->post_name == '50-cars-that-will-last-more-than-250000-miles-dd' || $post->post_name == '50-cars-that-are-plummeting-in-value-dd' || $post->post_name == '50-cars-that-will-last-more-than-250000-miles-d3' || $post->post_name == '50-cars-that-are-plummeting-in-value-d3' ) ) {
        return true;
    } else {
        return false;
    }
}

/* Check for DIT Test Variation : Htmlized */
function isHtmlizedDitTest() {
	global $post;
	if( isset($post) && isset($_GET['htmlized']) && $_GET['htmlized'] == 1 && 
		( $post->post_name == '50-cars-that-are-plummeting-in-value-d' || $post->post_name == '50-cars-that-will-last-more-than-250000-miles-d' || $post->post_name == '50-cars-that-will-last-more-than-250000-miles-dd' || $post->post_name == '50-cars-that-are-plummeting-in-value-dd' || $post->post_name == '50-cars-that-will-last-more-than-250000-miles-d3' || $post->post_name == '50-cars-that-are-plummeting-in-value-d3' ) ) {
		
		return true;
	} else {
		return false;	
	}
}

/* Check for DIT Test Variation : Htmlized and Defered */
function isHtmlizedDeferedDitTest() {
	global $post;
	
	if( isset($post) && isset($_GET['htmlized_defered']) && $_GET['htmlized_defered'] == 1 && 
		( $post->post_name == '50-cars-that-are-plummeting-in-value-d' || $post->post_name == '50-cars-that-will-last-more-than-250000-miles-d' || $post->post_name == '50-cars-that-will-last-more-than-250000-miles-dd' || $post->post_name == '50-cars-that-are-plummeting-in-value-dd' || $post->post_name == '50-cars-that-will-last-more-than-250000-miles-d3' || $post->post_name == '50-cars-that-are-plummeting-in-value-d3'  ) ) {
		
		return true;
	} else {
		return false;	
	}
}

/* Check for DIT Test Variation : Control */
function isControlDitTest() {
	global $post;
	
	if( isset($post) && isset($_GET['control']) && $_GET['control'] == 1 && 
		( $post->post_name == '50-cars-that-are-plummeting-in-value-d' || $post->post_name == '50-cars-that-will-last-more-than-250000-miles-d' || $post->post_name == '50-cars-that-will-last-more-than-250000-miles-dd' || $post->post_name == '50-cars-that-are-plummeting-in-value-dd' || $post->post_name == '50-cars-that-will-last-more-than-250000-miles-d3' || $post->post_name == '50-cars-that-are-plummeting-in-value-d3'  ) ) {
			
		return true;
	} else {
		return false;	
	}
}