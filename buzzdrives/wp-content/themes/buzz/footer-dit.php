</div><!-- end - #main -->

    <footer id="footer">
        <div id="footer-inner">
            <div class="container">
                <div class="footer-container">
                    <div id="footer-logo-wrapper">
                        <div>
                            <a href="<?php echo get_home_url(); ?>">
                                <img class="center-block img-responsive" src="<?php echo get_img_folder(); ?>/buzzdrives_logo_footer.png" alt="BuzzDrives Logo" width="208" height="23" />
                            </a>
                            <div id="footer-menu">
                                <?php wp_nav_menu(
                                    array(
                                        'theme_location' => 'footer-menu',
                                        'container' => '',
                                        'menu_id' => 'menu-footer',
                                        'menu_class' => 'list-inline clearfix text-center',
                                    )
                                ); ?>
                            </div><!-- end - #footer-menu -->
                        </div>
                    </div><!-- end - #footer-logo-wrapper -->
                    <div class="footer-right-wrapper">
                        <h3>Impressum</h3>
                        <p>Reverse Media Group</p>
                        <div>
                            <p>Found Studios, 1 Lindsey Street, London, EC1A 9HP</p>
                            <p>VAT number: GB 188 0307 01p</p>
                        </div>
                        <div>
                            <p><a href="mailto:partners@reversemediagroup.com">partners@reversemediagroup.com</a></p>
                            <p>+44 208133 9954</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="separator footer"></div>
            <div class="container">
                <div class="copyright_section">
                    <p>Copyright &copy; <?php echo date("Y"); ?> Buzzdrives</p>
                </div>
            </div>
        </div><!-- end - #footer-inner -->
    </footer><!-- end - #footer -->
</div><!-- end - #wrapper -->

<?php wp_footer(); ?>




<script type="text/javascript" >
    // code from the 2 scripts that have been commented out ( scripts.js and ga_events.js)
    /* ADSENSE - start */
    (function() {
        // init every adsense banner
        for(var i = 0; i < document.getElementsByClassName('adsbygoogle').length; i++) {
            (adsbygoogle = window.adsbygoogle || []).push({});
        }
    })();
    /* ADSENSE - end */
</script>


<?php /* LockerDome tracking pixel - start  - removed on 26.03.2019 ?>
		<script>
		(function () {
		  if (/ld_trk=1/.test(document.cookie)) {
			track();
		  } else if (/ld_trk=1/.test(window.location.search)) {
			document.cookie = 'ld_trk=1; path=/';
			track();
		  }
		  function track() {
			new Image(1,1).src = 'https://lockerdome.com/ldpix.gif?ldc=10530771157065216_reversemediagroup_impression&ord='+Date.now();
		  }
		})();
		</script>
		<?php /* LockerDome tracking pixel - end */?>

<?php /* MM sticky ads - start */ ?>
<script>
    (function(){
        if( isMobile() ) {
            document.write ('<div id="BD_Sticky_Bottom_mobile" style="position:fixed; bottom:0; left:0;right:0; margin:0 auto; z-index:9999999;text-align: center;" ></div>');
        } else {
            document.write ('<div id="BD_Sticky_Bottom_desktop" style="position:fixed; bottom:0; left:0;right:0; margin:0 auto; z-index:9999999;text-align: center;" ></div>');
        }
    }());
</script>
<?php /* MM sticky ads - end */ ?>




<?php /* Taboola Below Articles - start */ ?>
<script type="text/javascript">
    window._taboola = window._taboola || [];
    _taboola.push({flush: true});
</script>
<?php /* Taboola Below Articles - end */ ?>


<?php /* Start of the Defer JS implementation */ ?>

<script type="text/javascript">
    // insert the deferred JS functions to trigger after page Load
    function loadDeferJsFile() {
        console.log('loading deffered functionality start');

        /* COUNT SHARES - start */
        (function () {
            if( $('#social-buttons .counter').length > 0 ) {
                var c = 1;

                if( $('.post-title').length > 0 ) {
                    var c = parseFloat( $('.post-title').text().length / 13.3242 );
                }

                while (c > 9) {
                    c = c / 1.42;
                }

                $('#social-buttons .counter').text( c.toFixed(1) + 'k');
            }
        })();
        /* COUNT SHARES - end */

        var vizimgDefer = document.getElementsByClassName('defer-loading-images');
        for (var i=0; i<vizimgDefer.length; i++) {
            if(vizimgDefer[i].getAttribute('data-v')) {
                vizimgDefer[i].setAttribute('src',vizimgDefer[i].getAttribute('data-v'));
            }
        }

        <!-- GOOGLE ANALYTICS - start -->

        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        // Creates an adblock detection plugin.
        ga('provide', 'adblockTracker', function(tracker, opts) {
            var ad = document.createElement('ins');
            ad.className = 'AdSense';
            ad.style.display = 'block';
            ad.style.position = 'absolute';
            ad.style.top = '-1px';
            ad.style.height = '1px';
            document.body.appendChild(ad);
            tracker.set('dimension' + opts.dimensionIndex, !ad.clientHeight);
            document.body.removeChild(ad);
        });

        ga('create', 'UA-64470501-1', 'auto');
        ga('require', 'adblockTracker', {dimensionIndex: 6});

		<?php		
		// we use only one dimension for the current Dit Tests variations : 
		echo injectCustomDimensionValue('dimension1', ['htmlized_defered','defered']);
		?>
		
        ga('send', 'pageview');

        <!-- Facebook Pixel Code -->
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1861783217183966'); // Insert your pixel ID here.
        fbq('track', 'PageView');

        <!-- Google Tag Manager -->


		/* MonetizeMore Tracking  - start */ 
		window.m2hb = window.m2hb || {};
		window.m2hb.kvps = window.m2hb.kvps || {};
		<?php 
		if(isset($_GET['dfp']) && $_GET['dfp'] != '' ) { 
			echo 'window.m2hb.kvps["dfpQuery"] = "'. $_GET['dfp'].'"; ';
		} 
		if(isset($_GET['t']) && $_GET['t'] != '' ) { 
			echo 'window.m2hb.kvps["testQuery"] = "'. $_GET['t'].'"; ';
		} 
		?>
		/* MonetizeMore Tracking  - end */ 
		
		
        /* MM sticky ads - start */
        (function() {
            var isCmpTraffic = false;
            var isFbTraffic = false;
            if(
                window.location.search.toLowerCase().indexOf('utm_source=yg') != -1 ||
                window.location.search.toLowerCase().indexOf('utm_source=yahoo') != -1 ||
                window.location.search.toLowerCase().indexOf('utm_source=zemanta') != -1 ||
                window.location.search.toLowerCase().indexOf('utm_source=outbrain') != -1 ||
                window.location.search.toLowerCase().indexOf('utm_source=taboola') != -1 ||
                window.location.search.toLowerCase().indexOf('tict=1') != -1
            ) {
                isCmpTraffic = true;
            }
            if( window.location.search.toLowerCase().indexOf('utm_source=facebook') != -1 ||
                window.location.search.toLowerCase().indexOf('tmpfb=1') != -1
            ) {
                isFbTraffic = true;
            }
            if( !isCmpTraffic || isFbTraffic ) {
                window.m2hb = window.m2hb || {};
                m2hb.disabledUnits = m2hb.disabledUnits || [];
                m2hb.disabledUnits.push('/207764977/BD_Sticky_Bottom_desktop');
                m2hb.disabledUnits.push('/207764977/BD_Sticky_Bottom_mobile');
            }

            if( !isCmpTraffic ) {
                window.m2hb = window.m2hb || {};
                m2hb.disabledUnits = m2hb.disabledUnits || [];
                // we disable the units that we show only for cam traffic
                m2hb.disabledUnits.push('/207764977/BD_top_left_sidebar_desktop');
                m2hb.disabledUnits.push('/207764977/BD_sticky_sidebar_desktop');
            }

        })();


        // MM ads Device Check
        (function() {
            // check device and disable accordingly
            if( isMobile() ) {
                // disable the desktop units
                window.m2hb = window.m2hb || {};
                m2hb.disabledUnits = m2hb.disabledUnits || [];
                m2hb.disabledUnits.push('/207764977/top_banner_desktop');
                m2hb.disabledUnits.push('/207764977/top_right_sidebar_desktop');
                m2hb.disabledUnits.push('/207764977/BD_top_left_sidebar_desktop');
                m2hb.disabledUnits.push('/207764977/BD_Sticky_Bottom_desktop');
                m2hb.disabledUnits.push('/207764977/BD_sticky_sidebar_desktop');
                m2hb.disabledUnits.push('/207764977/BD_top_right_sidebar_desktop_2');
                m2hb.disabledUnits.push('/207764977/BD_IBV_Right_Sidebar_Desktop');
                m2hb.disabledUnits.push('/207764977/BD_Leaderboard_above_pagination_Desktop');
                m2hb.disabledUnits.push('/207764977/BD_Top_Right_Sidebar_Video');

            } else {
                // disable the mobile units
                window.m2hb = window.m2hb || {};
                m2hb.disabledUnits = m2hb.disabledUnits || [];
                m2hb.disabledUnits.push('/207764977/top_banner_mobile');
                m2hb.disabledUnits.push('/207764977/BD_Sticky_Bottom_mobile');
                m2hb.disabledUnits.push('/207764977/BD_Leaderboard_above_pagination_Mobile');
            }
        })();

        // we inject the MM DFP script
        window.M2_TIMEOUT = 1500;

        var element = document.createElement("script");
        element.src = "https://m2d.m2.ai/m2d.buzzdrives.alldevices.min.js";
        document.body.appendChild(element);

        var element1 = document.createElement("script");
        element1.src = "//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js";
        document.body.appendChild(element1);

        // check if we inject the DFP Adx units instead of the hardcoded Adsense ABOVE PAGINATION
		<?php if( switchHardcodedAdxWithDFPunit() ) { ?>

        var element2 = document.createElement("script");
        element2.src = "https://www.googletagservices.com/tag/js/gpt.js";
        document.body.appendChild(element2);

        var gptadslots = [];
        var googletag = googletag || {cmd:[]};
        googletag.cmd.push(function() {
            // Mobile Ad Units
            if( isMobile() ) {
                //Adslot 2 declaration
                gptadslots.push(googletag.defineSlot('/207764977/BD_AdSense_Pagination_Mobile', [[300,250]], 'div-gpt-ad-1542649-2')
                    .addService(googletag.pubads()));
            } else {
                //Adslot 1 declaration
                gptadslots.push(googletag.defineSlot('/207764977/BD_AdSense_Pagination_Desktop', [[728,90]], 'div-gpt-ad-1542649-1')
                    .addService(googletag.pubads()));
            }



            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
        });
		<?php } ?>

        <?php /* Cookie consent - start */ ?>
            window.cookieconsent_options = {
                "message": "This website uses cookies to ensure you get the best experience on our website",
                "dismiss": "Got it!",
                "learnMore": "More info",
                "link":"/privacy-policy/",
                "theme":"light-top"
            };
            var element7 = document.createElement("script");
            element7.src = "//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js";
            document.body.appendChild(element7);
        <?php /* Cookie consent - end */ ?>

        // taboola retargeting pixels - start
		<?php if(isset($page) && $page == 8 ) {
		if( isset($post) && isset($post->ID) && $post->ID == 8014 ) { ?>
        window._tfa = window._tfa || [];
        _tfa.push({ notify: 'mark',type: 'Plummeting-3' });

		<?php } ?>

        // 30-great-cars-that-nobody-buys-3
		<?php if( isset($post) && isset($post->ID) && $post->ID == 8025 ) { ?>

        window._tfa = window._tfa || [];
        _tfa.push({ notify: 'mark',type: 'Nobody-buys-3' });

		<?php } ?>
        // 30-cars-that-will-last-more-than-250000-miles
		<?php if( isset($post) && isset($post->ID) && $post->ID == 5981 ) { ?>

        window._tfa = window._tfa || [];
        _tfa.push({ notify: 'mark',type: 'Last-250k-miles' });

		<?php } ?>

        // insert the script if any of the postId from above;

		<?php if( isset($post) && isset($post->ID) && ($post->ID == 5981 || $post->ID == 8025 || $post->ID == 8014) ) { ?>
        var element3 = document.createElement("script");
        element3.src = "//cdn.taboola.com/libtrc/buzzdrives-sc/tfa.js";
        document.body.appendChild(element3);
		<?php } ?>
		<?php } ?>


        // Taboola Head Code Start
        window._taboola = window._taboola || [];
        _taboola.push({article:'auto'});
        !function (e, f, u, i) {
            if (!document.getElementById(i)){
                e.async = 1;
                e.src = u;
                e.id = i;
                f.parentNode.insertBefore(e, f);
            }
        }(document.createElement('script'),
            document.getElementsByTagName('script')[0],
            '//cdn.taboola.com/libtrc/buzzdrive/loader.js',
            'tb_loader_script');
        if(window.performance && typeof window.performance.mark == 'function')
        {window.performance.mark('tbl_ic');}


        // Taboola Below Articles - start
        window._taboola = window._taboola || [];
        _taboola.push({photo:'auto'});
        !function (e, f, u, i) {
            if (!document.getElementById(i)){
                e.async = 1;
                e.src = u;
                e.id = i;
                f.parentNode.insertBefore(e, f);
            }
        }(document.createElement('script'),
            document.getElementsByTagName('script')[0],
            '//cdn.taboola.com/libtrc/buzzdrive/loader.js',
            'tb_loader_script');
        if(window.performance && typeof window.performance.mark == 'function')
        {window.performance.mark('tbl_ic');}

        (function(){
            if( isMobile() ) {
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1542649-2'); });
                var divGptAd1 = document.getElementById('div-gpt-ad-1542649-1');
                divGptAd1.parentNode.removeChild(divGptAd1);;
            } else {
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1542649-1'); });
                var divGptAd2 = document.getElementById('div-gpt-ad-1542649-2');
                divGptAd2.parentNode.removeChild(divGptAd2);
            }
        }());

        var element4 = document.createElement("script");
        element4.src = "<?php echo get_template_directory_uri(); ?>/js/jquery.infinitescroll.min.js";
        document.body.appendChild(element4);

        var element6 = document.createElement("script");
        element6.src = "<?php echo get_template_directory_uri(); ?>/js/main-js-dit.js";
        document.body.appendChild(element6);

        // Persist the isCampaign query parameter across header and footer menu links
        if(
            window.location.search.toLowerCase().indexOf('utm_source=yg') != -1 ||
            window.location.search.toLowerCase().indexOf('utm_source=yahoo') != -1 ||
            window.location.search.toLowerCase().indexOf('utm_source=zemanta') != -1 ||
            window.location.search.toLowerCase().indexOf('utm_source=outbrain') != -1 ||
            window.location.search.toLowerCase().indexOf('utm_source=taboola') != -1 ||
            window.location.search.toLowerCase().indexOf('tict=1') != -1
        ) {

            // header menu links
            $('.menu-item-type-taxonomy a').each(function( index , el ){
                var link_url = $(el).attr('href');
                $(el).attr('href' , link_url + '?tict=1' );
            });

            // footer links
            $('.menu-item-type-post_type a').each(function( index , el ){
                var link_url = $(el).attr('href');
                $(el).attr('href' , link_url + '?tict=1' );
            });

        }

        var issticky = 0;
        var initial_sticky = $('.sidebar-sticky').offset().top;
        initial_sticky = 0;

        console.log('loading deffered functionality end');
    }

    // call the loadDeferJsFile function after the onLoad Window event has triggered
    if (window.addEventListener)
        window.addEventListener("load", loadDeferJsFile, false);
    else if (window.attachEvent)
        window.attachEvent("onload", loadDeferJsFile);
    else window.onload = loadDeferJsFile;

    /* Single article pages, left sidebar add class fixed on scroll window to make it sticky - start */
    function scrollDeferedJs () {
        $(window).on('scroll', function () {
            var headerHeaight = $('.cc_banner-wrapper ').height() + $('header').height();
            if ($(window).scrollTop() > headerHeaight) {
                $('#left_sidebar').addClass('fixed');
                $('#content-with-aside').addClass('fixed');
            } else {
                $('#left_sidebar').removeClass('fixed');
                $('#content-with-aside').removeClass('fixed');
            }
        });
    }
    if($('#left_sidebar').length > 0){
        if (window.addEventListener)
            window.addEventListener("scroll", scrollDeferedJs, false);
        else if (window.attachEvent)
            window.attachEvent("onscroll", scrollDeferedJs);
        else window.onscroll = scrollDeferedJs;
    }

    /* Single article pages, left sidebar add class fixed on scroll window to make it sticky - end */
</script>

</body>
</html>