<!DOCTYPE html>
<html lang="en">
	<head>		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php bloginfo('name'); ?> | <?php IsHome() ? bloginfo('description') : wp_title(''); ?></title>

        <?php
        $site_dev = ( (strpos($_SERVER['HTTP_HOST'] , "gdm") !== false ) ? true : false );
        if (!$site_dev) {
        ?>
            <!-- GOOGLE ANALYTICS - start -->
            <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            // Creates an adblock detection plugin.
            ga('provide', 'adblockTracker', function(tracker, opts) {
              var ad = document.createElement('ins');
              ad.className = 'AdSense';
              ad.style.display = 'block';
              ad.style.position = 'absolute';
              ad.style.top = '-1px';
              ad.style.height = '1px';
              document.body.appendChild(ad);
              tracker.set('dimension' + opts.dimensionIndex, !ad.clientHeight);
              document.body.removeChild(ad);
            });

            ga('create', 'UA-64470501-1', 'auto');
            ga('require', 'adblockTracker', {dimensionIndex: 6});

            <?php

            /* old custom dimension code for a given AB test - kept as refference */
            // get the GA custom dimensions that are used for the hard-coded AB test ( added on 25.03.2019 )
            // $custom_dimensions_code_manual = manualInjectCustomDimensionValues();
            // if( $custom_dimensions_code_manual != '' ) {
                // echo $custom_dimensions_code_manual;
            // }

            /* New way of setting up custom dimensions: */
            // inject the custom dimensions for the "switch between MM and DFP Adx units" AB test
            // echo injectCustomDimensionValue('dimension1', ['tau' , 'bpy' , 'tba' , 'nnp' , 'dmy']);

            // inject the custom dimension for the "Switch pagination hard-coded adsense units with DFP Adx" AB test
            // echo injectCustomDimensionValue('dimension2', ['shad' , 'shadco']);

            // we use only one dimension for the current tests ( "switch between MM and DFP Adx units" and "Switch pagination hard-coded adsense units with DFP Adx" ) :
            // added 'htmlized' and 'control' first in list to be used for the Dit TESTS ( HTMLized and Control variations ) - added on 15.04.2019
            echo injectCustomDimensionValue('dimension1', ['htmlized' , 'control' ,'smmap','tau' , 'bpy' , 'tba' , 'nnp' , 'dmy' , 'shad' , 'shadco']);
            ?>

            ga('send', 'pageview');
            </script>
            <!-- GOOGLE ANALYTICS - end -->

            <!-- Facebook Pixel Code -->
            <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1861783217183966'); // Insert your pixel ID here.
            fbq('track', 'PageView');
            </script>
            <noscript><img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=1861783217183966&ev=PageView&noscript=1"
            /></noscript>
            <!-- DO NOT MODIFY -->
            <!-- End Facebook Pixel Code -->

            <!-- OutBrain Pixel - start -->
            <script data-obct type="text/javascript">
                /* DO NOT MODIFY THIS CODE */
                !function(_window, _document) {
                    var OB_ADV_ID='00366e548b8be5b79fa722a54365a8f940';
                    if (_window.obApi) {var toArray = function(object) {return Object.prototype.toString.call(object) === '[object Array]' ? object : [object];};_window.obApi.marketerId = toArray(_window.obApi.marketerId).concat(toArray(OB_ADV_ID));return;}
                    var api = _window.obApi = function() {api.dispatch ? api.dispatch.apply(api, arguments) : api.queue.push(arguments);};api.version = '1.1';api.loaded = true;api.marketerId = OB_ADV_ID;api.queue = [];var tag = _document.createElement('script');tag.async = true;tag.src = '//amplify.outbrain.com/cp/obtp.js';tag.type = 'text/javascript';var script = _document.getElementsByTagName('script')[0];script.parentNode.insertBefore(tag, script);}(window, document);
                obApi('track', 'PAGE_VIEW');
            </script>
            <!-- OutBrain Pixel - end -->

        <?php } ?>

		<?php /* FACEBOOK SEO TAGS - start */ ?>
		<?php if( is_single() ) { ?>
			<meta property="og:url" content="<?php the_permalink($post->ID); ?>" />
			<meta property="og:type" content="website" />
			<meta property="og:title" content="<?php wp_title( '|', true, 'right' ) . bloginfo( 'name' ); ?>" />
			<meta property="og:description" content="<?php the_title(); ?>" />
			<meta property="og:image" content="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0]; ?>" />
			<?php } else { ?>
				<meta name="description" content="Fill your tank with our satisfying bites of trending car articles and news on latest models and moves from within the industry.">
		<?php } ?>
		<?php /* FACEBOOK SEO TAGS - end */ ?>
		
		<?php /* Google Verification - start */?>
		<meta name="google-site-verification" content="vuobpu9ZEcVJfqbyAG8iqiFZM3WtGjdwz44QmIVQLCM" />
		<meta name="google-site-verification" content="GICwiYwnx4mHDw4kZdilY2wHe129B2hNjXwoR5d0I40" />
		<?php /* Google Verification - end */?>

		<?php /* globals.js includes: GA code, global variables */ ?>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/globals.js"></script>
				
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-KMZ7DSR');</script>
		<!-- End Google Tag Manager -->
		
		
		<?php /* MonetizeMore Tracking  - start */ ?>
		<script>
		window.m2hb = window.m2hb || {};
		window.m2hb.kvps = window.m2hb.kvps || {};

		<?php 
		if(isset($_GET['dfp']) && $_GET['dfp'] != '' ) { 
			echo 'window.m2hb.kvps["dfpQuery"] = "'. $_GET['dfp'].'"; ';
		} 
		if(isset($_GET['t']) && $_GET['t'] != '' ) { 
			echo 'window.m2hb.kvps["testQuery"] = "'. $_GET['t'].'"; ';
		} 
		?>
		</script>
		<?php /* MonetizeMore Tracking  - end */ ?>
		
	
		<?php /* MM sticky ads - start */ ?>
		<script>
			(function() {
				var isCmpTraffic = false;
				var isFbTraffic = false;				
				if( 
					window.location.search.toLowerCase().indexOf('utm_source=yg') != -1 || 
					window.location.search.toLowerCase().indexOf('utm_source=yahoo') != -1 || 
					window.location.search.toLowerCase().indexOf('utm_source=zemanta') != -1 || 
					window.location.search.toLowerCase().indexOf('utm_source=outbrain') != -1 || 
					window.location.search.toLowerCase().indexOf('utm_source=taboola') != -1 ||
					window.location.search.toLowerCase().indexOf('tict=1') != -1 
					) {
					isCmpTraffic = true;
				}
				if( window.location.search.toLowerCase().indexOf('utm_source=facebook') != -1 ||
					window.location.search.toLowerCase().indexOf('tmpfb=1') != -1 
					) {
					isFbTraffic = true;
				}
				if( !isCmpTraffic || isFbTraffic ) {
					window.m2hb = window.m2hb || {};
					m2hb.disabledUnits = m2hb.disabledUnits || [];
					m2hb.disabledUnits.push('/207764977/BD_Sticky_Bottom_desktop');
					m2hb.disabledUnits.push('/207764977/BD_Sticky_Bottom_mobile');
				}
				
				if( !isCmpTraffic ) {
					window.m2hb = window.m2hb || {};
					m2hb.disabledUnits = m2hb.disabledUnits || [];
					// we disable the units that we show only for cam traffic 
					m2hb.disabledUnits.push('/207764977/BD_top_left_sidebar_desktop');
					m2hb.disabledUnits.push('/207764977/BD_sticky_sidebar_desktop');
				}
				
			})();
		</script>
		<?php /* MM sticky ads - end */ ?>
		
		<?php /* MM ads Device Check - start */ ?>
		<script>
			(function() {	
				// check device and disable accordingly
				if( isMobile() ) {
					// disable the desktop units 
					window.m2hb = window.m2hb || {};
					m2hb.disabledUnits = m2hb.disabledUnits || [];
					m2hb.disabledUnits.push('/207764977/top_banner_desktop');
					m2hb.disabledUnits.push('/207764977/top_right_sidebar_desktop');
					m2hb.disabledUnits.push('/207764977/BD_top_left_sidebar_desktop');
					m2hb.disabledUnits.push('/207764977/BD_Sticky_Bottom_desktop');
					m2hb.disabledUnits.push('/207764977/BD_sticky_sidebar_desktop');
					m2hb.disabledUnits.push('/207764977/BD_top_right_sidebar_desktop_2');
					m2hb.disabledUnits.push('/207764977/BD_IBV_Right_Sidebar_Desktop');
					m2hb.disabledUnits.push('/207764977/BD_Leaderboard_above_pagination_Desktop');
					m2hb.disabledUnits.push('/207764977/BD_Top_Right_Sidebar_Video');
					
				} else {
					// disable the mobile units 
					window.m2hb = window.m2hb || {};
					m2hb.disabledUnits = m2hb.disabledUnits || [];
					m2hb.disabledUnits.push('/207764977/top_banner_mobile');
					m2hb.disabledUnits.push('/207764977/BD_Sticky_Bottom_mobile');					
					m2hb.disabledUnits.push('/207764977/BD_Leaderboard_above_pagination_Mobile');					
				}
			})();
		</script>
		<?php /* MM ads Device Check - end */ ?>
		
		<?php /* MM / DFP - START */  ?>
		<?php if( isset($post) && isset($post->ID) && $post->ID == 3027 ) { 
			/* we don't inject the MM script in the 'privacy-policy' page ( post_id = 3027 ) */
		} else if( isset($post) && isset($post->ID) && $post->ID == 11246 ) { ?>
			<script> window.M2_TIMEOUT = 1500; </script>
			<script src="//m2d.m2.ai/m2d.buzzdrives.nativeepom.js" async></script>
		<?php } else { /* we inject the MM DFP script */ ?>
			<script> window.M2_TIMEOUT = 1500; </script>		
			<script type="text/javascript" src="https://m2d.m2.ai/m2d.buzzdrives.alldevices.min.js" async="async" ></script>
		<?php } ?>
		<?php /* MM / DFP - END */ ?>
		
		
		
		<?php /* check if we inject the DFP Adx units instead of the hardcoded Adsense ABOVE PAGINATION */ ?>
		<?php if( switchHardcodedAdxWithDFPunit() ) { ?>	
			<?php /* DFP ADX units - start */ ?>
			<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
			<script>
			  var gptadslots = [];
			  var googletag = googletag || {cmd:[]};
			</script>
			<script>
			googletag.cmd.push(function() {
				// Mobile Ad Units
				if( isMobile() ) {
					//Adslot 2 declaration
					gptadslots.push(googletag.defineSlot('/207764977/BD_AdSense_Pagination_Mobile', [[300,250]], 'div-gpt-ad-1542649-2')
											 .addService(googletag.pubads()));
				} else {
					//Adslot 1 declaration
					gptadslots.push(googletag.defineSlot('/207764977/BD_AdSense_Pagination_Desktop', [[728,90]], 'div-gpt-ad-1542649-1')
											 .addService(googletag.pubads()));
				}		
				
										 

				googletag.pubads().enableSingleRequest();
				googletag.enableServices();
			});
			</script>
			<?php /* DFP Adx units - end */ ?>		
		<?php } ?>
		
		
		
		<?php /* MM/DFP Video tag - START  ?>
		<?php if( testMmVideoTag() ) { ?>
			<script src="//m2d.m2.ai/pghb.buzzdrives.ibv.js" async></script>
		<?php } ?>
		<?php /*  MM/DFP Video tag - END */ ?>   
		
		
		<?php /* taboola retargeting pixels - start */ ?>
		<?php if(isset($page) && $page == 8 ) { ?>
			<?php // 20-cars-that-are-plummeting-in-value-3 ?>
			<?php if( isset($post) && isset($post->ID) && $post->ID == 8014 ) { ?>
				<script type="text/javascript">
				window._tfa = window._tfa || [];
				_tfa.push({ notify: 'mark',type: 'Plummeting-3' });
				</script>
				<script src="//cdn.taboola.com/libtrc/buzzdrives-sc/tfa.js"></script>
			<?php } ?>
			<?php // 30-great-cars-that-nobody-buys-3 ?>
			<?php if( isset($post) && isset($post->ID) && $post->ID == 8025 ) { ?>
				<script type="text/javascript">
				window._tfa = window._tfa || [];
				_tfa.push({ notify: 'mark',type: 'Nobody-buys-3' });
				</script>
				<script src="//cdn.taboola.com/libtrc/buzzdrives-sc/tfa.js"></script>
			<?php } ?>
			<?php // 30-cars-that-will-last-more-than-250000-miles ?>
			<?php if( isset($post) && isset($post->ID) && $post->ID == 5981 ) { ?>
				<script type="text/javascript">
				window._tfa = window._tfa || [];
				_tfa.push({ notify: 'mark',type: 'Last-250k-miles' });
				</script>
				<script src="//cdn.taboola.com/libtrc/buzzdrives-sc/tfa.js"></script>
			<?php } ?>
		<?php } ?>
		<?php /* taboola retargeting pixels - end */ ?>
				
				
		<?php /* Favicons - start */ ?>
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-180x180.png">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/favicon-16x16.png" sizes="16x16">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/favicon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-36x36.png" sizes="36x36">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-48x48.png" sizes="48x48">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-72x72.png" sizes="72x72">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-144x144.png" sizes="144x144">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-192x192.png" sizes="192x192">
		<link rel="manifest" href="<?php echo get_img_folder(); ?>/favicons/manifest.json">
		<meta name="apple-mobile-web-app-title" content="BuzzDrives">
		<meta name="application-name" content="BuzzDrives">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?php echo get_img_folder(); ?>/favicons/ms-icon-70x70.png">
		<meta name="msapplication-TileImage" content="<?php echo get_img_folder(); ?>/favicons/ms-icon-144x144.png">
		<meta name="msapplication-TileImage" content="<?php echo get_img_folder(); ?>/favicons/ms-icon-150x150.png">
		<meta name="msapplication-TileImage" content="<?php echo get_img_folder(); ?>/favicons/ms-icon-310x310.png">
		<meta name="theme-color" content="#ffffff">
		<?php /* Favicons - end */ ?>
		
		<?php //<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"> ?>
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" />
				
		<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />

		
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		        
        <?php /* load a fake adframe.js file that will be blocked by adblocker. this way we can determine if user uses an adblocker extension */ ?>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/adframe.js"></script>

        <?php /* Cookie consent - start */ ?>
        <?php if( !isset($_GET['cct']) ) { ?>
        <script type="text/javascript">
            window.cookieconsent_options = {
                "message": "This website uses cookies to ensure you get the best experience on our website",
                "dismiss": "Got it!",
                "learnMore": "More info",
                "link":"/privacy-policy/",
                "theme":"light-top"
            };
        </script>
        <script type="text/javascript" async src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
        <?php } ?>
        <?php /* Cookie consent - end */ ?>
		
		<?php wp_head(); ?>
		
		<?php /* Start Visual Website Optimizer Asynchronous Code - removed on 26.03.2019 ?>
			<!-- Start Visual Website Optimizer Asynchronous Code -->
			<script type='text/javascript'>
			var _vwo_code=(function(){
			var account_id=310134,
			settings_tolerance=2000,
			library_tolerance=2500,
			use_existing_jquery=false,
			// DO NOT EDIT BELOW THIS LINE 
			f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
			</script>
			<!-- End Visual Website Optimizer Asynchronous Code -->
		<?php /* End Visual Website Optimizer Asynchronous Code */ ?>
		
		
		<?php /* Taboola Head Code Start */ ?>
		<script type="text/javascript">
		window._taboola = window._taboola || [];
		_taboola.push({article:'auto'});
		!function (e, f, u, i) {
		if (!document.getElementById(i)){
		e.async = 1;
		e.src = u;
		e.id = i;
		f.parentNode.insertBefore(e, f);
		}
		}(document.createElement('script'),
		document.getElementsByTagName('script')[0],
		'//cdn.taboola.com/libtrc/buzzdrive/loader.js',
		'tb_loader_script');
		if(window.performance && typeof window.performance.mark == 'function')
		{window.performance.mark('tbl_ic');}
		</script>		
		<?php /* Taboola Head Code END */ ?>

        <?php /* Taboola Below Articles - start */ ?>
        <?php if( showTaboolaBelowArticlesTags() ) { ?>
			<script type="text/javascript">
				window._taboola = window._taboola || [];
				_taboola.push({photo:'auto'});
				!function (e, f, u, i) {
					if (!document.getElementById(i)){
						e.async = 1;
						e.src = u;
						e.id = i;
						f.parentNode.insertBefore(e, f);
					}
				}(document.createElement('script'),
					document.getElementsByTagName('script')[0],
					'//cdn.taboola.com/libtrc/buzzdrive/loader.js',
					'tb_loader_script');
				if(window.performance && typeof window.performance.mark == 'function')
				{window.performance.mark('tbl_ic');}
			</script>
		<?php } ?>
        <?php /* Taboola Below Articles - end */ ?>

        <?php /* Show the small video in the right sidebat - start - Removed on 27.02.2019 kept as reference ?>
        <?php if( showSmallVideoRightSidebar() ) { ?>
            <script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
            <script>
                var googletag = googletag || {};
                googletag.cmd = googletag.cmd || [];
            </script>
            <script>
                googletag.cmd.push(function() {
                    googletag.defineSlot('/207764977/BD_Top_Right_Sidebar_Video', [300, 250], 'div-gpt-ad-1548229964157-0').addService(googletag.pubads());
                    googletag.pubads().enableSingleRequest();
                    googletag.enableServices();
                });
            </script>
        <?php } ?>
        <?php /* Show the small video in the right sidebat - end */ ?>
						
		<?php /* Yahoo pixel - start - removed on 26.03.2019 ?>
		<script type="application/javascript">(function(w,d,t,r,u){w[u]=w[u]||[];w[u].push({'projectId':'10000','properties':{'pixelId':'10033016'}});var s=d.createElement(t);s.src=r;s.async=true;s.onload=s.onreadystatechange=function(){var y,rs=this.readyState,c=w[u];if(rs&&rs!="complete"&&rs!="loaded"){return}try{y=YAHOO.ywa.I13N.fireBeacon;w[u]=[];w[u].push=function(p){y([p])};y(c)}catch(e){}};var scr=d.getElementsByTagName(t)[0],par=scr.parentNode;par.insertBefore(s,scr)})(window,document,"script","https://s.yimg.com/wi/ytc.js","dotq");</script>
		<?php /* Yahoo pixel - stop */ ?>
					
		
	</head>
	
	<body <?php body_class(); ?> data-id="<?php echo get_the_ID(); ?>">
		
		<?php /* add class to the body for desktops */ ?>
		<script type="text/javascript" >
			if( isDesktop() ) {
				$('body').addClass('is_desktop');
			}
			if( isMobile() ) {
				$('body').addClass('is_mobile');
			}
		</script>
		
		<?php do_action('after_body'); // it hooks any plugins that require an early load and it includes HTML. ex: FB SDK ?>
					
		
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KMZ7DSR"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		
		<div id="wrapper">
			<div id="main-gradient"></div><!-- end - #main-gradient -->
			
			<header id="header" style="position:relative;" >
				<div id="header-inner">		
				
					<div id="header-nav-wrapper">
						<div class="container">
							<button type="button" id="mobile-menu-btn" class="visible-xs">Menu <span class="glyphicon glyphicon-menu-hamburger"></span></button>
							<nav id="nav" class="hidden-xs">
								<?php wp_nav_menu(
									array(
										'theme_location' => 'primary-menu',
										'container' => '',
										'menu_id' => '',
										'menu_class' => 'clearfix',
									)
								); ?>
							</nav><!-- end - #nav -->
							
							<!--
							<div id="header-search" class="hidden-xs">
								<?php // get_search_form(); ?>
							</div>  end - #header-search -->
							
						</div>
					</div><!-- end - #header-nav-wrapper -->
						
					<div id="header-logo">
						<div class="container">
							<div id="header-logo-inner">
								<div id="header-logo-round">
									<nav id="nav-mobile">
									<?php wp_nav_menu(
										array(
											'theme_location' => 'primary-menu',
											'container' => '',
											'menu_id' => 'menu-mobile',
											'menu_class' => 'clearfix',
										)
									); ?>
									</nav><!-- end - #nav-mobile -->
																
									<a href="<?php echo get_home_url(); ?><?php get_redesign_query_parameter(); ?>">
										<img class="center-block img-responsive" src="<?php echo get_img_folder(); ?>/buzzdrives_logo.png" width="334" height="55" alt="BuzzDrives Logo">
									</a>
								</div>
							</div>							
					
					<?php /* Don't show ads on the 404 page  */ ?>
					<?php if ( !is_404() ) { ?>
																						
							<div id="header-ad-inner">
								<div id='div_top_banner_desktop'>				
									<?php /* MM / DFP - START */ ?>
										<script>
											(function(){
												if( !isMobile() ) {
													document.write ('<div id="top_banner_desktop"></div>');
												}												
											}());
										</script>
									<?php /* MM / DFP - END */ ?>
											
								</div>
							</div><!-- end - #header-ad-inner -->
								
					
					<?php } ?>
					<?php /* Don't show ads on the 404 page  */ ?>
					
									
						</div>
					</div><!-- end - #header-logo -->
				</div><!-- end - #header-inner -->
			</header><!-- end - #header -->
			
			<div id="main">
