function setCookie(name, value, days) {
    var d = new Date;
    d.setTime(d.getTime() + 24*60*60*1000*days);
    document.cookie = name + "=" + value + ";path=/;expires=" + d.toGMTString();
}
function getCookie(name) {
    var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
    return v ? v[2] : null;
}
function deleteCookie(name) { setCookie(name, '', -1); }
jQuery(document).ready(function($){
    $('.quiz-container .start-quiz').click(function(e){
        $cookie_val = {
            started: true,
            finished: false,
            currentIndex: 0,
            total_score: 0,
            questions: []
        };
        setCookie('quiz_'+ $('.quiz-container').attr('data-postid'), JSON.stringify($cookie_val), 365);
        var quiz_container = $(this).closest('.quiz-container');
        if(quiz_container.hasClass('scroll')){
            quiz_container.find('.question-container').first().addClass('visible');
        }
    });

    $('.quiz-container .next-question').click(function(e){
        if($(this).closest('.question-container').hasClass('answered')){
            $(this).parent().next().addClass('visible');
        }
    });

    $('.quiz-container .answers-container button').click(function (e) {
        var answer_key = $(this).attr('data-answer-key');
        var question = $(this).closest('.question-container');
        var question_img = $(this).parent().siblings('.question-image-container');
        var is_correct = $(this).hasClass('correct');

        $(e.target).addClass('selected');
        /*$(this).parent().next().addClass('visible');
        $('.question-image-container').parent().append('<style>.question-image-container:before{background-color: rgba(0,0,0,0.75);}</style>');
        /*$('.question-image-container').parent().addClass('overlay');*/
        question.addClass('answered');
        question_img.addClass('overlay');
        if(is_correct){
            question.addClass('correct');
        } else {
            question.addClass('wrong');
        }

        question.find('.question-image-container .answer-explanation[data-answer-key="' + answer_key + '"]').show();

        //enable the end quiz button
        if(question.find('.end-quiz').length > 0 ){
            console.log('bla');
            $('.end-quiz').removeAttr('disabled');
        }

        //update progress cookie
        $cookie_val = JSON.parse(getCookie('quiz_' + $('.quiz-container').attr('data-postid')));
        $cookie_val.currentIndex = $(this).closest('.question-container').attr('data-key');
        $cookie_val.total_score = $(this).hasClass('correct') ? $cookie_val.total_score + 1 : $cookie_val.total_score;
        $cookie_val.questions.push({
            qkey: $(this).closest('.question-container').attr('data-key'),
            akey: answer_key,
            is_correct: is_correct
        });
        setCookie('quiz_'+ $('.quiz-container').attr('data-postid'), JSON.stringify($cookie_val), 365);
        if($(this).closest('.quiz-container').hasClass('scroll')){
            var current_question = $(this).closest('.question-container');
            if(current_question.hasClass('answered')){
                current_question.next().addClass('visible');
            }
        }
    });

    if ($('.question-container').hasClass('answered') && !$('.question-image-container').hasClass('overlay')){
        $('.question-image-container').addClass('overlay');
    }

    $('.quiz-container .end-quiz').click(function(e){
        $cookie_val = JSON.parse(getCookie('quiz_' + $('.quiz-container').attr('data-postid')));
        $cookie_val.finished = true;
        setCookie('quiz_'+ $('.quiz-container').attr('data-postid'), JSON.stringify($cookie_val), 365);

        if($(this).closest('.quiz-container').hasClass('scroll')){
            $('.results-container .total-score').text($cookie_val.total_score);
            $('.results-container').show();
        }
    });

    $('.quiz-container .start-over').click(function (e) {
        deleteCookie('quiz_' + $('.quiz-container').attr('data-postid'));
        if($('.quiz-container').hasClass('scroll')){
            window.location.reload();
        }
    });

    $('.start-quiz').click(function () {
        var first_quiz = document.querySelector('.question-container');
        $('.site-container').animate({
            scrollTop: first_quiz.offsetTop - 50
        }, 1500);
    });

    setTimeout(function(){
        $(window).on('unload', function() {
            $(window).scrollTop(0);
        }, 3000);
    });
});