jQuery(document).ready(function($){

    var question_markup = '<div class="question-repeater" data-key="${key+1}">\n' +
        '                <p>\n' +
        '                    <label for="questions[${key}][question_name]">Question name:</label>\n' +
        '                    <br/>\n' +
        '                    <input type="text" class="widefat" id="questions[${key}][question_name]" name="questions[${key}][question_name]" value="" size="30" required>\n' +
        '                </p>\n' +
        '				<p>' +
        '					<label for="questions[${key}][question_image]">Question image:</label>' +
        '					<br/>' +
        '					<input type="text" class="widefat" id="questions[${key}][question_image]" name="questions[${key}][question_image]" value="" size="30" required>' +
        '					<button type="button" class="upload_image_button button button-default">Add Image</button>' +
        '				</p>' +
        '				<p>' +
        '					<label for="questions[${key}][explanation]">Explanation:</label>' +
        '					<br/>' +
        '					<textarea class="widefat" rows="4" id="questions[${key}][explanation]" name="questions[${key}][explanation]" required></textarea>' +
        '				</p>' +
        '                    <div class="answer-repeater">\n' +
        '                        <p>\n' +
        '                            <label for="questions[${key}][answers][0][answer]">Answer:</label>\n' +
        '                            <br/>\n' +
        '                            <input type="text"\n' +
        '                                   class="widefat"\n' +
        '                                   name="questions[${key}][answers][0][answer]"\n' +
        '                                   id="questions[${key}][answers][0][answer]"\n' +
        '                                   value=""\n' +
        '                                   size="30"\n' +
        '                                   required\n' +
        '                            >\n' +
        '                        </p>\n' +
        '                        <p class="is-correct">\n' +
        '                            <label for="questions[${key}][answers][0][correct]">\n' +
        '                                <input type="checkbox"\n' +
        '                                       name="questions[${key}][answers][0][correct]"\n' +
        '                                       id="questions[${key}][answers][0][correct]"\n' +
        '                                >\n' +
        '                                Is correct?\n' +
        '                            </label>\n' +
        '                        </p>\n' +
        '                    </div>\n' +
        '                    <div class="answer-repeater">\n' +
        '                        <p>\n' +
        '                            <label for="questions[${key}][answers][1][answer]">Answer:</label>\n' +
        '                            <br/>\n' +
        '                            <input type="text"\n' +
        '                                   class="widefat"\n' +
        '                                   name="questions[${key}][answers][1][answer]"\n' +
        '                                   id="questions[${key}][answers][1][answer]"\n' +
        '                                   value=""\n' +
        '                                   size="30"\n' +
        '                            >\n' +
        '                        </p>\n' +
        '                        <p class="is-correct">\n' +
        '                            <label for="questions[${key}][answers][1][correct]">\n' +
        '                                <input type="checkbox"\n' +
        '                                       name="questions[${key}][answers][1][correct]"\n' +
        '                                       id="questions[${key}][answers][1][correct]"\n' +
        '                                >\n' +
        '                                Is correct?\n' +
        '                            </label>\n' +
        '                        </p>\n' +
        '                    </div>\n' +
        '                    <div class="answer-repeater">\n' +
        '                        <p>\n' +
        '                            <label for="questions[${key}][answers][2][answer]">Answer:</label>\n' +
        '                            <br/>\n' +
        '                            <input type="text"\n' +
        '                                   class="widefat"\n' +
        '                                   name="questions[${key}][answers][2][answer]"\n' +
        '                                   id="questions[${key}][answers][2][answer]"\n' +
        '                                   value=""\n' +
        '                                   size="30"\n' +
        '                            >\n' +
        '                        </p>\n' +
        '                        <p class="is-correct">\n' +
        '                            <label for="questions[${key}][answers][2][correct]">\n' +
        '                                <input type="checkbox"\n' +
        '                                       name="questions[${key}][answers][2][correct]"\n' +
        '                                       id="questions[${key}][answers][2][correct]"\n' +
        '                                >\n' +
        '                                Is correct?\n' +
        '                            </label>\n' +
        '                        </p>\n' +
        '                    </div>\n' +
        '                    <div class="answer-repeater">\n' +
        '                        <p>\n' +
        '                            <label for="questions[${key}][answers][3][answer]">Answer:</label>\n' +
        '                            <br/>\n' +
        '                            <input type="text"\n' +
        '                                   class="widefat"\n' +
        '                                   name="questions[${key}][answers][3][answer]"\n' +
        '                                   id="questions[${key}][answers][3][answer]"\n' +
        '                                   value=""\n' +
        '                                   size="30"\n' +
        '                            >\n' +
        '                        </p>\n' +
        '                        <p class="is-correct">\n' +
        '                            <label for="questions[${key}][answers][3][correct]">\n' +
        '                                <input type="checkbox"\n' +
        '                                       name="questions[${key}][answers][3][correct]"\n' +
        '                                       id="questions[${key}][answers][3][correct]"\n' +
        '                                >\n' +
        '                                Is correct?\n' +
        '                            </label>\n' +
        '                        </p>\n' +
        '                    </div>\n' +
        '                <p class="add-code" ${style}>\n' +
        '                    <label for="questions[${key}][ad_code]">Ad code:</label>\n' +
        '                    <br/>\n' +
        '                    <textarea rows="5" class="widefat" id="questions[${key}][ad_code]" name="questions[${key}][ad_code]"></textarea>\n' +
        '                </p>\n' +
        '                <button type="button" class="delete_question button button-default">Delete Question</button>\n' +
        '            </div>';

    //show/hide add_code field by quiz type
    $('select[name="quiz_type"]').change(function(e){
        if(e.target.value === 'scroll'){
            $('.add-code').show();
        } else {
            $('.add-code').hide();
        }
    });

    //There should be only one correct answer for a question
    $('.is-correct input[type="checkbox"]').change(function(e){
        if($(e.target).is(':checked')){
            var checkboxes = $(e.target).closest('.question-repeater').find('.is-correct');
            var target_name =  $(e.target).attr('name');
            checkboxes.each(function(e){
               if($(checkboxes[e]).find('input').attr('name') !== target_name) {
                   $(checkboxes[e]).find('input').removeAttr('checked');
               }
            });
        }
    });

    // Added upload media functionality
    var gk_media_init = function(selector, button_selector)  {
        var clicked_button = false;
        jQuery(selector).each(function (i, input) {
            var button = jQuery(input).next(button_selector);
            button.click(function (event) {
                event.preventDefault();
                var selected_img;
                clicked_button = jQuery(this);

                // check for media manager instance
                if(wp.media.frames.gk_frame) {
                    wp.media.frames.gk_frame.open();
                    return;
                }
                // configuration of the media manager new instance
                wp.media.frames.gk_frame = wp.media({
                    title: 'Select image',
                    multiple: false,
                    library: {
                        type: 'image'
                    },
                    button: {
                        text: 'Use selected image'
                    }
                });

                // Function used for the image selection and media manager closing
                var gk_media_set_image = function() {
                    var selection = wp.media.frames.gk_frame.state().get('selection');

                    // no selection
                    if (!selection) {
                        return;
                    }

                    // iterate through selected elements
                    selection.each(function(attachment) {
                        var url = attachment.attributes.url;
                        clicked_button.prev(selector).val(url);
                    });
                };

                // closing event for media manger
                wp.media.frames.gk_frame.on('close', gk_media_set_image);
                // image selection event
                wp.media.frames.gk_frame.on('select', gk_media_set_image);
                // showing media manager
                wp.media.frames.gk_frame.open();
            });
        });
    };

    // Added upload media functionality
    var gk_media_new_image = function(input, add_image_button)  {
        var clicked_button = false;
            $(add_image_button).click(function (event) {
                event.preventDefault();
                var selected_img;
                clicked_button = jQuery(this);

                // configuration of the media manager new instance
                wp.media.frames.gk_frame = wp.media({
                    title: 'Select image',
                    multiple: false,
                    library: {
                        type: 'image'
                    },
                    button: {
                        text: 'Use selected image'
                    }
                });

                // Function used for the image selection and media manager closing
                var gk_media_set_image = function() {
                    var selection = wp.media.frames.gk_frame.state().get('selection');
                    // no selection
                    if (!selection) {
                        return;
                    }

                    // iterate through selected elements
                    selection.each(function(attachment) {
                        var url = attachment.attributes.url;
                        clicked_button.prev(input).val(url);
                    });
                };
                // closing event for media manger
                wp.media.frames.gk_frame.on('close', gk_media_set_image);
                // image selection event
                wp.media.frames.gk_frame.on('select', gk_media_set_image);
                // showing media manager
                wp.media.frames.gk_frame.open();
            });
    };

    var upload_button =  $('.upload_image_button');
    var input = upload_button.prev('input');
    gk_media_init(input,upload_button);

    //Add Question
    $('.add_question').click(function(){
        var current_index = parseInt($('.question-repeater').last().attr('data-key'))+1;
        var new_question = question_markup.split('${key+1}').join(current_index);
        new_question = new_question.split('${key}').join(current_index-1);
        if($('select[name="quiz_type"]').val() === 'paginated'){
            new_question = new_question.split('${style}').join(' style="display:none;"');
        }
        $(new_question).insertAfter($('.question-repeater').last());

        var upload_button =  $('.upload_image_button')[current_index-1];
        var input = $(upload_button).prev('input')[0];
        gk_media_new_image(input, upload_button);
    });

    //delete Question
    $(document).on('click', '.delete_question', function(e){
        $(this).closest('.question-repeater').remove();

        //reindex questions
        $('.question-repeater').each(function(i, e){
            var current_index = parseInt($(e).attr('data-key')) - 1;
            $(e).attr('data-key',parseInt(i)+1);
            e = $(e)[0].outerHTML.split('questions[' + current_index + ']').join('questions[' + i + ']');
        });
    });


});