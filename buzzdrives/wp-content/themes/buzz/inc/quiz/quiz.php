<?php
/**
 * Add Custom Post Type: Quizzes.
 */
function quiz_register_my_cpts() {
    $labels = array(
        "name" => __( "Quizzes", "storefront" ),
        "singular_name" => __( "Quiz", "storefront" ),
    );

    $args = array(
        "label" => __( "Quizzes", "storefront" ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "delete_with_user" => false,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "quiz", "with_front" => true ),
        "query_var" => true,
        "menu_icon" => "dashicons-forms",
        "supports" => array( "title", "editor", "thumbnail" ),
    );

    register_post_type( "quiz", $args );
}
add_action( 'init', 'quiz_register_my_cpts' );

/**
 * Add Custom Meta Boxes
 */
function adding_custom_meta_boxes( $post ) {
    add_meta_box(
        'quiz-settings',
        __( 'Quiz settings' ),
        'quiz_render_my_meta_box',
        'quiz',
        'normal',
        'default'
    );
}
add_action( 'add_meta_boxes_quiz', 'adding_custom_meta_boxes' );

/**
 * Add Custom Image size
 */
add_action( 'after_setup_theme', 'quiz_theme_setup' );
function quiz_theme_setup() {
    add_image_size( 'quiz-thumb', 1022, 400, true );
}

function quiz_render_my_meta_box($post){
    $quiz_type = get_post_meta($post->ID, 'quiz_type', true);
    $questions = get_post_meta($post->ID, 'questions', true);
    ?>
    <p>
        <label for="quiz_type">Quiz type:</label>
        <br/>
        <select name="quiz_type" id="quiz_type" class="postbox">
            <option value="">Please select</option>
            <option value="paginated" <?php selected($quiz_type, 'paginated'); ?>>Paginated Quiz</option>
            <option value="scroll" <?php selected($quiz_type, 'scroll'); ?>>Scroll Quiz</option>
        </select>
    </p>
    <p>Questions:</p>
    <div class="question-container">
    <?php if(!empty($questions)){
        foreach ($questions as $key => $question){ ?>
            <div class="question-repeater" data-key="<?php echo $key+1;?>">
                <p>
                    <label for="questions[<?php echo $key;?>][question_name]">Question:</label>
                    <br/>
                    <input type="text" class="widefat" id="questions[<?php echo $key;?>][question_name]" name="questions[<?php echo $key;?>][question_name]" value="<?php echo $question["question_name"]?>" size="30" required>
                </p>
                <p>
                    <label for="questions[<?php echo $key;?>][question_image]">Question image:</label>
                    <br/>
                    <input type="text" class="widefat" id="questions[<?php echo $key;?>][question_image]" name="questions[<?php echo $key;?>][question_image]" value="<?php echo $question["question_image"]?>" size="30" required>
                    <button type="button" class="upload_image_button button button-default">Add Image</button>
                </p>
                <p>
                    <label for="questions[<?php echo $key;?>][explanation]">Explanation:</label>
                    <br/>
                    <textarea class="widefat" rows="4" id="questions[<?php echo $key;?>][explanation]" name="questions[<?php echo $key;?>][explanation]" required><?php echo $question["explanation"]?></textarea>
                </p>
                <?php foreach ($question["answers"] as $akey => $answer){ ?>
                    <div class="answer-repeater" data-key="<?php echo $akey+1;?>">
                        <p>
                            <label for="questions[<?php echo $key;?>][answers][<?php echo $akey;?>][answer]">Answer:</label>
                            <br/>
                            <input type="text"
                                   class="widefat"
                                   id="questions[<?php echo $key;?>][answers][<?php echo $akey;?>][answer]"
                                   name="questions[<?php echo $key;?>][answers][<?php echo $akey;?>][answer]"
                                   value="<?php echo $answer["answer"]?>"
                                   size="30"
                                   <?php $akey === 0 ? ' required' : '';?>
                            >
                        </p>
                        <p class="is-correct">
                            <label for="questions[<?php echo $key;?>][answers][<?php echo $akey;?>][correct]">
                                <input type="checkbox"
                                       name="questions[<?php echo $key;?>][answers][<?php echo $akey;?>][correct]"
                                       id="questions[<?php echo $key;?>][answers][<?php echo $akey;?>][correct]"
                                       <?php echo $answer["correct"] ? 'checked="checked"' : ''; ?>
                                >
                                Is correct?
                            </label>
                        </p>
                    </div>
                <?php } ?>
                <p class="add-code" <?php echo $quiz_type !== 'scroll' ? 'style="display: none;"' : '' ;?>>
                    <label for="questions[<?php echo $key;?>][ad_code]">Ad code:</label>
                    <br/>
                    <textarea rows="5" class="widefat" id="questions[<?php echo $key;?>][ad_code]" name="questions[<?php echo $key;?>][ad_code]"><?php echo $question["ad_code"]?></textarea>
                </p>
                <button type="button" class="delete_question button button-default">Delete Question</button>
            </div>
        <?php }
    } else { ?>
        <div class="question-repeater" data-key="1">
            <p>
                <label for="questions[0][question_name]">Question:</label>
                <br/>
                <input type="text" class="widefat" id="questions[0][question_name]" name="questions[0][question_name]" value="" size="30" required>
            </p>
            <p>
                <label for="questions[0][question_image]">Question image:</label>
                <br/>
                <input type="text" class="widefat" id="questions[0][question_image]" name="questions[0][question_image]" value="" size="30" required>
                <button type="button" class="upload_image_button button button-default">Add Image</button>
            </p>
            <p>
                <label for="questions[0][explanation]">Explanation:</label>
                <br/>
                <textarea class="widefat" rows="4" id="questions[0][explanation]" name="questions[0][explanation]" required></textarea>
            </p>
            <div class="answer-repeater" data-key="1">
                <p>
                    <label for="questions[0][answers][0][answer]">Answer:</label>
                    <br/>
                    <input type="text" class="widefat" id="questions[0][answers][0][answer]" name="questions[0][answers][0][answer]" value="" size="30" required>
                </p>
                <p class="is-correct">
                    <label for="questions[0][answers][0][correct]">
                        <input type="checkbox" name="questions[0][answers][0][correct]" id="questions[0][answers][0][correct]">
                        Is correct?
                    </label>
                </p>
            </div>
            <div class="answer-repeater" data-key="2">
                <p>
                    <label for="questions[0][answers][1][answer]">Answer:</label>
                    <br/>
                    <input type="text" class="widefat" id="questions[0][answers][1][answer]" name="questions[0][answers][1][answer]" value="" size="30">
                </p>
                <p class="is-correct">
                    <label for="questions[0][answers][1][correct]">
                        <input type="checkbox" name="questions[0][answers][1][correct]" id="questions[0][answers][1][correct]">
                        Is correct?
                    </label>
                </p>
            </div>
            <div class="answer-repeater" data-key="3">
                <p>
                    <label for="questions[0][answers][2][answer]">Answer:</label>
                    <br/>
                    <input type="text" class="widefat" id="questions[0][answers][2][answer]" name="questions[0][answers][2][answer]" value="" size="30">
                </p>
                <p class="is-correct">
                    <label for="questions[0][answers][2][correct]">
                        <input type="checkbox" name="questions[0][answers][2][correct]" id="questions[0][answers][2][correct]">
                        Is correct?
                    </label>
                </p>
            </div>
            <div class="answer-repeater" data-key="4">
                <p>
                    <label for="questions[0][answers][3][answer]">Answer:</label>
                    <br/>
                    <input type="text" class="widefat" id="questions[0][answers][3][answer]" name="questions[0][answers][3][answer]" value="" size="30">
                </p>
                <p class="is-correct">
                    <label for="questions[0][answers][3][correct]">
                        <input type="checkbox" name="questions[0][answers][3][correct]" id="questions[0][answers][3][correct]">
                        Is correct?
                    </label>
                </p>
            </div>

            <p class="add-code" style="display: none;">
                <label for="questions[0][ad_code]">Ad code:</label>
                <br/>
                <textarea rows="5" class="widefat" id="questions[0][ad_code]" name="questions[0][ad_code]"></textarea>
            </p>
            <button type="button" class="delete_question button button-default">Delete Question</button>
        </div>
    <?php }?>
        <button type="button" class="add_question button button-default">Add Question</button>
    </div>
<?php }

function quiz_save_postdata($post_id){
    if (array_key_exists('quiz_type', $_POST) && $_POST['quiz_type']) {
        update_post_meta(
            $post_id,
            'quiz_type',
            $_POST['quiz_type']
        );
    }
    if (array_key_exists('questions', $_POST) && $_POST['questions']) {
        update_post_meta(
            $post_id,
            'questions',
            $_POST['questions']
        );
    }
}
add_action('save_post', 'quiz_save_postdata');

// Add custom CSS & JS in Admin
function quiz_admin_style() {
    wp_enqueue_style( 'quiz-admin-css',get_template_directory_uri().'/inc/quiz/css/admin.css');
    wp_enqueue_script('quiz-admin-js', get_template_directory_uri().'/inc/quiz/js/admin.js');
}
add_action('admin_enqueue_scripts', 'quiz_admin_style');

//Add custom CSS & JS
function add_theme_scripts() {
    if ( is_singular('quiz') ) {
        wp_enqueue_style( 'quiz-client-css',get_template_directory_uri().'/inc/quiz/css/client.css');
        wp_enqueue_script('quiz-client-js', get_template_directory_uri().'/inc/quiz/js/client.js');
    }
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

//get question  html - helper function
function quiz_get_question_template($quiz_type, $qkey, $length, $question){
    $cookie = quiz_get_cookie();

    $question_html = '<div class="question-container' . ($cookie && !$cookie->questions[$qkey] && $cookie->questions[$qkey - 1] ? ' visible' : '') . ($cookie && $cookie->questions[$qkey] ? ' answered' : '') . ($cookie && $cookie->questions[$qkey] ? $cookie->questions[$qkey]->is_correct ? ' correct' : ' wrong' : '') .'" data-key="' . $qkey . '">';
    $question_html .= '<h2>' . ($qkey + 1) . '. '. $question['question_name'] . '</h2>';

    $question_html .= '<div class="question-image-container">';
        $question_html .= '<img src="' . $question['question_image'] . '" alt="' . $question['question_name'] . '" class="img-responsive"/>';
        $question_html .= '<div class="explanation-container">';
            $question_html .= '<h3 class="correct">Right answer</h3>';
            $question_html .= '<h3 class="wrong">Wrong answer</h3>';
            $question_html .= wpautop($question['explanation']);
        $question_html .= '</div>';
    $question_html .= '</div>';

    $question_html .= '<div class="answers-container">';
    foreach($question['answers'] as $key => $answer){
        if($answer['answer']){
            $correct = $answer['correct'] ? 'correct' : 'wrong';
            $question_html .= '<button class="answer ' . $correct . ($cookie && $cookie->questions[$qkey] && intval($cookie->questions[$qkey]->akey) === $key ? ' selected' : '') . '" data-answer-key="' .  $key . '">' . $answer['answer'] . '</button>';
        }
    }
    $question_html .= '</div>';
    if($question['ad_code'] && $quiz_type === 'scroll'){
          $question_html .= '<div class="quiz-ad">';
            $question_html .= $question['ad_code'];
          $question_html .= '</div>';
    }
    $question_html .= '<div class="" style="text-align: center">';
    if($length-1 === $qkey) {
        if($quiz_type === 'paginated'){
            $question_html .= '<a href="' . get_permalink(get_the_ID()) . ($qkey + 3) . '/" class="end-quiz center-button quiz-nav-button" disabled="disabled">End quiz</a>';
        } else {
            $question_html .= '<button class="end-quiz quiz-nav-button center-button" disabled="disabled">End quiz</button>';
        }
    }
    $question_html .= '</div>';

    $question_html .= '</div>';

    return $question_html;
}
//get results template - helper function
function quiz_get_results_template($length, $quiz_type){
    $cookie = quiz_get_cookie();
    $results_html = '<div class="results-container" ' . ($cookie && $cookie->finished ? 'style="display:block"' : '') . '>';

    $results_html .= '<p>Congratulations! You have finished the Quizz!</p>';
    $results_html .= '<p class="score">You scored <span class="total-score">' . ($cookie ? $cookie->total_score : '') . '</span> out of ' . $length . ' points!</p>';

    $results_html .= '<div class="buttons-container">';
    $results_html .='<style>#csp_btns {display: none;}</style>';
    if($quiz_type === 'paginated'){
        $results_html .= '<a href="' . get_permalink(get_the_ID()) . '2/" class="start-over quiz-nav-button">Start over</a>';
    } else {
        $results_html .= '<button class="start-over quiz-nav-button">Start over</button>';
    }
    $results_html .= get_next_post_link('%link') ? get_next_posts_link('%link', 'Next quiz') : get_previous_post_link('%link', 'Next quiz');
    $results_html .= '</div>';

    $results_html .= '</div>';

    return $results_html;
}

//get quiz cookie - helper function
function quiz_get_cookie(){
    if(isset($_COOKIE["quiz_" . get_the_ID()])){
        $cookie = str_replace('\\', '', $_COOKIE["quiz_" . get_the_ID()]);
        $cookie = json_decode($cookie);
        return $cookie;
    }

    return false;
}

//filter the content to return the actual quiz
function quiz_construct_quiz ($content) {
    if (is_singular('quiz')) {
        $post_id = get_the_ID();
        $quiz_type = get_post_meta($post_id, 'quiz_type', true);
        $questions = get_post_meta($post_id, 'questions', true);

        if ($quiz_type === 'paginated' && !empty($questions)) {
            global $page, $pages, $multipage, $numpages;

            // Init
            $out = '';
            $break = '<!--nextpage-->';
            $page = (int)get_query_var('page');

            //first page - quiz description
            $out .= '<div class="quiz-container ' . $quiz_type . '"  data-postid="' . $post_id . '">';
                if (has_post_thumbnail($post_id)) {
                    $out .= get_the_post_thumbnail($post_id, 'quiz-thumb', array( 'class' => 'img-responsive'));
                }
                $out .= $content;
                $out .= '<div style="text-align:center;">';
                    $out .= '<a href="' . get_permalink($post_id) . '2/" class="start-quiz center-button quiz-nav-button">Start quiz</a>';
                $out .= '</div>';
            $out .= '</div>' . $break;

            // Add page breaks between questions
            foreach ($questions as $key => $question) {
                $out .= '<div class="quiz-container ' . $quiz_type . '"  data-postid="' . $post_id . '">';
                    $out .= quiz_get_question_template($quiz_type, $key, count($questions), $question);
                $out .= '</div>' . $break;
            }

            $out .= '<div class="quiz-container ' . $quiz_type . '"  data-postid="' . $post_id . '">';
                $out .= quiz_get_results_template(count($questions), $quiz_type);
            $out .= '</div>';

            // Adjust the globals
            $pages = explode($break, $out);
            $page = ($page > count($pages) || $page < 1) ? 1 : $page;
            $numpages = count($pages);
            $multipage = 1;

            $content = $pages[$page - 1];
        } else {
            $out = '';
            $out .= '<div class="quiz-container ' . $quiz_type . '" data-postid="' . $post_id . '">';
                if (has_post_thumbnail($post_id)) {
                    $out .= get_the_post_thumbnail($post_id, 'quiz-thumb', array( 'class' => 'img-responsive'));
                }
                $out .= $content;
                $out .= '<div style="text-align:center;">';
                    $out .= '<button class="start-quiz center-button quiz-nav-button">Start quiz</button>';
                $out .= '</div>';


                foreach ($questions as $key => $question) {
                    $out .= quiz_get_question_template($quiz_type, $key, count($questions), $question);
                }
                $out .= quiz_get_results_template(count($questions), $quiz_type);
            $out .= '</div>';

            $content = $out;
        }
    }

    return $content;
}
add_filter( 'the_content', 'quiz_construct_quiz');