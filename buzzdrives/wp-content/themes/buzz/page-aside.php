<?php
/*
	Template Name: Page with sidebar
*/
?>

<?php get_header(); ?>

<div id="page-with-aside">
	<div class="container">
		<br />
		<div class="row">		
			<div id="content-with-aside">
				<?php get_template_part('loop', 'page'); ?>
			</div><!-- end - main content -->
			
			<aside id="aside" class="mobile-below">
				<?php do_action('buzz_page_aside'); ?>
			</aside><!-- end - #sidebar -->
		</div>
	</div>
</div><!-- page with sidebar template - end -->
	
<?php get_footer(); ?>