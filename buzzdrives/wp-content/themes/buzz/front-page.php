<?php
/*
	Template name: Homepage
*/
?>

<?php get_header(); ?>

<div id="home">
	<div class="container">
		<div id="featured-posts">
			<div class="clearfix">
				<?php
				// WP_Query arguments
				$args = array (
					'pagination'             => false,
					'post_type'              => 'post',
					'posts_per_page'         => '3',
					'order'                  => 'DESC',
					'orderby'                => 'date',
					'tax_query' => array(
						array(
							'taxonomy'  => 'category',
							'field'     => 'slug',
							'terms'     => 'uncategorized',
							'operator'  => 'NOT IN'
						)
					),
					'meta_query' => array(
						array(
							'key'       => 'hidden_post',
							'compare'   => 'NOT EXISTS',
							'type'      => 'CHAR',
						),
					),
				);

				// The Query
				$featured_posts = new WP_Query( $args );
				$i = 1;

				// The Loop
				if ( $featured_posts->have_posts() ) {
					while ( $featured_posts->have_posts() ) {
						$featured_posts->the_post();
				?>
						<div class="fp <?php echo $i == 1 ? 'fp-big' : 'fp-small'; ?>">
							<a href="<?php the_permalink(); ?>">
								<div class="fp-img">
									<?php
										if( $i == 1 ) {
											the_post_thumbnail('home-featured-big', array('class' => "center-block img-responsive"));
										} else {
											the_post_thumbnail('home-featured-small', array('class' => "center-block img-responsive"));
										}
									?>
								</div>
							
								<div class="fp-overlay">
									<div class="fp-text">
										<h2><?php the_title(); ?></h2>
										<?php // echo '<p>' . excerpt(150) . '</p>'; /* TO BE DONE */ ?>
									</div>
								</div>
								
                                <?php /*
								<div class="fp-label">
									<div class="fp-label-triangle"></div>
									<div class="fp-label-text">
										<?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?>
									</div>
								</div>
                                */ ?>
							</a>
						</div><!-- end - .featured-post -->
				<?php
						$i++;
					}
				}

				// Restore original Post Data
				wp_reset_postdata();
				?>
			</div>
		</div><!-- end - #featured-posts -->
		
		<br /><!-- vertical space -->
		
		<div class="row">
			<div id="content-with-aside">
				<?php get_template_part( 'section', 'all_articles' ); ?> 
			</div><!-- end - main content -->
			
			<aside id="aside">
				<?php get_sidebar('ads'); ?>
			</aside><!-- end - #sidebar -->
		</div>
	</div>
</div><!-- homepage template - end -->
	
<?php get_footer(); ?>