<?php
/*
	Template Name: Page - Contact us
*/
?>

<!DOCTYPE html>
<html lang="en">
	<head>		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php bloginfo('name'); ?> | <?php IsHome() ? bloginfo('description') : wp_title(''); ?></title>

		
		<!-- GOOGLE ANALYTICS - start -->
		<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		ga('create', 'UA-64470501-1', 'auto');
		ga('send', 'pageview');
		</script>
		<!-- GOOGLE ANALYTICS - end -->

		<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '1861783217183966'); // Insert your pixel ID here.
		fbq('track', 'PageView');
		</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=1861783217183966&ev=PageView&noscript=1"
		/></noscript>
		<!-- DO NOT MODIFY -->
		<!-- End Facebook Pixel Code -->

		<?php /* FACEBOOK SEO TAGS - start */ ?>
		<?php if( is_single() ) { ?>
			<meta property="og:url" content="<?php the_permalink($post->ID); ?>" />
			<meta property="og:type" content="website" />
			<meta property="og:title" content="<?php wp_title( '|', true, 'right' ) . bloginfo( 'name' ); ?>" />
			<meta property="og:description" content="<?php the_title(); ?>" />
			<meta property="og:image" content="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0]; ?>" />
			<?php } else { ?>
				<meta name="description" content="Fill your tank with our satisfying bites of trending car articles and news on latest models and moves from within the industry.">
		<?php } ?>
		<?php /* FACEBOOK SEO TAGS - end */ ?>

		<?php /* globals.js includes: GA code, global variables */ ?>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/globals.js"></script>
				
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-KMZ7DSR');</script>
		<!-- End Google Tag Manager -->
				
		<?php /* PREBID JS / HEADERBIDDING - START   ?>
		<script type="text/javascript" src="http://m2d.m2.ai/m2d.buzzdrives.alldevices.min.js" async="async" ></script>
		<?php /* PREBID JS / HEADERBIDDING - END */ ?>
		
		
		<?php /* taboola retargeting pixels - start */ ?>
		<?php if(isset($page) && $page == 8 ) { ?>
			<?php // 20-cars-that-are-plummeting-in-value-3 ?>
			<?php if( isset($post) && isset($post->ID) && $post->ID == 8014 ) { ?>
				<script type="text/javascript">
				window._tfa = window._tfa || [];
				_tfa.push({ notify: 'mark',type: 'Plummeting-3' });
				</script>
				<script src="//cdn.taboola.com/libtrc/buzzdrives-sc/tfa.js"></script>
			<?php } ?>
			<?php // 30-great-cars-that-nobody-buys-3 ?>
			<?php if( isset($post) && isset($post->ID) && $post->ID == 8025 ) { ?>
				<script type="text/javascript">
				window._tfa = window._tfa || [];
				_tfa.push({ notify: 'mark',type: 'Nobody-buys-3' });
				</script>
				<script src="//cdn.taboola.com/libtrc/buzzdrives-sc/tfa.js"></script>
			<?php } ?>
			<?php // 30-cars-that-will-last-more-than-250000-miles ?>
			<?php if( isset($post) && isset($post->ID) && $post->ID == 5981 ) { ?>
				<script type="text/javascript">
				window._tfa = window._tfa || [];
				_tfa.push({ notify: 'mark',type: 'Last-250k-miles' });
				</script>
				<script src="//cdn.taboola.com/libtrc/buzzdrives-sc/tfa.js"></script>
			<?php } ?>
		<?php } ?>
		<?php /* taboola retargeting pixels - end */ ?>
				
				
		<?php /* Favicons - start */ ?>
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_img_folder(); ?>/favicons/apple-icon-180x180.png">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/favicon-16x16.png" sizes="16x16">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/favicon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-36x36.png" sizes="36x36">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-48x48.png" sizes="48x48">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-72x72.png" sizes="72x72">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-144x144.png" sizes="144x144">
		<link rel="icon" type="image/png" href="<?php echo get_img_folder(); ?>/favicons/android-icon-192x192.png" sizes="192x192">
		<link rel="manifest" href="<?php echo get_img_folder(); ?>/favicons/manifest.json">
		<meta name="apple-mobile-web-app-title" content="BuzzDrives">
		<meta name="application-name" content="BuzzDrives">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?php echo get_img_folder(); ?>/favicons/ms-icon-70x70.png">
		<meta name="msapplication-TileImage" content="<?php echo get_img_folder(); ?>/favicons/ms-icon-144x144.png">
		<meta name="msapplication-TileImage" content="<?php echo get_img_folder(); ?>/favicons/ms-icon-150x150.png">
		<meta name="msapplication-TileImage" content="<?php echo get_img_folder(); ?>/favicons/ms-icon-310x310.png">
		<meta name="theme-color" content="#ffffff">
		<?php /* Favicons - end */ ?>
		
		<?php //<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"> ?>
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" />
				
		<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
				
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		        
        <?php /* load a fake adframe.js file that will be blocked by adblocker. this way we can determine if user uses an adblocker extension */ ?>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/adframe.js"></script>
					
		<?php wp_head(); ?>
		
		<?php /* Start Visual Website Optimizer Asynchronous Code */ ?>
		<script type='text/javascript'>
		var _vwo_code=(function(){
		var account_id=310134,
		settings_tolerance=2000,
		library_tolerance=2500,
		use_existing_jquery=false,
		// DO NOT EDIT BELOW THIS LINE 
		f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
		</script>
		<?php /* End Visual Website Optimizer Asynchronous Code */ ?>
		
		
		<?php /* Taboola Head Code Start */ ?>
		<script type="text/javascript">
		window._taboola = window._taboola || [];
		_taboola.push({article:'auto'});
		!function (e, f, u, i) {
		if (!document.getElementById(i)){
		e.async = 1;
		e.src = u;
		e.id = i;
		f.parentNode.insertBefore(e, f);
		}
		}(document.createElement('script'),
		document.getElementsByTagName('script')[0],
		'//cdn.taboola.com/libtrc/buzzdrive/loader.js',
		'tb_loader_script');
		if(window.performance && typeof window.performance.mark == 'function')
		{window.performance.mark('tbl_ic');}
		</script>		
		<?php /* Taboola Head Code END */ ?>
		
		
		<?php /* Nativo Header Tag - start  ?>
		<?php if( isCampaignTraffic() ) { ?>
			<script type="text/javascript" src="//s.ntv.io/serve/load.js" async></script>			
		<?php } ?>
		<?php /* Nativo Header Tag - end */ ?>
		
				
		<?php /* Yahoo pixel - start */ ?>
		<script type="application/javascript">(function(w,d,t,r,u){w[u]=w[u]||[];w[u].push({'projectId':'10000','properties':{'pixelId':'10033016'}});var s=d.createElement(t);s.src=r;s.async=true;s.onload=s.onreadystatechange=function(){var y,rs=this.readyState,c=w[u];if(rs&&rs!="complete"&&rs!="loaded"){return}try{y=YAHOO.ywa.I13N.fireBeacon;w[u]=[];w[u].push=function(p){y([p])};y(c)}catch(e){}};var scr=d.getElementsByTagName(t)[0],par=scr.parentNode;par.insertBefore(s,scr)})(window,document,"script","https://s.yimg.com/wi/ytc.js","dotq");</script>
		<?php /* Yahoo pixel - stop */ ?>
	</head>
	
	<body <?php body_class(); ?> data-id="<?php echo get_the_ID(); ?>">
		
		<?php /* add class to the body for desktops */ ?>
		<script type="text/javascript" >
			if( isDesktop() ) {
				$('body').addClass('is_desktop');
			}
		</script>
		
		<?php do_action('after_body'); // it hooks any plugins that require an early load and it includes HTML. ex: FB SDK ?>
				
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KMZ7DSR"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		
		<div id="wrapper">
			<div id="main-gradient"></div><!-- end - #main-gradient -->
			
			<header id="header" style="position:relative;" >
				<div id="header-inner">		
				
					<div id="header-nav-wrapper">
						<div class="container">
							<button type="button" id="mobile-menu-btn" class="visible-xs">Menu <span class="glyphicon glyphicon-menu-hamburger"></span></button>
							<nav id="nav" class="hidden-xs">
								<?php wp_nav_menu(
									array(
										'theme_location' => 'primary-menu',
										'container' => '',
										'menu_id' => '',
										'menu_class' => 'clearfix',
									)
								); ?>
							</nav><!-- end - #nav -->
							
							<!--
							<div id="header-search" class="hidden-xs">
								<?php // get_search_form(); ?>
							</div>  end - #header-search -->
							
						</div>
					</div><!-- end - #header-nav-wrapper -->
						
					<div id="header-logo">
						<div class="container">
							<div id="header-logo-inner">
								<div id="header-logo-round">
									<nav id="nav-mobile">
									<?php wp_nav_menu(
										array(
											'theme_location' => 'primary-menu',
											'container' => '',
											'menu_id' => 'menu-mobile',
											'menu_class' => 'clearfix',
										)
									); ?>
									</nav><!-- end - #nav-mobile -->
																
									<a href="<?php echo get_home_url(); ?>">
										<img class="center-block img-responsive" src="<?php echo get_img_folder(); ?>/buzzdrives_logo.png" width="334" height="55" alt="BuzzDrives Logo">
									</a>
								</div>
							</div>						
						</div>
					</div><!-- end - #header-logo -->
				</div><!-- end - #header-inner -->
			</header><!-- end - #header -->
			
			<div id="main">
				<div id="contact-us-page">
					<div class="container">
						<h1 class="page-title text-center"><?php the_title(); ?></h1>
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="information-content">
									<h4>Buzzdrives.com<br>Reverse Media Group Ltd</h4>
									<p>1 Lindsey Street<br>London,<br>EC1A 9HP</p>
									<p><strong>Telephone: </strong><a href="tel:+442081339954">+44 208 133 9954</a></p>
									<p><strong>For media and advertising enquiries: </strong><span class="link-contact" data-name="advertise" data-domain="reversemediagroup" data-extention="com">advertise@reversemediagroup.com</span></p>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-container">
									<?php echo do_shortcode('[contact-form-7 id="3756" title="Contact us form"]'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>

<?php get_footer(); ?>