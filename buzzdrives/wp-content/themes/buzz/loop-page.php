<div class="loop-page">
	<?php
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();
	?>
				<article id="page-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>>
					<h1 class="page-title"><?php the_title(); ?></h1><!-- page title - end -->
					
					<?php if( has_post_thumbnail( $post->ID ) ) { ?> 
						<div class="page-thumbnail">
							<?php the_post_thumbnail('full'); ?>
						</div><!-- page thumbnail - end -->
					<?php } ?>
					
					<div class="page-content">
						<?php the_content(); ?>
						
						<?php wp_link_pages(); ?>
					</div><!-- page content - end -->
				</article><!-- page article - end -->
	<?php
			} // end while
		} else {
	?>
		<p>No content found.</p>
	<?php
		}
	?>
	<?php edit_post_link('edit', '<p>', '</p>'); ?>
</div><!-- main loop of the page - end -->