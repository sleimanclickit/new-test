			</div><!-- end - #main -->
			
			<footer id="footer">
                <div id="footer-inner">
                    <div class="container">
                        <div class="footer-container">
                            <div id="footer-logo-wrapper">
                                <div>
                                    <a href="<?php echo get_home_url(); ?>">
                                        <img class="center-block img-responsive" src="<?php echo get_img_folder(); ?>/buzzdrives_logo_footer.png" alt="BuzzDrives Logo" width="208" height="23" />
                                    </a>
                                    <div id="footer-menu">
                                        <?php wp_nav_menu(
                                            array(
                                                'theme_location' => 'footer-menu',
                                                'container' => '',
                                                'menu_id' => 'menu-footer',
                                                'menu_class' => 'list-inline clearfix text-center',
                                            )
                                        ); ?>
                                    </div><!-- end - #footer-menu -->
                                </div>
                            </div><!-- end - #footer-logo-wrapper -->
                            <div class="footer-right-wrapper">
                                <h3>Impressum</h3>
                                <p>Reverse Media Group</p>
                                <div>
                                    <p>Found Studios, 1 Lindsey Street, London, EC1A 9HP</p>
                                    <p>VAT number: GB 188 0307 01p</p>
                                </div>
                                <div>
                                    <p><a href="mailto:partners@reversemediagroup.com">partners@reversemediagroup.com</a></p>
                                    <p>+44 208133 9954</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="separator footer"></div>
                    <div class="container">
                        <div class="copyright_section">
                            <p>Copyright &copy; <?php echo date("Y"); ?> Buzzdrives</p>
                        </div>
                    </div>
                </div><!-- end - #footer-inner -->
			</footer><!-- end - #footer -->
		</div><!-- end - #wrapper -->
                		
		<?php wp_footer(); ?>
		
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.infinitescroll.min.js"></script>
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

		<script type="text/javascript" >
		// code from the 2 scripts that have been commented out ( scripts.js and ga_events.js)
		/* ADSENSE - start */
		(function() {
			// init every adsense banner
			for(var i = 0; i < document.getElementsByClassName('adsbygoogle').length; i++) {
				(adsbygoogle = window.adsbygoogle || []).push({});
			}
		})();
		/* ADSENSE - end */
		</script>
		
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
		
		<?php /* LockerDome tracking pixel - start  - removed on 26.03.2019 ?>
		<script>
		(function () {
		  if (/ld_trk=1/.test(document.cookie)) {
			track();
		  } else if (/ld_trk=1/.test(window.location.search)) {
			document.cookie = 'ld_trk=1; path=/';
			track();
		  }
		  function track() {
			new Image(1,1).src = 'https://lockerdome.com/ldpix.gif?ldc=10530771157065216_reversemediagroup_impression&ord='+Date.now();
		  }
		})();
		</script>
		<?php /* LockerDome tracking pixel - end */?>
						
		<?php /* MM sticky ads - start */ ?>
			<script>
				(function(){
					if( isMobile() ) {
						document.write ('<div id="BD_Sticky_Bottom_mobile" style="position:fixed; bottom:0; left:0;right:0; margin:0 auto; z-index:9999999;text-align: center;" ></div>');
					} else {
						document.write ('<div id="BD_Sticky_Bottom_desktop" style="position:fixed; bottom:0; left:0;right:0; margin:0 auto; z-index:9999999;text-align: center;" ></div>');
					}											
				}());
			</script>			
		<?php /* MM sticky ads - end */ ?>
							
		
		
		<?php /* Persist the isCampaign query parameter across header and footer menu links */ ?>
		<script type="text/javascript">
		$(window).load(function() {
			if( 
				window.location.search.toLowerCase().indexOf('utm_source=yg') != -1 || 
				window.location.search.toLowerCase().indexOf('utm_source=yahoo') != -1 || 
				window.location.search.toLowerCase().indexOf('utm_source=zemanta') != -1 || 
				window.location.search.toLowerCase().indexOf('utm_source=outbrain') != -1 || 
				window.location.search.toLowerCase().indexOf('utm_source=taboola') != -1 ||
				window.location.search.toLowerCase().indexOf('tict=1') != -1 
				) {
				
				// header menu links
				$('.menu-item-type-taxonomy a').each(function( index , el ){
					var link_url = $(el).attr('href');
					$(el).attr('href' , link_url + '?tict=1' );
				});
				
				// footer links
				$('.menu-item-type-post_type a').each(function( index , el ){
					var link_url = $(el).attr('href');
					$(el).attr('href' , link_url + '?tict=1' );
				});
				
			}
		});
		</script>

        <?php /* Taboola Below Articles - start */ ?>
            <script type="text/javascript">
                window._taboola = window._taboola || [];
                _taboola.push({flush: true});
            </script>
        <?php /* Taboola Below Articles - end */ ?>
		
	</body>
</html>