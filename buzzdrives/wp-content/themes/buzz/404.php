<?php get_header(); ?>

<div id="_404">
	<div class="container">
		<div class="text-center">
			<br /><br /><br /><br /><br />
			<h1 class="page-title">Page not found.</h1>
			<b>&#8592; <a href="javascript:javascript:history.go(-1)"><?php _e( 'Go Back', 'radium' ); ?></a></b> or <b><a href="<?php echo home_url(); ?>"><?php _e( 'Go Home', 'radium' ); ?></a> &#8594;</b>
			<br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
		</div>
	</div>
</div><!-- 404 page template - end -->
	
<?php get_footer(); ?>