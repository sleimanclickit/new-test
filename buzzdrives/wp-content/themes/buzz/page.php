<?php
/*
	Template Name: Page full width
*/
?>

<?php get_header(); ?>

<div id="page-full-width">
	<div class="container">
		<?php get_template_part('loop', 'page'); ?>
	</div>
</div><!-- page full width template - end -->
	
<?php get_footer(); ?>