<div class="loop-single">
	<?php
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();
	?>
				<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>>
					<h1 class="post-title"><?php the_title(); ?></h1><!-- post title - end -->
                    
					<?php /* MM/DFP JS START */ ?>
					<?php if( isset($post) && isset($post->ID) && $post->ID == 11246 && isCampaignTraffic() ) { ?>
						<script>
							(function(){
								if( isMobile() ) {
									document.write ('<div id="Epom_Native_Test_mob"></div>');
								}												
							}());
						</script>
					<?php } else { ?>
						<script>
							(function(){
								if( isMobile() ) {
									document.write ('<div id="top_banner_mobile"></div>');
								}												
							}());
						</script>
					<?php } ?>					
					<?php /* MM/DFP JS END */ ?>
							
							
					<?php /* ADS -- AFTER TITLE - start */ ?>
					<!--
					<div id="ads-after-title">
					</div><!-- ads after title - end -->					
                    <?php /* ADS -- AFTER TITLE - end */ ?>
					
					<ul id="social-buttons">
						<li class="count"><div class="counter"></div><span class="shares">shares</span></li>
						<li><a class="social tw" href="javascript:void(0);" onclick="window.open('https://twitter.com/intent/tweet?text=' + encodeURIComponent(document.title) + '&amp;url=' + encodeURIComponent(document.location.href), 'sharer', 'top=200, left=200, toolbar=0, status=0, width=520, height=350'); return false;" target="_black"><span class="hidden-xxs">Twitter</span></a></li>
						<li><a class="social fb" href="javascript:void(0);" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(document.location.href), 'sharer', 'top=200, left=200, toolbar=0, status=0, width=520, height=350'); return false;" target="_blank"><span class="hidden-xxs">Facebook</span></a></li>
					</ul><!-- social button - end -->
					
					<div class="post-content">
						<div id="content" itemprop="articleBody">
							<?php the_content(); ?>
						</div>
						
						<?php /* ADS -- BEFORE PAGINATION - start */ ?>
						<div id="ads-before-pagination" class="text-center">						
							
							<?php if( !isLastPageOnMultipagePost() ) { ?>			
							
								<?php if( switchAdUnitBeforePaginationMM() ) { ?>			
									<?php // MM START ?>
									<script>
									(function(){
										if( isMobile() ) {
											document.write ('<div id="BD_Leaderboard_above_pagination_Mobile"></div>');
										} else {
											document.write ('<div id="BD_Leaderboard_above_pagination_Desktop"></div>');
										}										
									}());
									</script>		
									<?php // MM END  ?>
									
								<?php } else if( isDummyABtest() ){ ?>
								
									<?php // Adsense START ?>
									<div class="adsense" id="adsense_before_pag " >
										<div class="adsense_header_title">Advertisement</div>
										<script>
											(function(){
												// original 
												var adsense_client = "ca-pub-6449643190876495";
												var adsense_classes = 'adsbygoogle';
												var adsense_slot = "";
												// var mm_tracking_id = "BD_AdSense_Pagination_Desktop";
												var mm_tracking_id = "";
												var adsense_size = [];

												if ( winWid >= 768 ) {
													adsense_classes += ' adsense_bottom_desktop';
													adsense_slot = "3112298940"; /* buzzdrives.com - before pagination - AB test: dmy */
													adsense_size = ["100%", "90px"];
												} else {
													adsense_classes += ' adsense_bottom_mobile';
													// mm_tracking_id = "BD_AdSense_Pagination_Mobile";
													adsense_slot = "3850665549"; /* buzzdrives.com - before pagination - m - AB test: dmy */
													adsense_size = ["300px", "250px"];
												}

												document.write (												
												'<ins class="' + adsense_classes + '" id="' + mm_tracking_id + '" style="display:inline-block;width:' 
													+ adsense_size[0] + ';height:' 
													+ adsense_size[1] + '" data-ad-client="' 
													+ adsense_client + '" data-ad-slot="' 
													+ adsense_slot + '"></ins>'
												);
											})();
										</script>
									</div>
									<?php // Adsense END ?>		
								
								<?php } else if( makeButtonsYellow() ){ ?>
								
									<?php // Adsense START ?>
									<div class="adsense" id="adsense_before_pag " >
										<div class="adsense_header_title">Advertisement</div>
										<script>
											(function(){
												// original 
												var adsense_client = "ca-pub-6449643190876495";
												var adsense_classes = 'adsbygoogle';
												var adsense_slot = "";
												// var mm_tracking_id = "BD_AdSense_Pagination_Desktop";
												var mm_tracking_id = "";
												var adsense_size = [];

												if ( winWid >= 768 ) {
													adsense_classes += ' adsense_bottom_desktop';
													adsense_slot = "8747769002"; /* buzzdrives.com - before pagination - AB test: bpy */
													adsense_size = ["100%", "90px"];
												} else {
													adsense_classes += ' adsense_bottom_mobile';
													// mm_tracking_id = "BD_AdSense_Pagination_Mobile";
													adsense_slot = "8688059256"; /* buzzdrives.com - before pagination - m - AB test: bpy */
													adsense_size = ["300px", "250px"];
												}

												document.write (												
												'<ins class="' + adsense_classes + '" id="' + mm_tracking_id + '" style="display:inline-block;width:' 
													+ adsense_size[0] + ';height:' 
													+ adsense_size[1] + '" data-ad-client="' 
													+ adsense_client + '" data-ad-slot="' 
													+ adsense_slot + '"></ins>'
												);
											})();
										</script>
									</div>
									<?php // Adsense END ?>		
								
								<?php } else if( hideNumbersOnPagination() ){ ?>
								
									<?php // Adsense START ?>
									<div class="adsense" id="adsense_before_pag " >
										<div class="adsense_header_title">Advertisement</div>
										<script>
											(function(){
												// original 
												var adsense_client = "ca-pub-6449643190876495";
												var adsense_classes = 'adsbygoogle';
												var adsense_slot = "";
												// var mm_tracking_id = "BD_AdSense_Pagination_Desktop";
												var mm_tracking_id = "";
												var adsense_size = [];

												if ( winWid >= 768 ) {
													adsense_classes += ' adsense_bottom_desktop';
													adsense_slot = "2916857823"; /* buzzdrives.com - before pagination - AB test: nnp */
													adsense_size = ["100%", "90px"];
												} else {
													adsense_classes += ' adsense_bottom_mobile';
													// mm_tracking_id = "BD_AdSense_Pagination_Mobile";
													adsense_slot = "5348058973"; /* buzzdrives.com - before pagination - m - AB test: nnp */
													adsense_size = ["300px", "250px"];
												}

												document.write (												
												'<ins class="' + adsense_classes + '" id="' + mm_tracking_id + '" style="display:inline-block;width:' 
													+ adsense_size[0] + ';height:' 
													+ adsense_size[1] + '" data-ad-client="' 
													+ adsense_client + '" data-ad-slot="' 
													+ adsense_slot + '"></ins>'
												);
											})();
										</script>
									</div>
									<?php // Adsense END ?>		
								
								<?php } else if( showTaboolaBelowArticlesTags() ){ ?>
								
									<?php // Adsense START ?>
									<div class="adsense" id="adsense_before_pag " >
										<div class="adsense_header_title">Advertisement</div>
										<script>
											(function(){
												// original 
												var adsense_client = "ca-pub-6449643190876495";
												var adsense_classes = 'adsbygoogle';
												var adsense_slot = "";
												// var mm_tracking_id = "BD_AdSense_Pagination_Desktop";
												var mm_tracking_id = "";
												var adsense_size = [];

												if ( winWid >= 768 ) {
													adsense_classes += ' adsense_bottom_desktop';
													adsense_slot = "6500879614"; /* buzzdrives.com - before pagination - AB test: tba */
													adsense_size = ["100%", "90px"];
												} else {
													adsense_classes += ' adsense_bottom_mobile';
													// mm_tracking_id = "BD_AdSense_Pagination_Mobile";
													adsense_slot = "4418120683"; /* buzzdrives.com - before pagination - m - AB test: tba */
													adsense_size = ["300px", "250px"];
												}

												document.write (												
												'<ins class="' + adsense_classes + '" id="' + mm_tracking_id + '" style="display:inline-block;width:' 
													+ adsense_size[0] + ';height:' 
													+ adsense_size[1] + '" data-ad-client="' 
													+ adsense_client + '" data-ad-slot="' 
													+ adsense_slot + '"></ins>'
												);
											})();
										</script>
									</div>
									<?php // Adsense END ?>		
								
								<?php } else if( addTwoAdsenseUnits() ){ ?>

									<?php // Adsense test - start ?>
									<div class="adsense" id="adsense_before_pag_test ">
										<div class="ad_placeholder_left">
											<div class="adsense_header_title">Advertisement</div>
											<script>
												(function(){
													if( winWid >= 750 ) {
														// original
														var adsense_client = "ca-pub-6449643190876495";
														var adsense_classes = 'adsbygoogle';
														var adsense_slot = "";
														var adsense_size = [];

														adsense_classes += ' adsense_above_pagination';
														adsense_slot = "9566636127"; /* buzzdrives_300x250_above_pagination */
														adsense_size = ["300px", "250px"];

														document.write (
															'<ins class="' + adsense_classes + '" style="display:inline-block;width:'
															+ adsense_size[0] + ';height:'
															+ adsense_size[1] + '" data-ad-client="'
															+ adsense_client + '" data-ad-slot="'
															+ adsense_slot + '"></ins>'
														);
													}
												})();
											</script>
										</div>
										<div class="ad_placeholder_right">
											<div class="adsense_header_title">Advertisement</div>
											<script>
												(function(){
													if( winWid >= 750 ) {
														// original
														var adsense_client = "ca-pub-6449643190876495";
														var adsense_classes = 'adsbygoogle';
														var adsense_slot = "";
														// var mm_tracking_id = "BD_AdSense_Pagination_Desktop";
														var mm_tracking_id = "";
														var adsense_size = [];

														adsense_classes += ' adsense_above_pagination';
														adsense_slot = "7128175041"; /* buzzdrives_300x250_above_pagination_2ndunit */
														adsense_size = ["300px", "250px"];

														document.write (
															'<ins class="' + adsense_classes + '" id="'+mm_tracking_id+'" style="display:inline-block;width:'
															+ adsense_size[0] + ';height:'
															+ adsense_size[1] + '" data-ad-client="'
															+ adsense_client + '" data-ad-slot="'
															+ adsense_slot + '"></ins>'
														);
													}
												})();
											</script>
										</div>
									</div>
									<div class="adsense" id="adsense_before_pag_test_single">
										<div class="adsense_header_title">Advertisement</div>
										<script>
											(function(){
												if( winWid < 750 ) {
													// original
													var adsense_client = "ca-pub-6449643190876495";
													var adsense_classes = 'adsbygoogle';
													var adsense_slot = "";
													var adsense_size = [];

													adsense_classes += ' adsense_bottom_mobile';
													// var mm_tracking_id = "BD_AdSense_Pagination_Mobile";
													var mm_tracking_id = "";
													adsense_slot = "1490555366"; /* buzzdrives.com - before pagination - m */
													adsense_size = ["300px", "250px"];

													document.write (
														'<ins class="' + adsense_classes + '" id="'+mm_tracking_id+'" style="display:inline-block;width:'
														+ adsense_size[0] + ';height:'
														+ adsense_size[1] + '" data-ad-client="'
														+ adsense_client + '" data-ad-slot="'
														+ adsense_slot + '"></ins>'
													);
												}
											})();
										</script>
									</div>
									<?php // Adsense test - end ?>

								<?php } else { ?>
									
									
									<?php if( switchHardcodedAdxWithDFPunit() ){ ?>
										<div class="adsense_header_title">Advertisement</div>
										<?php /* Show the DFP Adx units */ ?>										
										<!-- GPT AdSlot 1 for Ad unit 'BD_AdSense_Pagination_Desktop' ### Size: [[728,90]] -->
										<div id='div-gpt-ad-1542649-1'></div>
										<!-- GPT AdSlot 2 for Ad unit 'BD_AdSense_Pagination_Mobile' ### Size: [[300,250]] -->
										<div id='div-gpt-ad-1542649-2'></div>
										<script>											
											(function(){
												if( isMobile() ) {
													googletag.cmd.push(function() { googletag.display('div-gpt-ad-1542649-2'); });
													$('#div-gpt-ad-1542649-1').remove();
												} else {
													googletag.cmd.push(function() { googletag.display('div-gpt-ad-1542649-1'); });
													$('#div-gpt-ad-1542649-2').remove();
												}
											  }());	
										</script>
										
									<?php } elseif( switchHardcodedAdxWithDFPunit_control() ) { ?>
										<?php /* AB test Control ( New ) hardcoded adsense START */ ?>	
										<div class="adsense" id="adsense_before_pag " >
											<div class="adsense_header_title">Advertisement</div>
											<script>
												(function(){
													var adsense_client = "ca-pub-6449643190876495";
													var adsense_classes = 'adsbygoogle';
													var adsense_slot = "";
													// var mm_tracking_id = "BD_AdSense_Pagination_Desktop";
													var mm_tracking_id = "";
													var adsense_size = [];

													if ( winWid >= 768 ) {
														adsense_classes += ' adsense_bottom_desktop';
														adsense_slot = "6403512971"; // buzzdrives.com - before pagination - AB control: shadco
														adsense_size = ["100%", "90px"];
													} else {
														adsense_classes += ' adsense_bottom_mobile';
														// mm_tracking_id = "BD_AdSense_Pagination_Mobile";
														adsense_slot = "3125655083"; // buzzdrives.com - before pagination - AB control: shadco - m
														adsense_size = ["300px", "250px"];
													}

													document.write (													
													'<ins class="' + adsense_classes + '" id="' + mm_tracking_id + '" style="display:inline-block;width:' 
														+ adsense_size[0] + ';height:' 
														+ adsense_size[1] + '" data-ad-client="' 
														+ adsense_client + '" data-ad-slot="' 
														+ adsense_slot + '"></ins>'
													);
												}());
											</script>
											
										</div><!-- adsense - end -->
								
									<?php } else { ?>										
																		
										<?php if( isHtmlizedDeferedDitTest() ) { ?>	
											<?php /* DIT Test Variation : HTMLized and Defered ( added on 15.04.2019 ) */ ?>
											<div class="adsense" id="adsense_before_pag " >
												<div class="adsense_header_title">Advertisement</div>
                                                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
												<script>
													(function(){
														// original 
														var adsense_client = "ca-pub-6449643190876495";
														var adsense_classes = 'adsbygoogle';
														var adsense_slot = "";
														// var mm_tracking_id = "BD_AdSense_Pagination_Desktop";
														var mm_tracking_id = "";
														var adsense_size = [];

														if ( winWid >= 768 ) {
															adsense_classes += ' adsense_bottom_desktop';
															adsense_slot = "5370852461"; /* buzzdrives.com - before pagination - DitTest HTMLized Defered */
															adsense_size = ["100%", "90px"];
														} else {
															adsense_classes += ' adsense_bottom_mobile';
															// mm_tracking_id = "BD_AdSense_Pagination_Mobile";
															adsense_slot = "1366537585"; /* buzzdrives.com - before pagination - DitTest HTMLized Defered - m */
															adsense_size = ["300px", "250px"];
														}

														document.write (														
														'<ins class="' + adsense_classes + '" id="' + mm_tracking_id + '" style="display:inline-block;width:' 
															+ adsense_size[0] + ';height:' 
															+ adsense_size[1] + '" data-ad-client="' 
															+ adsense_client + '" data-ad-slot="' 
															+ adsense_slot + '"></ins>'
														);
													})();
												</script>
											</div>
											
										<?php } elseif( isHtmlizedDitTest() ) { ?>
											<?php /* DIT Test Variation : HTMLized ( added on 15.04.2019 ) */ ?>	
											<div class="adsense" id="adsense_before_pag" >
												<div class="adsense_header_title">Advertisement</div>
												<script>
													(function(){
														// original 
														var adsense_client = "ca-pub-6449643190876495";
														var adsense_classes = 'adsbygoogle';
														var adsense_slot = "";
														// var mm_tracking_id = "BD_AdSense_Pagination_Desktop";
														var mm_tracking_id = "";
														var adsense_size = [];

														if ( winWid >= 768 ) {
															adsense_classes += ' adsense_bottom_desktop';
															adsense_slot = "4445441773"; /* buzzdrives.com - before pagination - DitTest - HTMLized */
															adsense_size = ["100%", "90px"];
														} else {
															adsense_classes += ' adsense_bottom_mobile';
															// mm_tracking_id = "BD_AdSense_Pagination_Mobile";
															adsense_slot = "1434343040"; /* buzzdrives.com - before pagination - DitTest - HTMLized - m */
															adsense_size = ["300px", "250px"];
														}

														document.write (														
														'<ins class="' + adsense_classes + '" id="' + mm_tracking_id + '" style="display:inline-block;width:' 
															+ adsense_size[0] + ';height:' 
															+ adsense_size[1] + '" data-ad-client="' 
															+ adsense_client + '" data-ad-slot="' 
															+ adsense_slot + '"></ins>'
														);
													})();
												</script>
											</div>
										
										<?php } elseif( isControlDitTest() ) { ?>
											<?php /* DIT Test Variation : Control ( added on 15.04.2019 ) */ ?>	
											<div class="adsense" id="adsense_before_pag" >
												<div class="adsense_header_title">Advertisement</div>
												<script>
													(function(){
														// original 
														var adsense_client = "ca-pub-6449643190876495";
														var adsense_classes = 'adsbygoogle';
														var adsense_slot = "";
														// var mm_tracking_id = "BD_AdSense_Pagination_Desktop";
														var mm_tracking_id = "";
														var adsense_size = [];

														if ( winWid >= 768 ) {
															adsense_classes += ' adsense_bottom_desktop';
															adsense_slot = "4758987688"; /* buzzdrives.com - before pagination - DitTest - Control */
															adsense_size = ["100%", "90px"];
														} else {
															adsense_classes += ' adsense_bottom_mobile';
															// mm_tracking_id = "BD_AdSense_Pagination_Mobile";
															adsense_slot = "4050031931"; /* buzzdrives.com - before pagination - DitTest - Control - m */
															adsense_size = ["300px", "250px"];
														}

														document.write (														
														'<ins class="' + adsense_classes + '" id="' + mm_tracking_id + '" style="display:inline-block;width:' 
															+ adsense_size[0] + ';height:' 
															+ adsense_size[1] + '" data-ad-client="' 
															+ adsense_client + '" data-ad-slot="' 
															+ adsense_slot + '"></ins>'
														);
													})();
												</script>
											</div>								
										
										<?php } elseif( isDeferedDitTest() ) { ?>
											<?php /* DIT Test Variation : Defered ( added on 15.04.2019 ) */ ?>	
											<div class="adsense" id="adsense_before_pag" >
												<div class="adsense_header_title">Advertisement</div>
                                                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
												<script>
													(function(){
														// original 
														var adsense_client = "ca-pub-6449643190876495";
														var adsense_classes = 'adsbygoogle';
														var adsense_slot = "";
														// var mm_tracking_id = "BD_AdSense_Pagination_Desktop";
														var mm_tracking_id = "";
														var adsense_size = [];

														if ( winWid >= 768 ) {
															adsense_classes += ' adsense_bottom_desktop';
															adsense_slot = "6101480200"; /* buzzdrives.com - before pagination - DitTest - Defered */
															adsense_size = ["100%", "90px"];
														} else {
															adsense_classes += ' adsense_bottom_mobile';
															// mm_tracking_id = "BD_AdSense_Pagination_Mobile";
															adsense_slot = "5526765131"; /* buzzdrives.com - before pagination - DitTest - Defered - m */
															adsense_size = ["300px", "250px"];
														}

														document.write (														
														'<ins class="' + adsense_classes + '" id="' + mm_tracking_id + '" style="display:inline-block;width:' 
															+ adsense_size[0] + ';height:' 
															+ adsense_size[1] + '" data-ad-client="' 
															+ adsense_client + '" data-ad-slot="' 
															+ adsense_slot + '"></ins>'
														);
													})();
												</script>
											</div>											
											
										<?php } elseif ( switchHarcodedAdsenseWithAdxAndHbs() ) { ?>
                                            <div class="adsense_header_title">Advertisement</div>
                                            <?php /* Show the AdX + HBS units */ ?>
                                            <div id='bd_adxhbs_above_pagination'></div>
                                            <div id='bd_adxhbs_above_pagination_mob'></div>
                                            <script>
                                            (function(){
                                                if( isMobile() ) {
                                                    googletag.cmd.push(function() { googletag.display('bd_adxhbs_above_pagination_mob'); });
                                                    $('#bd_adxhbs_above_pagination').remove();
                                                } else {
                                                    googletag.cmd.push(function() { googletag.display('bd_adxhbs_above_pagination'); });
                                                    $('#bd_adxhbs_above_pagination_mob').remove();
                                                }
                                                }());
                                            </script>
                                        <?php } else { ?>
											<?php /* Default setup - hardcoded adsense */ ?>										
											<?php // Adsense START ?>
											<div class="adsense" id="adsense_before_pag" >
												<div class="adsense_header_title">Advertisement</div>
												<script>
													(function(){
														// original 
														var adsense_client = "ca-pub-6449643190876495";
														var adsense_classes = 'adsbygoogle';
														var adsense_slot = "";
														// var mm_tracking_id = "BD_AdSense_Pagination_Desktop";														
														var mm_tracking_id = "";														
														var adsense_size = [];

														if ( winWid >= 768 ) {
															adsense_classes += ' adsense_bottom_desktop';
															adsense_slot = "9013822165"; /* buzzdrives.com - before pagination */
															adsense_size = ["100%", "90px"];
														} else {
															adsense_classes += ' adsense_bottom_mobile';
															// mm_tracking_id = "BD_AdSense_Pagination_Mobile";
															adsense_slot = "1490555366"; /* buzzdrives.com - before pagination - m */
															adsense_size = ["300px", "250px"];
														}

														document.write (
														'<ins class="' + adsense_classes + '" id="' + mm_tracking_id + '" style="display:inline-block;width:' 
															+ adsense_size[0] + ';height:' 
															+ adsense_size[1] + '" data-ad-client="' 
															+ adsense_client + '" data-ad-slot="' 
															+ adsense_slot + '"></ins>'
														);
													})();
												</script>
											</div>
											<?php // Adsense END ?>		
										<?php } ?>
									
									<?php } ?>
									
								<?php } ?>
							
							<?php } ?>
						
						</div><!-- ads before pagination - end -->
						<?php /* ADS -- BEFORE PAGINATION - end */ ?>
						
						<?php do_action('single_pagination'); ?>
						
						<?php /* ADS -- AFTER PAGINATION - start */ ?>
						<div id="ads-after-pagination" class="text-center">
						
							<?php /* Tan Media tags - start - removed on 26.03.2019 ?>
							<?php if( showTanMediaTags() ) { ?> 
								<script type="text/javascript" id="quantx-embed-tag" src="//cdn.elasticad.net/native/serve/js/quantx/nativeEmbed.gz.js"></script>
								<div id="quantx_generic_placement_34764"></div>
							<?php } ?> 
							<?php /* Tan Media tags - end */ ?>
														
							<?php /* TAN MEDIA DIV TAG START - removed on 22.11.2018 ?>
							<?php if(injectTanMediaFooterTag() && 1==2 ) { ?>
								<script>
									(function(){
										if( isDesktop() ) {
											document.write ('<div id="tan_media_footer_tag"></div>');
										}												
									}());
								</script>
							<?php } ?>
							<?php /* TAN MEDIA DIV TAG END */ ?>

                            <?php /* Taboola Below Articles Widget - start */ ?>
                            <?php if( showTaboolaBelowArticlesTags() ) { ?>
                                <div id="taboola-below-article-thumbnails"></div>
                                <script type="text/javascript">
                                    window._taboola = window._taboola || [];
                                    _taboola.push({
                                        mode: 'thumbnails-pag',
                                        container: 'taboola-below-article-thumbnails',
                                        placement: 'Below Article Thumbnails',
                                        target_type: 'mix'
                                    });
                                </script>
                            <?php } ?>
                            <?php /* Taboola Below Articles Widget - start */ ?>
							
						</div><!-- ads after pagination - end -->
						<?php /* ADS -- AFTER PAGINATION - end */ ?>
						
					</div><!-- post content - end -->
				</article><!-- post article - end -->
	<?php
			} // end while
		} else {
	?>
		<p>No content found.</p>
	<?php
		}
	?>
	<?php edit_post_link('edit', '<p>', '</p>'); ?>
</div><!-- main loop of the post - end -->