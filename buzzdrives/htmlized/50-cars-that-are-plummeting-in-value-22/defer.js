(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		// Creates an adblock detection plugin.
		ga('provide', 'adblockTracker', function(tracker, opts) {
		  var ad = document.createElement('ins');
		  ad.className = 'AdSense';
		  ad.style.display = 'block';
		  ad.style.position = 'absolute';
		  ad.style.top = '-1px';
		  ad.style.height = '1px';
		  document.body.appendChild(ad);
		  tracker.set('dimension' + opts.dimensionIndex, !ad.clientHeight);
		  document.body.removeChild(ad);
		});


		ga('create', 'UA-64470501-1', 'auto');
		ga('require', 'adblockTracker', {dimensionIndex: 6});
		
		ga('send', 'pageview');

!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1861783217183966'); // Insert your pixel ID here.
fbq('track', 'PageView');




(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-KMZ7DSR');

(function() {
		var isCmpTraffic = false;
		var isFbTraffic = false;				
		if( 
			window.location.search.toLowerCase().indexOf('utm_source=yg') != -1 || 
			window.location.search.toLowerCase().indexOf('utm_source=yahoo') != -1 || 
			window.location.search.toLowerCase().indexOf('utm_source=zemanta') != -1 || 
			window.location.search.toLowerCase().indexOf('utm_source=outbrain') != -1 || 
			window.location.search.toLowerCase().indexOf('utm_source=taboola') != -1 ||
			window.location.search.toLowerCase().indexOf('tict=1') != -1 
			) {
			isCmpTraffic = true;
		}
		if( window.location.search.toLowerCase().indexOf('utm_source=facebook') != -1 ||
			window.location.search.toLowerCase().indexOf('tmpfb=1') != -1 
			) {
			isFbTraffic = true;
		}
		if( !isCmpTraffic || isFbTraffic ) {
			window.m2hb = window.m2hb || {};
			m2hb.disabledUnits = m2hb.disabledUnits || [];
			m2hb.disabledUnits.push('/207764977/BD_Sticky_Bottom_desktop');
			m2hb.disabledUnits.push('/207764977/BD_Sticky_Bottom_mobile');
		}
		
		if( !isCmpTraffic ) {
			window.m2hb = window.m2hb || {};
			m2hb.disabledUnits = m2hb.disabledUnits || [];
			// we disable the units that we show only for cam traffic 
			m2hb.disabledUnits.push('/207764977/BD_top_left_sidebar_desktop');
			m2hb.disabledUnits.push('/207764977/BD_sticky_sidebar_desktop');
		}
		
	})();



(function() {	
				// check device and disable accordingly
				if( isMobile() ) {
					// disable the desktop units 
					window.m2hb = window.m2hb || {};
					m2hb.disabledUnits = m2hb.disabledUnits || [];
					m2hb.disabledUnits.push('/207764977/top_banner_desktop');
					m2hb.disabledUnits.push('/207764977/top_right_sidebar_desktop');
					m2hb.disabledUnits.push('/207764977/BD_top_left_sidebar_desktop');
					m2hb.disabledUnits.push('/207764977/BD_Sticky_Bottom_desktop');
					m2hb.disabledUnits.push('/207764977/BD_sticky_sidebar_desktop');
					m2hb.disabledUnits.push('/207764977/BD_top_right_sidebar_desktop_2');
					m2hb.disabledUnits.push('/207764977/BD_IBV_Right_Sidebar_Desktop');
					m2hb.disabledUnits.push('/207764977/BD_Leaderboard_above_pagination_Desktop');
					m2hb.disabledUnits.push('/207764977/BD_Top_Right_Sidebar_Video');
					
				} else {
					// disable the mobile units 
					window.m2hb = window.m2hb || {};
					m2hb.disabledUnits = m2hb.disabledUnits || [];
					m2hb.disabledUnits.push('/207764977/top_banner_mobile');
					m2hb.disabledUnits.push('/207764977/BD_Sticky_Bottom_mobile');					
					m2hb.disabledUnits.push('/207764977/BD_Leaderboard_above_pagination_Mobile');					
				}
			})();

window.M2_TIMEOUT = 1500;



var element2 = document.createElement("script");
element2.src = "https://m2d.m2.ai/m2d.buzzdrives.alldevices.min.js";
document.body.appendChild(element2);

var element3 = document.createElement("script");
element3.src = "https://d20efxdfa6g8fm.cloudfront.net/wp-content/themes/buzz/js/adframe.js.gzip";
document.body.appendChild(element3);


// var element4 = document.createElement("script");
// element4.src = "//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js";
// document.body.appendChild(element4);

// var element5 = document.createElement("script");
// element5.src = "//code.jquery.com/jquery-migrate-1.2.1.min.js";
// document.body.appendChild(element5);

// var element6 = document.createElement("script");
// element6.src = "//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js";
// document.body.appendChild(element6);



var ajaxurl = "https://buzzdrives.com/wp-admin/admin-ajax.php";var ajaxnonce = "d143167985";

window._taboola = window._taboola || [];
		_taboola.push({article:'auto'});
		!function (e, f, u, i) {
		if (!document.getElementById(i)){
		e.async = 1;
		e.src = u;
		e.id = i;
		f.parentNode.insertBefore(e, f);
		}
		}(document.createElement('script'),
		document.getElementsByTagName('script')[0],
		'//cdn.taboola.com/libtrc/buzzdrive/loader.js',
		'tb_loader_script');
		if(window.performance && typeof window.performance.mark == 'function')
		{window.performance.mark('tbl_ic');}







(function(){
								if( isMobile() ) {
									document.write ('<div id="top_banner_mobile"></div>');
								}												
							}());


(function(){
													// original 
													var adsense_client = "ca-pub-6449643190876495";
													var adsense_classes = 'adsbygoogle';
													var adsense_slot = "";
													var adsense_size = [];

													if ( winWid >= 768 ) {
														adsense_classes += ' adsense_bottom_desktop';
														adsense_slot = "9013822165"; /* buzzdrives.com - before pagination */
														adsense_size = ["100%", "90px"];
													} else {
														adsense_classes += ' adsense_bottom_mobile';
														adsense_slot = "1490555366"; /* buzzdrives.com - before pagination - m */
														adsense_size = ["300px", "250px"];
													}

													document.write (
													'<ins class="' + adsense_classes + '" style="display:inline-block;width:' 
														+ adsense_size[0] + ';height:' 
														+ adsense_size[1] + '" data-ad-client="' 
														+ adsense_client + '" data-ad-slot="' 
														+ adsense_slot + '"></ins>'
													);
												})();

var element7 = document.createElement("script");
element7.src = "//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js";
document.body.appendChild(element7);


(adsbygoogle = window.adsbygoogle || []).push({});


// (function(){
// 	console.log('running here');
// 				if( isDesktop() ) {
// 					console.log('is desktop');
// 					document.write('<div id="sidebar"><div id="BD_sticky_sidebar_desktop"  class="sidebar-sticky" style="margin-top:5px;z-index:99999;"></div></div> ');
// 				}												
// 			}());



var issticky = 0;
					var initial_sticky = $('.sidebar-sticky').offset().top;
					initial_sticky = 0;
					
					// on scroll action 
					$(window).scroll(function() {
						
						if( issticky == 0 ) {
							// is Static and we check if we should make it Sticky
							if( $(window).scrollTop() >= $('.sidebar-sticky').offset().top  ) {
								// get it's current position and set it as the "initial"
								if( initial_sticky == 0 ) {
									initial_sticky = $('.sidebar-sticky').offset().top;
								}
								
								$('#BD_sticky_sidebar_desktop').css({"position":'fixed'});
								$('#BD_sticky_sidebar_desktop').css({"top":'50px'});
								$('#BD_sticky_sidebar_desktop').css({"margin-top":'0px'});
								issticky = 1 ;
							}
						}else{
							// is Sticky and we check if we should make it Static
							if( initial_sticky > $(window).scrollTop() ) {
								$('#BD_sticky_sidebar_desktop').css({"position":'static'});
								$('#BD_sticky_sidebar_desktop').css({"top":'0px'});
								$('#BD_sticky_sidebar_desktop').css({"margin-top":'5px'});
								issticky = 0;
							}
						}
						
									
					});



window._taboola = window._taboola || [];
_taboola.push({
mode: 'organic-thumbnails-rr',
container: 'taboola-right-rail-thumbnails',
placement: 'Right Rail Thumbnails',
target_type: 'mix'
});



var element8 = document.createElement("script");
element8.src = "https://d20efxdfa6g8fm.cloudfront.net/wp-includes/js/wp-embed.min.js.gzip";
document.body.appendChild(element8);

var element9 = document.createElement("script");
element9.src = "https://d20efxdfa6g8fm.cloudfront.net/wp-content/themes/buzz/js/jquery.infinitescroll.min.js.gzip";
document.body.appendChild(element9);

var element10 = document.createElement("script");
element10.src = "//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js";
document.body.appendChild(element10);


(function() {
			// init every adsense banner
			for(var i = 0; i < document.getElementsByClassName('adsbygoogle').length; i++) {
				(adsbygoogle = window.adsbygoogle || []).push({});
			}
		})();

var element11 = document.createElement("script");
element11.src = "https://d20efxdfa6g8fm.cloudfront.net/wp-content/themes/buzz/js/main.js.gzip";
document.body.appendChild(element11);


(function(){
					if( isMobile() ) {
						document.write ('<div id="BD_Sticky_Bottom_mobile" style="position:fixed; bottom:0; left:0;right:0; margin:0 auto; z-index:9999999;text-align: center;" ></div>');
					} else {
						document.write ('<div id="BD_Sticky_Bottom_desktop" style="position:fixed; bottom:0; left:0;right:0; margin:0 auto; z-index:9999999;text-align: center;" ></div>');
					}											
				}());


if( 
				window.location.search.toLowerCase().indexOf('utm_source=yg') != -1 || 
				window.location.search.toLowerCase().indexOf('utm_source=yahoo') != -1 || 
				window.location.search.toLowerCase().indexOf('utm_source=zemanta') != -1 || 
				window.location.search.toLowerCase().indexOf('utm_source=outbrain') != -1 || 
				window.location.search.toLowerCase().indexOf('utm_source=taboola') != -1 ||
				window.location.search.toLowerCase().indexOf('tict=1') != -1 
				) {
				
				// header menu links
				$('.menu-item-type-taxonomy a').each(function( index , el ){
					var link_url = $(el).attr('href');
					$(el).attr('href' , link_url + '?tict=1' );
				});
				
				// footer links
				$('.menu-item-type-post_type a').each(function( index , el ){
					var link_url = $(el).attr('href');
					$(el).attr('href' , link_url + '?tict=1' );
				});
				
			}


window._taboola = window._taboola || [];
                _taboola.push({flush: true});







// defer also the images
var vizimgDefer = document.getElementsByClassName('defer-loading-images');
for (var i=0; i<vizimgDefer.length; i++) {
if(vizimgDefer[i].getAttribute('data-v')) {
vizimgDefer[i].setAttribute('src',vizimgDefer[i].getAttribute('data-v'));
} }


